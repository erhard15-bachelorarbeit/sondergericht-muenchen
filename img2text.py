import os
from utils import traverse
from shutil import rmtree
import pandas as pd
import cv2
import pytesseract
from io import StringIO
from tqdm import tqdm

class ImagesToText:
    def __init__(self):
        self.queue = traverse("processing/out")
        root_dir = "text"
        rmtree(root_dir, ignore_errors=True)
        self.text_e = root_dir + "/Eingestellte_Verfahren/"
        self.text_p = root_dir + "/Prozesse/"
        os.makedirs(self.text_p, exist_ok=True)
        os.makedirs(self.text_e, exist_ok=True)
        
    
    def process(self, improved=False):
        if improved:
            lang = "deu_improved"
        else:
            lang = "deu"
        for path, file in tqdm(self.queue.items()):
            if "Eingestellte" in file:
                out_filename = self.text_e + file.split(".png")[0]
            else:
                out_filename = self.text_p + file.split(".png")[0]
            try:
                img_bgr = cv2.imread(path)
                img_rgb = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2RGB)
                ocr_data = pytesseract.image_to_data(img_rgb, lang=lang)
                ocr_df = pd.read_table(StringIO(ocr_data), quoting=3)

                # Determine the text region based on the words (4+ characters) of high confidence (>90%)
                confident_words_df = ocr_df[
                    (ocr_df["conf"] > 90)
                    & (ocr_df["text"].str.len() - ocr_df["text"].str.count(" ") > 4)
                ]
                top = confident_words_df["top"].min()
                left = confident_words_df["left"].min()
                bot = (confident_words_df["top"] + confident_words_df["height"]).max()
                right = (confident_words_df["left"] + confident_words_df["width"]).max()
                # Obtain OCR string
                ocr_string = pytesseract.image_to_string(img_rgb[top:bot, left:right, :], lang=lang)
                with open(out_filename + ".txt", 'w+', encoding='utf8') as out:
                    out.write(ocr_string)
            except:
                continue