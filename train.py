import os
tdir = "lstmtrain/training"
traineddata = "lstmtrain/traineddata/deu.traineddata"

def makebox():
    for file in os.listdir(tdir):
        if file.endswith(".png"):
            # Path to file + backslash + filename
            file = "{}/{}".format(tdir, file)
            # Path to file + backslash + filename - .png extension + .box extension
            os.system("tesseract -l deu {} {} lstmbox".format(file, file.split(".")[0]))

def box2text():
    for file in os.listdir(tdir):
        if file.endswith(".box"):
            # Path to file + backslash + filename, UTF-8 encoding"
            with open("{}/{}".format(tdir, file), "r", encoding="utf8") as boxfile:
                # Initialize an empty boxstring
                boxstring = ""
                for i in boxfile.readlines():
                    # Add every 1st symbol from every line to the box string (remove the box coordinates, produce the actual text)
                    boxstring += i[0]
                print(boxstring)
            # Path to file + backslash + filename + .new extension , UTF-8 encoding"
                with open("{}/{}.new".format(tdir, file), "w+", encoding="utf8") as newboxfile:
                    newboxfile.write(boxstring)

def text2box():
    for file in os.listdir(tdir):
        if file.endswith("box.new"):
            box_corrected = ""
            with open("{}/{}".format(tdir, file), "r", encoding="utf8") as newboxfile:
                newbox = newboxfile.read()
            with open("{}/{}.box".format(tdir, file.split(".")[0]), "r", encoding="utf8") as oldboxfile:
                for n, i in enumerate(oldboxfile.readlines()):
                    # Replace the first symbol in every line with the corrected symbol from
                    
                    if newbox[n] == "%":
                        continue
                    else:
                        i = newbox[n] + i[1:]
                        box_corrected += i
            print(box_corrected)
            os.rename(r"{}/{}.box".format(tdir, file.split(".")[0]),r"{}/{}.old".format(tdir, file.split(".")[0]))
            with open("{}/{}.box".format(tdir, file.split(".")[0]), "w+", encoding="utf8") as oldboxfile:
                oldboxfile.write(box_corrected)


def genlstm():
    for file in os.listdir(tdir):
        if file.endswith(".box"):
            os.system("tesseract -l deu {tdir}/{filename}.png {tdir}/{filename} lstm.train".format(tdir = tdir, filename = file.split(".")[0]))
    lstmf_files = ""
    for file in os.listdir(tdir):
        if file.endswith("lstmf"):
            lstmf_files += "{}/{}\n".format(tdir, file)
    with open("{}/training_files.txt".format(tdir), "w", newline='\n') as tfiles:
        tfiles.write(lstmf_files)
    


def train():
    traindir = "{}/lstm_train".format(tdir)
    os.makedirs(traindir, exist_ok=True)
    os.system('combine_tessdata -e "{}" {}/deu.lstm'.format(traineddata, traindir))
    ftdir = "{}/fine_tuned".format(tdir)
    os.makedirs(ftdir)
    os.system('lstmtraining --model_output {ftdir}/deu_improved.traineddata \
         --continue_from {traindir}/deu.lstm \
         --train_listfile {tdir}/training_files.txt \
         --traineddata "{traindir}/deu.traineddata" \
         --max_iterations 1000'.format(ftdir = ftdir, traindir = traindir, tdir = tdir))

#makebox()
#box2text()
#text2box()
#genlstm()
#train()