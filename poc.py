import os
from utils import traverse

class ImagesToText:
    def __init__(self):
        self.queue = traverse("processing/in")
        root_dir = "text"
        self.text_e = root_dir + "/Eingestellte_Verfahren/"
        self.text_p = root_dir + "/Prozesse/"
        os.makedirs(self.text_p, exist_ok=True)
        os.makedirs(self.text_e, exist_ok=True)
        
    
    def process(self):
        for path, file in self.queue.items():
            if "Eingestellte" in file:
                out_filename = self.text_e + file.split(".png")[0]
            else:
                out_filename = self.text_p + file.split(".png")[0]
            # For some reason I can't get the same result using pytesseract
            # But that should do just fine
            os.system("tesseract -l deu " + path + " " + out_filename)

ImagesToText().process()