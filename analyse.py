import pandas as pd

df = pd.read_excel('xls_bak/1940.xlsx')
print("Total cases: {}".format(len(df["Prozessnummer"].unique())))
print("Total rows: {}".format(len(df)))
print("Non-empty dates: {}".format(len(df[df['Geburtsdatum'].notnull()])))
print("Non-empty jobs: {}".format(len(df[df['Beruf'].notnull()])))
print("Non-empty verdicts: {}".format(len(df[df['Urteil'].notnull()])))