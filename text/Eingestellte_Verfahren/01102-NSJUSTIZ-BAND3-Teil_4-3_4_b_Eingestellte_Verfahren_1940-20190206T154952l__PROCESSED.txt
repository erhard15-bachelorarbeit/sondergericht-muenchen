(5220)

(5221)

(5222)

(5223)

(5224)

5867

5868

5869

5871

5872

Ermittlungsverfahren gegen den Regie-
rungsvermessungsrat Johann AGGSTÄLLER
(geb. 16. Jun. 1895) aus München wegen
Bemerkungen zur deutschen Außenpolitik,
($ 2 HG)

Verfahren eingestellt

9. Apr. 1940 - 1. Mai 1940
(1 Js So 776/40)

Ermittlungsverfahen gegen die Kassiere-
rin Elisabeth HAMBERGER (geb. 17. Nov.
1903) aus Rosenheim wegen Äußerungen
über Goebbels.

(HG)

Verfahren wegen Amnestie eingestellt

16. Feb. 1940 - 15. Jun. 1940
(1 Js So 778/40)

Ermittlungsverfahren gegen die Bauersfrau
Dora GOTZFRIED (geb. 5. Jun. 1886) aus
Kleinkittzighofen (Lkr. Kaufbeuren), weil
sie bedauert haben soll, daß das Attentat
( y srbrukeller mißlungen sei.

HG

Verfahren eingestellt

25. Feb. 1940 - 23. Jun. 1940
(1 Js So 780/40)

Ermittlungsverfahren gegen den Landwirt
Martin EHMANN (geb. 12. Dez. 1912) aus
Sch-abenberg (Lkr. Pfaffenhofen) wegen
Totschlagversuchs.

($ 1 Gew.Verbr. VO; 88$ 43,212 StGB)

Verfahren eingestellt

28. Feb. 1940 - 2. Jul. 1940
(1 Js So 785/40; 1 Js So 685/40)

Ermittlungsverfahren gegen die Medizin-
studentin Gisela BÖNINGER (geb. 2. Aug.
1913) aus München wegen der Verbreitung
von Gerüchten über Meutereien beim stu-
(u) Erntearbeitseinsatz.

HG

Verfahren eingestellt

18. Apr. 1940 - 28. Itai 1940
(1 Js So 786/40)
