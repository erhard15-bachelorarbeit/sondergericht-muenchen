(1807)

(1808)

(1809)

(1810)

(1811)

3365

3366

3367

3368

3369

Ermittlungsverfahren gegen den Hilfsarbeiter
Otto STELZER (geb. 14. Jun. 1901) aus Aichach,
weil er gesagt haben soll, daß "die Deutsche
Arbeitsfront ein Schwindel"sei.

Verfahren eingestellt
S. wird in Schutihaft genommen.

4. Dez. 1937 - 21. Mrz. 1938
(1 c Js - So 1634/37)

Ermittlungsverfahren gegen den Versicherungs-
angestellten Karl KAMMERER (geb. 27. Apr.
1916) aus München, weil er bei seiner Ent-
lassung aus dem Arbeitsdienst erzählt haben
soll, daß es bei der Verkündung von der Ver-
längerung des Arbeitsdienstes zu einer großen
Demonstration im Lager gekommen sei, in deren
Verlauf man Görirg-Bilder von den Wänden ge-
rissen, diese verbrannt und die Internationale
gesungen habe.

Verfahren eingestellt

1. Nov. 1937 - 8. De. 1939
(1 b Js - So 1637/37)

Ermittlungsverfähren gegen die Lithographens-
frau Julie MAAR (geb. 20. Jul. 1868) und den
Kaufmann Josef SCHEIB (geb. 4. Apr. 1904), beide
aus München, wegen angeblicher abfälliger
Äußerungen über Hitler.

Verfahren eingestellt

18. Spt. 1937 - 20. Jan. 1938
(1 b Js - So 1638a-b/37)

Ermittlungsverfahren gegen den Steuersekretär
a.D. Anton WEISS (geb. 30. Jul. 1877) aus
Waldsassen (Lkr. Tirschenreuth) wegen unbe-
fugten Besitzes eines Parteimitgliedbuches.

Verfahren eingestellt

20. Dez. 1937 - 26. Mrz. 1938
(1 d Js - So 1641/37)

Ermittlungsverfahren gegen den ev. Pfarrer
Friedrich GREINER (geb. 17. Aug. 1892) aus
Nördlingen, weil er eine Schrift weiterge-
geben hat, in der behauptung wird, daß der
Großvater von Alfred Rosenberg Lette

die Mutter Französin, die Urgroßmutter Jüdin
u d der Urgroßvater Mongvole und Leibeigener
gewesen seien.

Verfahren eingestellt wegen Amnestie

22. Dez. 1937 - 8. Jun. 1938
(1 a Js - So 1642/37)
