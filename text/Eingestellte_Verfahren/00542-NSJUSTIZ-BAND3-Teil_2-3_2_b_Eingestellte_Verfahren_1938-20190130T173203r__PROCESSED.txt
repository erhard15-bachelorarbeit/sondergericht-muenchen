(2543)

(2544)

(2545)

(2546)

(2547)

3931

3932

3933

3934

3935

Ermittlungsverfahren gegen den kath.
Kaplan Andreas BADER (geb. 1. Mai
1911) aus Berching (Lkr. Beilngries)
wegen angeblicher politischer Bemer-
kungen in Predigten.

Verfahren wegen Amnestie eingestellt

16. Apr. 1938 - 3. Jun. 1938
(1a Js So 1062/38)

Ermittlungsverfahren gegen den Kanzlei-
obersekretär Friedrich HAAS (geb. 1.
Dez. 1877) aus Weidach (Lkr. Kempten),
weil er in einer Gastwirtschaft zum
Scherz "Heil Moskau" schrie.

Verfahren wegen Amnestie eingestellt

2. Mai 1938 - 30. Mai 1938
(1b Js So 1065/38)

Ermittlungsverfahren gegen die Frieda
DIETL (geb. 27. Feb. 1887) und deren
Ehemann, den Eisenbahnschaffner Josef
DIETL (geb. 25. Okt. 1880), beide aus
Baierbrunn (Lkr. Wolfratshausen) wegen
angeblicher politischer ÄAußerungen.

Verfahren wegen Amnestie eingestellt

27. Apr. 1938 - 3. Jun. 1938
(1a Js So 1066/38)

Ermittlungsverfahren gegen den Bau-
führer Leo PUDLOWSKY (geb. 9. Jan.
1897) aus Augsburg wegen angeblicher
Verächtlichmachung des Deutschen Grußes.

Verfahren wegen Amnestie eingestellt

18. Mrz. 1938 - 1. Jun. 1938
(1c Js So 1068/38)

Ermittlungsverfahren gegen den Wagner
Matthias MICHL (geb. 29. Jan. 18973 aus
Grimolzhausen (Lkr. Schrobenhausen
wegen angeblicher beleidigender Äußerune
über Hitler.

Verfahren eingestellt

6. Apr. 1938 - 4. Jun. 1938
(1c Js So 1069/38)
