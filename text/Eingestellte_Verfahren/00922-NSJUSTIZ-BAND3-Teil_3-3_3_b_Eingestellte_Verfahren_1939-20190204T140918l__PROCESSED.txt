(4440)

(4441)

(4442)

(4443)

(4444)

5592

5293

5594

9895

2595

Ermittlungsverfahren gegen den jüdischen
Juwelenhändler Karl BICK (geb. 3. Jan.
1878) aus München, wegen einer Äußerung
über Deutschlands Zukunft.

($ 2 HG)

Verfahren eingestellt

10. Nov. 1939 - 20. Apr. 1940
(1 Js So 2324/39)

Ermittlungsverfahren gegen den Maler
Georg MÜLLER (geb. 6. Mrz. 1888) aus
Rebdorf (Lkr. Eichstätt), wegen poli-
tischer Äußerungen.

(88 1,2 HG)

Verfahren eingestellt

4. Dez. 1939 - 27. Feb. 1940
(1 Js So 2337/39)

Ermittlungsverfahren gegen den Bauern
Sylvester UHLEMEYER (geb. 31. Dez. 1878)
aus Riegen (Lkr. Lindau), wegen Auße-
rungen über Hitler.

(HG)

Verfahren wegen Amnestie eingestellt

12. Okt. 1939 - 9. Feb. 1940
(1 Js So 2338/39)

Ermittlungsverfahren gegen die Nacht-
wädtersehefrau Amalie BRUNNER (geb. 31.
Dez. 1875) aus Kottern (Lkr. Kempten),
()“ einer Äußerung über den Krieg.

Verfahren eingestellt (Staatsanwalt-
schaft Kempten)

2. Okt. 1939 - 24. Apr. 1940
(1 Js So 2341/39)

Ermittlungsverfahren gegen die Kraft-
wagenfahrersehefrau Babette KÄRGL (geb.
4. Apr. 1891) aus München, wegen einer
zustimmenden Äußerung zum Attentat auf
Hitler am 9. 11. 1939

(HG)

Verfahren eingestellt

10. Nov. 1939 - 25. Jan. 1940
(1 Js So 2349/39)
