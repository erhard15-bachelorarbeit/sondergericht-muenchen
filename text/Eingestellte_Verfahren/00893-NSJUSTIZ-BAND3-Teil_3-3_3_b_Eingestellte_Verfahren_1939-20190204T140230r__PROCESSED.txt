(4294)

(4295)

(4296)

(4297)

(4298)

5493

5371

5372

5373

5363

Prozeß gegen den kath. Benefiziaten Mein-
rad SIGG (geb. 27. Mai 1907) aus Iller-
eichen (Lkr. Illertissen), wegen Bemer-
kungen über den Krieg.

(& 2 HG)

Verfahren wegen Amnestie eingestellt

153. Spt. 1939 - 29. Mai 1940
(1 Js So 1784/39; Qs So 55/40)

Ermittlungsverfahren gegen den Bäcker-
meister Blasius BEINER (geb. 6. Apr. 1903)
aus München, weil er in einem Gespräch
über das Münchner Abkommen gesagt hat:
"Das können die anderen auch, Verträge
zerreißen wie Hitler."

(HG)

Verfahren wegen Amnestie eingestellt

28. Spt. 1939 - 8. Jan. 1940
(1 Js So 1786/39)

Ermittlungsverfahren gegen den Oberingenieur
Johann VOHBURGER (geb. 25. Mai 1894) aus
Minchen, wegen politischer Außerungen.

($8 1,2 HG)

Verfahren eingestellt

6. Okt. 1939 - 28. Dez. 1939
(1 Js So 1787/39)

Ermittlungsverfahren gegen den Landwirt
und Zimmermann Friedrich RIMBECK (geb.
30. Mrz. 1888) aus Vorderreckenberg
(Gem. Winzer, Lkr. Deggendorf), wegen
Außsrungen über den Krieg.

(HG)

Verfahren wegen Amnestie eingestellt

12. Spt. 1939 - 29. Nov. 1939
(1 Js So 1791/39)

Ermittlungsverfahren gegen den Schlosser
Franz Xaver SCHMID (geb. 4. Dez. 1899)
aus München, weil er u.a. verbreitet ha-
ben soll, daß sich Offiziere und führende
Persönlichkeiten der SA bei den "Königs-
treuen" befänden.

($ 1 HG)

Verfahren eingestelit
