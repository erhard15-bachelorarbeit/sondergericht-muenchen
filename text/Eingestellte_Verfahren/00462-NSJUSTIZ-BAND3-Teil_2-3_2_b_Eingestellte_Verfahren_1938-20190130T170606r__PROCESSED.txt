(2160)

(2161)

(2162)

(2163)

(2164

3252

3553

3554

35352

Ermittlungsverfahren gegen den Schafhalter
Georg SEBFRIED (geb. 25. Apr. 1871) aus
Feldmoching (München) wegen angeblich be-
leidigender Außerungen über das Dritte
Reich.

Verfahren eingestellt

5. Feb. 1938 - 8. Mrz. 1938
(1c Js So 408/38)

Ermittlungsverfahren gegen den Schreiner
Ludwig ZIMMERMANN (geb. 5. Okt. 1906)
aus Schwandorf, weil er in einer Gast-
wirtschaft erzählt haben soll, daß er
einmal bei Vernehmungen durch SA-Beamte
mißhandelt wurde.

Verfahren eingestellt

1. Jan. 1938 - 10. Mrz. 1938
(1c Js So 412/38)

Ermittlungsverfahren gegen den Rentner
Josef BERGER (geb. 24. Aug. 1870) aus

Bihlerdorf (Lkr. Sonthofen) wegen be-

leidigender Äußerungen über Hitler und
die Partei.

Verfahren eingestellt

3. Feb. 1938 - 10. Mrz. 1938
(1a Js So 413/38)

Ermittlungsverfahren gegen die Haus-
besitzersfrau Elisabeth DUNZL (geb.

27. Spt. 1868) aus München wegen ab-
fälliger Äußerungen über die NS-
Frauenschaft gegenüber einer Nachbarin, wo-
bei sie versucht haben soll, diese vom Bei-
tritt in die Frauenschaft abzuhalten.

Verfahren eingestelit

24. Jan. 1938 - 15. Mrz. 1938
(1a Js So 414/38)

Ermittlungsverfahren gegen den Kultur-
arbeiter Andreas GIMPL (geb. 12. Jun.

1906) aus Vorcermiesenbach (Lkr. Traunstein)
und den Kulturarbeiter Franz HASSLBERGER
(geb. 26. Dez. 1899) aus Brandstätt (Lkr.
Traunstein) wegen angeblich beleidigender
Äußerungen über die SA und Hitler.

Verfahren eingestellt

4. Feb. 1938 - 28. Mrz. 1938
(1a Js So 415a-b/38)
