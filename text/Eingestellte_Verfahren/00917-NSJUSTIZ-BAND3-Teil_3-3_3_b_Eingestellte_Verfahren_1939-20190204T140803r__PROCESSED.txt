4415)

(4416)

(4417)

(4418)

(4419)

5568

53569

2570

3571

Ermittlungsverfahren gegen den Schlosser
Otto SPIRK (geb. 20. Mai 1909) aus
Schretzheim (Lkr. Dillingen), wegen po-
litischer Äußerungen in Lauingen (Lkr.
Dillingen).

(HG)

Verfahren eingestellt

3. Spt. 1939 - 4. Jan. 1940
(1 Js So 2233/39)

Ermittlungsverfahren gegen den Kunstmaler
Wilhelm SCHWAR (geb. 25. Mai 1860) aus
München, wegen Abhörens ausländischer
Sender.

(HG; Rdf.VO)

Verfahren eingestellt

29. Okt. 1939 - 10. Jan. 1940
(1 Js So 2234/39)

Ermittlungsverfahren gegen den Schlosser
Lorenz HEILMEIER (geb. 25. Dez. 1900) aus
München, früher Reichsbanner-, dann
Stahlhelm-, dann SA-Mitglied, wegen der
Äußerung: "Dem Hitler kommt auch noch
der Kopf runter, wenn der Krieg noch
länger dauert."

(Hg)

Verfahren eingestellt

Heilmeier wurde von seinem Arbeitsplatz
fristlos entlassen.

3. Nov. 1939 - 4. Mrz. 1940
(1 Js So 2239/39)

Ermittlungsverfahren gegen den Elektro-
mechaniker Franz Josef STARZINGER (geb. 4
Jan. 1898) aus München, wegen politischer
Bemerkungen in einem Brief nach Südtirol.
($ 2 HG)

Verfahren eingestellt

5. Nov. 1939 - 8. Jan. 1940
(1 Js So 2240/39)

Ermittlungsverfahren gegen den Verkäufer

Georg FEHLE (geb. 9. Jan. 1891) aus

a) wegen politischer Bemerkungen.
HG

Verfahren eingestellt

28. Spt. 1939 - 17. Feb. 1940
(1 Js So 2243/39)
