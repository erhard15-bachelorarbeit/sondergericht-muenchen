(2997)

(2998)

(2999)

(3000)

(3001)

4346

4347

4447

4448

4348

(1d Js So 2067/38)

Ermittlungsverfahren gegen den Kupferschmie-
dehelfer Andreas HÖCK (geb. 18. Feb. 1904),
früher SPD-Mitglied, und den Graphit-Hilfs-
dreher Johann SCHÄTZL (geb. 2. Jul. 1913),
früher SA-Mitglied, beide aus München, wegen
einer beleidigenden Außerung über Hitler.

Verfahren eingestellt

5. Okt. 1938 - 28. Nov. 1938
(1b Js So 2068/38)

Ermittlungsverfahren gegen den Bankbeamten
Erwin HIELSCHER (geb. 19. Dez. 1898) aus
München, SPD-Mitglied, weil er die Inter-
nationale gesummt hat.

Verfahren eingestellt

15. Okt. 1938 - 26. Nov. 1938
(1b Js So 2070/38)

Ermittlungsverfahren gegen den kath. Pfarrer
Karl BLEICHER (geb. 21. Okt. 1888) aus Beuern
(Lkr. Landsberg), weil er eine Flugschrift
über die wirtschaftliche Lage Deutschlands
weitergegeben hat.

Verfahren wegen Amnestie eingestellt

Anlage: 1 Flugschrift "I. Die Grenzen für
jede Aktivität auf wirtschaftlichem
Felde ..." (hektographiert, 8 Blätter).

6. Aug. 1938 - 21. Apr. 1939
(1a Js So 2071/38)

Ermittlungsverfahren gegen den Landgerichts-
direktor Dr. Ignaz TISCHLER (geb. 27. Jul. 1877)
aus Landshut, weil er sich kritisch über die

SA und die Verfolgung der Juden geäußert ha-
ben soll.

Verfahren eingestellt

11. Nov. 1938 - 5. Jun. 1939
(1d Js So 2072/38)

Ermittlungsverfahren gegen den Schwerkriegs-
beschädigten Ludwig FALTERMEIER (geb. 15. Mai
1876) , den Reichsbahnschaffner Theobald RÖDER
(geb. 1. Jul. 1889) und den Straßenbahnober-
schaffner Fried rich WIMMER (geb. 4. Okt. 1895),
alle aus München, wegen kritischer politischer
Bemerkungen.

Verfahren eingestellt

3. Okt. 1938 - 21. Dez. 1938
(1a Js So 2081/38)
