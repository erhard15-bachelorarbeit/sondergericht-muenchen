(4336)

(4337)

(4338)

(4339)

(4340)

5400

5401

5402

53403

5504

Ermittlungsverfahren gegen den Maschinisten
Anton HÖGNER (geb. 24. Mai 1891) aus
Schreckenmanklitz (Lkr. Lindau), früher

Pg. und SA-Mitglied, wegen der Behauptung,
Himmler und v. Epp seien verhaftet worden.

($ 1 Ha)

Verfahren wegen Amnestie eingestellt

13. Spt. 1939 - 28. Dez. 1939
(1 Js So 1913/39)

Prozeß gegen den Stereotypeur Ludwg KRÄNZ-
LE (geb. 27. Aug. 1897) aus München, Pg.,
(na) politischer ÄAußerungen.

HG

Verfahren wegen Amnestie eingestellt

6. Spt. 1939 - 18. Mrz. 1940
(1 Js So 1915/39)

Ermittlungsverfahren gegen den Buchhalter
und Kassier Franz Xaver FRIEDL (geb. 6.
Nov. 1881) aus Eichenau (Lkr. Fürsten-
feldbruck), Pg., wegen der Bemerkung,

wer die Gebühr für die Bescheinigung sei-
nes Kirchenaustritts nicht bezahlen wolle,
müsse zu Adolf Hitler gehen.

($ 2 Ha)

Verfahren eingestellt

19. Okt. 1939 - 6. Feb. 1940
(1 Js So 1919/39)

Ermittlungsverfahren gegen den Gütler
Ludwig FISCHBACHER (geb. 14. Dez. 1892)
aus Rumersham (Lkr. Traunstein), wegen
politischer Bemerkungen.

(HG)

Verfahren wegen Amnestie eingestellt

3. Okt. 1939 - 21. Dez. 1939
(1 Js So 1922/39)

Prozeß gegen den Erbhofbauern Georg GEBEN-
DORFER (geb. 25. Okt. 1875) aus Unter-
pindhart (Lkr. Pfaffenhofen), wegen Äuße-
rungen über Hitler.

(HG)

Verfahren wegen Amnestie eingestellt

12. Spt. 1939 - 3. Feb. 1940
(1 Js So 1931/39)
