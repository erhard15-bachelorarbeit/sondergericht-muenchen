(5392)

(5393)

(5394)

(5395)

(5396)

6392

6130

6038

600

Ermittlungsverfahren gegen den Hilfsar-
beiter Georg WEISS 6 8. Feb. 1887) und
seine Frau Barbara (geb. 12. Apr. 18681)
aus Immenstadt i. Allgäu (Lkr. Sonthofen)
wegen Äußerungen über den Krieg und WHVW-
Sammlungen sowie Abhörens ausländischer
Rundfunksender.

(HG; Raf.VO)

Verfahren eingestellt

22. Jul. 1940 - 17. Dez. 1942
(1 Js So 1517/40)

Ermittlungsverfahren gegen den Kuraten
Franz FRUHMANN (geb. 20. Spt. 1898) aus
Linz (österreich) wegen kritischer Äuße-
rungen über die nat.soz. Jugenderziehung
und dar Bemerkung, daß die Sittlichkeits-
prozesase gegen Geistliche "Lug und Trug"
seien.

(HG)

Verfahren eingestellt

26. Jul. 1940 - 17. Jan.1941
(1 Js So 518/40)

Ermittiungsverfahren gegen die Maurersfrau
Emilie JAVUREK (geb. 14. Mai 1889) aus
Strakonitz (CSSR), weil sie behauptet ha-
ben soll, daß die Deutschen bald aus der
ugj wa vertrieben würden.

HG

Verfahren eingesellt

20. Mrz. 1940 - 3. Okt. 1940
(1 Js So 1924/40)

rmittlungsverfahren gegen den Mechaniker
Franz Xaver MAYER (geb. 19. Nov. 1912) aus
Grießer (Lkr. Miesbach) wegen abfälliger
Außerungen über die deutsche Wehrmacht Sso-
wie üb-r die Versorgung der Soldaten in
Militärlazaretten.

(HG)

Verfahren eingestellt

23. Aug. 1940 - 17. Okt. 1940
(1 Js So 1529/40)

Ermittlungsverfahren gegen den Schäffler-
meister Anton MOSER (geb. 3. Jul. 1906)
aus Rcechrenfels (Lkr. Neuburg a.d. Donau),
weil er gesagt haben soll, daß Hitler am
Ausbruch des Krieges schuld sei.

(HG)

Verfahren eingestellt
