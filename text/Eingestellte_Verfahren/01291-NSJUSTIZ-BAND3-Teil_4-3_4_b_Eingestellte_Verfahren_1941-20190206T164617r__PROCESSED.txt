(6101)

(6102)

6103)

6104)

61653

6164

6165

6166

b) Eingestellt+a Verfahren

Ermittlungsverfahren gegen die Hausgehilfin
Rosa SEITLER (geb. 10. Nov. 1918) aus Augs-
burg, weil sie gesagt haben soll: "Der
Führer ist schuld am Krieg, dem habe ich
schon oft das Verrecken gewünscht."

($ 2 HG)

Verfahren eingestelit

24. Dez. 1940 - 21. Feb. 1941
(1 Js So 5/41)

Ermittlungsverfahren gegen den kfm. Ange-
stellten Ludwig HEPP (geb. 30. Spt. 1914)
aus München, weil er u.a. Hitler die Schuld
08 zugesprochen haben soll.

HG

Verfahren eingestellt

10. Jun. 1940 - 18. Jan. 1941
(1 Js So 12/41)

Ermittlungsverfahren gegen den Architekten
Hans BOSCHER (geb. 26. Mai 1886) aus Wolf-
ratshausen wegen verschiedener abfälliger

Außerungen über die Partei und das Dritte

Reich.

(HG; Rdf.VO)

Verfahren eingestellt

15. Nov. 1940 - 3. Mrz. 1941
(1 Js So 15/41)

Ermictlungsverfahren gegen den Sparkassen-
angestellten Hans BAUMGÄRTNER (geb. 15.

Apr. 1706) aus Scheidegg (Lkr. Lindau) und
den Friseur Karl HOLZMEIER (geb. 16. Dez.
1888) aus Lindenberg i. Allg. (Lkr. Lindau)
wegen der Verbreitung des Gerüchts, daß
Generalfeldmarschall Milch zusammen mit

21 offizieren wegen Verrats erschossen worden
sel.

($ 1 HG)

Verfahren eingestellt

30. Dez. 1940 - 1. Feb. 1941
(1 Js So 27/41)

Ermittlungsverfahren gegen die Gartenar-
beitersehefrau Kreszentia DOPFER (geb. 9.
Apr. 1891) aus Kempten wegen der Verbreitung
von Gerüchten über Propagandaminister
Goebbels.

(HG)

Verfahren eingestellt
