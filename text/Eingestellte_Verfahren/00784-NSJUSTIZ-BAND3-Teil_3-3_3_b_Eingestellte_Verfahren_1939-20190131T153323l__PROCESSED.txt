(3750)

(3751)

(3752)

(3753)

(3754)

4884

4885

4886

4887

4888

Ermittlungsverfahren gegen den kfm. Ange-
stellten Karl Jakob KRANZFELDER (geb. 15. Ap
1904) aus Augsburg, wegen der Bemerkung:
"Für mich ist das Dritte Reich Luft."

($ 2 HG; 134a StGB)

Verfahren eingeställt

15. Apr. 1939 - 16. Jun. 1939
(1 Js So 809/39)

Ermittlungsverfahren gegen den Kaffeehänd-
ler Johann JENNIGES (geb. 22. Feb. 1892)
aus Kaisheim (Lkr. Donauwörth), wegen
Äußerungen über die Regierung und die Juden-
politik.

(HG)

Verfahren eingestellt

19. Mrz. 1939 - 15. Jun. 1939
(1 Js So 821/39)

Ermittlungsverfahren gegen die Maurersehe-
frau Margaret SCHNELLER (geb. 28. Feb.
1884) aus Wattenberg (Lkr. Beilngries)
wegen einer Äußerung über die Sammlungen.
($ 2 HG)

Verfahren eingestellt

22. Apr. 1939 - 5. Spt. 1939
(1 Js So 824/39)

Ermittlungsverfahren gegen den Steinmetz-
meister Friedrich STOCKER (geb. 24. Apr.
1892) aus Weißenburg, wegen Äußerungen
über die Leistungsfähigkeit der Jugend in
einem Krieg.

($ 134b StGB)

Verfahren eingestellt

17. Mai 1939 - 28. Jun. 1939
(1 Js So 827/39)

Ermittlungsverfahren gegen den Gütler und
Holzarbeiter Kaspar BICHLER (geb. 26. Okt.
1874) aus Mühlbach (Lkr. Bad Tölz), wesen
politischer Bemerkungen in einem Brief
nach Amerika.

(HG)

Verfahren eingestellt

27. Mrz. 1939 - 8. Aug. 1939
(1 Js So 836/39)
