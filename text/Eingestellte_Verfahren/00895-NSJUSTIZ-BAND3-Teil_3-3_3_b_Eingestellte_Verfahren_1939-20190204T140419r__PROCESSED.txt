(4304)

(4305)

(4306)

(4307)

(4308)

5375

5376

5377

2378

Ermittlungsverfahren gegen die Zimmer-
mannsehefrau Franziska PRITZ (geb. 18. Mrz
1886) aus Gräfelfing (Lkr. München), we-
a) rungen über Hitler.
HG

Verfahren eingestellt

29. Jul. 1939 - 6. Dez.1939
(1 Js So 1801/39)

Ermittlungsverfahren gegen den Immobilien-
makler Ludwig Johann SCHOTT (geb. 12.

Jun. 1889) und dessen Ehefrau Maria (geb.
20. Apr. 1894), beide aus München, wegen
beleidigender Äußerungen über zwei Luft-
schutzwarte.

(HG)

Verfahren eingestellt

4. Spt. 1939 - 16. Dez. 1939
(1 Js So 1803/39)

Ermittlungsverfahren gegen den Landwirt
Andreas RING (geb. 26. Aug. 1893) aus
Oberdorf (Lkr. Fürstenfeldbruck), wegen
einer Außerung über Hitlers Forderungen
an Polen.

($ 2 HG)

Verfahren wegen Amnestie eingestellt

12. Spt. 1939 - 11. Dez. 1939
(1 Js So 1807/39)

Ermittlungsverfahren gegen die Majorsehe-
frau Therese Rosina SCHILLER (geb. 11. Mrz
1891) aus München, wegen Äußerungen über
den Krieg.

(HG)

Verfahren wegen Amnestie eingestellt

2. Spt. 1939 - 2. Dez. 1939
(1 Js So 1808/39)

Ermittlungsverfahren gegen Johanna EICHIN-
GER (geb. 20. Nov. 1910) aus Pfeffenhausen
(Lkr. Rottenburg), wegen einer Äußerung
über Hitler.

(HG)

Verfahren eingestellt

10. Okt. 1939 - 16. Nov. 1939
(1 Js So 1811/39)
