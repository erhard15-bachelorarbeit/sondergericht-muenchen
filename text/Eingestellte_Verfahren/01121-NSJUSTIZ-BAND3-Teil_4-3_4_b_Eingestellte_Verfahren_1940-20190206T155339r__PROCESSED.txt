(5316)

(5317)

(5318)

(5319)

(5320)

5971

5972

23973

5974

5975

Ermittlungsverfahren gegen den Kaufmann
Johann Fritz BERGER (geb. 4. Aug. 1917)
aus Nürnberg wegen Diebstahls.

(88 171.174,(197), 461 österr. StGB)

Das Verfahren wird in ein Sammelverfahren
übernommen

10. Feb. 1940 - 24. Jun. 1940
(1 Js So 1217/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Wilhelm WEISS (geb. 29. Mrz. 1905),
Pg. und 58S-Mitglied, aus München wegen
beleidigender Äußerungen über Gauleiter
Adolf Wagner.

($ 2 HG)

Verfahren eingestellt

13. Jun. 1940 - 15. Aug. 1940
(1 Js So 1222/40)

Ermittlungsverfahren gegen den Bäcker
Max Nikclaus JÖRG (geb. 29. Mai 1900),
Pg., aus Zell (Lkr. Memmingen), weil er
die 35 kritisiert hat.

(HG)

Verfahren eingestellt

25. Mrz. 1940 - 11. Dez. 1940
(1 Js So 1223/40)

Ermittlungsverfahren gegen den Landwirt
Johann SEIDL (geb. 19. Mrz. 1879) und
seineFrau Anna (geb. 11. Jan. 1881) aus
Otterskirchen (Lkr. Vilshofen) wegen ab-
fälliger Äußerungen über Hitler und die
nationale Bewegung.

($ 2 HG; 3 5 Kriegssonderstr.VO)

Verfahren eingestellt

13. Dez. 1939 - 2. Okt. 1940
(1 Js So 1224/40)

Ermittlungsverfahren gegen den Metzger-
meister Julius SEESSLE (geb. 22. Jun.
1888) und seine Frau Monika (geb. 21.

Apr. 1898) aus Augsburg. Sie sollen aus-
ländische Rundfunksender abgehört, Hitler
einen "Lumpen" genannt sowie durch "son-
stige, ständige Schimpfereien ihre staats-
feindliche Einstellung" zum Ausdruck ge-
bracht haben.

(HG; Räaf.VO)

Verfahren eingestellt

6. Jun. 1940 - 21. Spt. 1940
(1 Js So 1228/40)
