(4032)

(4033)

(4034)

(4035)

(4036)

5145

5146

5456

5147

Prozeß gegen die Hilfsarbeitersfrau Mag-
dalena NIEDERHUBER (geb. 26. Jun. 1907
aus Leinburg (Lkr. Nürnberg), weil sie
sich in Kolbermoor (Lkr. Bad Aibling) ab-
fällig über Hier geäußert und ihrer
kommunistischen Einstellung durch "Ver-
herrlichung der Zustände in Rußland"
Ausdruck gegeben haben soll.

(HG; 3 83 StGB)

Verfahren eingestellt

22. Okt. 1938 - 22. Nov. 1939
(1 Js So 1328/39; 1 j, So 2337/38)

Ermittlungsverfahren gegen den Gütlerssohn
Hermann REGER (geb. 30. Jan. 1908) und
den Holzarbeiter Ignatz KLAFFENBÖCK (geb.
17. Jun. 1897) aus Hirschbach (Lkr. Pfarr-
kirchen), weil sie während einer Unterhal-
tung der Vermutung Ausdruck gegeben haben
sollen, daß die Regierung bei einer Ver-
schlechterung der wirtschaftlichen Lage

estürzt werde.

HG)

Verfahren eingestellt

26. Aug. 1939 - 2. Okt. 1939
(1 Js So 1329/39)

Ermittlungsverfahren gegen den Hilfsarbei-
ter Ernst RISCHBLICK (geb. 25. Okt. 1889)
aus München, weil er gesagt haben soll,
daß Danzig "keinen Kopf Blut wert sei" und
außerdem "die heutigen Generäle Kriegsge-
winnler" seien.

(HG)

Verfahren wegen Amnestie eingestellt

2. Spt. 1939 - 30. Okt. 1939
(1 Js So 1330/39)

Ermittlungsverfahren gegen den Brauer Georg
STEIN (geb. 23. Nov. 1877) aus Schmelz (Lkr.
Traunstein), wegen abwertender Äußerungen
über Hitler.

(HG)

Verfahren wegen Amnestie eingestellt

3. Spt. 1939 - 25. Jan. 1940
(1 Js So 1331/39)

Ermittlungsverfahren gegen den Hausierer Wil
helm SEIBOLD (geb. 15. Mai 1909) aus Mün-
chen, weil er gesagt hat, daß der Krieg

5 Jahre dauern und keiner der Frontsolda-
ten zurückkommen werde.
