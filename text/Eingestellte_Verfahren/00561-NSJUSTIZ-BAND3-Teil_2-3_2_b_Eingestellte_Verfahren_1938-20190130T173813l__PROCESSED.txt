(2650)

(2651)

(2652)

(2653)

(2654)

4038

4039

4040

4041

4042

gesprochen haben sollen.
Verfahren wegen Amnestie eingestellt

2. Mai 1938 - 13. Jul. 1938
(1c Js So 125 1 a-b/38)

Ermittlungsverfahren gegen den Metzger
Ludwig WEXLBERGER (geb. 18. Mai 1903)
aus Traunwalchen (Lkr. Traunstein), weil
er sich für eine kommunistische Regierung
ausgesprochen haben soll.

Verfahren wegen Amnestie eingestellt
W. wird in Schutzhaft genommen.

29. Apr. 1938 - 16. Aug. 1938
(1d Js So 1255/38)

Ermittlungsverfahren gegen den Haus-
meister Franz HELMINGER (geb. 1. Dez.
1888) aus Alzing (Lkr. Traunstein),
weil er Göring einen "hergelaufenen
Handwerksbursch" genannt haben soll.

Verfahren wegen Amnestie eingestellt

17. Mai 1938 - 6. Aug. 1938
(1b Js So 1259/38)

Ermittlungsverfahren gegen die Haus-
hälterin Theres FRIEDRICH (geb. 5.
Feb. 1882) aus Teising (Lkr. Mühldorf),
weil sie sich in einem Brief kritisch
über den Einmarsch in Österreich ge-
äußert hat.

Verfahren eingestellt

22. Jun. 1938 - 11. Jul. 1938
(1a Js So 1267/38)

Ermittlungsverfahren gegen den Bauhilfs-
arbeiter Josef ELFINGER (geb. 21. Jan.
1894) aus München, weil er an der
Reichstagswahl am 10. Apr. nicht teilge-
nommen hat und sich für ein kommunistisch
System ausgesprochen haben Soll.

Verfahren wegen Amnestie eingestellt

5. Mai 1938 - 15. Jul. 1938
(1a Js So 1269/38)

Ermittlungsverfahren gegen den Gastwirt
und Metzger Georg SIEBENBURGER (geb. 4 )
Nov. 1899) aus Nandlstadt (Lkr. Freisinß
weil er einen SA-Mann einen Idioten 69T
nannt und gesagt haben soll, daß pe
die größten Lumpen und Bazi an führende
Stelle sind."

Verfahren eingestellt

30. Apr. 1938 - 21. Jul. 1938
(1d Js So 1270/38)
