(5014)

(5015)

(5016)

(5017)

(5018)

(5019)

5654

5655

5656

5657

5658

5659

Ermittlungsverfahren gegen den Austrägler
Klement HUBER (geb. 26. Apr. 1878) aus
Attaching (Lkr. Freising) wegen einer Äuße
(HG) über Hitler und den Krieg.

Verfahren eingestellt

4. Jan. 1940 - 14. Feb. 1940
(1 Js So 102/40)

Ermittlungsverfahren gegen den Fabrikar-
beiter Leo MAYER (geb. 2. Jul. 1908) aus
München wegen Sabotage.

($8 1,2 Wehrkraft VO

Verfahren eingestellt

28. Dez. 1939 - 5. Mrz. 1940
(1 Js So 103/40)

Ermittlungsverfahren gegen die Witwe
Kreszenz EICHNER (geb. 13. Mai 1889) aus
München wegen Nichtablieferns zweier ge-
fundener Parteiabzeichen. -

($ 5 HG)

Verfahren eingestellt

15. Nov. 1939 - 9. Mai 1940
(1 Js So 104/40)

Ermittlungsverfahren gegen den Maler Lud-
wig Anton KNIEPS (geb. 9. Jun. 1916) aus
Berlin wegen der Außerung in München,
Hitler sei ein Simulant.

(HG)

Verfahren wegen Amnestie eingestellt

14. Spt. 1939 - 12. Feb. 1940
(1 Js So 105/40)

Ermittlungsverfahren gegen den Maschinen-
schlosser Gottfried OBERMEIER (geb. 1.

Okt. 1902) aus München, früher SA-Mitglied,
wegen Schimpfens über Hitler.

(HG)

Verfahren eingestellt

1. Dez. 1939 - 4. Mrz. 1940
(1 Js So 106/40)

Ermittlungsverfahren gegen den Kaufmann
Max ARIMOND (geb. 7. Okt. 1889) aus Muün-
chen wegen der Außerung: "Lieber Wilhelm
von Gottes Gnaden, als den Depp von Berch-
tesgaden."
