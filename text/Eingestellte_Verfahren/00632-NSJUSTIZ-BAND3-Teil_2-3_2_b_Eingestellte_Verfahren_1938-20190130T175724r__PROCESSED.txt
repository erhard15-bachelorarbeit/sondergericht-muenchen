(3042)

(3043)

(3044)

(3045)

(3046)

(3047)

4366

4470

4367

4368

4471

4472

Ermittlungsverfahren gegen den Schachtmeister
Andreas FELBERMEIER (geb. 31. Okt. 1908) und
den Hilfsarbeiter Alfred TRAGL (geb. 14. Spt.
1882), beide aus München, weil sie Briefe für
KZ-Häftlinge beförderten.

Verfahren wegen Verjährung eingestellt

F. und T. wurden in Polizei- und Schutzhaft
genommen.

1. Spt. 1938 - 26. Dez. 1938
(1a Js So 2152/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Andreas KEZMANN (geb. 2. Dez. 1909) aus Hirt-
bichl (Lkr. Garmisch-Partenkrichen), weil er
gesagt haben soll, daß ihm die Juden lieber
seien als der nationale Staat.

Verfahren eingestellt

17. Nov. 1938 - 13. Apr. 1939
(1b Js So 2154/38)

Ermittlungsverfahren gegen die Geschäftsfüh-
rerin Maria KOPLINGER (geb. 6. Spt. 1889) aus
Neuötting (Lkr. Altötting), Pg., weil sie er-
zählt haben soll, bei Rosenheim sei ein Ge-
heimsender entdeckt worden, an dem auch die
SA beteiligt gewesen sei.

Verfahren eingestellt

8. Nov. 1938 - 29. Dez. 1938
(1b Js So 2155/38)

Ermittlungsverfahren gegen den Dienstknecht
Bernhard HUBER (geb. 14. Mai 1913) aus Mün-
chen, wegen angebl. abwertender Außerungen
über die HJ.

Verfahren wegen Amnestie eingestellt

19. Jan. 1938 - 15. Dez. 1938
(1b Js So 2156/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Matthias MARTINSCHITZ (geb. 16. Feb. 1909)
aus München, wegen unberechtigten Tragens
eines Parteiabzeichens.

Verfahren eingestellt

1. Nov. 1938 - 10. Mrz. 1939
(le Js So 2157/38)

Ermittlungsverfahren gegen die Näherin Magda-
lena MÜHLBERGER (geb. 9. Feb. 1904) aus AL-
tenerding (Lkr. Erding), weil sie die SA be-
leidigt haben soll.

Verfahren eingestellt
