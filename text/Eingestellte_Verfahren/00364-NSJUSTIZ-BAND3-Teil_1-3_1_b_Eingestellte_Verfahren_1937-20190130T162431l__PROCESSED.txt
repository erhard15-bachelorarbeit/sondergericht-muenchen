(1733)

(1734)

(1735)

(1736)

(1737)

33053

3231

3232

3304

3305

Ermittlungsverfahren gegen den Regierungs-
rat a.D. Paul MÜLLER (geb. 22. Dez. 1893)
aus Hochsträß (Lkr. Lindau), weil er wahre
einer Radio-Übertragung einer Hitler-Rede
u.a. geschrieen hat: ""Du Lump, Schuft,
Lügner".

Verfahren eingestellt

22. Nov. 1937 - 24. Jan. 1938
(1 b Js - So 1455/37)

Ermittlungsverfahren gegen den Kaufmann
Bernhard SCHLEGTENDAHL (geb. 8. Dez. 1901)
und seine Ehefrau Else SCHLEGTENDAHL (geb.
6. Jan. 1906), beide aus München, weil
Frau S. erzählt haben soll, daß sie beide
gegen das Dritte Reich eingestellt seien.

Verfahren eingestellt

20. Aug. 1937 - 10. Dez. 1937
(1 c Js - So 1457 a-b/37)

Ermittlungsverfahren gegen die Störmnäherin
Elisabeth SCHMUCK (geb. 19. Apr. 1887) aus
Rosenheim, weil sie auf den Deutschen Gruß
geantwortet haben soll: "Sie, ich bin kein
Hitler, wir haben ihn früher auch nicht
gehabt, den Bazi".

Verfahren eingestellt

8. Nov. 1937 - 20. Dez. 1937
(1 c Js - So 1458/37)

Ermittlungsverfahren gegen die Schlossers-
ehefrau Therese PANKRATZ (geb. 16. Nov.
1899) aus Ruhstorf (Lkr. Griesbach), weil
sie im Gefängnis zu einer Zellengenossin
u.a. gesagt haben soll:"Hitler hilft auch
bloß den Großen und den Kleinen nicht".

Verfahren eingestellt

25. Okt. 1937 - 17. Jan. 1938
(1 b Js - So 1460/37)

Ermittlungsverfahren gegen den Hilfsar-
beiter Alfons SCHEIFELE (geb. 8. Mrz.
1912) und seinen Schwiegervater, den
Hilfsarbeiter Otto GROTTMEIER (geb. 11.
Mai 1878) aus Augsburg, weil S. seinem
Schwiegervater unberechtigterweise seine
SA-Diensthose überlassen hat.

Verfahren eingestellt

30. Nov. 1937 - 4. Jan. 1938
(1 d Js - So 1464/37)
