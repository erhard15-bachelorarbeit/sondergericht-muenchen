(5377)

(5378)

(5379)

(5380)

(5381)

6024

6391

6026

6027

6128

(1 Js So 1465/40)

Ermittlungsverfahren gegen die Landwirts-

tochter Maria MITTERMEIER (geb. 6. Jun.
1887) aus Voglarn (Lkr. Pfarrkirchen) we-
en Beschimpfung der Wehrmacht.

(S 2 HG; 3 134a StGB)

Verfahren eingestellt

5. Jul. 1940 - 23. Spt. 1940
(1 Js So 1467/40)

BErmittlungsverfahren gegen den Architekten
und luxemburgischen Honorarkonsul Otto
HEILMANN (geb. 22. Aug. 1888) aus München,
weil er gesagt haben soll, daß "das Münch-
ner Attentat durch die NSDAP in Verbin-
dung mit der Sicherheitspolizei vorberei-
tet worden sei, um den Volkshass gegen
(a) auf das Höchste zu steigern".

HG

Verfahren eingestellt

3. Dez. 1939 - 5. Jan. 1942
(1 Js So 1470/40)

Ermittlungsverfahren gegen die Landwirts-
frau Maria LINDENMEIER (geb. 8. Jun. 1898)
aus Waltershofen (Lkr. Wertingen) wegen
a) der Äußerungen über Hitler.

HG

Verfahren wegen Amnestie eingestellt

22. Jul. 1940 - 14. Spt. 1940
(1 Js So 1472/40)

Ermittlungsverfahren gegen den Gütler Jo-
sef SCHLÖGL (geb. 10. Nov. 1888) aus Ja-

ging (Lkr. Vilshofen), weil er die Radio-

berichterstattung als "Schwindel" bezeich-

net und außerdem verbreitet haben soll,

(c) "dem Hitler den Kopf weggehauen".
HG

Verfahren eingestellt

29. Jul. 1940 - 17. Nov. 1940
(1 Js So 1476/40)

Ermittlungsverfahren gegen den Maschinisten
Eduard MAIER (geb. 20. Apr. 1884) und sei-
ne Frau Therese (geb. 5. Jul. 1881) aus
München, weil sie den deutschen Einmarsch
in Belgien und Holland kritisiert und von
Schirach einen "Schnallentreiber" genannt
haben sollen
