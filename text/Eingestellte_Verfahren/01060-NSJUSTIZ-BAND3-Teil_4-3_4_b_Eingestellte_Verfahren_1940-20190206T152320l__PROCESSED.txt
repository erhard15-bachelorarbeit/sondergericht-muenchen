(5009)

(5010)

(5011)

(5012)

(5013)

53649

5650

5651

5652

5653

Ermittlungsverfahren gegen den NOolZRater
Johann Baptist MAUCHER (geb. 13. Jun.
1866) aus Wangen (Lkr. Kempten) Zentruss-
VG lied, wegen Äußerungen über den Krieg.
HG

Verfahren eingestellt

29. Okt. 1939 - 6. Apr. 1940
(1 Js So 88/40)

Ermittlungsverfahren gegen den Wagnerge-
hilfen Gebhard KELLER (geb. 15. Mai 1903)
pn Amtsanmaßung in Anzing (Lkr. Ebers-
berg).

(HG; Amtsanmaßung VO)

Verfähren eingestellt

29. Apr. 1939 - 27. Mai 1940
(1 Js So 89/40)

Ermittlungsverfahren gegen die Kassiererin
Josefa MEIER (geb. 22. Aug. 1909) aus
Fürstenfeldbruck wegen der Behauptung,
wenn das Attentat im Bürgerbräukeller
nicht gelungen wäre, ware ein ähnlicher
Anschlag an anderen Stellen in München
vorgesehen gewesen.

(HG

Verfahren eingestellt

11. Nov. 1939 - 3. Feb. 1940
(1 Js So 93/40)

Ermittlungsverfahren gegen die Angestell-
tenehefrau Gisela EDER (geb. 23. Mrz. 1911)
aus Dachau wegen einer Äußerung über Hit-
ler.

($ 2 HG)

Verfahren eingestellt

10. Nov. 1939 - 14. Feb. 1940
(1 Js So 97/40)

Ermittlungsverfahren gegen den Spengler-
meister Max HACKL (geb. 28. Jan. 1893)
aus Passau, Pg., wegen der ÄAußerung in
Münzkirchen (Lkr. Schärding, Österreich),
er schäme sich, Pg. zu sein.

(HG)

Verfahren wegen Amnestie eingestellt

5. Dez. 1939 - 3. Feb. 1940
(1 Js So 101/40)
