(3790)

(3791)

(3792)

(3793)

(3794)

4922

4923

4924

4925

4926

Ermittlungsverfahren gegen den Kutscher
Franz KOBES (geb. 7. Jan. 1884) aus Cho-
denschloß (CSSR), wegen politischer Unmuts-
äußerungen.

(HG)

Verfahren eingestellt

14. Jun. 1939 - 31. Aug. 1939
(1 Js So 909/39)

Ermittlungsverfahren gegen die Arbeiters-

und Kleingütlersehefrau Maria LINDL (geb.
6. Apr. 1900) aus Karlskron (Lkr. Neuburg),

weil sie einen Pg. und SA-Mann mit "Hitler-
(6) dreckiger" beschimpft haben soll.
HG

Verfahren eingestellt

53. Jun. 1939 - 7. Jul. 1939
(1 Js So 912/39)

Ermittlungsverfahren gegen den Schlosser-
gehilfen Otto NEUMAIER (geb. 20. Jan. 1912)

aus Neuburg, wegen abwertender ÄAußerungen
über das Regime.
(HG)

Verfahren wegen Amnestie eingestellt

6. Feb. 1939 - 6. Jul. 1939
(1 Js So 914/39)

Ermittlungsverfahren gegen die Bahnarbei-
tersfrau Johanna RIEPL (geb. 29. Dez. 19091

aus Regen, wegen beleidigender Äußerungen
über Hitler.
(HG)

Verfahren eingestellt

6. Mai 1939 - 14. Jul. 1939
(1 Js So 916/39)

Ermittlungsverfahren gegen den Fabrikanten
Karl STEINBERGER (geb. 2. Mrz. 18883),

aus Landshut, wegen hochstaplerisher An-
gaben über seine Beziehungen zu führenden
Parteimitgliedern.

(38 3,4,5 HG)
Verfahren eingestellt

23. Jul. 1939 - 2. Spt. 1939
(1 Js So 917/39)
