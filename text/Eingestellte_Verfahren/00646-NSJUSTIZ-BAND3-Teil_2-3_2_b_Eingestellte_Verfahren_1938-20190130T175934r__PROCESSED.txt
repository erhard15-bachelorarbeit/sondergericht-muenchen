(3121)

(3122)

(3123)

4506

4507

4508

Ermittlungsverfahren gegen den Landwirt
Dr. Cornelius KUTSCHKE (geb. 3. Jul. 1877)
aus Wiesen (Lkr. Wolfratshausen), weil er
nat.soz. Organisationen beschimpft haben
soll.

Verfahren eingestellt

5, Okt. 1938 - 12. Jan. 1939
(1b Js So 2329/38)

Ermittlungsverfahren gegen den Bauern
Eduard HARRINGER (geb. 3. Feb. 1897) aus
Birnbach (Lkr. Griesbach), wegen kritischer
Bemerkungen über die Sudetenkrise.

Verfahren eingestellt

1. Okt. 1938 - 20. Mrz. 1939
(1b Js So 2330/38)

Ermittlungsverfahren gegen den Metzger
Richard RIEDER (geb. 13. Dez. 1908) aus
Dachau, wegen einer abwertenden Bemerkung
über die Regierung.

Verfahren eingestellt

4. Nov. 1938 - 15. Mrz. 1939
(1c Js So 2340/38)
