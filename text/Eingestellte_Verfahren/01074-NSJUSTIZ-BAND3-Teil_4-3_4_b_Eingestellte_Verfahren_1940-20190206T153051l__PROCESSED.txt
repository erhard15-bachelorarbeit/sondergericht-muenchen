(5080)

(5081)

(5082)

(5083)

(5084)

5720

5721

5722

5724

5725

Ermittlungsverfahren gegen die Konsuls-
witwe Margarethe DUTOIT (geb. 19. Jun.
1883) aus München, weil sie u.a. Verbrei-
tet haben soll, daß feindliche Flie er
den Fliegerhorst bei Fürstenfeldbruck
bombadiert und zertrümmert hätten.

($ 1 Ha)

Verfahren eingestellt

9. Nov. 1939 - 29. Mrz. 1940
(1 Js So 241/40)

Ermittlungsverfahren gegen den tschechi-
schen Tischlergesellen Johann CAP (geb.
5. Mrz. 1916), wohnhaft in Schwäbisch-
Gmünd, weil er verbreitet haben soll, da3
Angehörige der SS seinen Bruder erschossen
hätten.

(HG)

Verfahren eingestellt

8. Dez. 1939 - 17. Feb. 1940
(1 Js So 242/40)

Ermittlungsverfahren gegen die Wekrmeister
witwe Anna HECKER (geb. 28. Mai 1886),
die Kioskbesitzerin Therese GAHR (geb.
17. Feb. 1891) und den Angestellten Anton
PARINGER (geb. 11. Jul. 1892) aus Ingol-
stadt, wegen Verbreitung von Gerüchten
(HG) Hitler und führende Pgg.

HG

Verfahren eingestellt

10. Jan. 1940 - 18. Mai 1940
(1 Js So 249/40)

Ermittlungsverfahren gegen den Ingenieur
Hara’d PFINGST (geb. 1. Jan. 1916), geb.
in Berlin, ohne festen Wohnsitz, wegen
Verbreitung von Gerüchten über die Juden-
verfolgung in Deutschland.

(HG)

Verfahren eingestellt

19. Feb. 1939 - 15. Nov. 1940
(1 Js So 254/40)

Ermittlungsverfahren gegen die Rentnerin
Babette VÖLZ (geb. 31. Mrz. 1877) und die
Buchhalterin Gertrud GADE (geb. 21. Jul.
1898) aus München, weil sie sich abfällig

(6) die Regierung geäußert haben sollen.
HG

Verfahren eingestellt

9. Nov. 1939 - 1. Mrz. 1940
(1 Js So 255/40)
