(2145)

(2146)

(2147)

(2148)

(2149)

3536

3537

3538

3539

3540

Ermittlungsverfahren gegen den Ingenieur
Heinrich AVENMARG (geb. 1. Aug. 1906)
aus München wegen der angeblichen Be-
hauptung, man müsse in Berlin ganze
Ortsgruppen und HJ-Verbände auflösen,
wenn Homosexualität etwas Schlimmes sei.

Verfahren wegen Amnestie eingestellt

4. Feb. 1938 - 12. Mai 1938
(1a Js So 378/38)

Ermittlungsverfahren gegen den BEisenbahn-
sekretär Max MURR (geb. 20. Okt. 7
aus Seeshaupt-Kieswerk (Lkr. Weilheim
wegen Schimpfens in einem Brief an den
Reichsinnenminister über den Staat und
die Partei.

Verfahren eingestelit

22. Nov. 1937 - 3. Mrz. 1938
(1b Js So 382/38)

Ermittlungsverfahren gegen den Kaminkehrer-
meister Alois KASPAR (geb. 15. Okt. 1892)
aus Aidenbach (Lkr. Vilshofen), früher
Stahlhelm-, dann SA-Mitglied, wego-n an-
geblicher Gleichsetzung des NS mit dem
Kommunismus.

Verfahren eingestellt

18. Nov. 1937 - 8. Apr. 1938
(1b Js So 387/38)

Ermittlungsverfahren gegen den Bauern
Martin HUBER (geb. 4. Jun. 1896) aus
Unterammergau (Lkr. Garmisch-Partenkir-
chen), früher Pg, wegen der angeblichen
Behauptung, der frühere Adjutant des Innen-
minister Wagner sei wegen Unterschlagung
verhaftet worden.

Verfahren eingestellt

24. Jan. 1938 - 4. Jun. 1938
(1a Js So 390/38)

Ermittlungsverfahren gegen den Schirm-
macher Martin MAYROCK (geb. 10. Jan.
1884) aus Memmingen, BVP-Mitglied, wegen
Bemerkungen über den BDM.

Verfahren eingestellt

29. Nov. 1937 - 2. Mai 1944
(1b Js So 391/38)
