(2915

(2916)

(2917)

(2918)

(2919)

(2920)

4286

4287

4288

4289

4290

4291

Ermittlungsverfahren gegen den Gastwirt und
Bauern Joseph SCHORER (geb. 27. Jan. 1891)

aus Bobingen (Lkr. Schwabmünchen), weil er

kritische politische Äußerungen gemacht ha-
ben soll.

Verfahren eingestellt

12. okt. 1938 - 22. Dez. 1938 R
(1d Js So 1916/38)

Ermittlungsverfahren gegen den kfm. Ange-
stellten Hans Andreas DENK (geb. 4. Jan. 1890)
aus München, weil er das Regime kritisiert
haben soll.

Verfahren wegen Amnestie eingestellt

5. Mai 1938 - 11. Nov. 1938
(1a Js So 1920/38)

Ermittlungsverfahren gegen den Friseur Anton
ELLWANGER (geb. 4. Feb. 1890) aus München,
weil er kritische politische Bemerkungen ge-
macht haben soll.

Verfahren wegen Amnestie eingestellt

8. Spt. 1938 - 7. Nov. 1938
(1a Js So 1922/38)

Ermittlungsverfahren gegen den Diplomland-
wirt Theodor v. COULON (geb. 31. Aug. 1889)
aus Eichenau (Lkr. Fürstenfeldbruck) und
dessen Mutter, die Hauptmannswitwe Maria

v. COULON (geb. 18. Feb. 1863) aus Fürsten-
feldbruck, weil sie kritische politische
Äußerungen gemacht haben sollen.

Verfahren eingestellt

9. Okt. 1938 - 3. Nov. 1938
(1a Js So 1923/38)

Ermittlungsverfahren gegen den Maurermeister
Xaver LUTZENBERGER (geb. 25. Okt. 1884) aus
Eppishausen (Lkr. Mindelheim), weil er ge-
sagt haben soll, Hitler habe in der Ordens-
burg Sonthofen scharf durchgegriffen.

Verfahren eingestellt

12. Okt. 1938 - 2. Nov. 1938
(1b Js So 1925/38)

Ermittlungsverfahren gegen den Schreinerge-
hilfen Franz Xaver MAYER (geb. 4. Jun. 1910)
aus Diessen (Lkr. Landsberg), weil er sich
kritisch über Hitler geäußert haben Soll.

Verfahren eingestellt

4. Okt. 1938 - 3. Jan. 1939
(1c Js So 1926/38)

> > A
