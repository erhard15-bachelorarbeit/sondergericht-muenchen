(5175)

(5176)

(5177)

(5178)

(5179)

5821

5822

5825

Ermittlungsverfahren gegen Adolf HANGL-
BERGER und Ewald PRÖSCHILD (0o.D.) wegen
kritischer Außerungen in Traunstein über
den Krieg.

(8 2 HG)

Verfähren eingestellt

30. Mrz. 1940 - 24. Jun. 1940
(1 Js So 607/40)

Ermittlungsverfahren gegen die Witwe Berta
DENNER (geb. 8. Okt. 1881) aus München,
weil sie an polnische Kriegsgefangene
Zigaretten verteilt haben soll.

($ 4 Wehrkraft VO)

Verfahren eingestellt

4. Mrz. 1940 - 10. Apr. 1940
(1 Js Su 6609/40)

Ermittlungsverfahren gegen Max KNAPPE
(o.D.)
(HG)

Verfahren eingestellt

Es handelt sich um die bereits unter
1 Js 353 649/39) bearbeitete und einge-
stellte Sache. (s. Nr. 3676)

23. Mrz. 1540 - 4. Apr. 1940
(1 Js Sc 611/40)

Ermittlungsverfahren gegen den Konditor
Gebhard HIRFT (geb. 7. Aug. 1907) aus
Munchen wegen abfälliger Bemerkungen über
Hitler und das Dritte Reich.

(HG)

Verfahren eingestellt

8. Mrz. 1940 . 21. Jun. 1940
(1 Js So 612/40)

Ermittlungsverfahren gegen den Fräser
Stephan LÄUTENBACHER (geb. 9. Feb. 1909)
aus Augsburg. Er soll nach dem Attentat
im Bürgerbräukeller, als die Anzahl der
Opfer mit sechs angegeben wurde, gesagt
haben: "Jetzt sind es wieder sechs so
Sauhunde weniger."

(HG)

Verfahren eingestellt

7. Mrz. 1940 - 16. Mai 1940
(1 Js So 614/40)
