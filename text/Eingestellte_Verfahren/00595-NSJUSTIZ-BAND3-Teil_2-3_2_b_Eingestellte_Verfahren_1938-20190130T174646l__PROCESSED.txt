(2831) 4213 FErmittlungsverfahren gegen die Feinmechanikers-

frau Paula ZEHEND (geb. 24. Jun. 1909), den
Polier Josef ORTMEIER (geb. 14. Mrz. 1904),
die Reichsbahnhilfsarbeitersfrau Helene LODER
(geb. 14. Mrz. 1909), alle aus München, weil
sie den deutschen Gruß nicht geleistet und
über die Regierung geschimpft bmzw. falsche
Anschuldigungen gemacht haben sollen.

Verfahren eingestellt

17. Jul. 1938 - 7. Okt. 1938
(1c Js So 1743/38)

(2831 a) 5635 BErmittlungsverfahren gegen den Schachtmeister
Adolf Josef SANDL (geb. 19. Mai 1904) aus
Bad Aibling wegen politischer Bemerkungen in
Hauzenberg (Lkr. Wegscheid).

Verfahren wegen Amnestie eingestellt

31. Jul. 1938 - 31. Jan. 1940
(1 Js So 1746/38)
(1 Js So 59/40)

(2832) 4211 Ermittlungsverfahren gegen den Kaufmann Hans
PAPPENBERGER (geb. 14. Spt. 1870) aus Passau,
weil er aus Angst vor einem Krieg mit Ruß-
land seine Waren wegbrachte.

Verfahren eingestellt

26. Spt. 1938 - 21. Okt. 1938
(1c Js So 1753/38)

(2833) 4212 Ermittlungsverfahren gegen den Korbmacher
Oskar MÜLLER (geb. 28. Dez. 1905) aus Nieder-
straubing (Lkr. Erding), weil er erzählte,
der Sender Straßburg habe die Nachricht von
einer bevorstehenden Mobilmachung in Deutsch-
land gebracht.

Verfahren eingestellt

1. Okt. 1938 - 4. Okt. 1938
(1c Js So 1754/38)

(2834) 4415 Ermittlungsverfahren gegen den Studienrat
Gregor HÖNIG (geb. 4. Feb. 1889) aus Forch-
heim, früher BVP-Mitglied, Stahlhelm-, dann
SA-Mitglied, Pg., weil er in Oberammergau
(Lkr. Garmisch-Partenkirchen) kritische poli-
tische Äußerungen machte.

Verfahren eingestellt

16. Jul. 1938 - 18. Jul. 1940
(1b Js So 1756/38)
