(318)

(319)

(320)

(321)

(322)

(323)

7662

7529

7663

7664

7665

7666

Ermittlungsverfähren gegen den Hulfsarbeiter
Joseph LACKERMEIER (geb. 14. Feb. 1896) aus
Landshut, SPD-Mitglied, wegen kritischer Aus-
serungen über die politische Lage.

Anklage vom Staatsanwalt zurückgezogen.‘

14. Okt. 1933 - 15. Dez. 1933
(So E 50/33)

Ermittlungsverfahren gegen den Kanzleisekretär
Georg STREHL (geb. 14. Aug. 1892) aus München
wegen herabsetzender Äußerungen über die Re-
gierung.

Verfahren eingestellt

6. Jul. 1933 - 15. Dez. 1933
(So E 51/33)

Ermittlungsverfahren gegen den Fabrikarbeiter
und KPD-Anhänger Frmirich KELLER (geb. 8. Nov.
1899) aus Neudorf (Lkr. Kempten) wegen Verbrei-
tung des "Kemptner Mosaik".

Verfahren wegen Verjährung eingestellt

24. Nov. 1933 - 27. Nov. 1933
(So E 52/33)

Ermittlungsverfahren gegen die Arztfrau Else
BOUSTEDT (geb. 30. Nov. 1885) aus Pappenheim
(Lkr. Weissenburg) wegen der Behauptung, die

Wahlen vom 13. Nov. 1933 seien ein Schwindel
gewesen.

Eröffnung der Hauptverhandlung abgelehnt

15. Nov. 1933 - 28. Dez. 1933
(So E 54/33)

Ermittlungsverfahren gegen Annemarie HERMANN
(geb. 11. Nov. 1884) aus Tegernsee (Lkr.
Miesbach) wegen einer Äußerung, das Geld für
die Winterhilfe würde vielleicht auch für
Rüstungszwecke verwendet.

Eröffnung der Hauptverhandlung abgelehnt

15. Nov. 1933 - 19. Dez. 1933
(So E 53/33)

Ermittlungsverfahren gggen den Autoreparatur-
Geschäftsinhaber Hans GRAUVOGEL (geb. 27. Jul.
1890) und den Autoreparatur-Geschäftsinhaber
Frxirich Wilhelm GRAUVOGEL (geb. 15. Mrz. 1898),
beide aus München, wegen der Äußerung, Hitler
habe im Braunen Haus eın Zechgelage vorgefunden.
