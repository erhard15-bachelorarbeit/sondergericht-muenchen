(3505)

(3506)

(3507)

(3508)

(3509)

4655

4656

4657

4658

4659

Ermittlungsverfahren gegen die Silber-
fuchszüchterin Silvia LOEPERS (geb. 24.
Jul. 1903) aus Ehrwang (Lkr. Füssen), weil
sie in einem Brief die Maßnahmen der nat.
soz. Regierung gegen die Juden als "Kultur-
schande" bezeichnet hat.

(88 1,2 HG)

Verfahren eingestellt

5. Feb. 1939 - 8. Aug. 1939
(1 Js So 256/39)

Ermittlungsverfahren gegen den Schuhmacher-
meister Franz MAYER (geb. 13. Jun. 1892)
aus Ruhpolding (Lkr. Traunstein), weil er
u.a. gesagt haben soll, daß es Diebstahl
sei, wenn man einem Juden den Führerschein
entzieht.

(HG)

Verfahren eingestellt

30. Jan. 1939 - 24. Okt. 1939
(1 Js So 257/39)

Ermittlungsverfahren gegen den Landwirt
Josef RID (geb. 28. Aug. 1908), den
Kleinwirt Martin BARTENSCHLAGER (geb. 22.
Spt. 1879) aus Rieden (Lkr. Kaufbeuren)
und den Viehhändler Fritz BREY (geb. 1.
Jul. 1905) aus Memmingen, weil sie sich
abfällig über Propagandaminister Goebbels
eäußert haben sollen.
(HG)

Verfahren eingestellt

30. Jan. - 7. Aug. 1939 |
(1 Js So 259/39)

Ermittlungsverfahren gegen den Polizei-
sekretär Emil PETZENDORFER (geb. 19. Dez.
1891), B., aus München, weil er sich
kritisch über führende Persönlichkeiten
fg) Reiches geäußert haben soll.
HG

Verfahren eingestellt
16. Feb. 1939 - 10. Mrz. 1939 (1 Js So 260/3

Ermittlungsverfahren gegen den Brauerei-
beamten Ludwig PFAILER (geb. 26. Jun. 1899)
aus München, weil er u.a. gesagt haben
soll: "Die an der Regierung sind, sind
alle Idioten."

(HG)

Verfahren eingestellt

27. Jan. 1939 - 15. Apr. 1939
(1 Js So 261/39) P
