(5145)

(5146)

(5147)

(5148)

(5149)

5792

5793

5794

5796

53795

Ermittlungsverfahren gegen den Landwirt
Josef SCHICK (geb. 21. Jan 1884) aus
Mörmoosen (Lkr. Mühldorf a. Inn), weil
er verbreitet haben soll, daß Hitler ein
"Bastard" sei.

(HG)

Verfahren eingestellt

14. Nov. 1939 - 19. Apr. 1940
(1 Js So 522/40)

Ermittlungsverfahren gegen den Versiche-
rungsvertreter Hans RAUCH (geb. 29. Jan.
1889) aus München, weil er Hitler einen
"heazlosen Menschen ohne Seele" genannt
haben soll.

(HG)

Verfahren eingestellt

14. Nov. 1939 - 30. Mrz. 1940
(1 Js So 527/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Karl ZETTLER (geb. 16. Jul. 1900)
aus Bobingen (Lkr. Schwabmünchen), wegen
Abhörens ausländischer Rundfunksender.
(Raf.VO)

Verfahren eingestellt

5. Mrz. 1940 - 17. Apr. 1940
(1 Js So 528/40)

Ermittlungsverfahren gegen den Schlosser
Theodor BLÖCHL (geb. 13. Okt. 1903) ohne
festen Wcehnsitz, wegen Betrugs und Ur-
kundenfälschung in Augsburg.

($ 4 VVO)

Verfahren eingestellt

21. Dez. 1939 - 14. Mrz. 1940
(1 Js So 531/40; 1 Js So 267/40)

Ermittlungsverfahren gegen den Dienst-
knecht Wolfgang MAIER (geb. 15. Feb. 1921)
aus Pilsting (Lkr. Landau a.d. Isar), wegen
Diebstahls.

($ 242 StGB; 3 4 WVVO)

Verfahren eingestellt

14. Feb. 1940 - 21. Mai 1940
(1 Js So 434/40)
