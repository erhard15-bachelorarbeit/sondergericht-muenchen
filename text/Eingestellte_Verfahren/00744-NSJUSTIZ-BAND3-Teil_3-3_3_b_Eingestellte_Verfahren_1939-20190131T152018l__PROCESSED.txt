(3550)

(3551)

(3552)

(3553)

(3554)

4700

4701

4702

4703

4704

Ermittlungsverfahren gegen den Kaufmann
Karl WEIGL (geb. 17. Apr. 1912) aus
Würzburg, weil er sich abfällig über
Hitler geäußert und den nat.soz. Antisemi-
Gi) kritisiert haben soll.

HG

Verfahren eingestellt

27. Feb. 1939 - 19. Spt. 1939
(1 Js So 351/39)

Ermittlungsverfahren gegen den Monteur
Nikolaus SCHILLING (geb. 3. Dez. 1903)
aus Landsberg a.L., weil er Gerüchte über
sexuelle Ausschweifungen führender Partei-
mitglieder von Coburg verbreitet haben

soll.
(88 1,2 HG; 38 134b,186 StGB)

Verfahren eingestellt

3. Mrz. 1939 - 15. Aug. 1939
(1 Js So 352/39)

Ermittlungsverfahren gegen den Hilfsarbeiter
Viktor SCHWEIGER (geb. 21. Jul. 1905)

aus Liezen (Österreich), weil er sich

auf Grund der unwahren Angabe, er sei Pe.
und Angehöriger der SA, Bargeldbeträge
erschwindelt haben soll. Schweiger wird
vom SoG Dortmund am 28. Apr. 1939 zu einer
Zuchthausstrafe von drei Jahren und zur
Aberkennung der bürgerlichen Ehrenrechte
für die Dauer von fünf Jahren verurteilt.
($ 3 HG; 8 360 StGB)

Verfahren eingestellt

27. Spt. 1938 - 21. Aug. 1939
(1 Js So 353/39 ; 1 Js So 86,144/39)

Ermittlungsverfahren gegen den Gastwirts-
und Brauereibesitzer Paul ERL (geb. 31.
Mai 1882) aus Schollbach (Lkr. Erding),
weil er gesagt haben soll: "Ich bin ein
Bazi und der Führer ist derselbe Bazi
wie ich."

(HG)

Verfahren eingestellt

24. Feb. 1939 - 2. Aug. 1939
(1 Js So 356/39)

Ermittlungsverfahren gegen den Architek-
ten Georg SCHIESSL (geb. 29. Dez. 1887)
aus Otterskirchen (Lkr. Vilshofen),
weil er sich abfällig über die NSKOV
geäußert haben soll.
