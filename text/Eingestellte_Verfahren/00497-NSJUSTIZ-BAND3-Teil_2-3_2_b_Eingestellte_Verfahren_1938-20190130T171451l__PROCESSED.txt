(2329)

(2330)

(2331)

(2332)

(2333)

3720

3721

3722

3723

3724

Ermittlungsverfahren gegen den Elektri-
zitätswerksbesitzer Dr. Johann KIRCHNER

geb. 16. Jun. 1890) aus Biberwier
(Tirol) wegen angeblicher politischer
Äußerungen und Bezug einer Zeitschrift
von Otto Strasser.

Verfahren eingestellt

28. Dez. 1935 - 25. Apr. 1938
(1b Js So 769/38)

Ermittlungsverfahren gegen die Ver-
treterin Anna Josefa SCHÜTT (geb. 30.
Dez. 1894) aus München wegen angeblicher
Äußerungen über die Feiern zum 9. Nov..

Verfahren eingestellt

19. Jan. 1938 - 17. Apr. 1938
(1c Js So 770/38)

Ermittlungsverfahren gegen den kath.
Pfarrer Josef FIEGL - 20. Aug.
1892) aus Ottmaring (Lkr. Friedberg)
wegen angeblicher Kritik über die
Mobilmachung von 1938.

Verfahren wegen Amnestie eingestellt

5. Apr. 1938 - 2. Spt. 1943
(1a Js So 771/38)

Ermittlungsverfahren gegen den Bauern-
sohn Michl PETER (geb. 20. Spt. 1915)
aus Reitberg (Lkr. Deggendorf) wegen
einer angeblichen Bemerkung über Hitler.

Verfahren wegen Amnestie eingestellt

24. Feb. 1938 - 10. Mai 1938
(1b Js So 772/38)

Ermittlungsverfahren gegen den Schmied
Heinrich SCHMID (geb. 14. Okt. 1905)

aus Fischerdorf (Lkr. Deggendorf) und
den Spengler Franz SÜSS (geb. 15. Nov.
1907) aus Deggendorf, KPD-Mitglied wegen
Bemerkungen über den Anschluß Österreichs
und die Behandlung der Kommunisten im KZ.

Verfahren wegen Amnestie eingestellt

Süss war vom Mrz. 1933 bis Jan. 1934
im KZ Dachau.

20. Mrz. 1938 - 21. Mai 1938

(1e Js So 773/38)
