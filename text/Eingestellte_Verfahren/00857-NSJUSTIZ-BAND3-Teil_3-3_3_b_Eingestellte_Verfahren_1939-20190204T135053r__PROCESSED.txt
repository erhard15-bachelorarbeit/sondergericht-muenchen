(4111)

(4112)

(4113)

(4114)

(4115)

5214

5215

5209

5217

2218

Ermittlungsverfahren gegen den Gemeinde-
diener Ferdinand FREINA (geb. 8. Jul.
1900) aus Alkofen (Lkr. Vilshofen), weil
er sich abfällig über die Wehrmacht ge-
äußert haben soll.

(HG)

Verfahren eingestellt

6. Spt. 1939 - 9. Okt. 1939
(1 Js So 1434/39)

Ermittlungsverfahren gegen den Architekten
Bernhard KAPFHAMMER (geb. 20. Aug. 1894)
aus München, weil er gesagt haben soll:
(HG) der Staat bricht doch zusammen."
(HG

Verfahren eingestellt

23. Aug. 1939 - 7. Nov. 1939
(1 Js So 1435/39)

Ermittlungsverfahren gegen den Bergmann
Xaver KRONFELDNER (geb. 30. Jun. 1876)
aus Hausham (Lkr. Miesbach), weil er sich
de) über die SA geäußert haben soll.
HG

Verfahren wegen Amnestie eingestellt

30. Aug. 1939 - 14. Okt. 1939
(1 Js So 1436/39)

Ermittlungsverfahren gegen den Rittmeister
Guido von LEWINSKI (geb. 3. Mai 1878)

aus München, weil er sich über den NS ge-
äußert und "Verhältnisse wie früher" her-
beigewünscht haben soll.

(HG)

Verfahren wegen Amnestie eingestellt

31. Jul. 1939 - 6. Nov. 1939
(1 Js So 1438/39)

Ermittlungsverfahren gegen den kath.

Pfarrer Johannes LUTZ (geb. 22. Feb. 1880)

aus Erkheim (Lkr. Memmingen), weil er

während einer Predigt die Verfolgung der

Kirchen in Rußland mit der in Deutschland
leichgesetzt haben soll.

(8 2 HG; $ 130a StGB)

Verfahren eingestellt

25. Mai 1939 - 25. Nov. 1939
(1 Js So 1439/39)
