(2269)

(2270)

(2271)

(2272)

(2273)

3660

3661

3662

3663

3664

Ermittlungsverfahren gegen die Friseurs-
ehefrau Elisabeth HOSEMANN (geb. 1.
Nov. 1882) aus Laufen wegen politischer
Äußerungen.

Verfahren wegen Amnestie eingestellt

19. Mrz. 1938 - 10. Mai 1938
(1a Js So 644/38)

Ermittlungsverfahren gegen die Kranken-
pflegerin Hildegard NEUMEYER (geb. 5.
Apr. 1900) aus Bad Reichenhall (Lkr.
Berchtesgaden), weil bei einer Durch-
suchung anläßlich eines Grenzüber-
tritts bei ihr ein Zettel mit denm Vers
"Leute ohne Sorgen sagen guten Morgen,
Leute vom alten Schlag sagen guten Tag,
Heil Hitler sagen die Profitler !" ge-
funden wurde.

Verfahren eingestellt

13. Mrz. 1938 - 7. Apr. 1938
(1b Js So 645/38)

Ermittlungsverfahren gegen die Hilfs-
arbeiterehefrau Therese SCHMIDBAUER
(geb. 23. Nov. 1896) aus Arnstorf (Lkr.
Eggenfelden) wegen angeblicher Be-
leidigung Hitlers.

Verfahren wegen Amnestie eingestellt

23. Feb. 1938 - 4. Apr. 1938
(1c Js So 648/38)

Ermittlungsverfahren gegen die Hilfs-
arbeitersehefrau Maria STEPHAN (geb.
2. Mai 1909) aus München, Mitglied
der Roten Hilfe, wegen unbefugten
Besitzes eines Parteiabzeichens.

Verfahren wegen Amnestie eingestellt

24. Mrz. 1938 - 12. Mai 1938
(1d Js So 650/38)

Ermittlungsverfahren gegen den Ingenieur
Diomys HUBER (geb. 12. Mrz. 1903) aus
Berlin wegen abwertender Außerungen
über den Deutschen Gruß.

Verfahren eingestellt

17. Jan. 1938 - 11. Apr. 1938
(1a Js So 658/38)
