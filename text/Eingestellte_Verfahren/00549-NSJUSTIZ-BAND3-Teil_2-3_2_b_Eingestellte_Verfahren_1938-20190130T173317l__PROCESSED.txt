(2577)

(2578)

(2579)

(2580)

(2581)

3965

3966

3967

3968

3969

Ermittlungsverfahren gegen den
Angestellten Martin BIS” (geb.
27. Mrz. 1909) aus Oberstdorf
(Lkr. Sonthofen) wegen der angeb-
lichen unberechtigten Behauptung,
NSDAP- und SS-Mitglied zu sein.

Verfahren wegen Amnestie eingestellt

18. Jan. 1938 - 8. Jun. 1938
(1e Js So 1112/38)

Ermittlungsverfahren gegen den Austra
bauern Josef EDER (geb. 12. Mrz. 18/70
aus Strass (Lkr. Laufen) wegen angeb-
licher Äußerungen über Hitler.

Verfahren wegen Amnestie eingestellt

22. Mai 1938 - 10. Jun. 1938
(1a Js So 1113/38)

Ermittlungsverfahren gegen den
Kaufmann Franz Xaver HEILMEIER

(geb. 17. Feb. 1887) aus Dachau
wegen angeblicher Verächtlichmachung
der SA-Uniform.

Verfahren eingestellt wegen Amnestie

9. Mai 1938 - 10. Jun. 1938
(1b Js So 1115/38)

Ermittlungsverfahren gegen den kath.
Pfarrer Albert STEIGENBERGER (geb.
14. Apr. 1888) aus Großberg-

hofen (Lkr. Dachau) wegen des angeb-
Lich geäußerten Satzes: "Mussolini is
für die Arbeiter, Hitler aber nicht."

Verfahren wegen Amnestie eingestellt

(1d Js So 1117/38)

Ermittlungsverfahren gegen den
Austragsbauern Xaver GLASS (geb.

28. Okt. 1864), BVP-Mitglied,

dessen Ehefrau Johanna GLASS (geb.
25. Nov. 1868), deren Sohn Michael
GLASS (geb. 16. Aug. 1908), alle aus
Genderkingen (Lkr. Donauwörth) wegen
angeblicher Äußerungen gegen den NS.

Verfahren wegen Amnestie eingestellt

9. Mai 1938 - 10. Jun. 1938
(1b Js So 1118/38)
