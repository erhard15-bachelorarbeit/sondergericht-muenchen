(2715)

(2716)

(2717)

(2718)

(2719)

4103

4104

4105

44.08

4106

Ermittlungsverfahren gegen den Elektromeister
Franz NIEDERMEIER (geb. 18. Feb, 1898) aus
Rottach (Lkr. Miesbach), weil er Hitler be-
leidigt und außerdem gesagt haben soll, daß
in Deutschland nicht mehr die Wahrheit ge-
sagt wird.

Verfahren eingestellt wegen Amnestie

22. Feb; 1938 - 24. Aug. 1938
(1c Js So 1440/38)

Ermittlungsverfahren gegen die Land-
wirtsfrau Barbara FISCHHABER (geb. 24.
Jul. 1894) aus Faistenhaar (Lkr. München),
weil sie Hitler einen "Hanswurst" genannt
haben soll.

Verfahren eingestellt

20. Jul. 1938 - 16. Aug. 1938
(1a Js So 1444/38)

Ermittlungsverfahren gegen den Koch Hermann
MÖHL (geb. 31. Jul. 1893) aus München, weil
er wiederholt Moskauer Sender gehört und
gesagt haben soll, daß das, was deutsche
Zeitungen berichten würden, lauter "Schwindel"
sei.

Verfahren wegen Amnestie eingestellt

21. Mai 1938 - 17. Aug. 1938
(1c Js So 1452/38)

Ermittlungsverfahren gegen den kath.
Pfarrer und Dekan Andreas RAMPP (geb.
29. Nov. 1888) aus Warmisried (Lkr.
Mindelheim), weil er in Predigten
kritische politische Bemerkungen gemacht
haben soll.

Verfahren eingestellt

R. war von 12. Aug. 1938 bis 23. Dez. 1938
in Schutzhaft.

8. Aug. 1938 - 15. Mai 1939
(1c Js So 1454/38)

Ermittlungsverfahren gegen den Bauaifseher
Martin BÖSWALD (geb. 19. Apr. 1899) aus
Rögling (Lkr. Donauwörth), weil er den
Straßburger Sender gehört und verbreitet
haben soll, daß in Spanien Francos Anhänger
verlieren würden und schließlich gesagt
habe, daß in Deutschland der Kommunismus
siegen werde.

Verfahren wegen Amnestie eingestellt

17. Feb. 1938 - 22. Aug. 1938
(1a Js So 1456/38) 545
