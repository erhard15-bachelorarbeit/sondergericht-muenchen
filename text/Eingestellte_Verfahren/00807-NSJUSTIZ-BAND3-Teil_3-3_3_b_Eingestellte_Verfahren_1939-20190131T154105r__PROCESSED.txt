(2607)

(3864)

(3865)

(3866)

(3867)

(3868)

4990

4993

4994

4995

4996

4997

Ermittlungsverfahren gegen den Bauern
Lorenz MAIER (geb. 4. Jul. 1903) aus Oed
(Lkr. Miesbach), früher Pg., wegen kri-
(HG) Äußerungen über führende Nat.Sozz.

Verfahren eingestellt

8. Mai 1939 - 25. Aug. 1939
(1 Js So 1037/39)

Ermittlungsverfahren gegen den Ingenieur
Paul SÄNGER (geb. 21. Feb. 1892) aus
München, wegen der ÄAußerung, Hitler sei
ein "hergelaufener Kerl".

(HG)

Verfahren eingestellt

5. Jul. 1939 - 11. Aug. 1939
(1 Js So 1038/39)

Ermittlungsverfahren gegen den Holzhänd-
ler Georg BISCHL (geb. 12. Spt. 1909) aus
Habach (Lkr. Weilheim), Pg. und SS-Mit-
glied, wegen einer Bemerkung über Frei-
willige der Wehrmacht.

($ 134a StGB)

Verfahren eingestellt

15. Mai 1939 - 31. Jul. 1939
(1 Js So 1040/39)

Ermittlungsverfahren gegen den kath. Pfarrer
Johann HUBER (geb. 2. Apr. 1892) aus Lan-
dau, weil er die Entfernung des Pax-Zei-
chens vom kath. Jugendheim in Landau als
"Bolschewistenarbeit" bezeichnete.

(8 2 HG; $8 134b StGB)

Verfahren eingestellt

21. Mai 1939 - 23. Nov. 1939
(1 Js So 1041/39)

Ermittlungsverfahren gegen den Kaminfeger
Karl KRISTEN (geb. 17. Apr. 1894), wegen
Bemerkungen in Kaltenbach (CSSR), über
die deutsche Wehrmacht.

($ 2 HG; $ 134a StGB) *

Ermittlungsverfahren gegen den Werkzeug-
macher Jakob SCHMITTNER (geb. 14. Jul.
1920) aus München, wegen Verächtlich-
machung einer Hakenkreuzfahne auf der
Hubertushütte (Gem. Fischbachau, Lkr.
Miesbach).

(HG)

Verfahren eingestellt

12. Jun. 1939 - 26. Okt. 1939
(1 Js So 1045/39)

«Verfahren eingestellt
29. Jun. 1939 - 7. Aug. 1939
(1 Js So 1043/39)

769
