(5210)

(5211)

(5212)

(5213)

(5214)

5855

5856

5857

2858

2859

Ermittlungsverfahren gegen die berufslose
Margarethe FLORACK (geb. 31. Jan. 1889)
aus Hemhof (Lkr. Rosenheim) wegen Abhörens
ausländischer Rundfunksender und Verbrei-
tung ihrer Nachrichten.

(Rdf.VO)

Verfahren eingestellt

15. Jan. 1940 - 7. Jun. 1940
(1 Js So 723/40)

Ermittlungsverfahren gegen die Zimmer-
mädchen Maria SCHMIDT (geb. 2. Feb. 1915),
Maria HÖRMANN (geb. 14. Jul. 1921) und
Theresia HANSBAUER (geb. 1. Okt. 1920)
aus Jettingen (Lkr. Günzburg) wegen Ab-
hörens ausländischer Rundfunksender.
(Rdf.VO)

Verfahren eingestellt

8. Apr. 1940 - 26. Apr. 1940
(1 Js So 726/40)

Ermittlungsverfahren gegen den Landwirt
Martin EHMANN (geb. 22. Feb. 1907) aus
Schabenberg (Lkr. Pfaffenhofen), weil er
sich abfällig über die Regierung geäußert
fa) er einen "Bazi" genannt haben soll,
HG

Verfahren eingestellt

4. Mrz. 1940 - 29. Mai 1940
(1 Js So 728/40)

Ermittlungsverfahren gegen den Erbhof-
bauern Martin MANGSTL (geb. 10. Mai 1881)
und seinen gleichnamigen Sohn (geb. 15.
Dez. 1919) aus Oberseebach (Lkr. Erding)
wegen Abhörens ausländischer Rundfunk-
sender.

(Rdf.VO)

Verfahren eingestellt

8. Feb. 1940 - 23. Apr. 1940
(1 Js So 729/40)

Ermittlungsverfahren gegen den Landwirt
Lorenz FOTTNER (geb. 20. Spt. 1890) aus
Schabenberg (Lkr. Pfaffenhofen), weile
über die Partei und die Regierung geschimp
haben soll.

(HG)

Verfahren eingestellt

17. Mrz. 1940 - 30. Apr. 1940
(1 Js So 730/40)
