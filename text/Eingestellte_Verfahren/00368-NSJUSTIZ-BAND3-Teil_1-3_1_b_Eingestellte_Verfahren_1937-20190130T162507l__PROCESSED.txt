(1752)

(1753)

(1754)

(1755)

(1756)

3237

3316

3317

3318

3319 .

Ermittiungsverfakren gegen den Vertreter
Alois DICHTL (geb. 20. Jun. 1903), öster-
reichischer Staatsangehöriger aus Ruhpol-
ding (Lkr. Traunstein), weil er das Ge-
rücht verbreitet haben soll, auf Mussolini
sei anläßlich seines Besuches in München
ein Attentat verübt worde. Weiter soll

er sich über die Ermordung deutscher
flieger in Spanien geäußert haben.

Verfahren eingestellt

23. Nov. 1937 - 27. Dez. 1937
(1 a Js - So 1504/37)

Ermittlungsverfahren gegen den Post-
assistenten Johann SOHN (geb. 29. Nov.
1878) aus Augsburg, die Gastwirtsfrau
Therese RÖGER (geb. 24. Aug. 1885) aus
Dillingen/Donau und den Oberpostschaffner
Gottlieb DIETRICH (geb. 27. Mai 1886)
aus Augsburg wegen "unwahrer und gröblich
entstellter" Behauptungen über die NSDAP
in Bachhagel(Lkr. Dillingen).

Verfahren eingestellt

8. Dez. 1937 - 13. Jan. 1938
(1 ec Js - So 1506/37)

Ermittlungsverfahren gegen den Hilfsarbeiter
Ludwig MOOSMÜLLER (geb. 16. Nov. 1911)

aus Straubing, weil er gesagt haben soll,
"Heil Moskau, der Hitler ist eine Drecksau".

Verfahren eingestellt

13. Nov. 1937 - 17. Jan. 1938
(1 b Js - So 1508/37)

FErmittlurgsverfahren gegen den Spengler
Georg GRUBER (geb. 15. Dez. 1902) aus
München, weil er Propagandaminister
Goebbels einen "Marchenerzaähler" genannt
haben soll.

Verfahren eingestellt

3. Okt. 1937 - 16. Feb. 1938
(1 a Js - So 1512/37)

Ermittlungsverfahren gegen den Installateur
Andreas Adam MEIER (geb. 13. Jul. 1902) aus
München, weil er mit einer Jüdin liiert sein
und "in aufhetzerischer Weise gegen die
Regierung arbeiten" soll.

Verfahren eingestellt

15. Okt. 1937 - 10. Jan. 1938
(1 d Js - So 1514/37)
