(2188)

(2189)

(2190)

(2191)

(2192)

3579

3580

3581

3382

3583

Ermittlungsverfahren gegen den Hilfs-
arbeiter Josef NIEDERMEIER (geb. 21. Dez.
1898) aus Haslach (Lkr. Grafenau) wegen
einer angeblichen ÄAußerung über eine
‚tler-Rede.

Verfahren wegen Amnestie eingestellt

10. Feb. 1938 - 25. Mai 1938
(1b Js So 462/38)

Ermittlungsverfahren gegen den ev. Pfarrer
Dr. Paul SCHATTENMANN (geb. 6. Feb. 1892)
aus München, weil er während eines Vor-
trags mit dem Thema "Leicende Kirche"

von 150 Pfarrern berichtet habe, die in
Ostpraßen wegen einer Kollekte für die
Bekennende Kirche verhaftet wurden.

Die Abschrift eines Beschwerdebriefes
ostpreußischer Bauern an das General-
kommando ist im Akt enthalten.

Verfahren wegen Amnestie eingestellt

27. Jan. 1938 - 17. Mai 1938
(1c Js So 463/38)

Ermittlungsverfahren gegen das Zimmer-
mädchen Hermine ZEINTL (geb. 22. Nov.
1919) aus München, weil sie den Kommu-
nistengruß "Heil Moskau" verwendet haben

soll.

Verfahren eingestellt

21. Feb. 1938 - 18. Mrz. 1938
(1c Js So 464/38)

Ermittlungsverfahren gegen den Kraft-
wagenführer und Vertreter Friedrich
WAGNER (geb. 17. Mai 1903) aus München,
weil er sich über Reden Hitler's lustig
gemacht und sich abfällig über die Frau
von Göring geäußert haben soll.

Verfahren eingestellt

25. Feb. 1938 - 22. Apr. 1938
(1c Js So 465/38)

Ermittlungsverfahren gegen den Schreiner
Leo RIFEDER (geb. 19 . Apr. 1907) aus Mün-
chen, italienischer Staatsangehöriger,
weil er sich in Briefen an seinen Kolping-
bruder in "gehässiger und staatsfeind-
licher Weise" über Propagandaminister
Goebbels geäußert haben sowie demselben
eine Karikatur von Goebbels zugesandt haben
soll, die er aus einer holländischen Zei-
tung ausgeschnitten hat.
