(5240)

(5241)

(5242)

(5243)

(5244)

5888

5895

2890

5891

5892

Ermittlungsverfahren gegen den Straßen-
bahnschaffner Alois MÜHLBAUER (geb. 17.
Mai 1873) aus München, weil er Hitler
einen "Bazi" genannt und die Rundfunk-
Nachrichten als "Lügen" bezeichnet haben
soll.

(HG)

Verfahren eingestellt

2. Apr. 1940 - 23. Jul. 1940
(1 Js So 827/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Ottwin SCHRASS (geb. 21. Feb. 1904)
und den Schlosser Josef IBERSEDER (geb.
10. Okt. 1922) aus Mühldorf a. Inn wegen
Diebstahls.

($ 242 StGB; $8 4 VVO)

Verfähren eingestellt

5. Jan. 1940 - 9. Mai 1940
(1 Js So 832/40)

Ermittlungsverfahren gegen die Geschäfts-
inhaberin Anna MICHL (geb. 28. Apr. 1903)
aus München, weil sie bedauert haben soll,
daß das Attentat im Bürgerbräukeller miß-
lungen sei.

(HG

Verfahren eingestellt

1. Dez. 1939 - 29. Jul. 1940
(1 Js So 835/40)

Ermittlungsverfahren gegen den Rentner
Benno AUERBECK (geb. 19. Apr. 1877) aus
Regen wegen abfälliger Äußerungen über den
deutschen Gruß.

(HG)

Verfahren eingestellt

18. Nov. 1939 - 6. Jun. 1940
(1 Js So 838/40)

Ermittlungsverfahren gegen den Angestellten
Lorenz UNTERLUGAUER (geb. 13. Nov. 1906)
und sein Frau Elfriede (0o.D.) aus München
wegen politischer Außerungen.

(HG)

Verfahren eingestellt

In Akt wird eine KZ-Haft im Jahr 1938 von
Karl KAMMERER aus München erwähnt.

8. Dez. 1939 - 15. Mai 1940
(1 Js So 847/40; 1 Js So 318/40)
