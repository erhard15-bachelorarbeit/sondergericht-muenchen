(2294)

(2295)

(2296)

(2297)

(2298)

3685

3686

3687

3688

3689

Ermittlungsverfahren gegen den Land-
wirt Martin STIEGLAUER (geb. 31. Mrz.
1897), den Arbeiter Gallus STÖCKL (geb.
16. Dez. 1909), beide aus Oberndorf
(Lkr. Donauwörth) wegen Erzählens poli-
tischer Witze.

Verfahren eingestellt

12. Det. 1937 - 7. Apr. 1938
(1e Js So 695a-b/38)

Ermittlungsverfahren gegen den kath.
Pfarrer August STEGMANN (geb. 22.

Aug. 1887) aus Schmiechen (Lkr. Fried-
berg), weil er angeblich als politi-
sche Demonstration für eine Beerdigung
vom Schulunterricht keine Ministranten
freistellen wollte.

Verfahren wegen Amnestie eingestellt

25. Feb. 1938 - 16. Mai 1938
(1c Js So 696/38)

Ermittlungsverfahren gegen den Bauern
Wilhelm ALBRECHT (geb. 3. Apr. 1892)
aus Emetzheim (Lkr. Weißenburg) wegen
angeblicher Äußerungen über die Mobil-
machung 1938.

Verfahren eingestellt

14. Mrz. 1938 - 6. Apr. 1938
(1a Js So 699/38)

Ermittlungsverfahren gegen den Brau-
meister Alois HOHENTHANNER (geb. 22.
Apr. 1877) aus Pillham (Lkr. Griesbach),
NSDAP-Mitglied, wegen Kritik am Regime.

Verfahren eingestellt

11. Jan. 1938 - 6. Mai 1938
(1a Js So 700/38)

Ermittlungsverfahren gegen den Kraft-
wagenführer Amandus MECKEL (geb. 24.
Okt. 1910) aus Würzburg, früher NSDAP-
und SA-Mitglied, den Automechaniker
Alois WEGNER (geb. 24. Aug. 1912) aus
Würzburg, NSDAP-Mitglied, wegen poli-
tischer Bemerkungen in Ergoldsbach (Lkr.
Mallersdorf).

Verfahren wegen Amnestie eingestellt

15. Mrz. 1938 - 20. Jul. 1938
(1b Js So 702 a-b/38)
