(2813)

(2814)

(2815)

(2816)

(2817)

(2818)

31975)

4194

4195

4196

4197

4198

Ermittlungsverfahren gegen den Feinmechaniker
Wolfgang Karl ENDRES (geb. 20. Jan. 1902) aus
Gauting (Lkr. Starnberg), weil er über das
Regime geschimpft haben soll.

Verfahren eingestellt

11. Spt. 1938 - 1. okt. 1938
(1a Js So 1698/38)

Ermittlungsverfahren gegen den Gütler und
Reichsbahnoberschaffner Alois GÖPPEL (geb.
24. Mai 1864) aus Westerndorf (Lkr. Rosen-
heim), weil er abwertend über Hitler ge-
sprochen haben soll.

Verfahren eingestellt

12. Spt. 1938 - 7. Okt. 1938
(1b Js So 1699/38)

Ermittlungsverfahren gegen den Liftführer +
Paul MEYER (geb. 17.Spt. 1887) aus München,
weil er über das Regime geschimpft haben
soll. | '

/
Verfahren eingestellt

30. Jul. 1938 - 1. Okt. 1938
(1c Js So 1701/38)

Ermittlungsverfahren gegen den Gastwirt Franz
FELSL (geb. 5. Okt. 1881) und den Tapezierer-
meister Arthur Viktor MAKOSCH (geb. 20. Apr.
1886), beide aus München, weil sie kritische
politische Bemerkungen gemacht haben sollen.

Verfahren eingestellt

30. Mai 1938 - 1. Okt. 1938
(1a Js So 1702/38)

Ermittlungsverfahren gegen die Rentnerin Anna
HÄFNER (geb. 22. Feb. 1870) aus München, weil
sie Hitler beleidigt haben soll.

Verfahren eingestellt

28. Jul. 1938 - 10. Okt. 1938
(1b Js So 1705/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Paul OTTERSBACH (geb. 28. Mrz. 1907) aus Mün-
chen, weil er sich als SA-Sturmbannführer
ausgegeben haben soll.

4

Verfahren eingestellt

8. Spt. 1938 - 1. okt. 1938

(1e Js So 1706/38)
