(4003)

(4004)

(4005)

(4006)

(4007)

5116

5117

53119

5120

5121

Ermittlungsverfahren gegen den Elektromon-
teur Peter HOHENADL (geb. 8. Aug. 1904)
aus München, weil er mit "Heil Moskau"
egrüßt haben soll.
HG)

Verfahren eingestellt

26. Aug. 1939 - 18. Spt. 1939
(1 Js So 1289/39)

Ermittlungen gegen den Geschäftsführer Au-
gust SCHÄFER (geb. 9. Nov. 1896) aus Ro-
senheim, weil er u.a. verbreitet haben
soll, daß Hitler in Holland für sich 1 1/2
Mill. RM deponiert habe und die Frau des
Generalfeldmarschalls Göring in 1. Ehe
Ta) Juden verheiratet gewesen sei.
HG

Siehe auch StAnw. 5118 (1 Js So 1450/39)

30. Aug. 1939 - 20. Spt. 1939
(1 Js So 1290/39)

Ermittlungsverfahren gegen den Heizer Georg
WAGNER (geb. 24. Mai 1887) aus München, wei
er das Sammeln für das WHW als "Bettlerei"
bezeichnet und die Initiatoren eine "Räu-
berbande" genannt haben soll.

(HG)

Verfahren wegen Amnestie eingestellt

31. Jul. 1939 - 17. Okt. 1939
(1 Js So 1292/39)

Ermittlungsverfahren gegen den Landarbeiter
Karl WÜHRER (geb. 20. Mrz. 1918) aus Höhn-
hart (Österreich), weil er unberechtigt
(ig) zeichen getragen haben soll.

HG

Verfahren wegen Amnestie eingestellt

24. Jun. 1939 - 4. Okt. 1939
(1 Js So 1293/39)

Ermittlungsverfahren gegen den Musiker Hans
GRUBER (geb. 28. Jul. 1877) aus Kempten,
wegen einer angeblichen politischen Be-
merkung.

(HG)

Verfahren eingestellt

8. Jul. 1939 - 22. Spt. 1939
(1 Js So 1294/39)
