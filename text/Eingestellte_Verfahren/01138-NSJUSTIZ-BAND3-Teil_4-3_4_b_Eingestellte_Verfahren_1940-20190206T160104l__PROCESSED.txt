(5402)

(5403)

(5404)

646

6047

6

fehl

13

ND

rmitilungsverfahren gegen den Vertreter
Franz NEHER (geb. 29. Okt. 1878) aus Kemp-
ten, weil er sich abfällig über das Dritte
Reich geäußert und außerdem gesagt haben
soll, daß Deutschland den Krieg nicht ge-
winnen werde.

(S$ 2 HG; Räf.VOo)

Verfahren eingestellt

3. Jul. 1940 - 27. Spt. 1940
(1 Js So 1543/40)

Ermittilungsverfahren gegen den Fabrikar-
beiter Rupert ZEISELMEIER (geb. 26. Jul.
1873) aus Außergefild (CSSR), Weil er über
Hitler geschimpft und die Besetzung Öster-
reichs und der Tschechoslowakei kritisiert
haben soli.

(HG)

Verfahren eingestellt

31. Jul. 1940 - 17. Dez. 1940
(1 Js: So 1544/40)

Ermittlungsverfahren gegen den Hilfsarbei-
ter Vinzent KOJ (geb. 4. Apr. 1883) aus
Naklo (Polen), weil er in Ettringen (Lkr.
Mıindesiheim) die offizielle Kriegsbericht-
erstattung als erlogen bezeichnet haben
soll.

($5 1,2 Ha)
Verfanren eingestelit

Im Akt Urteil des SoG-Salzburg vom 16. Aug
1940 gegen Koj zu 1 Jahr 6 Monaten Gefäng-
nis. ;

26. Mai 1940 - 25. Spt. 1940

(1 Js So» 1547/40)

Ermittlungsverfahren gegen den Erbhof-
kauern Georg HASLBECK (geb. 23. Apr. 1902)
aus Atzmannsberg (Lkr. Dingolfing) wegen
hußerngen uber das Dritte Reich.

CHG)

Verfahren eingestellt

28. Mai 1940 - 4. Feb. 1941
(1 Js So 1549/40)

Ermittlungsverfahren gegen die Pensions-
inhaberin Anna ENGELBREIT (geb. 13. Aug.
1870) aus Bühl a. Alpsee (Lkr. Sonthofen);
weil sie die nat.soz. Mütterehrung als
"Krampf" und Parteimitglieder als "Schla-
aaa bezeichnet haben soll.

HG

Vorfahren esingestelit
