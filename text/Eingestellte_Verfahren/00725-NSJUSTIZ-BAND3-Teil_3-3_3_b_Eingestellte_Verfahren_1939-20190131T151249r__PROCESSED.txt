(3455)

(3456)

(3457)

(3458)

(3459)

4605

4606

4607

4608

4609

Ermittlungsverfahren gegen den Glasbläser
Johann TEICHTMANN (geb. 24. Jul. 1913) aus
München, weil er ein dem Parteiabzeichen
ähnliches Abzeichen getragen hat.

($ 5 HG)

Verfahren eingestellt

29. Jun. 1938 - 3. Aug. 1939
(1 Js So 135/39)

Ermittlungsverfahren gegen den Maurer Georg
KOWIESER (geb. 31. Mrz. 1867) aus München,
weil in seiner Küche ein Bild von Kurt Eis-
ner hängt und er gesagt haben soll, daß die
Weltrevolution bald kommen werde.

($ 83 StGB)

Verfahren eingestellt

16. Nov. 1938 - 29. Apr. 1939
(1 Js So 136/39)

Ermittlungsverfahren gegen den österreichischen
Staatsangehörigen und Tierarzt Heinrich von
KURZ (geb. 20. Jun. 1895) und seine Frau
Frieda (geb. 15. Jun. 1906) aus Grassau

(Lkr. Traunstein), weil sie sich abfällig
(HG) Seiß-Inquart geäußert haben sollen.

HG

Verfahren eingestellt

9. Mai 1938 - 21. Jan. 1939
(1 Js So 139/39)

Ermittlungsverfahren gegen den Spenglermeister
Karl BURKHARD (geb. 25. Spt. 1894) aus Gross-
aitingen (Lkr. Schwabmünchen), und den
Schaltwärter Karl HINTERSTÖBßER (geb. 2. Mrz.
1886) aus Schwabmünchen, weil sie verbreitet
haben sollen, daß in Tölz ein von SS-Männern
bedienter Geheimsender aufgefunden wurde und
daraufhin 70 dieser SS-Männer erschossen wor-
den seien.
(HG)

Verfahren eingestellt

24. Jan. - 8. Mrz. 1939
(1 Js So 141/39)

Ermittlungsverfahren gegen den Kraftwagen-
führer Mathias HAIDER (geb. 18. Jan. 1896)
aus München, weil er verbreitet haben soll,
daß am Westwall bei den Befestigungsbauten
lebende Menschen einbetoniert und mehrere in
Österreich reisende deutsche Kraftfahrer von
Österreichern aus Haß gegen die Deutschen
erstochen wurden.

(88 1,2 HG)

Verfahren eingestellt

2. Jan. 1939 - 5. Mai 1939
(1 Js So 149/39) 687
