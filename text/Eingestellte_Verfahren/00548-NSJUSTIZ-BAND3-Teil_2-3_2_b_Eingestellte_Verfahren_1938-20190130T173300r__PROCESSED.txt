(2572)

(2573)

(2574)

(2575)

(2576)

3961

3960

3962

39653

3964

Ermittlungsverfahren gegen den
Hilfsarbeiter Ernst RALL (geb.
19. Jun. 1 aus Weißenhorn
(Lkr. Neu-Ulm) wegen Schimpfens
über seine ehemaligen militäri-
schen Vorgesetzten.

Verfahren wegen Amnestie eingestellt

12. Mai 1938 - 7. Jun. 1938
(1c Js So 1107/38)

Ermittlungsverfahren gegen die
Gärtnersfrau Therese SCHULZ (geb.

12. Dez. 1899) aus Tutzing (Lkr.
Starnberg) wegen angeblichen Schimpfens
über das Regime.

Verfahren wegen ÄAmnestie eingestellt

10. Mai 1938 - 3. Jun. 1938
(14 Js So 1108/38)

Ermittlungsverfahren gegen den
Rentner Anton STOCKER (geb. 6. Jun.
1882) aus München wegen angeblicher
Äußerungen über das Regime.

Verfahren wegen Amnestie eingestellt

30. Mrz. 1938 - 13. Jun. 1938
(1d Js So 1109/38)

Ermittlungsverfahren gegen den
Dreher Ludwig WOHLSCHLAGER (geb.
24. Aug. 1909), früher SA-Mitglied,
den Dreher Rudolf AUGSBURGER (geb.
3. Mai 1908), den Eisendreher
Friedrich OPPACHER (geb. 10. Okt.
1914), den Dreher Franz-KXaver
SAULÄCHER (geb. 29. Nov. 1901),
KPD-Mitglied, alle aus München,
wegen angeblicher politischer
Äußerungen.

Verfahren wegen Amnestie eingestellt

10. Mai 1938 - 9. Jun, 1938
(1e Js So 1110/38)

Ermittlungsverfahren gegen den
Landwirt Jakob STANGLMEIER (geb.
17. Jul. 1886) aus Mitterscheyern
(Lkr. Pfaffenhofen) wegen angeb-
licher Außerungen über eine Haken-
kreuzfahne.

Verfahren wegen Amnestie eingestellt

25. Apr. 1938 - 20. Jun. 1938
(1d Js So 1111/38)
