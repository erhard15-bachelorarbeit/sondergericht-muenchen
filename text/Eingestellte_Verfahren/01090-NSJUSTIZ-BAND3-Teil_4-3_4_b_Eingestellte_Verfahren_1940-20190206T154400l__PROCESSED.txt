(5160)

(5161)

(5162)

(5163)

(5164)

5805

5806

5808

5809

5810

Ermittlungsverfahren gegen die Rentnerin
Elsa HÖLZLE (geb. 6. Spt. 1882) aus Mün-
chen, weil sie u.a. gesagt haben soll:
"... jede monarchistische Regierung wäre
mir lieber wie die heutige."

($ 2 HG; 38 185, 186 StB)

Verfahren eingestellt

20. Jan. 1940 - 21. Aug. 1940
(1 Js So 563/40)

Ermittlungsverfahren gegen den Hilfsarbei-
ter Max MAGER (geb. 16. Feb. 1892) aus
Aubing (Stadt München), wegen beleidigen-
der Außerungen über die Partei.

(8 134b StGB)

Verfahren eingestellt

2. Feb. 1940 - 22. Apr. 1940
(1 Js So 564/40)

Ermittlungsverfahren gegen den Uhrmacher-
meister Joseph OBERMEYER (geb. 4. Apr.
1882) aus Buchloe (Lkr. Kaufbeuren), wegen
Äußerungen über die deutsche Kriegsfüh-
rung.

(HG

Verfahren eingestellt

30. Dez. 1939 - 7. Jun. 1940
(1 Js So 572/40)

Ermittlungsverfahren gegen den Landwirt
und Forsterbeiter Friedrich-Wilhelm
WARTELSTEINER (geb. 9. Jul. 1906) aus
Haid (Lkr. Weilheim) wegen "nächtlichen
Anschlags mit einer Schußwaffe mit even-
tueller Tötungsabsicht".

($ 1 Gew.Verbr.VO)

Verfahren eingestellt

6. Mrz. 1940 - 23. Apr. 1940
(1 Js So 578/40)

Ermittlungsverfahren gegen die Oberstleut-
nantswitwe Margarethe SPILLECKE (geb. 22.
Jul. 1886) aus Fürstenfeldbruck, weil sie
gesagt haben soll, daß man nach dem Krieg
mit Hitler abrechnen werde.

(HG)

Verfahren wegen Amnestie eingestellt

12. Jan. 1940 - 5. Jun. 190
(1 Js So 574/40)
