(2090)

(2091)

(2092)

(2093)

3482

3483

3484

3485

Verfahren eingestellt

W.war vom 16. Jan. 1938 bis 4. Feb.
1938 in Polizeihaft.

16. Jan. 1938 - 4. Jul. 1938
(1d Js So 259/38)

Ermittlungsverfahren gegen den
Ingenieur Renato EYCK (geb. 7.
Spt. 1903) aus München wegen
angeblicher Außerungen über die
politischen Verhältnisse in
Deutschland.

Verfahren eingestellt

30. Dez. 1937 - 6. Mai 1938
(1a Js So 260/38)

Ermittlungsverfahren gegen den kath.
Pfarrvikar Florian JAUMANN (geb.

10. Dez. 1896) aus Eurasburg (Lkr.
Friedberg) wegen angeblicher Äußerungen
über Hitler und gegen die Gemeinschafts-
schule.

Verfahren eingestellt

1. Dez. 1937 - 30. Mai 1938
(1a Js So 261/38)

Ermittlungsverfahren gegen den Korrektor
Josef PFLÜGL (geb. 28. Nov. 1892) aus
München, früher SPD-Mitglied, dann NSBO-
Mitglied, Parteianwärter, wegen unbe-
rechtigten Tragens des Parteiabzeichens.

Verfahren eingestellt

11. Jan. 1938 - 24. Feb. 1938
(1d Js So 263/38)

Ermittlungsverfahren gegen die Ver-
sicherungsangestellte Johanna GOLD-
SCHMIDT (geb. 5. Feb.1912) aus
Ottobrunn (Lkr. München), weil sie
politische Witze erzählt und sich
kritisch über die politischen Ver-
hältnisse in Deutschland geäußert haben
soll.

Verfahren eingestellt

8. Jan. 1938 -—_ 7. Apr. 1938
(1a Js So 266/38)
