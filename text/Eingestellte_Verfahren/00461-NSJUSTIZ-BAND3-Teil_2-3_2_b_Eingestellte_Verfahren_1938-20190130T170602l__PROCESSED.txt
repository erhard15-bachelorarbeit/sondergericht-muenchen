(2155)

(2156)

(2157)

(2158)

(2159)

3546

3547

3548

3549

3550

Ermittlungsverfahren gegen den Kraftwagen-
besitzer Eduard KELLERBAUER (geb. 15.
Okt. 1889) aus Passau wegen angeblicher
Äußerungen über die Gefallenen des 9.
Nov. 1923.

Verfahren eingestellt

18. Nov. 1937 - 21. Mai 1938
(1b Js So 401/38)

Ermittlungsverfahren gegen den Erbhofbauern
und Viehhändler Albert KRAUSNECKER (geb,
28. Jun. 1901) aus Niederleierndorf (Lkr.
Rottenburg), Pg, wegen angeblicher ÄAuße-
rungen über das Regime.

Verfahren wegen Amnestie eingestellt

21. Feb. 1938 - 25. Mai 1938
(1b Js So 402/38)

Ermittlungsverfahren gegen den Elektro-
monteur Martin LUGER (geb. 6. Mai 1902)
aus Pfarrkirchen wegen der Erzählung

an seiner Arbeitsstätte, einer Baustelle
werde nicht mit "Heil Hitler" gegrüßt
und die Arbeiter stünden der Partei
feindlich gegenüber.

Verfahren eingestellt

29. Nov. 1937 - 7. Mrz. 1938
(1 b Js So 404/38)

Ermittlungsverfahren gegen den kath.
Pfarrer Max MARTIN (geb. 17. Aug.
1880) und dessen Haushälterin Sophie
HENGELER (geb. 18. Aug. 1897), beide
aus Buxheim (Lkr. Memmingen), wegen
angeblicher Äußerungen zu kirchen-
politischen Dingen.

Verfahren wegen Amnestie eingestellt

18. Jan. 1938 - 14, Jun. 1938
(1b JS So 405/38)

Ermittlungsverfahren gegen den Händler
Kurt SCHMIDT (geb. 3. Okt. 1897) aus
Berlin, früher Stahlhelm-, dann SA-
Mitglied und Pg, weil er sich noch
nach seinem Ausschluß aus der SA als
Mitglied ausgegeben haben soll.

Verfahren wegen Amnestie eingestellt

14. Aug. 1937 - 24. Mai 1938
(1e Js So 407/38)
