(2069)

(2070)

(2071)

(2072)

(2073)

3462

3463

3464

3465

Ermittlungsverfahren gegen den Ver-
sicherungsdirektor Friedrich HIRSCH-
BERGER (geb. 5. Mrz. 1880) aus Mün-
chen, weil er Hitler einen "Schlawiner"
und die Regierung eine "Schwindelbande"
genannt haben soll.

Verfahren wird eingestellt

30. Okt. 1937 - 23. Feb. 1938
(1b Js So 208/38)

Ermittlungsverfahren gegen den Ver-
treter Friedrich WEINFURTNER (geb.

16. Apr. 1905) aus München, SA-MitßBlied,
wegen unberechtigten Tragens des Blut-
ordens.

Verfahren wegen Amnestie eingestellt

25. Jan. 1938 - 9. Mai 1938
(1d Js So 212/38)

Ermittlungsverfahren gegen den Maler
Max KRAFFT (geb. 10. Mrz. 1880) aus
München wegen unberechtigten Tragens
des goldenen Parteiabzeichens.

Verfahren wegen Amnestie eingestellt

26. Jan. 1938 - 11. Mai 1938
(1d Js So 214/38)

Ermittlungsverfahren gegen den Kom-
ponisten Peter Paul KREUDER (geb.
18. Aug. 1908) aus München, wegen
angeblicher Außerungen über die
Pflichtarbeit für höhere Beamte.

Verfahren eingestellt

27.8.1937 - 17.2.1938
(1b Js So 215/38)

Ermittlungsverfahren gegen den
Schümacher Rudolf BUBLIK (geb.

28. Jun. 1882) aus Lindau wegen
angeblicher Äußerungen über den
Röhm-Putsch, die Wahlen, den
Aufbau der Wehrmacht, die Rhein-
land-Besetzung und die Wirtschafts-
politik.

Verfahren wegen Amnestie eingestellt

14. Dez. 1937 - 4. Mai 1938
(1a Js So 222/38)
