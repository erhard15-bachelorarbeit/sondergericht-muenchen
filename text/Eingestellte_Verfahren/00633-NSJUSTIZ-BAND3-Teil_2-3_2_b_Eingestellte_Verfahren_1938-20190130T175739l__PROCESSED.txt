(3048)

(3049)

(3050)

(3051)

(3052)

4369

4473

4370

4474

4475

M. wird in schutzhaft genommen.

4. Nov. 1938 - 24. Mrz. 1939
(1c Js So 2158/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Johann NATTENMILLER (geb. 5. Apr. 1902) aus
Lindau, wegen angebl. kritischer politischer
Bemerkungen.

Verfahren eingestellt

3. Nov. 1938 - 29. Dez. 1938
(1c Js So 2159/38)

Ermittlungsverfahren gegen den Gastwirt und
Bauern Josef BRENNINGER (geb. 28. Dez. 1900),
Pg. und SA-Mitglied, und dessen Mutter Eli-
sabeth BRENNINGER (geb. 7. Dez. 1869), beide
aus Buch (Lkr. Ebersberg), weil sie die Re-
gierung kritisiert haben sollen.

Verfahren eingestellt

13. Nov. 1938 - 6. Mrz. 1939
(1a Js So 2162/38)

Ermittlungsverfahren gegen den Schmied Jo-
hann GRUBER (geb. 21. Dez. 1885) aus Wallers-
dorf (Lkr. Landau) und den Hafner August
WANNINGER (geb. 23. Okt. 1891) aus Zehol-
fingermoos (Lkr. Landau), weil sie Hitler
kritisiert haben sollen.

Verfahren eingestelit

23. Spt. 1938 - 9. Dez. 1938
(1b Js So 2164/38)

Ermittlungsverfahren gegen Katharina HOF-
STETTER (geb. 25. Spt. 1894) und Katharina
FLIESER (geb. 23. Mai u beide aus Gei-
senhausen (Lkr. Vilsbiburg), weil sie Greuel-
geschichten über das KZ Dachau erzahlt ha-
ben sollen.

Verfahren eingestellt

8. Okt. 1938 - 16. Mrz. 1939
(1b Js So 2165/38)

Ermittlungsverfahren gegen den Viehhändler
Andreas ISMAYER (geb. 11. Nov. 1901) aus Rott
(Lkr. Fegenfelden), weil er in Eggenfelden
über das Regime schimpfte.

Verfahren eingestellt

8. Okt. 1938 - 24. Feb. 1939
(1b Js So 2166/38)
