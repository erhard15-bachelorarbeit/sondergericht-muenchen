(3653)

(3654)

(3655)

(3656)

(3657)

4802

4803

4804

4805

4806

Ermittlungsverfahren gegen den jüdischen
Rechtsanwalt Moritz DREYFUSS (geb. 9. Mai
1878) aus München, weil er u.a. verbreitet
haben soll, daß die Frau von Göring in
erster Ehe mit einem Juden verheiratet
ewesen sei.
HG)

Verfahren eingestellt

12. Apr. 1939 - 10. Okt. 1939
(1 Js So 578/39)

Ermittlungsverfahren gegen die Speditions-
arbeitersehefrau Therese SENDTNER (geb.

28. Mrz. 1896) aus Aubing (München), weil
(Ha) Hitlers Mut bezweifelt haben soll.
HG

Verfahren eingestellt

29. Mrz. 1939 - 29. Jun. 1939
(1 Js So 582/39)

Prozeß gegen den katholischen Expositus
Ludwig HEIMERL (geb. 27. Apr. 1899) aus
Hüttenkofen (Lkr. Dingolfing), weil er
u.a. in einer Predigt den Papst als
"erößten Bekämpfer des Bolschewismus" be-
zichnet und gemeint haben soll, daß der-
jenige, der den Papst bekämpfe, ein Bol-
schewist sei.

(HG)

Verfahren eingestellt

20. Mrz. 1939 - 22. Nov. 1939
(1 Js So 584/39)

Ermittlungsverfahren gegen den Maurerpo-
lier Johann SEESTALLER eb. 24. Jun. 1894)
aus Gmund (Lkr. Miesbach), weil er sich
abfällig über die Wehrmacht geäußert ha-
ben soll.

($ 2 HG; 3 134b StGB)

Verfahren eingestellt

28. Feb. 1939 - 10. Mai 1939
(1 Js So 592/39)

Ermittlungsverfahren gegen den techn.
Angestellten Eduard HAIL (geb. 30. Spt.
1890) aus Memmingen, Pg., früher SA-Mit-
glied, wegen abwertender Äußerungen über
() Parteimitglieder.

HG

Verfahren wegen Amnestie eingestellt

5. Apr. 1939 - 11. Mai 1939
(1 Js So 593/39; 1 Js So 2136/38)
