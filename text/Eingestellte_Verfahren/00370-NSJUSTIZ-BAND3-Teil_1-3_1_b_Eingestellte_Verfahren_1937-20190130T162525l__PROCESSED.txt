(1773)

(1774)

(1775)

(1776)

3334

33352

3336

33537

hann WEICHSELGARTNER (geb. 27. Dez. 1904),
alle aus Landshut, und den jüdischen Kauf-
mann Albert DANN (geb. 5. Jan. 1868) aus
Augsburg, wegen kritischer Äußerungen ge-
gegen den NS, Hitler und die wirtschaftliche
Lage.

Verfahren eingestellt wegen Amnestie

9. Okt. 1937 - 25. Mai 1938
(1 c Js - So 1550a-f/37)

Ermittlungsvertahren gegen den Ziegeleiar-
beiter Friedrich WEISS (geb. 15. Spt. 1907)
aus Puttenhausen (Lkr. Mainburg), weil er
eine Rede Hitlers als "Schmarrn" bezeichnet
haben soll.

Verfahren eingestellt

30. Nov. 1937 - 13. Jan. 1938
(1 c Js - So 1551/37)

Ermittlungsverfahren gegen den Portier
Georg MITZ (geb. 12. Mai 1882) aus
Hohenschäftlarn (Lkr. Wolfratshausen),
weil er kritisiert haben soll, daß
Allerheiligen als Feiertag abgeschafft und
dafür der 9. November als Feiertag ein-
geführt worden ist.

Verfahren eingestellt

5. Nov. 1937 - 30. Mrz. 1938
(1 b Js - So 1552/37)

Ermittlungsverfahren gegen den Gütler
Peter KAMMERLOHER (geb. 31. Mai 1892)
aus Hohenbachern (Lkr. Freising) wegen
angeblicher beleidigender Äußerungen
über Goring.

Verfahren eingestellt

30. Nov. 1937 - 11. Feb. 1938
(1 b Js - So 1556/37)

Ermittlungsverfahren gegen den Bauern

und Bürgermeister Xaver MAHLIL (geb. 12.

Jan 1881) aus Haunsried (Lkr. Aichach) wegen
der angeblichen Außerung, Hitler sei
ein"Lump".

Verfaähren eingestellt

21. Dez. 1937 - 21. Apr. 1938
(1 b Js - So 1560/37)
