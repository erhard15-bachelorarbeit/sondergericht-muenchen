(5502) 5906

(5503) 5923

(5504) 5927

(5505) 5967

(5505a) 6091

Verfahren eingestellt

3. Apr. 1940 - 16. Jun. 1940
(1 Js So 755/40; K 322/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Johann HÖRBECK (geb. 7. Spt. 1698)
aus München, weil er über Hitler, Göring
und Goebbels geschimpft haben soll.

(&$ 2 Ha)

Verfahren eingestellt

17. Apr. 1940 - 27. Jul. 1940
(1 Js So 966/40; K 329/40)

Ermittlungsverfahren gegen den Schneider
Franz Xaver HEILMAIER (geb. 16. Spt. 1907)
aus Felizenzell (Lkr. Vilsbiburg), weil
er u.a. den Krieg und die Partei als "na-
tionalen Schwindel" bezeichnet haben soll.
($ 2 HG; $ 134a StGB)

Verfahren eingestellt

29. Apr. 1940 - 16. Jul. 1940
(1 Js So 1021/40; K 340/40)

Ermittlungsverfahren gegen die Hilfsar-

beiterin Maria NIETMANN (geb. 7. Apr. 1914)
aus Hammerau (Lkr. Laufen) wegen abfälli-
er Äußerungen über Hitler.

(8 2 HG)

Verfahren eingestellt

15. Mai 1940 - 18. Okt. 1940
(1 Js So 1043/40; K 354/40)

Prozeß gegen den Kraftfahrer Eduard FICHTL
(geb. 153. Dez. 1912) aus München wegen be-
(1) Außerungen über Hitler.

HG

Verfahren eingestellt

23. Mai 1940 - 7. Aug. 1940
(1 Js So 1205/40; K 383/40)

Ermittlungsverfahren gegen den Schauspieler
Viktor LIPP (geb. 28. Jul. 1898) aus Mün-
chen wegen Vorbereitung zum Hochverrat,
weil er gesagt haben soll, wenn es einen
Krieg gäbe, sei er der erste, der auf Hitler
schieße.

(8 93 StGB)

Verfahren eingestellt

13. Nov. 1939 - 1. Feb. 1940
(1 Js 26/40)
