(3411)

(3412)

(3413)

(3414)

(3415)

(3416)

4561

4562

45653

456

4565

4566

Ermittlungsverfahren gegen den Erbhofbauern
Josef KOCH (geb. 3. Mrz. 1884) aus Nordheim
(Lkr. Donauwörth), wegen angeblicher Äußerungen
über die Minister Darre und Göring.

($ 2 Ha)

Verfahren eingestellt

25. Spt. 1938 - 13. Apr. 1939
(1 Js So 44/39)

Ermittlungsverfahren gegen den Erbhofbauern
Mathias KÖLBECK (geb. 21. Feb. 1878) aus
Gotzdorf (Lkr. Landshut), weil er über die
Steuern schimpfte und rief "Heil Moskau".
($ 83 StGB)

Verfahren eingestellt

14. Okt. 1938 - 4. Feb. 1939
(1 Js So 45/39)

Ermittlungsverfahren gegen den Lokomotiv-
heizer Magnus ANHOFER (geb. 28. Okt. 1875)
aus Neu-Ulm, wegen Schimpfens über Hiler und
das Regime.

(HG)

Verfahren eingestellt

15. Dez. 1938 - 5. Mai 1939
(1 Js So 48/39)

Ermittlungsverfahren gegen den Ingenieur
Wilhelm BUDICH (geb. 15: Feb. 1892) aus
Neubiberg (Lkr. München), Mitglied der NSDAF,
(6) politischer Außerungen.

HG

Verfahren eingestellt

14. Okt. 1938 - 17. Jan. 1939
(1 Js So 53/39)

Ermittlungsverfahren gen den Hilfsarbeiter
Johann OSWALD (geb. 26. Mai 1906) aus
Eichet (Lkr. Rosenheim), wegen unberechtigten
(ic ens des Parteizeichens.

HG

Verfahren eingestellt

10. Dez. 1938 - 17. Jan. 1939
(1 Js So 56/39)

Ermittlungsverfahren gegen den Hilfsarbeiter
Franz MOSER (geb. 17. Jan. 1908) aus Brandst-

(Lkr. Traunstein), wegen Schimpfens über Cd.
Deutsche Arbeitsfront.

(HG)
Verfahren eingestellt

14. Dez. 1938 - 23. Mai 1939
(1 Js So 62/39)
