(5095)

(5096)

(5097)

(5098)

(5099)

5737

5738

5739

5740

5741

Ermittlungsverfahren gegen den Maurer
Michael LACHERMAIER (geb. 2. Mai 1894)
aus München, weil er die Vermutung ge-
äußert haben soll, daß die Regierung ge-
(6) und Hitler "weggeräumt" werde.

G

Verfahren eingestelit

Lachermaier wurde am 8. Jul. 1933 wegen
politischer Äußerungen bzw. groben Unfugs
zu 1 Woche Haft verurteilt.

9. Nov. 1939 - 29. Mrz. 1939
(1 Js So 280/40)

Ermittlungsverfahren gegen den Musiker
Narry ZIMMERMANN (geb. 19. Aug. 1907)
aus Königsberg (UdSSR) wegen Erzahlens
politischer Witze.

(HG)

Verfahren eingestellt

15. Dez. 1939 - 16. Feb. 1940
(1 Js So 281/40)

Ermittlungsverfahren gegen den Schreiner
Reinhard DIETZE (geb. 3. Mrz. 1872) aus
München wegen abfälliger Äußerungen über
die Regierung.

(HG)

Verfahren eingestellt

6. Feb. 1940 - 6. Jun. 1940
(1 Js So 285/40)

Ermittlungsverfahren gegen den Schuhmacher
Georg MEISTER (geb. 27. Mai 1889) aus
München, weil er bedauert haben soll, daß
das Attentat im Bürgerbräukeller fehlge-
schlagen ist.

(HG)

Verfahren eingestellt

17. Nov. 1939 - 18. Mrz. 1940

(1 Js So 294/40)

Ermittlungsverfahren gegen den Kaplan
Eirch BENEKE (geb. 16. Jan. 1913) aus Penz-
berg (Lkr. Weilheim), weil er einen Ad-
ventsbrief mit "staatsfeindlichem Inhalt"
an Jugendliche verteilt hat.

(8 2 HG; $ 130a StGB)

Verfahren eingestellt
29. Jan. 1940 - 13. Jun. 1940
