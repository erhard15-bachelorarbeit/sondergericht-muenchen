(2254)

(2255)

(2256)

(2257)

(2258)

3645

3646

3647

3648

3649

Verfahren wegen Amnestie eingestellt

Anlage: 1 Abschrift des Fastenhirten-
briefes von 1938 von Kardinal
Faulhaber

8. Mrz. 1938 - 21. Nov. 1938
(1a Js So 609/38)

Ermittlungsverfahren gegen den Modellschreiner
Heinrich KUFNER (geb. 31. Okt. 1874) aus
Freising wegen Außerungen über die Mobil-
machung beim Anschluß Osterreichs.

Verfahren eingestellt

15. Mrz. 1938 - 11. Apr. 1938
(1b Js So 613/38)

Ermittlungsverfahren gegen den Vertreter
Johann Baptist REIS (geb. 31. Mrz. 1877)
aus München wegen Äußerungen über die
Sammlungen und das Regime.

Verfahren eingestellt

2. Feb. 1938 - 10. Mai 1938
(1e Js So 615/38)

Ermittlungsverfahren gegen den Kamin-
kehrer Otto STRIEDL (geb. 23. Nov.
183837) aus München wegen politischer
Bemerkungen.

Verfahren eingestellt

18. Feb. 1938 - 14. Spt. 1938
(1e Js So 616/38)

Ermittlungsverfahren gegen den kfm.
Angestellten Richard HOLZINGER (geb.
13. Aug. 1895) aus München wegen der
angeblichen Behauptung, es herrsche
Holzknappheit.

Verfahren eingestellt

28. Feb. 1938 - 4. Apr. 1938
(1a Js So 618/38)

Ermittlungsverfahren gegen den kath.
Kaplan Georg LIESCH aus Lechbruck
(Lkr. Füssen) wegen politischer Äuße-
rungen in einer Predigt.

Verfahren eingestellt

16. Mrz. 1938 - 30. Mrz. 1938
(1b Js So 619/38)
