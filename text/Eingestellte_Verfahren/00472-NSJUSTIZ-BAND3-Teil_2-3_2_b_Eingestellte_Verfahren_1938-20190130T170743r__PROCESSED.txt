(2206)

(2207)

(2208)

(2209)

(2210)

3597

3599

3600

3601

3602

wirt Heinrich DÖBLER (geb. 3. Mrz.
1891) aus Kaibachhof (Lkr. Donauwörth)
wegen angeblicher Verbreitung des Ge-
rüchts, daß der Reichskriegsminister
seine Zusage zur Teilnahme an einer
Tagung von der Zusicherung nicht über
Kirchenfragen zu reden, abhängig ge-
macht hat. Andernfalls werde er mit de
gesamten Wehrmacht aus der Kirche aus-
treten.

Ermittlungsverfahren (as den Land-

Verfahren eingestellt

25. Dez. 1937 - 17. Mrz. 1938
(1a Js So 496/38)

Ermittlungsverfahren gegen den Land-
wirt Georg STRASSER (geb. 19. Feb.
1898) aus Biburg (Lkr. Augsburg)
wegen der angeblichen Außerung, daß
Hitler nach Auflösung der kath.
Kongregationen und Abschaffung kath.
Blätter "kaputt geht, wie eine Ratten-
maus."

Verfahren eingestellt

13. Feb. 1938 - 3. Mai 1938
(1c Js So 500/38)

Ermittlungsverfahren gegen den Buch-
binder Franz BARTHEL (geb. 9. Feb.
1911) aus München, weil er u.a. ge-
sagt haben soll: "Die Regierung sowie
alles drum und dran sind lauter
Schmarotzer."

Verfahren wegen Amnestie eingestellt

10. Feb. 1938 - 16. Mai 1938
(1a Js So 501/38)

Ermittlungsverfahren gegen den Gütler
Max THOMA (geb. 1. Okt. 1863) aus
Hutthurm (Lkr. Passau) wegen angeb-
licher abfälliger Außerungen über
nat.soz. Fahnen.

Verfahren eingestellt

15. Feb. 1938 - 15. Mrz. 1938
(1c Js So 503/38)

Ermittlungsverfahren gegen den Aus-
fahrer Thomas GIMPL (geb. 21. Spt.
1901) und die Eisenbahnarbeiters-
witwe Therese RUMMEL (geb. 14. Spt.
1883) aus München wegen Verbreitung
