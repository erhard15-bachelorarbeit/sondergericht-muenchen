(4168)

(4169)

(4170)

(4171)

(4172)

5266

5469

5267

5268

5269

(1 Js So 1525/39)

Prozeß gegen den Bauern Ignaz KREIT (geb.
4. Jan. 1908) aus Pforzen (Lkr. Kaufbeuren),
Bayernwacht-Mitglied, wegen der Außerung:
"Dem Hitler gehört der Kopf runter, daß
(na) Größenwahn vergeht."

HG

Verfahren wegen Amnestie eingestellt

4. Spt. 1939 - 24. Nov. 1939
(1 Js So 1527/39)

Ermittlungsverfahren gegen den Ingenieur
Heinrich WIESER (geb. 22. Apr. 1888) aus
München, wegen kritischer Äußerungen über
die Gestapo.

(HG; 8 42 StGB)

Verfahren eingestellt

1. Feb. 1939 - 12. Feb. 1940
(1 Js So 1528/39)

Prozeß gegen den Käser Alfred STAUFFER
(geb. 11. Spt. 1877) aus Sachsenried

(Lkr. Kempten), Schweizer, wegen politischer
Äußerungen.

(HG)

Verfahren wegen Amnestie eingestellt

4. Spt. 1939 - 9. Nov. 1939
(1 Js So 1530/39)

Ermittlungsverfahren gegen den Darlehens-
kassenrechner Ludwig WIEST (geb. 26. Nov.
1884) aus Sulzberg (Lkr. Kempten), Pg.,
wegen politischer Außerungen.

(& 2 HG)

Verfahren wegen Amnestie eingestellt

3. Spt. 1939 - 30. Okt. 1939
(1 Js So 1531/39)

Ermittlungsverfahren gegen die Haustochter
Theresia WIEST (geb. 14. Dez. 1914) aus
Sulzberg (Lkr. Kempten), wegen politischer
Äußerungen.

(& 2 HG

Verfahren wegen Amnestie eingestellt

5. Spt. 1939 - 30. Okt. 1939
(1 Js So 1532/39)
