(3695)

(3696)

(3697)

(3698)

(3699)

4843

4844

4845

4911

4846

Ermittlungsverfahren gegen den Bauern
Dyonis BURGETSMEIER (geb. 15. Nov. 1898)
aus Fünfstetten (Lkr. Donauwörth), Pg.,
() einer Äußerung über Hitler.

HG

Verfahren eingestellt

1. Mai 1939 - 3. Jul. 1939
(1 Js So 699/39)

Ermittlungsverfahren gegen den Käsereige-

hilfen Josef STROBL (geb. 29. Jan. 1912)
aus Erlis (Lkr. Memmingen), wegen Äußerung

(a) der Sudetenkrise über das Regime.
HG

Verfahren eingestellt

3. Apr. 1939 - 29. Jul. 1939
(1 Js So 700/39)

Ermittlungsverfahren gegen den Kies- und
Sandlieferanten Franz Xaver ERL (geb. 5.
Jun. 1889) aus Vilshofen, wegen abfälliger
Äußerungen über Hitlers Geburtstag, an dem
er keinen Lohn an seine Arbeiter zahlen
wollte.

(HG)

Verfahren eingestellt

4. Mai 1939 - 11. Aug. 1939
(1 Js So 702/39)

Ermittlungsverfahren gegen die Gräfin
Maria Hedwig Juliane Huberta la ROSEE
(geb. 30. Jul. 1893) aus München, BVP-
Mitglied, wegen Veranstaltung eines un-
angemeldeten Einkehrtages des "Frauen-
hilfswerkes für Priesterberufe".

(VO v. 28. Feb. 1933; Versammlungsverbot)

Verfahren eingestellt

22. Nov. 1938 - 31. Aug. 1939
(1 Js So 703,883/39)

Ermittlungsverfahren gegen den Hilfsmon-
teur Oskar GANSTERER (geb. 19. Jul. 1918)
aus Haunersdorf (Lkr. Vilsbiburg), wegen
a) Verhaltens.

HG

Verfahren eingestellt

1. Mai 1939 - 6. Okt. 1939
(1 Js So 707/39)
