(3391)

(3392)

(3393)

(3394)

(3395)

4541

4542

4543

4544

4545

Ermittlungsverfahren gegen die Schneiderin
Maria KIRCHENBAUER (geb. 5. Spt. 1900) und
Frau EBNER (ohne nähere Angaben), beide aus
(HG) wegen politischer Außerungen.

HG

Verfahren eingestellt

(1b Js So 4 und 24/39)

Ermittlungsverfahren gegen den kath. Pater
Georg BARTEL OSB (geb. 5. Mai 1891) aus
Schäftlarn (Lkr. Wolfratshausen) wegen
politischer Außerungen.

(HG)

Verfahren eingestellt

4. Mai 1937 - 4. Mai 1939
(1a Js So 8/39)

Ermittlungsverfahren gegen den Hausierer
Konrad PREIS (geb. 16. Jan. 1911) und dessen
Ehefrau Rosa PREIS (geb. 17. Jan. 1915),
beide aus Pilstingermoos (Lkr. Eggenfelden)
wegen Äußerungen über die Judenpolitik, die
Regierung und Sammlungen in Hofstetten (Lkr.
Eggenfelden)

(HG)

Verfahren eingestellt

7. Dez. 1938 - 2. Mai 1939
(1c Js So 9/39)

Ermittlungsverfahren gegen den kath. Kapldn
Bruno KÜBLER (geb. 18. Feb. 1896) aus Kottern
Gde. St. Mang (Lkr. Kempten), weil er in
einer Versammlung der kath. Arbeiter- und
Arbeiterinnenvereine den Bischof Ketteler
zitierte, der gesagt habe, wo Christus nicht
sei, sei der Tod.

($ 2 HG; $ 130a StGB)

Verfahren eingestellt

7. Dez. 1938 - 12. Mai 1939
(1b Js So 10/39)

Ermittlungsverfahren gegen den Rentner
Theodor RINGGER (geb. 19. Apr. 1869) aus
München, wegen Kritik an der Judenpolitik urd
der Regierung.

(HG)

Verfahren eingestellt

14. Dez. 1938 - 19. Apr. 1939
(1c Js So 15/39)
