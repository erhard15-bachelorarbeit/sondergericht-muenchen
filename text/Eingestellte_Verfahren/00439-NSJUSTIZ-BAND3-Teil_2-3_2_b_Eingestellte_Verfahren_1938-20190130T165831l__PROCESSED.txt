(2047)

(2048)

(2049)

(2050)

3437

3438

3439

3440

deutscher Brotwaren beklagt haben
soll.

Verfahren eingestellt

3. Jan. 1938 - 15. Feb. 1938
(1c Js So 141/38)

BErmittlungsverfahren gegen den
Mechaniker Richard OSTLER aus
München wegen der angeblichen
Äußerung über die Ausstellung
"Die entartete Kunst", daß er
nicht die Kunst selbst, sondern
nur den Namen dieser Ausstellung
für "entartet" halte.

Verfahren eingestellt

11. Jan. 1938 - 2. Feb. 1938
(1b Js So 140/38)

Ermittlungsverfahren gegen den Polizei-
hauptwachtmeister Nikolaus ZEIDLER
(geb. 15. Spt. 1889) aus München,
weil er SS-Männer als "Lausbuben"
bezeichnet und die Überzeugung
geäußert haben soll, daß das der-
zeitigc Regime sich nicht halten
werde.

Verfahren eingestellt

30. Jul. 1937 - 22. Jan. 1938
(1c Js So 144/38)

Ermittlungsverfahren gegen die Gast-
wirtsfrau Josefa ARTMANN (geb. 7.
Mrz. 1898) aus München wegen angeb-
licher Äußerungen über die DAF.

Verfahren eingestellt

8. Jan. 1938 - 28. Mrz. 1938
(1a Js So 145/38)

Ermittlungsyverfahren gegen den Arzt
Dr. Josef DÖRSCHING (geb. 19. Mai
1890) aus München, weil er sich
unberechtigt als Parteimitglied aus-
gegeben hat.

Verfahren eingestellt

15. Dez. 1937 - 4. Jun. 1938
(1d Js So 146/38)
