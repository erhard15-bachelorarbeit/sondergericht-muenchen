(2617)

(2618)

(2619)

(2620)

4005

4.006

4007

4008

Apr. 1938) und die Schrift von Alfred
Rosenberg "Der Arier".

Verfahren eingestellt

M. wird in Polizeihaft genommen.
Anlagen: alle angegebenen Schriften

8. Mai 1938 - 1. Jul. 1938
(1c Js So 1170/38)

Ermittlungsverfahren gegen den Vertreter
Walter CLASSEN (geb. 4. Nov. 1888) aus
München, weil er "sich gemeinste Ver-
leumdungen der Person des Führers erlaubt"
und andere aufgefordert haben soll, mit ihr
den Moskauer Sender zu hören.

Verfahren wegen Amnestie eingestellt

2. Apr. 1938 - 28. Jun. 1938
(1a Js So 1177/38)

Ermittlungsverfahren gegen den Hilfsar-
beiter Eduard EPPLE (geb. 20. Nov. 1911)
aus München wegen der angeblichen Außerung,
daß es bei der Auszählung der Stimmen bei
der Reichstagswahl vom 10. Apr. nicht mit
rechten Dingen zugegangen sei.

Verfahren eingestellt
E. wird in Folizeihaft genommen.

10. Jun. 1938 - 13. Aug. 1938
(1a Js So 1178/38)

Ermittlungsverfahren gegen den Bauern
Andreas DAUM (geb. 13. Spt. 1883) aus
Gerolfing (Lkr. Ingolstadt) und den
Bierfahrer Josef NISSL (geb. 25. Nov.
1891) aus Ingolstadt wegen angeblicher
Äußerungen in einer Gastwirtschaft über
die Partei, die SA und die 55.

Verfahren wegen Amnestie eingestellt

30. Apr. 1938 - 29. Jun. 1938
(1a Js So 1181/a-b/38)

Ermittlungsverfahren gegen den Hilfsar-
beiter Otto GRÖTZINGER (geb. 21. Mai
1901) aus Rosenheim wegen angeblicher
Beschimpfung Hitlers.

Verfahren wegen Amnestie eingestellt

24. Mai 1938 - 30. Jun. 1938
(1b Js So 1183/38)
