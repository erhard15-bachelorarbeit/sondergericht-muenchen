(61531)

(6182)

(6183)

(6134)

(6185)

(A

6243

6244

6245

6246

MLELLLNESVerfahren gegen die Studentin
Elisabeth KASULAKOW (geb. 5. Mrz. 1917),
weil sie im Frauenzuchthaus in Aichach auf
einer ALbildung von drei Soldaten in der
Zeitschrift "Der Leuchtturm" mit der Strick-
nadel deren Augen durchstoßen hat.

(HG; 8 154 StGB)

Verfahren eingestellt
(7 Mai 1941 - 14. Jun. 1941
7

i
Js So 345/41)
Ermittiungsverfahren gegen die Hilfsangestellte
beim OKW Olga von COSSEL (geb. 22. Jun.
1875) aus Munchen wegen Äußerungen über den
Krieg.
(HG)

Verfahren eingestellt

11. Dez. 1940 - 27. Jul. 1941
(1 Js So 347/41)

Ermittlungsverfahren gegen den Postfach-
arbeiter Karl GÖLLER (geb. 1. Jan 11908)
aus Munchen, weil er Mitglieder der NS-
(ng) beleidigt haben soll.

HG

Verfähren eingestellt

12. Mrz. 1941 - 20. Jun. 1941
(1 Js So 3483/41)

Ermittlungsverfahren gegen den Arbeitsmann
beim RAD Karl WENZEL (geb. 3. Okt. 1920)
aus Lechfeld (Lkr. Schwabmünchen), weil er
die Auffassung geäußert hat, daß Italien
militärisch die Vereinigten Staaten unter-
stützen werde.

(HG, dd 154 StGB)

Verfahren eingestellt

12. Aug. 1940 - 9. Jun. 1941
(1 Js Su 352/41)

Ermittlungsverfahren gegen den Arbeiter
Frieinicn LUGER (geb. 20. Feb. 1890) aus
Obersalzberg (Lkr. Berchtesgaden) wegen ab-
fäilıiger Bemerkungen über den Deutschen

(HG)

Verfahren eingestellt

15. Apr. 1941 - 14. Jul. 1941
(1 Js Sc: 362/41)
