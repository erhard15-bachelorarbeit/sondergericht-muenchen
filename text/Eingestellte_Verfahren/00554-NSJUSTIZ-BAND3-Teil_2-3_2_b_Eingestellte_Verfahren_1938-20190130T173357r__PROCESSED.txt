(2605)

(2606)

(2607)

(2608)

(2609)

(2610)

3983

3984

3985

3986

3987

3988

Ermittlungsverfahren gegen den Bäcker
Andreas RETTINGER (geb. 23. Jul. 1865),
tschechoslowakischer Staatsbürger, wegen
Schimpfens über politische Dinge.

Verfahren eingestellt

20. Mai 1938 - 22. Aug. 1938
(1c Js So 1154/38)

Ermittlungsverfahren gegen den Huf- und
Wagenschmied Eduard Anton DEMLING (geb.
19. Nov. 1902) wegen Schimpfens über
den Anschluß Österreichs.

Verfahren wegen Amnestie eingestellt

16. Mai 1938 - 13. Aug. 1938
(1a Js So 1155/38)

Ermittlungsverfahren gegen den Studien-
assessor Max WERMINGHAUS (geb. 12. Jan.
1901), Halbjude, aus München, wegen
angeblicher Äußerungen über den Anschluß
Osterreichs.

Verfahren wegen Amnestie eingestellt

6. Apr. 1938 - 21. Jun. 1938
(1d Js So 1156/38)

Ermittlungsverfahren gegen den Schreiner
Friedrich PETER (geb. 24. Jan. 1903) aus
Erisried (Lkr. Mindelheim) wegen Äuße-
rungen über die Wahl vom 10. Apr. 1938.

Verfahren wegen Amnestie eingestellt

10. Mai 1938 - 5. Jul. 1938
(1c Js So 1157/38)

Ermittlungsverfahren gegen den kfm. Ange-
stellten Ötto DAFNER (geb. 17. Okt. 1893),
NSDAP- und SA-Angehöriger, und dessen
Ehefrau Therese DAFNER (geb. 20. Mrz. 1893),
beide aus München, wegen angeblicher politi-
scher Bemerkungen im In- und Ausland.

Verfahren wegen Amnestie eingestellt

28. Jun. 1937 - 26. Jul. 1938
(1a Js So 1159/38)

Ermittlungsverfahren gegen den Gast- und
Landwirt Georg LEITHE (geb. 10. Feb. 1872)
und dessen Ehefrau Josefa LEITHE (geb. 3.
Apr. 1879), beide aus Hangnach (Lkr. Lindau)
wegen angeblicher Äußerungen über den An-
schluß Österreichs.

Verfahren wegen AÄAmnestie eingestellt

24. Mai 1938 - 20. Jun. 1938
(1b Js So 1160/38) 523
