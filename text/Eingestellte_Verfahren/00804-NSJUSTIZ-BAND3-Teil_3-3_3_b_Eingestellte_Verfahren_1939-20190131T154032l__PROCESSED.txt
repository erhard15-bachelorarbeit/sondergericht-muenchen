(3848)

(3849)

(3850)

(3851)

(3852)

4976

4977

4978

4979

4980

Ermittlungsverfahren gegen den Erbhof-
bauern Johann SCHWEIGHART (geb. 21. Nov.
1881) aus Fahls (Lkr. Kempten), wegen der
(Hy "Hitler ist ein Lump."

HG

Verfahren eingestellt

13. Jun. 1939 - 12. Okt. 1939
(1 Js So 1017/39)

Ermittlungsverfahren gegen den Bautech-
niker Max Moritz HOECHSTER (geb. 13. Apr
1889) aus München, Halbjude, wegen poli-
tischer Außerungen in der Sowjetunion.
(HG; $ 83 StGB)

Verfahren wegen Amnestie eingestellt

3. Jan. 1938 - 9. Aug. 1939
(1 Js So 1018/39)

Ermittlungsverfahren gegen den Rechtsan-
walt Karl ALTENEDER (geb. 18. Spt. 1901)
aus Arnstorf (Lkr. Eggenfelden), Pg.,
früher SA-Mitglied, wegen unbefugten Be-
(1) von Parteiuniformteilen.

HG

Verfahren eingestellt

20. Jun. 1939 - 30. Aug. 1939
(1 Js So 1019/39)

Ermittlungsverfahren pegen die Bäuerin
Elisabeth BERNDLSTETTER (geb. 19. Nov.
1866) aus Hörzing (Lkr. Laufen), wegen
politischer Äußerungen in Briefen nach
Brasilien.

(HG)

Verfahren eingestellt

2. Jul. 1939 - 28. Spt. 1939
(1 Js So 1020/39)

Ermittlungsverfahren gegen Therese HILI
BRAND (geb. 27. Okt. 1883) und den Ge-
schäftsführer Adolf DOLL (geb. 29. Jul.
1902), beide aus München, wegen der Be-
hauptung, gute Bezidhungen zu führenden
Nat.Sozz. zu haben.

(HG)

Verfahren eingestellt

6. Jan. 1939 - 3. Aug. 1939
(1 Js So 1021/39)
