(2869)

(2870)

(2871)

(2872)

(2873)

(2874)

4246

4247

4248

3951

4250

4249

Ermittlungsverfahren gegen den Tischler und
Bau-Hilfsarbeiter Ferdinand WEINZEDL (geb.

6. Feb. 1892) aus Agatharied (Lkr. Miesbach),
weil er die Regierung kritisiert haben soll.

Verfahren eingestellt

23. Spt. 1938 - 22. Okt. 1938
(1d Js So 1830/38)

Ermittlungsverfahren gegen Barbara EHEGARTNER
(geb. 16. Spt. 1897) aus Karlskron (Lkr.Neu-
burg), weil sie über das Regime schimpfte.

Verfahren eingestellt

4. Okt. 1938 + 28. Nov. 1938
(1a Js So 1833/38)

Ermittlungsverfahren gegen den Kaufmann Hel-
mut HABERMEYER (geb. 30. Mrz. 1912) aus Neu-
burg, weil er kritische politische Außerungen
machte.

Verfahren eingestellt

3. Okt. 1938 - 17. Okt. 1938
(1b Js So 1834/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Adolf GREIL (geb. 12. Feb. 1900) aus Bischofs-
wiesen (Lkr. Berchtesgaden), weil er kritische
politische Außerungen gemacht haben soll.

Verfahren eingestellt

53. Okt. 1938 - 30. Jan. 1939
(1b Js So 1835/38)

Ermittlungsverfahren gegen den Gärtnergehil-
fen Bernhard HACKER aus Ingolstadt, weil er
unberechtigt die SS-Uniform getragen haben
soll.

Verfahren eingestellt

2. Jun. 1938 - 3. Jan. 1939
(1e Js So 1839/38)
früher 1e Js So 1166/38)

Ermittlungsverfahren gegen den Gartenbautech-
niker Fritz, BRENDEL (geb. 2. Okt. 1914) aus
München, Pg., weil er kritische politische
Äußerungen gemacht haben soll.

Verfahren wegen Amnestie eingestellt

3. Apr. 1938 - 25. Okt. 1938
(1a Js So 1842/38)
