(2991)

(2992)

(2993)

(2994)

(2995)

(2996)

4342

4343

4344

4445

4446

4345

Verfahren eingestellt

5. Okt. 1938 - 11. Mrz. 1939
(1a Js So 2060/38)

Ermittlungsverfahren gegen den Versicherungs-
inspektor Georg ZALLER (geb. 18. Nov. 1896
aus München, weil er in Hopferau (Lkr. Füssen)
kritische Bemerkungen zur Sudetenkrise ge-
macht haben soll.

Verfahren eingestellt

25. Spt. 19383 - 25. Nov. 1938
(1d Js So 2061/38)

Ermittlungsverfahren gegen den Maurer Karl
LEDER (geb. 12. Mai 1873) aus Augsburg, weil
er gesagt haben soll, Hitler sei ein"FSpina-
terer".

Verfahren wegen Amnestie eingestellt

15. Nov. 1938 - 7. Dez. 1938
(1b Js So 2063/38)

Ermittlungsverfahren gegen die Zimmermanns-
frau Lucia KONRAD (geb. 14. Apr. 1894) aus
Donauwörth, weil sie sich kritisch zur Su-
detenkrise geäußert haben soll.

Verfahren eingestellt

30. Okt. 1938 - 20. Dez. 1938
(1b Js So 2064/38)

Ermittlungsverfahren gegen den Vorarbeiter
Franz MÜHLBAUER (geb. 26. Feb. 1908) aus
Aich (Lkr. Freising), weil er sich kritisch
über das Regime geäußert haben soll.

Verfahren eingestellt

6. Okt. 1938 - 18. Feb. 1939
(1c Js So 2065/38)

Ermittlungsverfahren gegen den Händler Fritz
REITHUBER (geb. 27. Aug. 1897) aus Bruckmühl
(Lkr. Bad Aibling), weil er Göring kritisier-
te und auf den Gruß "Heil Hitler" mit "Profit-
ler" antwortete.

Verfahren eingestellt

2. Nov. 1938 - 15. Feb. 1939
(1 Js So 2066/38)

Ermittlungsverfahren gegen den Friseur Johann
STROBL (geb. 29. Apr. 1909) aus Bruckmühl (Lkr.
Bad Aibling), weil er sich kritisch über die
Sudetenkrise und die Wehrmacht geäußert haben
soll.

Verfahren eingestellt
