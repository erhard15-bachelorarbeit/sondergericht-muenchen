(4219)

(4220)

(4221)

(4222)

(4223)

5476

2311

5312

53313

5314

Ermittlungsverfahren gegen die Buchhalterin
Maria MAURER (geb. 19. Mai 1906) aus
Garmisch-Partenkirchen, wegen Äußerungen
über Hitler und den Krieg. |
(HG)

Verfahren eingestellt

5. Spt. 1939 - 19. Feb. 1940
(1 Js So 1626/39)

Ermittlungsverfahren gegen den Bäcker Jo-
SS
a) wegen einer Äußerung über Hitler.
HG

Verfahren wegen Amnestie eingestellt

Messerer wurde nach Anordnung des Gestapa
Berlin vom 30. Okt. 1939 für 8 Wochen in
Schutzhaft genommen.

10. Spt. 1939 - 14. Nov. 1939
(1 Js So 1628/39)

Ermittlungsverfahren gegen den Reichsbahn-
bediensteten Xaver RAUH (geb. 18. Okt. 18686)
aus Kottern (Gde. St. Mang, Lkr. Kempten),
wegen abwertender Außerungen über Hitler.
(HG; 8 51 StGB)

Verfahren wegen Amnestie eingestellt

13. Spt. 1939 - 10. Jan. 1940
(1 Js So 1629/39)

Ermittlungsverfahren gegen den Schneider-
meister Christian SCHMUTZLER (geb. 13. ©kt.
1880) aus Augsburg, Pg., wegen politischer
Äußerungen während einer Schutzhaft vom
(a 1939 bis 16. Spt. 1939.

Verfahren eingestellt

22. Spt. 1939 - 8. Feb. 1940
(1 Js So 1630/39)

Prozeß gegen den Gemeindebeamten a.D.
Johann BRANDAUER (geb. 27. Mai a aus
Bayerisch Gmain (Lkr. Berchtesgaden), we-
gen der Behauptung, Hitler trage die Schuld
am Krieg.

(HG)

Verfahren wegen Amnestie eingestellt

3. Okt. 1939 - 22. Nov. 193
(1 Js So 1632/39) 9
