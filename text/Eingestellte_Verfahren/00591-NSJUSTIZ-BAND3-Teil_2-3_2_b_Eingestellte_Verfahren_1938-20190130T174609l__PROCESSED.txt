(2807)

(2808)

(2809)

(2810)

(2811)

(2812)

4187

4188

4189

4190

4191

4192

Ermittlungsverfahren gegen die Hilfsarbeiters-
frau Ottilie RENG (geb. 10. Aug. 1907) aus
München, weil sie sich über Göring lustig
machte.

Verfahren eingestellt

18. Spt. 1938 - 1. Okt. 1938
(1c Js So 1683/38)

Ermittlungsverfahren gegen den Dekorateur
Raffael RHEINGOLD (geb. 1. Nov. 1910) aus
München, staatenlos, weil er kritische poli-
tische Äußerungen gemacht haben soll.

Verfahren wegen Amnestie eingestellt.

6. Jul. 1938 - 17. Okt. 1938
(1c Js So 1684/38)

Ermittlungsverfahren gegen den Schleifer
Kaspar PREMS (geb. 4. Jan. 1892) und dessen
Ehefrau Katharina PREMS (geb. 11. Apr. 1887),
beide aus München, weil sie sich gegen den
NS betätigt haben sollen.

Verfahren eingestellt

27. Jun. 1938 - 10. Okt. 1938
(1c Js So 1685/38)

Ermittlungsverfahren gegen den Kaufmann
Albert Melchior KINDLIMANN (geb. 28. Jul.
1879) aus Augsburg, weil er die Regierung
kritisiert haben soll.

Verfahren eingestellt

17. Spt. 1938 - 10. Nov. 1938
(1b Js So 1687/38)

Ermittlungsverfahren gegen den Techniker
Johann WIELAND (geb. 23. Spt. 1910) aus Mün-
chen, früher Pg. und SS-Mitglied, weil er
unberechtigterweise Parteiabzeichen und SS-
Uniform getragen hat.

Verfahren eingestellt
12. Aug. 1938 - 24. Nov. 1939
(1e Js So 1694/38)

Ermittlungsverfahren gegen den Rittmeister
Julius Frhr. v. THIELMANN (geb. 28. Mrz. 1882)
aus Fürstenfeldbruck, weil er über ein Hitler-
bild schimpfte.

Verfahren eingestellt

T. wird in Schutzhaft genommen

19. Spt. 1938 - 11. Nov. 1938
(1A Js So 1695/38)
