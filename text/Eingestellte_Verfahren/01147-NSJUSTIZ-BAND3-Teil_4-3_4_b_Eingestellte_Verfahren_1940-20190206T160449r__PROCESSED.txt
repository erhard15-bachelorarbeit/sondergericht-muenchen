(5447)

(5448)

(5449)

(5450)

(5451)

6073

6148

6074

6075

6076

Ermittlungsverfahren gegen den Kriegsin-
validen Kaspar SCHREINER (geb. 24. Spt.
1888) aus Wegscheid, weil er sich abfällig
(6) die NSV geäußert haben soll.

HG

Verfahren eingestellt

21. Aug. 1940 - 11. Nov. 1940
(1 Js So 1689/40)

Ermittlungsverfahren gegen den Erbhof-

bauern Josef HARTL (geb. 19. Aug. 1887)
aus Weichselried (Lkr. Viechtach), weil

ti y einen "Lump" genannt haben soll.
HG

Verfahren eingestellt

28. Okt. 1940 - 20. Feb. 1941
(1 Js So 1694/40)

Ermittlungsverfahren gegen den Bergarbeiter
Joseph WOLF (geb. 4. Spt. 1885) aus Boden-
mais (Lkr. Regen), weil er Reden von Reichs-
marschall Göring und Ministerialdirigent
Fritsche als "Schwindel" bezeichnet haben
soll.

($ 2 HG)

Verfahren eingestellt

25. Okt. 1940 - 15. Nov. 1940
(1 Js So 1696/40)

Ermittlungsverfahren gegen die Maurers-
witwe Anna HARTBERGER (0o.D.) aus Altötting,
weil sie u.a. gesagt haben soll, Hitler
"gehöre ertränkt".

(HG)

Verfahren eingestellt

10. Okt. 1940 - 8. Nov. 1940
(1 Js So 1697/40)

Ermittlungsverfahren gegen den Ingenieur
Rudolf KEIL (geb. 18. Jun. 1881), früher
Pg., aus München wegen unberechtigten Tra-
gens der SA-Uniform und verschiedener po-
litischer Außerungen.

(HG)

Verfahren eingestellt

11. Jun. 1940 - 17. Dez. 1940
(1 Js So 1700/40)
