(3669)

(3670)

(3671)

(3672)

(3673)

7A)

4818

4819

4820

4821

4822

Ermittlungsverfahren gegen den Kaufmann
Karl FUHRICH (geb. 13. Jul. 1897) aus
Breslau, weil er im Lager Dürreck bei

Berchtesgaden das Hakenkreuz beschimpft
haben soll.

(HG)

Verfahren eingestellt

17. Apr. 1939 - 20. Spt. 1939
(1 Js So 628/39)

Ermittlungsverfahren gegen den Hoch- und
Tiefbautechniker Wilhelm PRÖPPER (geb.
17. Spt. 1912), den Oberfeldmeister beim
Reichsarbeitsdienst Karl RÜHLING (geb. 14.
Jan. 1911), Pg. und SA-Mitglied, den Feld-
meister beim Reichsarbeitsdienst Roland
SEYER (geb. 8. Jan. 1915), Pg. und SA-
Mitglied, alle aus Schönram-Filz (Lkr.
Laufen), wegen Spielens der Internationale
auf dem Klavier und Benützens des Kommu-
nistengrußes.

($ 2 HG; 3 83 StGB)

Verfahren eingestellt

9. Mrz. 1939 - 16. Aug. 1939
(1 Js So 629/39)

Ermittlungsverfahren gegen den Maler Georg
MOHRING (geb. 3. Mai 1893), weil er in
Niedersonthofen (Lkr. Sonthofen), Baldur
von Schirach als einen Lausbuben bezeich-
net haben soll.

(HG)

Verfahren eingestellt

19. Mrz. 1939 - 5. Aug. 1939
(1 Js So 632/39)

Ermittlungsverfahren gegen die Köchin
Kreszenz ROGGERMEIER (geb. 8. Mai 1897)
aus München, wegen kritischer Äußerungen
über die Juden- und Kirchenpolitik und
über das Regime.

(HG)

Verfahren eingestellt

29. Mrz. 1939 - 27. Jul. 1939
(1 Js So 633/39)

Ermittlungsverfahren gegen den Arbeiter

Robert ROTH, (geb. 127 Dez. 1905) aus

Waging (Lkr. Laufen), wegen unbefugten Ir
ens des Parteiabzeichens.

(85 3,5 HG)

Verfahren eingestellt

29. Apr. 1939 - 22. Spt. 1939
(1 Js So 640/39)
