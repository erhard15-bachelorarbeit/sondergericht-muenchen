(2962)

(2963)

(2964)

(2965)

(2966)

(2967)

4325

4326

4327

4328

4329

4330

Verfahren eingestellt

7. Okt. 1938 - 27. Jan. 1939
(1a Js So 2012/38)

Ermittlungsverfähren gegen den Erbhofbauern
Josef KLINGER (geh. 31. Jul. 1892) aus TIraxl
(Lkr. Ebersberg), , weil er Hitler einen "Stier-
kopf" nannte. . ;

‘“
Verfahren eingestellt

19. Okt. 1938 - 2. Feb. 1939
(1b Js So 2013/38)

Ermittlungsverfahren gegen die Zugeherin Anna
FROMM (geb. 25. Feb. 1902) aus Landshut, weil
sie sich abwertend zur Bevölkerungspolitik ge-
äußert haben soll.

Verfahren eingestellt

20. Jul. 1938 - 22. Nov: 1938
(1a Js So 2015/38)

Ermittlungsverfahren gegen die Offiziantens-
witwe Maria BETZ (geb. 26. Spt. 1883) aus
München, weil sie kritische politische Auße-
rungen gemacht haben soll.

Verfahren wegen Amnöstie eingestellt

15. Spt. 1938 - 22. Nov. 1938
(1a Js So 2017/38)

Ermittlungsverfahren gegen den Mechaniker-

meister Richard KELLENDORFER (geb. 15. Okt.
1889) aus München, weil er Hitler beleidigt
haben soll.

Verfahren wegen Amnestie eingestellt

9. Spt. 1938 - 22, Nov. 1938
(1b Js So 2018/38)

Ermittlungsverfahren gegen den Kofferfabri-
kanten Wilhelm KELLENDORFER (geb. 7. Jan.
1898) aus München, weil er kritische poli-
tische Äußerungen gemacht haben soll.

Verfahren eingestellt

11. Okt. 1938 - 25. Nov. 1938
(1b Js So 2023/38)

Ermittlungsverfahren gegen den Brauer Bugen
VEIT (geb. 28. Feb. 1899) aus München, Wweil
er über politische Dinge geschimpft haben
soll.

s

Verfahren eingestellt

3. Nov. 1938 '- 3. Jan. 1939
(1d Js So 2024/38)
