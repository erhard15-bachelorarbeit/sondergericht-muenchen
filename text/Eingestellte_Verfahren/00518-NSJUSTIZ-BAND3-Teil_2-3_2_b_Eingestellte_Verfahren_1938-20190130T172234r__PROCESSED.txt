(2431)

(2432)

(2433)

(2434)

(2435)

3822

3823

3824

3825

3821

Ermittlungsverfahren gegen die Taglöhnerin
Maria KLEBER (geb. 27. Jan. 1895) aus
Mickhausen (Lkr. Schwabmünchen) wegen

der brieflichen Äußerung, daß es bald

zu einem Krieg kommen wird.

Verfahren wegen Amnestie eingestellt

17. Mrz. 1938 - 16. Mai 1938
(1b Js So 912/38)

Ermittlungsverfahren gegen den Bauern
Magnus HARTMANN (geb. 12. Mai 1889)
aus Seeweiler (Lkr. Füssen) wegen der
angeblichen Bezeichnung Hitlers als
"Hergottsschlawack", "der gleich ver-
recken" möge.

Verfahren eingestellt
H. wird in Schutzhaft genommen.

31. Mrz. 1938 - 17. Jun. 1938
(1b Js So 913/38)

Ermittlungsverfahren gegen den Monteur
Stefan HIEBER (geb. 29. Jan. 1903)

aus München, weil er mit "Heil Moskau"
gegrüßt und gefragt haben soll, was
das Dritte Reich denn biete.

Verfahren eingestellt

11. Mrz. 1938 - 16. Mai 1938
(1b Js So 314/38)

Ermittlungsverfahren gegen den
Böttchermeister Wilhelm HORNICH
(geb. 9. Dez. 1870) aus Hamburg
und Frau PUTZ aus Hals (Lkr. Passau)
wegen Schimpfens über die Regierung.

Verfahren wegen Amnestie eingestellt

30. Apr. 1938 - 27. Jun. 1938
(1b Js So 915, 1141/38)

Ermittlungsverfahren gegen den
Buchhalter Eduard LEISS (geb. 9.
Dez. 1882) aus München, weil er
Hitler einen "Betrüger und Lügner"
und Dr. Ley einen "Hanswurst" ge-
nannt haben soll.

Verfahren wegen Amnestie eingestellt

16. Mrz. 1938 - 16. Mai 1938
(1b Js So 916/38)
