(4106)

(4107)

(4108)

(4109)

(4110)

5210

5211

5212

5213

Ermittlungsverfahren gegen die Kulturar-
beitersfrau Rosina DETTER (geb. 13. Feb.
1890) aus Aicha (Lkr. Griesbach), weil
sie Hitler der Kriegstreiberei bezichtigt
haben soll.

(HG)

Verfahren wegen Amnestie eingestellt

30. Aug. 1939 - 21. Okt. 1939
(1 Js So 1428/39)

Ermittlungsverfahren gegen den Landwirt
Josef BAUMGARTNER (geb. 5. Spt. 1891)
aus Grottham (Lkr. Griesbach), weil er
gesagt haben soll, daß Hitler am Krieg
mit Polen schuld sei.

(HG)

Verfahren eingestellt

5. Spt. 1939 - 22. Nov. 1939
(1 Js So 1429/39)

Prozeß gegen den Landwirt Karl BLEICHER
(geb. 2. Nov. 1884) aus Tutzing (Lkr.
Starnberg), weil er die Regierung belei-
digt haben soll.

(HG)

Verfahren eingestelit

1. Spt. 1939 - 23. Dez. 1939
(1 Js So 1431/39)

Ermittlungsverfahren gegen den Kraftwa-
genführer Ludwig BAYERL (geb. 28. Okt.
1909) und den Glasmacher Josef‘ EICHINGER
(geb. 18. Apr. 1914) aus Regenhütte (Lkr.
Regen), weil sie ein Rede Hitlers als
y bezeichnet haben sollen.

HG

Verfahren eingestellt

26. Aug. 1939 - 26. Okt. 1939
(1 Js So 1432/39)

Ermittlungsverfähren gegen den Erbhof-
bauern Josef ERTL (geb. 8. Dez. 1882)
aus Bärnzell (Lkr. Regen), weil er sich
abfällig über die Regierung geäußert
haben soll.

($ 2 HG)

Verfahren eingestellt

7. Spt. 1939 - 13. Okt. 1939
(1 Js So 1433/39)
