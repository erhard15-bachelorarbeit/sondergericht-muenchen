(5387)

(5388)

(5389)

(5390)

(5391)

6034

6035

6129

6036

6037

Verfahren eingestellt

23. Aug. 1940 - 7. Spt. 1940
(1 Js So 1494/40)

Ermittlungsverfahren gegen den Arzt Dr.
Robert NORTZ (geb. 12. Mai 1895) aus Pe-
terskirchen (Lkr. Traunstein), weil er
sich kritisch über die wirtschaftliche
Lage Deutschlands und lobend über die mi-
litärische Stärke der Schweiz geäußert
haben soll.

(HG)

Verfahren eingestellt

15. Mai 1940 - 15. Spt. 1940
(1 Js So 1498/40)

Ermittlungsverfahren gegen den Mechaniker
Ludwig WANNER (geb. 2. Aug. 1897) aus
München wegen Äußerungen über Hitler und
Verbreitung von Gerüchten über die SA.

($ 2 HG)

Verfahren eingestellt

29. Nov. 1939 - 5. Spt. 1940
(1 Js So 1499/40; 1 Js So 14/40)

Ermitlungsverfahren gegen die Gastwirtin
Pauline WEINZIERL (geb. 18. Jun. 1887) au
München, weil sie wiederholt öffentliche
über Hitler und die Regierung geschimpft
Faben soll.

(HG)

Verfahren eingestelit

5. Spt. 1940 - 5. Mai 1941
(1 Js So 1502/40)

Ermittlungsverfahren gegen den Maler und
Hilfsarbeiter Josef WITTMANN (geb. 12. Ma
1921) aus München wegen unberechtigten
Tragens einer HJ-Bluse.

(HG)

Verfahren eingestellt

22. Apr. 1940 - 3. Spt. 1940
(1 Js So 1505/40; 1 Js So 859/40)

Ermittlungsverfahren gegen den Landwirt

Ludwig KNÖDLSEDER (geb. 27. Jul. 1884)
aus Windpassing (Lkr. Wegscheid), weil
e sr Hitler geschimpft haben soll.
HG

Verfahren eingestellt

16. Jul. 1940 - 7. Spt. 1940
(1 Js So 1515/40)
