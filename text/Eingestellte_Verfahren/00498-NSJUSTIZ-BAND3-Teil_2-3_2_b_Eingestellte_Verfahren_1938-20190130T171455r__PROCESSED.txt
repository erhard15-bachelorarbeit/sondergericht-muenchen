(2334)

(2335)

(2336)

(2337)

(2338)

(2339)

3725

3732

3726

3727

3728

3729

Ermittlungsverfahren gegen die
Bürstenmacherin Kreszenz HAGELSTEIN
(geb. 19. Dez. 1897) aus München
wegen angeblicher Kritik am Regime.

Verfahren eingestellt

19. Mrz. 1938 - 13. Mai 1938
(1a Js So 774/38)

Ermittlungsverfahren gegen die Haus-
angestellte Sophie SAUERMANN (geb.
3. Aug. 1901) aus München wegen an-
geblicher politischer Bemerkungen.

Verfahren wegen Amnestie eingestellt

8. Apr. 1938 - 19. Mai 1938
(1c Js So 775/38)

Ermittlungsverfahren gegen den Hilfs-
arbeiter Hubert JELLINGER (geb. 6.
Okt. 1900) aus Trostberg, weil er
sich für eine kommunistische Welt-
oränung aussprach.

Verfahren eingestellt

2. Feb. 1938 - 23. Apr. 1938
(1a Js So 776/38)

Ermittlungsverfahren gegen den Hafner-
meister Ignatz KÖNIGER (geb. 18. Jul.
1887) aus Mering (Lkr. Friedberg)
wegen Schimpfens über das Regime.

Verfahren wegen Amnestie eingestellt

3. Mrz. 1938 - 1. Jun. 1938
(1b Js So 777/38)

Ermittlungsverfahren gegen den Land-
wirtssohn Karl SCHNELL (geb. 16.
Jun. 1908) aus Pertenau (Lkr. Schro-
benhausen) wegen Außerungen über den
Anschluß Österreichs.

Verfahren eingestellt

17. Mrz. 1938 - 25. Apr. 1938
(1c Js So 778/38)

Ermittlungsverfahren gegen den Friseur
Ernst STEPHAN (geb. 9. Aug. 1918) aus
Niederaschau (Lkr. Rosenheim), weil er
Hitler einen Hochstapler nannte.

Verfahren eingestellt

18. Mrz. 1938 - 7. Mai 1938
(1c Js So 779/38)
