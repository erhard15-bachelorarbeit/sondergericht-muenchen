(5030)

(5031)

(5032)

(5033)

(5034)

5670

5671

5672

5673

5674

Ermittlungsverfahren gegen den Hausbesitzer
Hans STEFPE (geb. 4. Jun. 1904) aus Mittel-
berg (Osterreich) ,weil er sich skeptisch
über den Ausgang des Krieges geäußert ha-
ben soll.

(& 2 Ha)

Verfahren eingestellt

25. Okt. 1939 - 12. Feb. 1940
(1 Js So 131/40)

Ermittlungsverfahren gegen den Buchhändler
Karl Walter GEYH (geb. 30. Okt. 1901) aus
Innsbruck (Osterreich), weil er in Hüssen
verbreitet haben soll, daß Generaloberst
von Fritsch nicht bei einer Kampfhandlung
ums Leben gekommen sei, sondem "so wegge-
räumt" worden sei.

(HG)

Verfahren eingestellt

30. Nov. 1939 - 5. Mrz. 1940
(1 Js So 132/40)

Ermittlungsverfahren gegen den Holzhauer
Adolf HANUS (geb. 15. Jun. 1883) aus Kusch-
warda (Tschechoslowakei), weil er die
wirtschaftliche Lage Deutschlands als sehr
schlecht bezeichnet haben soll.

($ 2 HG)

Verfahren eingestellt

20. Dez. 1939 - 27. Mrz. 1940
(1 Js So 133/40)

Ermittlungsverfahren gegen den Wohlfahrts-
rentner Fritz SEDLMAIR (geb. 9. Dez. 1864)
aus München, weil er uber Hitler und ßgie
u) geschimpft haben soll.

HG

Verfahren eingestellt

31. Okt. 1939 - 22. Apr. 1940
(1 Js So 134/40)

Ermittlungsverfahren gegen den Kraftwasen-
führer Richard LESTI (geb. 7. Mai 19099
aus Munchen, weil er Anfang November ge-
sagt haben soll, daß es am 9. Nov. in
München "krachen" werde.

(HG)

Verfahren eingestellt

19. Nov. 1939 - 17. Feb. 1940
(1 Js So 135/40)
