(3964)

(3965)

(3966)

(3967)

(3968)

5078

5079

5080

5081

5082

Ermittlungsverfahren gegen die Volks-
schullehrerin Ilse HAGER (geb. 28. Mrz.
1899) aus Göggingen (Lkr. Augsburg),
wegen einer Äußerung über die Wohnungs-
politik.

($ 2 HG)

Verfahren eingestellt

16. Mai 1939 - 21. Spt. 1939
(1 Js So 1224/39)

Ermittlungsverfahren gegen den Bäcker
Maximilian LEITNER aus Goisern (Lkr.
Gmunden, Österreich), wegen Erzählens
von Greuelnachrichten über die Konzentra-
tionslager und den Röhm-Putsch.

(HG; $ 89 StGB)

Verfahren wegen Amnestie eingestellt

10. Aug. 1939 - 30. Okt. 1939
(1 Js So 1229/39)

Ermittlungsverfahren gegen den Hilfsar-
beiter Erasmus DIALLER (geb. 27. Jan.
1888) aus Feilnbach (Lkr. Bad Aibling),
() Äußerungen über die Soldaten.

HG

Verfahren wegen Amnestie eingestellt

4. Aug. 1939 - 28. Spt. 1939
(1 Js So 1231/39)

Ermittlungsverfahren gegen den Erbhof-
bauern Matthias GRUNDLER (geb. 24. Spt.
1898) aus Wabern (Lkr. Landsberg),
a) Äußerungen über das Regime.

HG

Verfahren eingestellt

23. Jul. 1939 - 14. Okt. 1939
(1 Js So 1232/39)

Ermitlungsverfahren gegen den Gastwirt
Anton LICHTENECKERT (geb. 5. Feb. 1869)
aus Wallern (CSSR), Pg., wegen Verächt-
(a) des deutschen Grußes.

HG

Verfahren eingestellt

1. Jul. 1939 - Spt. 1939
(1 Js So 1233,1278/39)
