(3983)

(3984)

(3985)

(3986)

(3987)

5098

5099

5100

5101

Ermittlungsverfahren gegen die Erbhofbäue-
rin Regina RÖLLNREITER (geb. 11. Spt. 1893)
aus Niklasreuth (Lkr. Miesbach), wegen ei-
ner Außerung über Hitler.

($ 2 HG)

Verfahren eingestellt

6. Aug. 1939 - 18. Spt. 1939
(1 Js So 1259/39)

Ermittlungsverfahren gegen den Zimmerer
Georg SCHNEIDER (geb. 26. Spt. 1919) aus
Hohenbrunn (Lkr. München), weil er zu
einem SS-Mann sagte: "Heil Moskau! Ich
möchte verhaftet werden! Es ist keine
Kameradschaft mehr!"

($ 2 HG; 8 83 StGB)

Verfahren eingestellt

17. Aug. 1939 - 11. Spt. 1939
(1 Js So 1260/39)

Ermittlungsverfahren gegen die Bauerswitwe
Anna FISCHER (geb. 8. Aug. 1894) und die
Hilfsarbeitersehefrau Antonie SCHWEIER (geb.
29. Mrz. 1893), beide aus Untermeitingen
(Lkr. Schwabmünchen), wegen Äußerungen

über das Regime und über die Inschutz-
haftnahme des kath. Pfarrers BRENNER aus
Untermeitingen.

(HG)

Verfahren eingestellt

9. Aug. 1939 - 9. Spt. 1939
(1 Js So 1261/39)

Ermittlungsverfahren gegen die Hauptlehrers-
gattin Gisela WEIGL (geb. 3. Okt. 1881)

aus München, wegen Außerungen über die
Regierung.

(HG)

Verfahren wegen Amnestie eingestellt

18. Aug. 1939 - 2. Nov. 1939
(1 Js So 1263/39)

Ermittlungsverfahren gegen den Hilfsarbeiter
Alois VINZENZ aus Aiblingerau (Lkr. Bad
lin), wegen Außerungen über Hitler.

HG

Verfahren eingestellt

24. Aug. 1939 - 18. Spt. 1939
(1 Js So 1264/39)
