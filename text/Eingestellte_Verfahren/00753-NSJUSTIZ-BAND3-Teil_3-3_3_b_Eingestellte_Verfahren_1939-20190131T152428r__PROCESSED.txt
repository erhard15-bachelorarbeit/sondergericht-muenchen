(3593)

(3594)

(3595)

(3596)

(3597)

4743

4744

4745

4746

4747

Ermittlungsverfahren gegen den Bankvorstand
Johann REINGRUBER, Pg., (geb. 3. Jun. 1886)
aus Ichenhausen (Lkr. Günzburg), weil er
Hitlers Außenpolitk "Theaterspielerei"
genannt haben soll.

(Ha)

Verfahren eingestellt

13. Jan. 1939 - 22. Aug. 1940
(1 Js So 441/39)

Ermittlungsverfahren gegen den Landwirt
Michael WÖLFLE (geb. 5. Apr. 1881) aus
Moosmühle (Lkr. Memmingen), weil er u.a.
gesagt haben soll, daß Hitler größenwahn-
sinnig sei.

(HG)

Verfahren eingestellt

28. Feb. 1939 - 26. Mrz. 1939
(1 Js So 442/39)

Ermittlungsverfahren gegen den Dipl-Ing.
und tschechischen Staatsangehörigen Leo
SANTO-PASSO (geb. 12. Okt. 1879) aus Neu-
biberg (Lkr. München), weil er unberechtigt
das Parteiabzeichen und das Braunhemd ge-
tragen haben soll.

(HG)

Verfahren eingestellt

15. Dez. 1938 - 13. Jul. 1939
(1 Js So 445/39)

Ermittlungsverfahren gegen den Getreide-
händler Philipp RÖLL (geb, 23. Dez. 1907)
aus Mainburg, weil er den nat.soz. Staat
beschuldigt haben soll, an der schlechten
wirtschaftlichen Situation der Bauern
schuld zu sein.

(HG)

Verfahren eingestelit

9. Mrz. 1939 - 11. Aug. 1939
(1 Js So 449/39)

Ermittlungsverfahren gegen den Bauern
Bartholomäus FISCHER (geb. 14. Mrz. 18391)
aus Viecht (Lkr. Ebersberg), weil er sich
abfällig über die Regierung geäußert
haben soll.

(HG)

Verfahren eingestellt

22. Mrz. 1939 - 4. Jul. 1939
(1 Js So 452/39)
