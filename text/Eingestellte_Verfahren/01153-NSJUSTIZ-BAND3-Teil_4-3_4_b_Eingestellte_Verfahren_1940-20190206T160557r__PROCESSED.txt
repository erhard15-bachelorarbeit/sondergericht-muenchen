(5477)

(5478)

(5479)

(5480)

(5481)

6086

6160

6161

6121

Ermittlungsverfahren gegen den Lageran-
gestellten &Lastian MEINDL (geb. 22. Jun.
1905) aus Mainburg, weil er in Ebenhausen
(Lkr. Ingolstadt) auf das Dritte Reich
(8 13a’ haben soll.

$ 134a StGB)

Verfahren eingestellt

2. Nov. 1940 - 9. Jan. 1941
(1 Js So 1814/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Erich FÜSSL (geb 22. Feb. 1908)
aus Penzberg (Lkr. Weilheim) wegen un-
beri:chtigten Besitzes eines Parteiabzei-
chens.

(HG)

Verfahren eingestellt

27. Spt. 1940 - 12. Dez. 1940
(1 Js So 1815/40; 1 J So 1637/40)

Ermittlungsverfahren gegen den Reichsbahn-
gehilfen Heinrich FEUSTEL (geb. 17. Aug.
1898) aus Lindau, KPD-Mitglied, weil er
gesagt haben soll, "der Führer sei der
erste und beste Nationalsozialist", alle
anderen aber "Lumpen".

(HG; 3 330 StGB)

Verfahren eingestellt

27. Nov. 1940 - 22. Jan. 1941
(1 Js So 1818/40)

Ermittlungsverfahren gegen den Bauern

° Georg ECHTLER (geb. 30. Mrz. 1894) aus

Steingaden (Lkr. Schongau), weil er auf
Ortsbauernführer geschimpft und Hitler
einen "Lumpen und Sprüchemacher" genannt
haben soll.

(HG; $8 330 StGB)

Verfahren eingestellt

15. Dez. 1940 - 6. Feb. 1941
(1 Js So 1822/40)

Ermittlungsverfahren gegen den Maurer Max
EINHELLINGER 7. Spt. 1894) aus
Reifziehberg (Lkr. Vilshofen) und die
Wirtschaftspächterswitwe Balbine FEIL-
MEIER (geb. 19. Okt. 1891) aus Vilshofen.
Einhellinger soll sich abfällig über Par-
teimitglieder geäußert und Feilmeier soll
gegen besseres Wissen zugunsten von F.
ausgesagt haben.

(8 2 HG; 383 134b StGB)
