(630)

(631)

(632)

(633)

(634)

7814

7815

7817

7816

7728 .

Eröffnung der Hauptverhandlung abgelehnt

15. Nov. 1933 - 15. Feb. 1934
(So E 34/34)

Ermittlungsverfahren gegen den Ingenieur
und Direktor Gustav HEILMANN (geb. 20.
Dez. 1876) aus München, NSDAP-Mitglied,
wegen einer kritischen Äußerung über die
Vergabe-Politik der Regierung.

Eröffnung der Hauptverhandlung abgelehnt

24. Nov. 1933 - 19. Feb. 1934
(So E 35/34)

Ermittlungsverfahren gegen den Apotheker
Alois SCHNITZBAUER (geb. 1. Jul. 1881) aus
München, Mitglied des Kyffhäuserbundes, we-
gen abwertender Äußerungen über Hitlers nmı-
litärische Vergangenheit.

Eröffnung der Hauptverhandlung abgelehnt

8. Nov. 1933 - 5. Mrz. 1934
(So E 36/34)

Ermittlungsverfahren gegen den Versicherungs
vertreter Alexander MEYER (geb. 7. Okt. 1087)
aus Münster wegen Zechprellerei in Starnmnberz.

Anklage zurückgezogen

22. Nov. 1933 - 29. Jun. 1934
(So E 37/34)

Ermittlungsverfahren gegen den Bildhauer
Knut Samuel ANDERSON (geb. 4. Aug. 1884),
Schwede, aus München wegen kritischer
Äußerungen gegen die NSDAP.

Eröffnung der Hauptverhandlung abgelehnt

29. Dez. 1933 - 26. Feb. 1934
(So E 38/34)

Ermittlungsverfahren gegen den Schlosser
Ludwig ROHRHIRSCH (geb. 20. Jul. 1906)

aus München wegen Verbreitung kommunistische
Schriften.

Anklage erhoben und wegen Verjährung zurück-
gezogen

14. Aug. 1933 - 6. Mrz. 1934

(So E 39/34)
