(2746)

(2747)

(2743)

(2749)

47191

4132

4133

4134

4135

Ermittlungsverfahren gegen den Farbwaren-
händler Vinzenz HEIMBACH (geb. 16. Mrz.
1902) aus München und den Angestellten
Alfred VOGLSANG (geb. 6. Feb. 1908) aus
Dillingen, weil sie u.a. über die Zwangs-
mitgliedschaft in der DAF geschimpft haben
sollen.

Verfahren eingestellt

28. Jul. 1938 - 9. Dez. 1938
(1 b Js So 1524 a-b/38)

Ermittlungsverfahren gegen den Hilfsarbei-
ter Ottmar HEINLE (geb. 24. Aug. 1919) aus
Düsseldorf, Wachtposten bei der SS in Dachau,
weil er gesagt haben soll: "Wenn im Lager
ein Gefangener bloß einen Schritt verkehrt
macht... wird er erschossen. ... Uber die
Greuel in Dachau könnte ich ein Buch schrei-
ben."

Verfahren wegen Amnestie eingestellt

25. Jun. 1938 - 15. Spt. 1938
(1b Js So 1528/38)

Ermittlungsverfahren gegen den Elektromon-
teur Johann JAIS (geb. 25. Mai 1906) aus
Gersthofen (Lkr. Augsburg), weil er gesagt
haben soil, daß er seinen Kindern den
deutschen Gruß nicht beibringe, denn "in
zwei Jahren sagt niemand mehr 'Heil Hitler'!".

Verfahren eingestellt

13. Aug. 1938 - 21. Nov. 1938
(1b Js So 1531/38)

Ermittlungsverfahren gegen den Landwirt
Josef KLEMMER (geb. 19. Mrz. 1887) aus
Ustersbach (Lkr. Augsburg), weil er gesagt
haben soll, daß das 3. Reich bis 1940 "ver-
reckt" sei. .

Verfahren eingestellt

10. Aug. 1938 - 29. Dez. 1938
(1b Js So 1533/38)

Ermittlungsverfahren gegen den Kaminkehrer-
gehilfen Max WENZL (geb. 2. Mai 1900) aus
Ebersberg, weil er gesagt haben soll, daß
Göring "der größte Bazi und Schlawiner" sei.

Verfahren eingestelitt

1. Aug. 1938 - 18. Nov. 1938
(1d JS So 1534/38)
