(4284)

(4285)

(4286)

(4287)

(4288)

5354

5368

5369

5489

2370

Ermittlungsverfahren gegen den Landwirt
Josef SEITZ (geb. 1. Mai 1876) aus Pfaffen-
hausen (Lkr. Mindelheim), weil er die
Nachrichten des Schweizer Senders gehört
hat.

($ 5 Rdf.VO)

Verfahren eingestellt

16. Okt. 1939 - 9. Nov. 1939
(1 Js So 1744/39)

Ermittlungsverfahren gegen den Hilfsar-
beiter Josef MAURER (geb. 1. Apr. 1887)
aus Holzkirchen (Lkr. Miesbach), weil er
gesagt hat: "Deutschland war 1914 am Krieg
schuld und ist genau heute so schuld."

(HG)

Verfahren eingestellt

30. Spt. 1939 - 24. Nov. 1939
(1 Js So 1752/39)

Ermittlungsverfahren gegen den Invaliden-
rentner Franz ROTTNER (geb. 6. Spt. 1882)
aus Augsburg, wegen Außerungen über das
Dritte Reich.

($ 2 HG)

Verfahren eingestellt

18. Okt. 1939 - 24. Nov. 1939
(1 Js So 1756/39)

Ermittlungsverfahren gegen den kfm. An-
gestellten Heinrich SCHWARZ (geb. 16. Aug.
1890) aus Gersthofen (Lkr. Augsburg),
wegen Bemerkungen über die Stimmung der
Bevölkerung.

(HG)

Verfahren eingestellt

25. Spt. 1939 - 16. Feb. 1940
(1 Js So 1757/39)

Ermittlungsverfahren gegen den Hilfsar-
beiter Leo LECHNER (geb. 21. Mrz. 1898)
aus Pfeffenhausen (Lkr. Rottenburg a.d.
Laaber), weil er gesagt haben soll: "Für
wen soll ich kämpfen, ich habe ja kein
Vaterland."

(HG)

Verfahren eingestellt

22. Spt. 1939 - 24. Nov. 1939
(1 Js So 1758/39)
