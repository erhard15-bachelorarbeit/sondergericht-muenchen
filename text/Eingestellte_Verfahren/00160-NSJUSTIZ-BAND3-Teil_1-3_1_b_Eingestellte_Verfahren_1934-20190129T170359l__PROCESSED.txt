(706)

(707)

(708)

(709)

(710)

(711)

7855

7856

7857

7858

7859

7860

Ermittlungsverfahren gegen den ÄArbeiter
Johann HÖRMANN (geb. 24. Feb. 1904) aus
Huisheim (Lkr. Donauwörth) wegen Spreng-
stoffdiebstahls.

Verfahren eingestellt

30. Apr. 1934 -21. Aug. 1934
(So E 129/34)

Ermittlungsverfahren gegen den Eisenbahn-
sekretär a.D. Johann MANN (geb. 6. Dez.
1872) aus München wegen kritischer Auße-
rungen über Hitler.

Verfahren eingestellt

3. Jun. 1934 - 20. Aug. 1934
(So E 130/34)

Ermittlungsverfahren gegen den Hausierer
Anton KOTTER (geb. 9. Jan. 1902) aus
Traunstein wegen abwertender Äußerungen
über Hitler.

Verfahren eingestellt

19. Jun. 1934 - 23. Aug. 1934
(So E 131/34)

Ermittlungsverfahren gegen die Kaufmanns-
ehefrau Elise GAUSS (geb. 28. Feb. 1873)
aus München wegen abwertender Außerungen
über führende Nat.Sozz..

Verfahren eingestellt

25. Mai 1934 - 23. Aug. 1934
(So E 133/34)

Ermittlungsverfahren gegen den Verwaltungs-
sekretär Siegfried Johann ALTENBECK (geb.
20. Jul. 1897) aus München, NSDAP-Mitglied,
wegen kritischer ÄAußerungen über die Wirt-
schaftspolitik Hitlers.

Verfahren eingestellt

5. Jul. 1934 - 23. Aug. 1934
(So E 134/34)

Ermittlungsverfahren gegen den Privatier
Adolf TROST (geb. 13. Okt. 1875) aus Dirle-
wang (Lkr. Mindelheim), früher SPD-Mitglied,
nun NSDAP-Mitglied, wegen abwertender ÄAuße-
rungen über führende Nat.Sozz..

Verfahren eingestellt

22. Jun. 1934 - 23. Aug. 1934
(So E 135/34)
