(5254)

(5255)

(5256)

(5257)

(5258)

5903

5904

5905

5907

53908

Ermittlungsverfahren gegen den Vertreter
Ludwig ZACHER (geb. 11. Aug. 1910) aus
München wegen unberechtigten Tragens von
Parteiabzeichen.

(HG)

Verfahren eingestellt

15. Spt. 1939 - 23. Apr. 1940
(1 Js So 886/40; 1 Js So 1306/39)

Ermittlungsverfahren gegen den Bücher-
revisor Johann OSTERTAG (geb. 13. Apr.
1873) aus Augsburg, Pg., weil er Hitler
einen "Schwindler" genannt haben soll.
($ 2 Ha)

Verfahren eingestellt

30. Apr. 1940 - 21. Aug. 1940
(1 Js So 930/40)

Ermittlungsverfahren gegen den Postfach-
arbeiter Ludwig LEITERMANN (geb. 1. Spt.
1907) aus München, weil er Zweifel über
die Richtigkeit der Kriegsberichter-
(a) geäußert haben soll.

HG

Verfahren eingestellt

10 Jan. 1940 - 3. Jun. 1940
(1 Js So 939/40)

Ermittlungsverfahren gegen den Mechaniker

Adam MAYR (geb. 29. Jan. 1906), Pg., aus
Fürstenfeldbruck, weil er eine Militär-

kapelle nicht mit dem deutschen Gruß ge-
rüßt hat.

(5 134a StGB)

Verfahren eingestellt

4. Apr. 1940 - 6. Jun. 1940
(1 Js So 970/40; 1 Js So 218/40)

Ermittlungsverfahren gegen den Schreiner
Ludwig UHLAND (geb. 28. Feb. 1908) aus
Lindau, wegen abfälliger Äußerungen über
Hitler.

(HG)

Verfahren eingestellt

5. Mai 1940 - 17. Jun. 1940
(1 Js So 977/40)
