(5346)

(5347)

(5348)

(5349)

(5350)

2999

6110

6000

6111

6001

Ermittlungsverfahren gegen den Architekten
Paul LIESE (geb. 5. Jan. 1871) aus Berlin,
weil er in Bad Tölz über die Regierung
(ua) pt haben soll.

Verfihren eingestellt

31. Mai 1940 - 1. Aug. 1940
(1 Js So 1399/40)

Ermittlungsverfahren gegen den Erbhof-
bauern Johann HOLZER (geb. 7. Jun. 1890)
aus Unterzolling (Lkr. Freising), weil
er u.a. den Krieg einen "Schwindel" und
die Regierung eine "Schwindelbande" ge-
nannt haben soll.

(HG)

Verfahren eingestellt

8. Mai 1940 - 19. Feb. 1941
(1 Js So 1401/40)

Ermittlungsverfahren gegen den Magazin-
arbeiter Lorenz BINDGEN (geb. 14. Aug.
1884) aus Saarbrücken, weil er in Gerlen-
hofen (Lkr. Neu-Ulm) gesagt haben soll,
daß er bevorzugen würde, wenn das Saar-
(g) zu Frankreich gehören würde.

HG

Verfahren eingestellt

28. Apr. 1940 - 7. Aug. 1940
(1 Js So 1404/40; 1 Js So 975/40)

Ermittlungsverfahren gegen den Viehhändler
und Gastwirt Josef HEILMEIER (geb. 12.

Jul. 1891) aus Ensdorf (Lkr. Mühldorf a.
Inn), weil er sich abfällig über das Dritte
Reich geäußert haben soll.

(HG)

Verfahren eingestellt

5. Jul. 1940 + 5. Feb. 1941
(1 Js So 1405/40)

Ermittlungsverfahren gegen den Austrägler
Mathias RÄTZINGER (geb. 15. Apr. 1880
aus Kemnat (Lkr. Günzburg) wegen Äuße-
rungen über Hitler.

(HG

Verfahren eingestellt

29. Jun. 1940 - 25. Spt. 1940
(1 Js So 1406/40)
