(2790)

(2791)

(2792)

(2793)

(2794)

L412

4173

4174

4413

4175

Verfahren eingestellt

8. Spt. 1938 - 29. Nov. 1938
(1d Js So 1639/38)

Ermittlungsverfahren gegen den Ausgeher
Ludwig HEISS (geb. 5. Jun. 1910) aus München,
früher Pg. und SA-Mitglied, weil er unbe-
rechtigt ein Parteiabzeichen besaß.

Verfahren eingestellt

15. Spt. 1938 - 31. Jan. 1939
(ie Js So 1640/38)

Ermittlungsverfahren gegen den Vertreter Jo-
hann ULRICH (geb. 24. Mai 1907) aus Augsburg
und den Heilpraktiker Hermann HÖRMANN (ob.
8. Nov. 1901) aus Günzburg, weil sie in Ober-
beuren (Lkr. Kaufbeuren) kritische politische
Bemerkungen gemacht haben sollen.

Verfahren wegen Amnestie eingestellt

15. Jan. 1938 - 23. Spt. 1938
(1d Js So 1642/38)

Ermittlungsverfahren gegen den Invaliden
Mathias DUBOIS (geb. 25. Jun. 1882) aus Lau-
terbrunn (Lkr. Wertingen), weil er vor einem
Hitlerbild ausgespuckt haben und eine Knie-
beuge vor einem Kruzifix gemacht haben soll.

Verfahren eingestellt

7. Spt. 1938 - 2. Dez. 1938
(1a Js So 1643/38)

Ermittlungsverfahren gegen den Werbeleiter
Johann IVERSEN (geb. 31. Aug. 1865) aus Füssen
und den Apotheker Ludwig LAUER aus Naumburg
(Lkr. Weißenfels) weil sie den Reichsamtslei-
ter im Hauptamt für Volksgesundheit, Dr. Bern-
hard Hörmann, mit publizistischen Mitteln
angriffen.

Verfahren wegen Amnestie eingestellt

12. Spt. 1938 - 17. Nov. 1939
(1 Js So 1645, 1769, 1832/38)

Ermittlungsverfahren gegen den Schreinereibe-
sitzer Andras FUCHS (geb. 30. Mai 1884) aus
München, SPD-Mitglied, weil er eine Hitler-
rede kritisiert haben soll.

Verfahren wegen Amnestie eingestellt

12. Aug. 1938 - 26. Spt. 1938 | .
(1a Js So 1646/38)
