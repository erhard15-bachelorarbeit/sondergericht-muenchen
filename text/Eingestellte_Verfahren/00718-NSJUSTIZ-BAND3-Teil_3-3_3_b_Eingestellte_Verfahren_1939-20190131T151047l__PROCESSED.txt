(3422)

(3423)

(3424)

(3425)

(3426)

4572

4575

4574

4575

4576

Ermittlungsverfahren gegen den Landwirt und
Hilfsarbeiter Johann BAUER (geb. 10. Dez.
1886) aus Apfeldorf (Lkr. Schongau), wegen
Kritik an der Deutschen Arbeitsfront und
(6) den "Juden Kurt Eisner".

HG

Verfahren eingestellt

13. Dez. 1938 - 25. Jan. 1939
(1 Js So 76/39)

Ermittlungsverfahren gegen den Schlosser
Ludwig BÜTTNER (geb. 2. Mrz. 1907) aus
Niederding (Lkr. Erding). Er soll Greuelge-
(ng) über das KZ Dachau erzählt haben.
HG

Verfahren eingestellt

Ludwig Büttner war als KPD-Mitglied vom 18.
Dez. 1933 bis 30. Apr. 1934 wegen "F6Gottesläs-
terung" im KZ Dachau gewesen.

Büttner ist seit 4. Feb. 1939 in Schutzhaft.

27. Jul. 1938 - 13. Mai 1939
(1 Js So 77/39)

Ermittlungsverfahren gegen die evangelische
Diakonisse Margarte Barbara Anna BETZ (geb.
12. Jan. 1889) aus Augsburg, wegen abwerten-
(a) über Goebbels und Streicher.
HG

Verfahren eingestellt

11. Jan. 1939 - 16. Aug. 1939
(1 Js So 78/39)

Ermittlungsverfahren gegen die Krankenschwes-
ter Amalie HUBER (geb. 14. Apr. 1894) aus
Durach (Lkr. Kempten), wegen einer kritischen
Äußerung zur Sudetenkrise.

(HG)

Verfahren eingestellt

26. Dez. 1938 - 28. Mrz. 1939
(1 Js So 81/39)

Ermittlungsverfahren gegen den Arbeiter Lud
HUBER (geb. 27.7.1894) aus Moosburg (Lkr.
Freising), weil er ein Hitlerbild verbrannte,
(8 2 HG; $ 185 staB)

Verfahren eingestellt

15. Jan. 1939 - 3. Feb. 1939
(1 Js So 82/39)
