(4091)

(4092)

(4093)

(4094)

(4095)

5195

5196

5197

5198

3199

Ermittlungsverfahren gegen die Angestellte
Maria HAB (geb. 15. Aug. 1895) aus Gar-
misch-Partenkirchen, weil sie Hitler einen
"Sauhund", der das Land ruiniert, genannt
hat.

(HG)

Verfahren wegen Amnestie eingestellt

1. Spt. 1939 - 11. Okt. 1939
(1 Js So 1408/39)

Ermittlungsverfahren gegen den Angestellten
Johann INGERL (geb. 19. Apr. 1911) aus
Sonthofen, weil er sich als Kommunisten
bezeichnet hat.

(HG)

Verfahren wegen Amnestie eingestellt

31. Aug. 1939 - 28. Spt. 1939
(1 Js So 1409/39)

Ermittlungsverfahren gegen die Rentnerin
Anna LANGMEIER (geb. 25. Jan. 1894) aus
München, weil sie in einem Brief an ihren
Sohn von Erschießungen und Verhaftungen
aktiver Offiziere in München berichtet
hat.

(HG)

Verfahren wegen Amnestie eingestellt

9. Spt. 1939 - 11. Okt. 1939
(1 Js So 1410/39)

Ermittlungsverfahren gegen den Schmied-
meister Josef GRUBER (geb. 26. Nov. 1886)
und seine Frau Barbara eb. 18. Dez.

1892) aus Obertrennbach (Lkr. Eggenfelden),
wegen "übler Nachrede" von Generalfeld-
marschall Goring und Propagandaminister
Goebbels.

(& 2 HG)

Verfahren eingestellt

10. Aug. 1939 - 13. Nov. 1939
(1 Js So 1406/39)

Ermittlungsverfahren gegen den Immobilien-
makler Paul UNMÜSSIG (geb. 27. Spt. 1885)
aus Munchen, weil er gesagt haben soll:

"Was aer Völkische Beobachter schreibt ist
Bluff. Deutschland ist vor dem Zusammenbruch

(AG)
Verfahren eingestellt

22. Aug. 1939 - 17. Jan. 1940
(1 Js So 1411/39)
