(5165)

(5166)

(5167)

(5168)

(5169)

3811

5812

3813

5814

53815

Ermittlungsverfahren gegen die Kassiere-
rin Johanna SCHLIG (geb. 21. Jun. 1914)
aus Munchen, weil sie sich u.a. abfällig
über die militärische Führung geäußert
haben soll.

(HG)

Verfahren eingestellt

7. Nov. 1939 - 27. Jul. 1940
(1 Js So 575/40)

Ermittlungsverfahren gegen die Hilfsar-
beiter Franz STEINMÜLLER (geb. 6. Mai
1908) und Heinrich STADLBAUER (geb. 6.
Feb. 1907) aus München wegen schweren
Diebstahls.

($ 242 StGB; VVO)

Verfahren eingeställt

21. Feb. 1940 - 29. Mrz. 1940
(1 Js So 576/40)

Ermittlungsverfahren gegen die Sozial-
pflegerin Hildegard ZOUMER (geb. 21.
Feb. 1910) aus München wegen kritischer
Äußerungen über den Westwall.

(HG)

Verfahren eingestellt

9. Mrz. 1940 - 8. Jun. 1940
(1 Js So 579/40)

Ermittiungsverfahren gegen den Verwalter
Josef KASTENMAYER (geb. 6. Jan.1903) aus
Rosenau (Lkr. Dingolfing) wegen abfälliger
Bemerkungen über die Arbeitsleistung deut-
scher Arbeiter.

(HG)

Verfahren eingestellt

18. Jan. 1940 - 3. Apr. 1940
(1 Js So 581/40)

Ermittlungsverfahren gegen den Maschinen-
schlosser Franz Josef LINDER (geb. 7.

Nov. 1913) aus Rottenmann (Lkr. Deggen-
dorf), weil er verbreitet hat, daß "Kai-
serslautern und Umgebung zusammenge-

fg) und nur noch ein Steinhaufen sei"
HG

Verfahren eingestellt

13. Feb. 1940 - 8. Apr. 1940
(1 Js So 583/40)
