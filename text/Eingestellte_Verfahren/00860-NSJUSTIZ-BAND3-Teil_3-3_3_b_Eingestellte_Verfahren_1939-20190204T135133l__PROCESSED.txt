(4126)

(4127)

(4128)

(4129)

(4130)

5227

5228

5229

5230

5231

Ermittlungsverfahren gegen den Bauern Georg
BAUER (geb. 14. Dez. 1895) und dessen Ehe-
frau Theres (geb. 31. Mrz. 1893), beide
aus Burg (Lkr. Vilsbiburg), und den Land-
wirt Johann NIGGL (geb. 19. Mrz. 1908) aus
Putzing (Lkr. Vilsbiburg), wegen Äußerungen
über den Krieg.

(HG)

Verfahren eingestellt

11. Spt. 1939 - 17. Okt. 1939
(1 Js So 1456/3539)

Ermittlungsverfahren gegen die Wirtschafts-
besitzerin Anna SCHOPF ( eb. 7. Spt. 1900)
aus Anger (Lkr. Bad Tölz), wegen Äußerun-
en über das Regime.
HG)

Verfahren wegen Amnestie eingestellt

13. Spt. 1939 - 2. Nov. 1939
(1 Js So 1457/39)

Ermittlungsverfahren gegen den Metzger
Wilhelm BÜRZLE (geb. 22. Spt. 1913) aus
Bellenberg (Lkr. Illertissen), wegen ÄAuße-
Ci en über die Kriegsaussichten.

HG

Verfahren eingestellt

30. Aug. 1939 - 7. Okt. 1939
(1 Js So 1458/39)

Ermittlungsverfahren gegen die Landwirte
Ludwig ENGL (geb. 24. Aug. 1881) aus Mög-
ling (Lkr. Landau) und Max HOFMANN (geb.
2. Aug. 1872) aus Trieching (Lkr. Landau),
( Äußerungen über das Regime.

HG

Verfahren wegen Amnestie eingestellt

7. Spt. 1939 - 20. Nov. 1939
(1 Js So 1459/39)

Ermittlungsverfahren gegen die KaffeehauS
besitzerin Helena KUGELMANN (geb. 9. Aug.
1894) aus Schöngeising (Lkr. Fürstenfeld-
brück), wegen Äußerungen über das Regime.
(HG)

Verfahren eingestellt

27. Aug. 1939 - 20. Nov. 1939
(1 Js So 1460/39)
