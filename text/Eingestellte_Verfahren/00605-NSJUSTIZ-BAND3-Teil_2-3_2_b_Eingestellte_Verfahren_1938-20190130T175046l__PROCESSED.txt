(2886)

(2887)

(2888)

(2889)

(2890)

(2891)

4261

4259

4262

4263

4264

4265

Ermittlungsverfahren gegen den Zimmermann
Mathias BRAUMANDL (geb. 1. Jan. 1879), weil
er in München eine kritische Außerung über
das 3. Reich gemacht haben soll.

Verfahren wegen Amnestie eingestellt

27. Aug. 1938 - 25. Okt. 1938
(1a Js So 1859/38)

Ermittlungsverfahren gegen den Bauarbeiter
Georg DOLPP (geb. 14. Jun. 1897) aus Kempten,
weil er sich kritisch über die Sudetendeut-
schen geäußert haben soll.

Verfahren eingestellt

30. Spt. 1938 - 25. Okt. 1938
(1a Js So 1860/38)

Ermittlungsverfahren gegen den Maurergehil-
fen Joseph EHRENBÖCK (geb. 17. Mrz. 1900)
aus München, weil er die Regierung beschimpft
haben soll.

Verfahren eingestellt

7. Okt. 1938 - 28. Okt. 1938
(1a Js So 1861/38)

Ermittlungsverfahren gegen den Schlosserge-
hilfen Andreas HUBER (geb. 9. Aug. 1905) aus
München, weil er sich gegen den Dienst beim
Militär ausgesprochen haben soll.

Verfahren eingestellt

28. Spt. 1938 - 31. Okt. 1938
(1b Js So 1864/38)

Ermittlungsverfahren gegen den Friseur Eduard
MÖSLE (geb. 24. Feb. 1881) aus Durach (Lkr.
Kempten), weil er über die Sudetendeutschen
und die deutsche Regierung geschimpft hat.

Verfahren eingestellt

3. Okt. 1938 - 13. Dez. 1939
(1c Js So 1865/38)

Ermittlungsverfahren gegen den Mechaniker
Alois SCHMIDBAUER (geb. 27. Dez. 1914) aus
München, früher SA-Mitglied, weil er sich
kritisch zur sudetendeutschen Frage geäußert
haben soll.

Verfahren eingestellt

22. Spt. 1938 - 27. Okt. 1938
(1d Js So 1866/38)
