(2355)

(2356)

(2357)

(2358)

(2359)

(2360)

3746

3747

3748

3749

3750

3751

Ermittlungsverfahren gegen die
Strickerin Antonie AICHNER (geb.

4. Mai 1901) aus München wegen
angeblicher politischer Bemerkungen.

Verfahren wegen Amnestie eingestellt

26. Mrz. 1938 - 13. Mai 1938
(1a Js So 802/38)

Ermittlungsverfahren gegen den Gastwirt
Gregor PRÄSSLER (geb. 19. Feb. 1901)
aus Sudaäfeld (Lkr. Rosenheim) wegen
angeblicher politischer Außerungen.

Verfahren eingestellt

2. Mrz. 1938 - 3. Mai 1938
(1b Js So 803/38)

Ermittlungsverfahren gegen die Bäuerin
Franziska HANDSCHUHER - 15. Okt.
1899) aus Oberhaching (Lkr. München)
wegen politischer Klatschereien.

Verfahren eingestellt

7. Apr. 1938 - 2. Mai 1938
(1a Js So 804/38)

Ermittlungsverfahren gegen den Knecht

Max HARTMANN (geb. 26. Apr. 1910) aus

Mooseurach (Lkr. Wolfratshausen), weil
er das Winterhilfswerk einen "Krampf"

nannte.

Verfahren eingestellt

29. Mrz. 1938 - 2. Mai 1938
(1a Js So 805/38)

Ermittlungsverfahren gegen den Kauf-
mann Friedrich SCHLAMP (geb. 3. Mrz.
1871) aus München wegen politischer
Äußerungen.

Verfahren eingestellt

11. Apr. 1938 - 24. Mai 1938
(1c Js So 808/38)

Ermittlungsverfahren gegen den Bauern
Franz Xaver WÖLLINGER (geb. 17. Dez.
1895) und seine Frau Elisabeth (geb.
23 Okt. 1899) aus Bauhof (Lkr. Ebers-
berg) wegen Äußerungen über Hitler,
Vaterland und Einmarsch in Österreich.

Verfahren eingestellt
