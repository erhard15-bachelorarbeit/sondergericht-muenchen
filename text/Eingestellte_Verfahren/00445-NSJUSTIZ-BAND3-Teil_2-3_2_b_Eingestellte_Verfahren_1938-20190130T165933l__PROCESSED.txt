(2074)

(2075)

(2076)

(2077)

(2078)

3466

3467

3468

3471

3470

Ermittlungsverfahren gegen den
Studenten Josef KNOTT (geb. 29.
Apr. 1915) aus München, früher
Stahlhelm-, dann SA-Mitglied,
wegen Außerungen über das Vor-
gehen des nat.soz. Fachschafts-
leiters in der zeitungswissen-
schaftlichen Fachschaft der
Münchener Universität.

Verfahren eingestellt

14. Dez. 1937 - 3. Feb. 1938
(1b Js So 224/38)

Ermittlungsverfahren gegen den Ziegel-
brenner Johann HABERL (geb. 16. Jun.
1893) aus München, früher SPD-, dann
USP-, dann KPD-Mitglied, wegen an-
geblicher Äußerungen gegenüber Aus-
ländern über die Lage der Arbeiter,
Hitler und die Zeitungen.

Verfahren eingestellt

28. Dez. 1937 - 9. Feb. 1938
(1b Js So 225/38)

Ermittlungsverfahren gegen die
Viktualienhändlerstochter Katharina
WOLF (geb. 25. Okt. 1906) aus Mitten-
wald (Lkr. Garmisch-Partenkirchen)
wegen einer angeblichen Außerung

von einem künftigen Vierten Reich.

Verfahren eingestellt

18. Jan. 1938 - 10. Feb.1938
(1c Js So 229/38)

Ermittlungsverfahren gegen den
Schlosser Franz Xaver DORFNER (geb.
27. Feb. 1913) aus Krailling (Lkr.
Starnberg) wegen der Erzählung, Göring
habe eine Zeche von 20 000 RM gemacht.

Verfahren eingestellt

1. Aug. 1937 - 25. Feb. 1938
(1a Js So 232/38)

Ermittlungsverfahren gegen den
Schlossermeister Josef KLEBER

(geb. 2. Jan. 1375) aus München,

weil er die Nat.Sozz. als "Blutshitler"
beschimpft haben Soll.

Verfahren eingestellt

23. Nov. 1937 - 25. Feb. 1938
(1b Js So 233/38)
