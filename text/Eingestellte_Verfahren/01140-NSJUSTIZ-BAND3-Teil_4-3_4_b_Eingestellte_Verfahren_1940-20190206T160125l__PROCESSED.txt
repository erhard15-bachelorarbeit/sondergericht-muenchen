(5412)

(5413)

(5414)

(5415)

(5416)

6054

6055

6132

6056

6057

Verfahren eingestellt

31. Mrz. 1940 - 21. Spt. 1940
(1 Js So 1568/40)

Ermittlungsverfahren gegen den Pförtner
Michael STEET (geb. 10. Jan. 1892) aus
Hart (Lkr. Altötting), weil er den Krieg
und das Dritte Reich als "Riesenschwindel"
bezeichnet haben soll.

(& 2 HG)

Verfahren ei: gestellt

9. Aug. 1940 - 17. Okt. 1940
(1 Js So 1572/40)

Ermittlungsverfahren gegen den Kaufmann
Wilhelm von SCHEIDT (geb. 4. Feb. 1888)
aus München wegen unberechtigten Tragens
von Parteiabzeichen.

(HG)

Verfahren eingestellt

8. Aug. 1940 - 29. Nov. 1940
(1 Js So 1573/40)

Ermittlungsverfahren gegen den Bauern
Georg FROSCHMEIER (geb. 23. Mrz. 1900) aus
Günding (Lkr. Dachau),weil er gesagt ha-
(6) daß die SS nichts tauge.

HG

Verfahren eingestellt

3. Mai 1940 - 1. Mrz. 1941
(1 Js So 1575/40)

Ermittlungsverfahren gegen den kath. Pfar-
rer Benno LANG (geb. 20. Dez. 1882) aus
Mariakirchen (Lkr. Eggenfelden), weil er
mit der Andeutung einer möglichen Geld-
entwertung zu Großzügigkeit bei einer
(a) mung aufgefordert haben soll.
HG

Verfahren eingestellt

31. Aug. 1940 -19. Dez. 1940
(1 Js So 1577/40)

Ermittlungsverfahren gegen den Reichs-
bahnrottenmeister Max WINKLHOFER (geb. 2.
Jan. 1892) aus Neufahrn i. NB (Lkr.
Mallersdorf) wegen abfälliger Äußerungen
über die SA.

(88 1,2 HG)

Verfahren eingestellt

16. Mrz. 1940 - 19. Okt. 1940
(1 Js So 1578/40)
