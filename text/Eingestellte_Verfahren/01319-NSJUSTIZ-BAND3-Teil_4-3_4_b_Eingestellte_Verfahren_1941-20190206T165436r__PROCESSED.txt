Ermittlungsverfahren gegen den Nilfsarbeiter
Gabriel HUTTERER (geb. 6. Mrz. 1891) aus
Feldolling (Lkr. Bad Aibling) wegen belei-
digender Äußerungen über Hitler und ille-
aler Betätigung als Bibelforscher.

HG; VO v. 28. Feb. 1933)

Verfahren eingestellt

8. Aug. 1941 - 24. Spt. 1941
(1 Js So 576/41)

Ermittlungsverfahren gegen die Verkäuferin
Elisabeth DRUMMER (geb. 20. Okt. 1906) aus
Berchtesgaden, weil sie kritisiert haben
soll, daß "die Hohen am Obersalzberg alles
hätten und daß in der Reichskanzlei die
u) geschlemmt werde".

HG

Verfahren eingestellt

5. Aug. 1941 - 9. Apr. 1942
(1 Js So 579/41)

Ermittlungsverfahren gegen die Beamtenehefrau
Katharina SCHEUFLER (geb. 26. Mai 1889)

aus Gronsdorf (Lkr. München), weil sie den
Deutschen Gruß nicht benützt und Hitler

iQ) Fb bezeichnet haben soll.

HG

Verfahren eingestellt

21. Mai 1941 - 27. Aug. 1941
(1 Js So 585/41)

Ermittlungsverfahren gegen die Verkäuferin
Elisabeth KIENLE (geb. 18. Jul. 1896) aus
Kempten wegen abfälliger Äußerungen über
die SA und die Gestapo.

($ 2 HG)

Verfahren eingestellt

30 . Mai 1941 - 9. Jan. 1942
(1 Js So 589/41)

Ermittlungsverfahren gegen den Friseur Adam
AKSENRUS (geb. 15. Jul. 1915) aus München

(5 unberechtigten Tragens einer SA-Bluse.
HG

Verfahren eingestellt

2. Jul. 1941 - 17. Dez. 1941
(1 Js So 593/41)
