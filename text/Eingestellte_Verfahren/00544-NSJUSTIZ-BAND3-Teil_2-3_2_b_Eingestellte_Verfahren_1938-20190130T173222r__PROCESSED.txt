(2553)

(2554)

(2555)

(2556)

(2557)

3941

3942

3943

3944

3945

Ermittlungsverfahren gegen die Kunst-
malerin Maria BILLING (geb. 12. Feb.
1870) , die Säuglingsschwester Therese
PONN (geb. 14. Jan. 1899), Maria ARNOLD
(geb. 30. Spt. 1891), alle aus München,
wegen angeblicher Betätigung als Ernste
Bibelforscher.

Verfahren wegen Amnestie eingestellt

23. Mai 1938 - 7. Jun. 1938
(1e Js So 1080/38)

Ermittlungsverfahren gegen den
Schirmmacher Anton BRILLER (geb.
25. Mai 1886) aus Viechtach wegen
angeblicher Beschimpfung der NSDAP.

Verfahren eingestellt

11. Apr. 1938 - 13. Jun. 1938
(1a Js So 1082/38)

Ermittlungsverfahren gegen die Marianne
SCHWAB (geb. 26. Feb. 1883) aus Augs-
burg wegen Verächtlichmachung des
Deutschen Grußes.

Verfahren wegen Amnestie eingestellt

5. Apr. 1938 - 4. Jun. 1938
(1d Js So 1084/38)

Ermittlungsverfahren gegen den Ange-
stellten Georg WEGERTSEDER (geb. 21.
Apr. 1897) aus Deggendorf, NSDAP-
Mitglied, früher SA-Mitglied, wegen
Erzählung von "Schlageterfeiern" im
KZ Dachau.

Verfahren wegen Amnestie eingestellt

15. Jan. 1938 - 9. Jun. 1938
(1d Js So 1085/38)

Ermittlungsverfahren gegen den
Metzger Peter ZEHETMAIR - 6.
Aug. 1909) aus Burgrain (Lkr.
Wasserburg) wegen der Bezeichnung
Hitlers als "Dreckbazi".

Verfahren wegen Amnestie eingestellt

24. Mai 1938 - 3. Jun. 1938
(1d Js So 1088/38)
