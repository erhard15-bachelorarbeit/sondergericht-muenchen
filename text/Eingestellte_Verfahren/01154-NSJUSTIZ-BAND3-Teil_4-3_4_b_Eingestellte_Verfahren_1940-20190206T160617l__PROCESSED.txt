(5482)

(5483)

(5484)

(5485)

(5486)

6122

6123

6124

6125

6126

Verfahren eingestellt

15. Okt. 1940 - 19. Feb. 1941
(1 Js So 1825/40)

Ermittlungsverfahren gegen Johanna STEIGER
(o. Berufsangabe und Geburtsdatum) aus
Donauwörth wegen abfälliger Äußerungen
über Hitler und Beleidigung der Wehrmacht.
5. begeht am 27. Nov. 1940 Selbstmord.

($ 134a StGB)

Verfahren eingestellt

29. Nov. 1940 - 3. Jan. 1941
(1 Js So 1833/40)

Ermittlungsverfahren Ben die Kontoristin
Annemarie KELLER (geb. 6. Mrz. 1918) aus
München, weil sie bedauert haben soll,
daß Hitler bei dem Attentat im Bürgerbräu-
keller nicht getötet wurde.

(HG)

Verfahren eingestellt

9. Dez. 1940 - 8. Mrz. 1941
(1 Js So 1836/40)

Ermittlungsverfahren gegen den Apotheker
Bernhard SEEGER (geb. 13. Mrz. 1887), den
Kaufmann August LEISER (geb. 26. Nov. 1870
und den Kaufmann WilhelmvSCHEIDT (geb.

4. Feb. 1888) aus Munchen wegen verschie-
dener abfälliger Außerungen über das
Dritte Reich und Verbreitung von Gerüch-
ten über den Reichsführer SS Heinrich
Himmler.

(HG)

Verfahren eingestellt

24. Jan. 1940 - 20. Jan. 1941,
(1 Js So 1840/40)

Ermittlungsverfahren gegen den Dompfarrer
Johannes KRAUS (geb. 26. Nov. 1890) aus
Eichstätt wegen Außerungen über die Eich-
stätter Polizei.

($ 2 HG; 88 130a,185 StGB)

Verfahren eingestellt

26. Nov. 1940 - 13. Okt. 1941
(1 Js So 1842/40)

Ermittlungsverfahren gegen den Bauern Jo-
sef ANGERMAIER (geb. 18. Aug. 1896) und
seine Frau Walburga (geb. 16. Feb. 1893) a
Langenmosen (Lkr. Schrobenhausen) wegen
beleidigender Äußerungen über die Regierun
