(5120)

(5121)

(5122)

(5123)

(5124)

5763

5764

5765

5766

5767

Ermittlungsverfahren gegen den HNilfsar-ı
beiter Ulrich SCHMID (geb. 20. Apr. 1668
aus Aislingen (Lkr. Dillingen a.d. Donau)
wegen abfälliger Äußerungen über die Re-
gierung, insbesondere über Propagandani-
nister Goebbels.

(HG)

Verfahren eingestellt

20. Dez. 1939 - 12. Mrz. 1940
(1 Js So 403/40)

Ermittlungsverfahren gegen den Domprediger
Johann Nepomuk OBERSTALLER (geb. 10. Spt.
1879) aus Augsburg, weil er die Füiirchen-
feindliche Politik des NS kritisiert ha-
ben soll.

(3 2 HG; 3 130a StGB)

Verfahren eingestellt

19. Feb. 1940 - 2. Aug. 1940
(1 Js So 404/40)

Ermittlungsverfahren gegen den Ingenieur
Alexander SATTLER (geb. 9. Jun. 16881) aus
Kempten, weil er bei einer Diskussion über
den Polenkrieg gesagt haben soll: "Jetzt
ist der Hitler erledigt."

(HG)

Verfahren eingestellt

30. Jan. 1940 - 17. Jun. 1940
(1 Js So 405/40)

Ermittlungsverfahren gegen den Landwirt
Josef BRUTSCHER (geb. 14. Jul. 18983) aus
Laufenegg (Lkr. Sonthofen), weil er u.a.
gesagt haben soll, daß die"*heutige, glau-
benlose Regierung weg müsse".

(5 2 HG)

Verfahren eingestellt

4. Jan. 1940 - 10. Jun. 1940
(1 Js So 406/40)

Ermittlungsverfahren gegen den Bauern Jo-
hann SCHMID (geb. 23. Mrz. 1905) aus Berb-
ling (Lkr. Bad Aibling), weil er nach dem
Attentat im Bürgerbräukeller gesagt haben
soll: "Es wäre nicht schade gewesen, wenn
es den Führer erwischt hätt"

($ 2 HG)

Verfahren eingestellt

22. Nov. 1939 - 14. Mrz. 1940
(1 Js So 407/40)
