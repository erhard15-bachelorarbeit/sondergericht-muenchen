(5281)

(5282)

5283)

(5284)

(5285)

5934

5935

5936

5937

Ermittlungsverfahren gegen den Schlosser
Josef RUPEZ (geb. 24. Apr. 1915), z. Zzt.
der Anzeige im Gefängnis München-Stadel-
heim, wegen unberechtigten Besitzes eines
Parteiabzeichens.

($ 5 HG)

Verfahren eingestellt

2. Apr. 1940 - 7. Jun. 1940
(1 Js So 1087/40)

Ermittlungsverfahren gegen den Bauern
Alois WUNDERER (geb. 20. Mrz. 1879) aus
Bibelsberg (Lkr. Memmingen). Er soll
gesagt haben, daß man noch froh sein wer-
de, wenn die Franzosen kämen und "uns
von Hitler erlösen".

(HG)

Verfahren eingestellt

29. Spt. 1939 - 30. Mai 1940
(1 Js So 1089/40)

Ermittlungsverfahren gegen den Landwirt
Michael BERNHARD (geb. 26. Spt. 173)
aus Unterspechtrain (Lkr. Dingolfing),
weil er den Einmarsch in Dänemark und
Norwegen als "Betrug" bezeichnet haben
soll.

(HG)

Verfahren eingestellt

4. Mai 1940 - 10. Jun. 1940
(1 Js So 1091/40)

Ermittlungsverfahren gegen die Vertreterin
Rosa BEER (geb. 1. Jan. 1894) aus Inns-
bruck (Österreich), weil sie in Dingolfing
verbreitet hat, daß die Ortsgruppenlei-
tung an der Einberufung mehrer Männer
schuld sei.

(HG)

Verfahren eingestellt

19. Mrz. 1940 - 20. Spt. 1940
(1 Js So 1095/40)

Ermittlungsverfahren gegen den Hilfsar-
beiter Eduard HÄMMERLE (geb. 30. Mrz. 1889)
aus Welden (Lkr. Augsburg) wegen abfälliger
(a) über die "Oberen".

HG

Verfahren eingestellt
