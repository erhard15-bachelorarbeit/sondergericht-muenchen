3588)

(3589)

(3590)

(3591)

(3592)

4/36

4739

4740

4741

4742

Ermittlungsverfahren gegen den jJugosla-
wischen Staatsangehörigen und Schneider An-
ton VRECER (geb. 20. Mai 1884) aus München,
weil er sich beleidigend über Hitler ge-
äußert haben soll.

(HG)

Verfahren eingestellt

Vrecer wird in Haft genommen; V. wird am
7. Jul. nach Jugoslawien abgeschoben.

12. Mrz. 1939 - 17. Aug. 1939
(1 Js So 428/39)

Ermittlungsverfahren gegen den Friseur
Eduard NIEDERREUTHER (geb. 30. Apr. 1895)
aus Ebersberg, weil er gesagt haben soll,
daß im Völkischen Beobachter nur "Betrug
und Schwindel" stehe.

(HG)

Verfahren eingestellt

7. Mrz. 1939 - 30. Mrz. 1939
(1 Js So 431/39)

Ermittlungsverfahren gegen den Zimmermann
Wenzel KOPP (geb. 23. Jul. 1899) aus Holle-
titz (CSSR), weil er gesagt haben soll:

"...bei der SA und bei der NSDAP sind lauter
Gauner."

($$ 134b StGB)

Verfahren eingestellt

9. Feb. 1939 - 30. Jun. 1939
(1 Js So 436/39)

Ermittlungsverfahren gegen den Landwirt
Michael SCHEDLBAUER (geb. 4. Dez. 1885)
aus Ogleinsmais (Lkr. Viechtach), weil er

sich abfällig über die Regierung geäußert
haben soll.

(HG)

Verfahren eingestellt

28. Feb. 1939 - 28. Mrz. 1939
(1 Js So 437/39)

Ermittlungsverfahren gegen den Arbeiter-
sohn Jakob ULRICH (geb. 28. Mrz. 1923)
aus Podrasnitz (CSSR), weil er jemanden
(a) "Hitlersau" genannt haben soll.

HG

Verfahren eingestellt
