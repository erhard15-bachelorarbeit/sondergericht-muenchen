(4037)

(4038)

(4039)

(4040)

5148

5457

53159

5458

Verfähren eingestellt

2. Spt. 1939 - 22. Nov. 1939
(1 Js So 1333/39)

Prozeß gegen den Rentner Josef BEER (geb.
26. Jun. 1877) aus Rain (Lkr. Miesbach)

weil er Hitler als "den größten Drücke-

er, den es gibt" bezeichnet haben soll.
HG

Verfahren eingestellt

3. Spt. 1939 - 18. Okt. 1939
(1 Js So 1335/39)

Ermittlungsverfhren gegen die Bauerstoch-
ter Kreszenz ALTMANN (geb. 30. Mrz. 1895)
die Bauersfrau Elisabeth ALTMANN (geb. 5.
Mai 1898), beide aus Bergarn (Lkr. Erding)
und den kath. Pfarrer Josef ELFINGER (geb.
11. Feb. 1893) aus Bockhorn (Lkr. Erding),
wegen der Äußerung, der Staat sei in
Cu) ad von der Kirche getrennt.
HG

Verfahren eingestellt

4. Aug. 1939 - 21. Mrz. 1940
(1 Js So 1336/39)

Ermittlungsverfahren gegen die Krämerei-
inhaberin Berta KRAUS (geb. 26. Jun. 1905)
aus München, weil sie gesagt haben soll,
daß nicht die Polen,sondern "eine andere
Seite" den Krieg wolle.

(HG)

Verfahren eingestellt

30. Aug. 1939 - 25. Spt. 1939
(1 Js So 1338/39)

Ermittlungsverfahren gegen die techn. An-
gestellten Franz STURM (geb. 6. Jan. 1901)
aus Lochham (Lkr. München) und Josef
STADLER (geb. 3. Feb. 1903) aus München,
(ne politischer Bemerkungen.

HG

Verfahren wegen Amnestie eingestellt

4. Spt. 1939 - 23. Feb. 1940
(1 Js So 17Z346/39)
