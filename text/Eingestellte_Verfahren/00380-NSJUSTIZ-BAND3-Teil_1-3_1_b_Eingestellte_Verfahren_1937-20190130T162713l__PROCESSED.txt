(1812)

(1813)

(1814)

(1815)

(1816)

3370

3371

3372

33732

3374

Ermittlungsverfahren gegen den kath.
Pfarrer Franz Xaver WOHNHAS tgeo.

9. Nov. 1883) aus Ludwigsmoos (Lkr.Schroben-
hausen), weil er während einer Predigt
u.a. Reichsminister Dr. Frick kritisiert
hat, weil dieser in einer Rede am 7. Jul.
1935 die "völlige Entkontessionalisierung"
verlangt hat.

Verfahren eingestellt wegen Amnestie

27. Nov. 193/ - 8. Jun. 1938
(1 c Js - So 1645/37)

Ermittlungsverfahren gegen den Geschäfts-
inhaber Josef MILLER (geb. 15. Okt. 1898)
aus München wegen abgeblicher abfälliger
Äußerungen über Oberhürgermeister Fiehler
und die mangelnde Pressefreiheit in Deutsch-
land.

Verfahren eingestellt

8. Spt. 1937 - 2. Mrz. 1938
(1 b Js - So 1647/37)

Ermittlungsverfahren gegen den Schlosser
Friedrich RAST (geb. 2. Mrz. 1901) aus

Hausham (Lkr. Miesbach) wegen Verbreitung
von Greuelnachrichten über das KZ Dachau.

Verfahren eingestellt
R. war 1 Jahr im KZ Dachau inhaftiert.

26. Nov. 1937 - 28. Feb. 1938
(1 c Js - So 1648/37)

Ermittlungsverfahren gegen den Hilfsarbeiter
Martin HARTL (geb. 6. Dez. 1889) aus Frauen-
dorf (Lkr. Mühldorf) wegen der angeblichen
Behauptung, daß die Bauern heute in Deutsch-
land kein Recht mehr haben, die deutschen
Zeitungen lügen und es nicht mehr lange
dauern werde, bis alles zusammenbreche.

Verfahren eingestellt

10. Spt. 1937 - 10. Feb. 1938
(1 b Js - So 1649/37)

Ermittlungsverfahren gegen den Malerge-
hilfen Paul FÜRST (geb. 23. Mai 1908

aus Ingolstadt, weil er gesagt haben soll:
"Das deutsche Volk wird von seiner Regie-
rung nur verblödet und verdummt".

Verfahren eingestellt

30. Nov. 1937 - 8. Feb. 1938
(1 a Js - So 1654/37)
