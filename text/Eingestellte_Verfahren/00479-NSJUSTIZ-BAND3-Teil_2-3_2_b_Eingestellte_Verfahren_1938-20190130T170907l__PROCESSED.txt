(2238)

(2239)

(2240)

(2241)

(2242)

3640

3641

3642

3623

3624

Ermittlungsverfahren gegen den Bäcker-
gesellen Julius STEINER (geb. 8. Mai
1902) aus München, weil er sich unbe-
rechtigt als Parteimitglied bezeichnet
und Parteiabzeichen getragen hat.

Verfahren eingestellt

6. Jan. 1938 - 3. Mai 1938
(ld Js So 579/38)

Ermittlungsverfahren gegen den kath.
Pfarrer Josef ERNST (geb. 18. Mrz.
1876) aus Fahlenbach (Lkr. Pfaffenhofen)
wegen einiger politischer Äußerungen in
einem Brief.

Verfahren wegen Amnestie eingestellt

14. Mrz. 1938 - 14. Mai 1938
(1a Js So 586/38)

Ermittlungsverfahren gegen die Hilfs-
arbeiterin Elsa KIEHL (geb. 15. Feb. 1899)
aus München wegen angeblicher Kritik am
Regime.

Verfahren wegen Amnestie eingestellt

20. Spt. 1937 - 10. Mai 1938
(1b Js So 587/38)

Ermittlungsverfahren gegen den Dompfarrer
Johannes KRAUS (geb. 26. Nov. 1890) und
den Domlaplan Markus HARRER (geb. 23.
Apr. 1905) aus Eichstätt, weil sie an-
läßlich des Namenstages des Bischofs von
Eichstätt das Losungswort "Für Christi
Reich und ein neues Deutschland" ausge-
geben haben sollen.

Verfahren eingestellt

19. Mrz. 1938 - 11. Aug. 1939
(1b Js So 589/a-b/38)

Ermittlungsverfahren gegen den Landwirt
Xaver WEINMANN (geb. 8. Mrz. 1895) aus
Mörslingen (Lkr. Dillingen), weil er
Hitler und Göring Lausbuben genannt haben
soll.

Verfahren wegen Amnestie eingestellt

W. wird in Schutzhaft genommen.

15. Mrz. 1938 - 24. Mai 1938
(1c Js So 591/38; 888/38)
