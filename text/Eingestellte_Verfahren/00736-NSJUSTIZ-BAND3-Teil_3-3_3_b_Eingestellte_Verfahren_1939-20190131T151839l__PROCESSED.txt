(3510)

(3511)

(3512)

(3513)

(3514)

4660

4661

4662

4663

4664

Ermittlungsverfahren gegen den Molkerei-
besitzer Adalbert BURGER (geb. 7. Okt.
1880) aus Wehringen (Lkr. Schwabmünchen),
weil er Hitler und Mussolini "Maurersbu-
ben" und die Regierung "eine Lumperei"
enannt haben soll.
HG)

Verfahren wegen Amnestie eingestellt

11. Feb. 1939 - 28. Feb. 1939
(1 Js So 262/39)

Ermittlungsverfahren gegen den Zimmermann
Josef ÖTTINGER (geb. 22. Mai 1900) aus
München, weil er einen SS-Hauptscharführer
und Blutordensträger beleidigt und ihm
vorgeworfen haben soll, er habe sich den
Blutorden erschwindelt.

(HG; 3 185 StGB)

Verfahren eingestellt

30. Nov. 1938 - 21. Mrz. 1939
(1 Js So 263/39)

Ermittlungsverfahren gegen den Bauern

Max KNOPF (geb. 15. Feb. 1897) aus Wallers-
dorf (Lkr. Landau a.d. Isar), weil er
Staatsminister Wagner einen "Muhakl und
Hammel" genannt haben soll.

(& 2 HG)

Verfahren eingestellt

11. Dez, 1938 - 14. Apr. 1939

(1 Js So 264/39) .
Ermittlungsverfahren gegen den Lagerarbeiter
Engelbert GSCHWIND (geb. 12. Apr. 1900)

aus Augsburg, weil er gesagt haben soll,
daß Deutschland 85 Milliarden RM Inland-
schulden habe, die durch die Abwertung
(a) gedeckt werden müßten.

HG

Verfahren eingestellt

13. Feb. 1939 - 27. Apr. 1939
(1 Js So 265/39)

Ermittlungsverfahren gegen den Ordens-
geistlichen Rupert SAMETSAMER (geb. 15.
Äug. 1911) aus Deggendorf. Er soll in einer
Predigt von "dunklen Elementem gesprochen
haben, die in Deutschland-ebenso wie bei
"sinem Volk im Osten" - "am Werke seien, Äda
heilige Weihnachtsfest abzuschaffen".
Weiter soll er kritisiert haben, daß heid-
nische Bräuche (Wintersonnenwende) wieder
eingeführt werden.
