(2704)

(2705)

(2706)

(2707)

(2708)

(2709)

4092

4093

4094

4095

4096

4097

Ermittlungsverfahren gegen die Maler-
meistersehefrau Maria STOIBER (geb, 29.
Apr. 1905) aus Zwiesel (Lkr. Regenj, weil
sie gegenüber einer Frau, deren Mann im
KZ Dachau inhaftiert ist, die Vermutung
geäußert haben soll, daß dieser bereits er
schossen worden sei.

Verfahren wegen Amnestie eingestellt

16. Jul. 1938 - 15. Aug. 1938
(1d Js So 1418/38)

Ermittlungsverfahren gegen den Monteur

Theodor OETTER (geb. 4. Mrz. 1885) aus

München, weil er sich abfällig über den
Vierjahresplan geäußert haben soll.

Verfahren eingestellt

10. Jul. 1938 - 18. Aug. 1938
(1c Js So 1421/38)

Ermittlungsverfahren gegen den Schuhmacher
Ludwig NEUMAYER (geb. 19. Aug. 1896) aus
München, weil er einen NSDAP-Zellenleiter
beIeidigt und gemeint haben soll, daß bald
die KPD die Macht ergreifen werde.

Verfahren eingestellt

7. Jul. 1938 - 12. Dez. 1938
(1c Js So 1422/38)

Ermittlungsverfahren gegen den.Syndikus
Dr. Josef KERN (geb. 5. Mai 1890) aus
München, weil er sich abfällig über die
Partei geäußert haben soll.

Verfahren wegen Amnestie eingestellt

7. Jul. 1938 - 1. Okt. 1938
(1b Js So 1425/38)

Ermittlungsverfahren gegen die Haus-
angestellte Therese NIEBER (geb. 21.
Okt. 1903) aus München, weil sie die
Vermutung geäußert haben soll, daß die
nat.soz. Regierung nicht mehr lange
an der Macht sein werde.

Verfahren eingestellt

18. Jul. 1938 - 15. Nov. 1938
(1c Js So 1430/38)

Ermittlungsverfahren gegen den Vertreter
Richard SUSCHKE (geb. 7. Jul. 1891) aus
München, weil er Parteimitglieder als
"Bonzen" bezeichnet haben soll.

Verfahren wegen Amnestie eingestellt

5. Jun. 1938 - 12. Aug. 1938
(1d Js So 1431/38) 543
