(4229)

(4230)

(4231)

(4232)

(4233)

5479

5318

5319

5320

5321

Ermittlungsverfahren gegen den Bäcker-
meister Konrad SCHNEIDER (geb. 31. Aug.
1890), Pg., aus Murnau (Lkr. Weilheim)
und die Straßenaufsehersehefrau Therese
REITMEIR (geb. 20. Mrz. 1901) aus Eschen-
lohe (Lkr. Garmisch-Partenkirchen), wegen
Äußerungen über den Pakt mit Rußland.

($ 2 HG)

Verfahren eingestellt

9. Spt. 1939 - 5. Feb. 1940
(1 Js So 1645/39; 1 Js So 92/40)

Ermittlungsverfahren gegen die Bauersfrau
Maria SCHWENDINGER (e- 4. Nov. 1909)
aus Neuhaus am Inn (Lkr. Passau), wegen
einer Äußerung über Hitler.

(HG)

Verfahren wegen Amnestie eingestellt

9. Spt. 1939 - 12. Dez. 1939
(1 Js So 1646/39)

Ermittlungsverfahren gegen die Metzgers-
ehefrau Josefine STROBL (geb. 19. Nov.
1879) aus Neubiberg (Lkr. München), wegen
politischer Außerungen.

(HG)

Verfahren wegen Amnestie eingestellt

5. Spt. 1939 - 12. Jan. 1940
(1 Js So 1648/39)

Ermittlungsverfahren gegen die Wehrmachts-
angestelltenehefrau Filomena WILHELM (geb.
15. Okt. 1899) aus Mittenwald (Lkr. Gar-
misch-Partenkirchen), wegen Abhörens aus-
ländischer Rundfunksender.

(8 5 Rdf.VO)

Verfahren eingestellt

29. Spt. 1939 - 23. Dez. 1939
(1 Js So 1649/39)

Ermittlungsverfahren gegen den Zimmermann
Franz KILLINGER (geb. 26. Okt. 1907) aus
Weißenstein (Gde. und Lkr. Regen), wegen
Äußerungen über den Krieg.

(HG)

Verfahren eingestellt

21. Spt. 1939 - 3. Nov. 1939
(1 Js So 1651/39)
