(2938)

(2939)

(2940)

(2941)

(2942)

(2943)

4312

4304

4305

4306

4307

4308

Ermittlungsverfahren gegen die Kaffeehausbe-
sitzerin Kreszenz FÄHNLE (geb. 27. Mrz. .1893)
aus Altusried (Lkr. Kempten), BVP-Mitglied,
weil sie kritische politische Bemerkungen
gemacht haben soll.

Verfahren eingestellt

17. Okt. 1938 - 2. Jan. 1939
(1a Js So 1960/38)

Ermittlungsverfahren gegen die Bäckersfrau
Therese SÜLZBERGER (geb. 24. Jan. 1908) aus
Bruckmühl (Lkr. Bad Aibling), weil sie sich
abwertend über Hitler geäußert haben soll.

Verfahren eingestellt

12. Okt. 1938 - 26. Jan. 1939 f
(1d Js So 1961/38)

Ermittlungsverfahren gegen den Hafnergehil-
fen August FUCHS (geb. 15. Feb. 1894) aus
München, weil er kritische politische Auße-
rungen gemacht haben soll.

Verfahren eingestellt

24. Jun. 1938 - 8. Nov. 1938
(1a Js So 1962/38)

Ermittlungsverfahren gegen den Hilfsarbeiter
Heinrich HAGN (geb. 12. Jul. 1891) aus Win-
den (Lkr. Pfaffenhofen), weil er sich kri-
tisch über die Sudetendeutschen geäußert
haben soll.

Verfahren eingestellt

26. Okt. 1938 - 18. Nov. 1938
(1b Js So 1963/38)

Ermittlungsverfahren gegen die Land- und Gast-
wirtin Maria AMPLETZER (geb. 12. Feb. 1890)
aus Sauerlach (Lkr. Wofratshausen), weil sie
eine abwertende Bemerkung über Hitler ge-
macht haben soll.

Verfahren eingestellt

16. Okt. 1938 - 17. Jan. 1939
(1a Js So 1964/38)

Ermittlungsverfahren gegen den Straßenbahn-
schaffner Kaspar KRAUS (geb. 12. Aug. 1875):
aus Landshut, weil er sich kritisch zur Su-
detenkrise geäußert haben soll. -

Verfahren eingestellt

2. Okt. 1938 - 18. Nov. 1938
(1b Js So 1968/38)
