(4988)

(4989)

(4990)

(4991)

(4992)

50625

5626

5629

5630

5631

Ermittlungsverfahren gegen den Ranrier-
gehilfen Friedrich Wilhelm OEBTTEL geb.
31. Mrz. 1916) aus Lindau, früher SS-
Mitglied, wegen einer Außerung über die
SA-Wehrmannschaften.

(HG)

Verfahren eingestellt

21. Dez. 1939 - 31. Jan. 1940
(1 Js So 40/40)

Ermittlungsverfahren gegen die Kranken-
pflegerin Ursula HERMANN (geb. 3. Mrz.
1879) aus Schon: (Lkr. Berchtesgaden)
sr einer Außerung uber Hitler.

HG

Verfähren eingestellt

9. Nov. 1939 - 24. Apr. 1940
(1 Js So 43/40)

Ermittlungsverfahren gegen den Zimmer-
meister Mathias MAYER (geb. 22. Spt. 1387)
und die Wirtschafterin Katharina SPECK-
MEIER (geb. 25. Mrz. 1903), beide aus
Grünwald (Lkr. Munchen), wegen politischer
Äußerungen.

(HG)

Verfahren wegen Amnestie eingestellt

26. Jan. 1940 - 3. Feb. 1941
(1 Js So 47/40)

Ermittlungsverfahren gegen den Rechtsan-
walt v. BARY und Hermann LANG aus Ettringen
( Mindelheim) wegen"Heimtückevergehens-
HG

Verfahren eingestellt, da keine konkreten
Vorwürfe gemacht wurden.

30. Dez. 1939 - 2. Okt. 1940

(1 Js So 49/40)

Ermittlungsverfahren gegen den Pelzgeschäfts-
inhaber Hermann CLAASSEN (geb. 2. Jan.
1898) aus München wegen politischer Äuße-
rungen.

(H)

Verfahren eingestellt

25. Jul. 1939 - 1. Mrz. 1940
(1 Js So 50/40)
