(3839)

(3840)

(3841)

(3842)

4967

4968

4969

4970

Verfahren eingestellt

20. Jun. 1939 - 12. Aug. 1939
(1 Js So 1003,39)

Ermittlungsverfahren gegen den Lagerar-
beiter Franz Xaver KRÄNZLE (geb. 25. Mrz.
1885) aus Dachau, wegen der ÄAußerung,
Franco baue die Kirchen auf und Hitler
reiße sie nieder.

(HG)

Verfahren eingestellt

23. Jun. 1939 - 22. Spt. 1939
(1 Js So 1004/39)

Ermittlungsverfahren gegen den Holzhauer
Alexander MÜLLER (geb. 24. Jan. 1899)
aus Tusset (CSSR), weil er in einem Onm-
nibus " Heil Moskau" rief.

(& 2 HG; 8 330 StGB)

Verfahren eingestellt

Die Ehefrau des M., Veronika Müller, wurde
1938 für mehrere Wochen in Schutzhaft
genommen.

18. Apr. 1939 - 25. Aug. 1939
(1 Js So 1005/39)

Ermittlungsverfahren gegen den Kaufmann
Josef SCHMELZER (geb. 23. Mrz. 1884) aus
Nürnberg, früher KPD-Mitglied, wegen
politischer Außerungen in Münchsmünster
(Lkr. Pfaffenhofen).

(HG)

Verfahren wegen Amnestie eingestellt

18. Apr. 1939 - 9. Mai 1940
(1 Js So 1008/39)

Ermittlungsverfahren gegen den Studenten
Morgens Erich HOLBO (geb. 23. Feb. 1916)
und den Zimmermann Carno Christian Wiilhel
HOFMANN (geb. 13. Jun. 1913), beide dä-
nische Staatsbürger aus München, wegen
politischer Außerungen.

(HG)

Verfahren nach Reichsverweisung eingestel

7. Jul. 1939 - 28. Spt. 1939
(1 Js So 1010/39)
