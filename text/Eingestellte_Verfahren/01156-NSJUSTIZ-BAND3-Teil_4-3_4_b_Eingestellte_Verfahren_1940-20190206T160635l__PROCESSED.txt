(5491)

(5492)

(5493)

(5494)

(5495)

2758

6025

6031

5776

5785

knecht Johann HUMMEL (geb. 27. Jan. 1905)
aus Schwarzenbühlers (Lkr. Kempten) wegen
Abhörens ausländischer Rundfunksender.
(Rdf.VO)

Ermittlungsverfahren (as. den Dienst-

Verfahren eingestellt

8. Feb. 1940 - 19. Mrz. 1940
(1 Js So 310/40; K 116/40)

Prozeß gegen den Hausbesitzer Adalbert
LINDER (geb. 24. Apr. 1872) aus Oberkirch
(Lkr. Füssen) wegen abfälliger Bemerkungen
über die Höhe staatlicher Zuschüsse für
die Kirche.

(HG)

Verfahren wegen Amnestie eingestellt

26. Aug. 1939 - 5. Spt. 1940
(1 Js So 1471/40; K 136/40)

Prozeß gegen den Kaufmann Joseph DIRR
(geb. 20. Mai 1899) aus Bihlerdorf (Lkr.
oe) wegen Äußerungen über den NS.
HG

Verfahren eingestellt

5. Mrz. 1940 - 9. Spt. 1940
(1 Js So 1490/40; K 145/40)

Ermittlungsverfahren gegen die Flicknä-
herin Anna LERMER (geb. 19. Nov. 1889)
aus Laberweinting (Lkr. Mallersdorf), weil
sie behauptet haben soll, daß Deutschland
den Krieg verlieren werde.

(HG)

Verfahren eingestellt

9. Feb. 1940 - 26. Apr. 1940
(1 Js So 432/40; K 150/40)

Ermittlungsverfahren gegen die Fabrikarbei-
terin Maria MÜLLER (geb. 24. Apr. 19900)
aus Prachatitz (CSSR) wegen abfälliger
Äußerungen über das Dritte Reich, Hitler
und Goebbels.

($ 2 HG)

Verfahren eingestellt

21. Feb. 1940 - 4. Jun. 1940
(1 Js So 466/40; K 162/40)
