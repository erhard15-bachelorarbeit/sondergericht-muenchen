(5336)

(5337)

(5338)

(5339)

(5340)

2990

2991

5992

53993

5994

Ermittlungsverfahren gegen die berufslose
Maria BILLMAIER (geb. 21. Mai 1910) aus
Hengersberg (Lkr. Deggendorf) wegen kri-
tischer Bemerkungen über die HJ und den
(6) über die Reichskristallnacht.

Verfahren eingestellt

20. Mai 1940 - 26. Jul. 1940
(1 Js So 1373/40)

Ermittlungsverfahren gegen den Landwirt
Michael NIRSCHL (geb. 23. Jul. 1881) aus
Auerbach (Lkr. Deggendorf) wegen beleidi-
8) Äußerungen über die Regierung.

HG

Verfahren eingestellt

19. Mrz. 1940 - 31. Jul. 1940
(1 Js So 1374/40)

Ermittlungsverfahren gegen den Straßen-
aufseher Josef FREY (geb. 16. Jan. 1883)
aus Nordendorf (Lkr. Donauwörth) wegen
Bemerkungen zum Wahrheitsgehalt der Zei-
c) und Rundfunkberichterstattung.
HG

Verfahren eingestellt

15. Mai 1940 - 9. Dez. 1940
(1 Js So 1379/40)

Ermittlungsverfahren gegen den Schreiner-
meister Michael HÄMMERLE (geb. 26. Apr.
1875) aus Haunstetten (Lkr. Augsburg),wei
er abfällige Bemerkungen über Hitler ge-
macht und bedauert haben soll, daß der
Attentäter vom Bürgerbräukeller gefaßt
worden sei.

(HG)

Verfahren eingestellt

3. Jul. 1940 - 17. Okt. 1940
(1 Js So 1381/40)

Ermittlungsverfahren gegen den Bauern Pau
WITZIGMANN (geb. 25. Jan. 1899) aus Bo-
dolz (Lkr. Lindau), weil er gesagt haben
soll, daß er Hitler erschießen wolle, so-
bald sich eine Gelegenheit böte.

(HG)

Verfahren eingestellt

24. Jun. 1940 - 1. Aug. 1940
(1 Js So 1383/40)
