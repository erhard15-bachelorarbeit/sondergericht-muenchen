(2779)

(2780)

(2781)

(2782)

(2783)

FIV)

4164

4244

4165

4166

4167

Ermittlungsverfahren gegen den Bauern Micha
el STEBER (geb. 8. Jul. 1900) aus Peretsho-
fen (Lkr. Fürstenfeldbruck), weil er den
Führer, die Kreisbauernschaft und die Kreis-
leitung beleidigt haben soll.

Verfahren eingestellt

24. Aug. 1938 - 10. Jan. 1939
(1d Js So 1606/38)

Ermittlungsverfahren gegen den Maschinen-
Schlosser Josef WITTMANN (geb. 11. Nov. 1889)
äus Augsburg, weil er die militärischen Maß-
nahmen im Zusammenhang mit der Angliederung
Österreichs an das Reich kritisiert und den
Einmarsch der deutschen Truppen als "Gemein-
heit" bezeichnet haben soll.

Verfahren wegen Amnestie eingestellt

6. Spt. 1938 - 20. Spt. 1938
(1d Js So 1608/38)

Ermittlungsverfahren gegen den Tischler Willi
BELLWITZ (geb. 3. Mrz. 1911) aus Waltershau-

sen (Thüringen), weil er bei der Rückkehr aus
der Schweiz eh Exemplar des "Neuen Vorwärts"

mit sich führte.

Verfahren eingestellt

Anl.: 1 "Neuer Vorwärts" vom 19. Jun. 1938

17. Aug. 1938 - 17. Okt. 1938
(1a Js So 1544, 1823/38)

Ermittlungsverfahren gegen den Schreiner Karl
CSANEK (geb. 2. Jul. 1898) aus Weidach (Lkr.
Wolfratshausen), ungarischer Staatsburger,
weil er Hitler einen "Maurergesellen" genannt
haben soil.

Verfahren wegen Amnestie eingestellt

8. Aug. 1938 - 25. Spt. 1938
(1a Js So 1614/38)

Ermittlungsverfahren gegen den Kraftfahrer
Hyazinth WÖHRLE (geb. 11. Jun. 1901) aus Mün-
chen, weil er den Munchner Oberburgermeister
Fiehler beleidigt haben soll.

Verfahren eingestelit

4. Spt. 1938 - 14. Nov. 1938
(1d ds So 1616/38)

Ermittlungsverfahren gegen den Oberingenieur
karl HAGGENMILLER (geb. 18. Jul. 1875) aus
München, weil er kritische politische Auße-
rungen gemacht haben soll.

Verfahren wegen Amnestie eingestellt

49. Aug. 1938 - 19. Spt. 1938
„7 30 1622/38) 557
