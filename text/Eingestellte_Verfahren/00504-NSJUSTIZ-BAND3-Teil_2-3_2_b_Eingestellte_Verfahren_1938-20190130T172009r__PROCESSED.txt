(2366)

(2367)

(2368)

(2369)

(2370)

3758

3759

3760

3761

Ermittlungsverfahren gegen Johanna
ROHDE (geb. 14. Jan. 1895) aus Neu-
Ulm wegen der angeblichen Bezeichnung
führender Parteimitglieder in Neu-
Ulm als Hanswursten.

Verfahren eingestellt

15. Mrz. 1938 - 2. Mai 1938
(1d Js So 817/38)

Ermittlungsverfahren gegen den Land-
wirt Georg WAGNER (geb. 21. Mrz. 1885)
aus Ziertheim (Lkr. Dillingen) wegen
angeblichen Schimpfens über den Reichs-
bauernführer Darree.

Verfahren eingestellt

15. Mrz. 1938 - 2. Mai 1938
(1d Js So 818/38)

Ermittlungsverfahren gegen den Dipl.
Landwirt Wilhelm WÖRWÄAG (geb. 25.
Okt. 1897) aus Neu-Ulm wegen angeb-
licher Beleidigung Hitlers und der
Behauptung, das Dritte Reich sei ein
"Hundestaat".

Verfahren eingestellt

15. Mrz. 1938 - 3. Mai 1938
(1d Js So 820/38)

Ermittlungsverfahren gegen den
Apotheker Georg STOBER (geb. 10.
Jan. 1893) aus Regen wegen angeb-
lich wiederholter Außerungen über
das Dritte Reich.

Verfahren eingestellt

14. Apr. 1938 - 4. Spt. 1938
(1d Js So 821/38, 951/38)

Ermittlungsverfahren gegen den Bäcker
Rupert STEINBRECHER (geb. 9. Aug. 1915)
aus Murnau (Lkr. Weilheim) wegen unbe-
rechtigten Tragens einer NSKK-Uniform.

Verfahren eingestellt

12. Apr. 1938 - 3. Mai 1938
(1e Js So 822/38)
