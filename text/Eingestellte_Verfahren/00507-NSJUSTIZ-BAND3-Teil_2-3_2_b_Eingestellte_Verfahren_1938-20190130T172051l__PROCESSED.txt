(2381)

(2382)

(2383)

(2384)

(2385)

76

VG

3775

3774

3775

53776

Johann GASSNER (geb. 11. Jul. 1903) aus
Göggenhofen (Lkr. Bad Aibling) wegen
einer angeblichen ÄAußerung bei einer
Versammlung in "gemeiner Weise über
den Führer und das Dritte Reich".

Verfahren eingestellt

30. Mrz. 1938 - 5. Mai 1938
(1d Js So 839/38)

Ermittlungsverfahren gegen den kath.
Pfarrer Franz SCHÖNMETZLER (geb. 22.
Feb. 1905) aus Frankenhofen (Lkr. Kauf-
beuren), weil er bei der öffentlichen
Verlesung eines Erlasses von Gauleiter
Wagner über die deutsche Gemeinschafts-
schule gesagt haben soll: "Ich erhebe
dagegen feierlichen Protest ! Das ist
eine Lüge."

Verfahren eingestellt
Anlage: 1 Abschrift des Erlasses

4. Apr. 1938 - 19. Mai 1938
(1d Js So 845/38)

Ermittlungsverfahren gegen den Senats-
präsidenten a.D. Dr. Heinrich SCHULTZ
(geb.27. Dez. 1867) aus München wegen
angeblicher Verweigerung des Deutschen
Grußes und der Äußerung, daß "der ganze
NS ein ausgemachter Schwindel" sei.

Verfahren wegen Amnestie eingestellt

14. Jan. 1938 - 9. Dez. 1938
(1d Js So 847/38)

Ermittlungsverfahren gegen den Bauern
Jakob WEISS (geb. 10. Jan. 1899) aus
Billenhausen (Lkr. Krumbach) wegen an-
geblicher Äußerungen über das Dritte
Reich und der Bezeichnung Hitlers als
Lumpen.

Verfahren eingestellt

9. Apr. 1938 - 25. Nov. 1938
(1d Js So 849/38)

Ermittlungsverfahren gegen den Friseur-
meister Ernst THRIENE (geb. 18. Mrz.
1889) aus München, weil er sich abfällig
über Göring und Himmler geäußert und
außerdem Hitler einen Falott genannt
haben soll, "der erschossen gehört".

Verfahren wegen Amnestie eingestelit

14. Apr. 1938 - 20. Mai 1938
(1d Js So 850/38)
