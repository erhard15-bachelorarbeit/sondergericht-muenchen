(4400)

(4401)

(4402)

(4403)

(4404)

5551

5552

53554

5255

5556

Ermittlungsverfahren gegen den Immobilien-
händler Georg HETTERICH (geb. 2. Jan.
1885) aus Fürstenfeldbruck, wegen Verbrei-
tung eines Gerüchtes, daß für den 9. Nov.
(0) ein Attentat vorbereitet sei.

HG

Verfahren eingeställt

10. Nov. 1939 - 19. Jan. 1940
(1 Js So 2168/39)

Ermittlungsverfahren gegen den Hilfsar-

beiter Rudolf SCHÜSSLER (geb. 30. Aug. 1899)

aus Erding, wegen Äußerungen über die Re-
ierung.

(HC)

Verfahren eingestellt

Schüssler wurde weiter in Polizeihaft be-
halten.

28. Nov. 1939 - 22. Feb. 1940
(1 Js So 2172/39)

Ermittlungsverfahren gegen die Geschäfts-
inhaberin Anna FRÖHLICH (geb. 14. Jan.
1876) aus Füssen, wegen Äußerungen über
den Krieg.

(HG)

Verfahren eingestellt

15. Nov. 1939 - 2. Feb. 1940
(1 Js So 2195/39)

Ermittlungsverfahren gegen den Schreiner-
meister Anton SAUTER (geb. 12. Spt. 1875)
aus Fischen (Lkr. Sonthofen), wegen der
Erzählung, beim Einmarsch in das Sudeten-
land habe es hohe deutsche Truppenver-
luste gegeben.

(HG)

Verfahren eingestellt

18 Aug. 1939 - 8. Jan. 1940
(1 Js So 2198/39)

Ermittlungsverfahren gegen den Kohlen-
händler Josef WANK (geb. 8. Aug. 1897)
aus München, SPD-Mitglied, wegen ÄAuße-
Ce en über die NSDAP.

HG

Verfahren eingestellt

12. Nov. 1939 - 29. Apr. 1940
(1 Js So 2207/39)
