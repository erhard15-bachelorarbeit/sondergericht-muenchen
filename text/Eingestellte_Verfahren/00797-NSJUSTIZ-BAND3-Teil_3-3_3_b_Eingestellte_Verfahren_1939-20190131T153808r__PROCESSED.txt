(3314)

(3315)

(3816)

(3817)

(3818)

4947

4948

4949

4950

Verfahren gegen den Wachmann Nikolaus
BIRKENEDER (geb. 1. Dez. 1888) aus München,
5 politscher Bemerkungen.

HG

Verfahren eingestellt

29. Mai 1939 - 25. Aug. 1939
(1 Js So 956/39)

Ermittlungsverfahren gegen den Lagerar-

beiter Wilhelm Erhard Max BRUCKNER (geb.
23. Feb. 1874) aus Augsburg, verheiratet
mit einer Jüdin, wegen einer abwertenden
Bemerkung über das Regime.

(HG)

Verfahren eingestellt

12. Jun. 1939 - 14. Spt. 1939
(1 Js So 958/39)

Ermittlungsverfahren gegen die Arbeiterin
Anna FRIEDLEIN (geb. 25. Mai 1897) und
Creszenz FERSTL (eb. 2. Nov. 1877), beide
aus Eichstätt, wegen abwertender Bemer-
kungen über das "Ehrenkreuz der Deutschen
Mutter".

(HG)

Verfahren wegen Amnestie eingestellt

12. Jun. 1939 - 30. Okt. 1939
(1 Js So 960/39)

Verfahren gegen den Bauarbeiter Andreas
HEIDERSBERGER (geb. 21. Nov. 1889) aus
Immenstadt (Lkr. Sonthofen), vermutlich
SPD-Mitglied, wegen einer abwertenden
Äußerung über das "Ehrenkreuz der Deutschen
Mutter".

(HG)

Verfahren wegen Amnestie eingestellt

17. Jun. 1939 - 16. Nov. 1939
(1 Js So 962/39)

Ermittlungsyerfahren gegen den Knecht
Josef OBERMÜLLER (geb. 1. Feb. 1880)
aus Moos (Lkr. Vilshofen), wegen poli-
tischer Schimpfereien.

(3 2 HG; $3 134a StGB)

Verfahren eingestellt

24. Mai 1939 - 14. Aug. 1939
(1 Js So 966/39)
