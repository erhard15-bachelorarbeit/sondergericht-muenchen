(6272)

(6273)

(6274)

753

6384

6304

Ermittlungsverfahren gegen die Tapezierer-
meistersehefrau Mathilde SEITZ (geb. 29.
Spt. 1889) aus München wegen Äußerungen
() Hitler und das Dritte Reich.

Verfahren eingestellt

30. Apr. 1941 - 3. Feb. 1942
(1 Js So 688/41)

Ermittlungsverfahren gegen die Zugehfrau
Sabina IBERER (geb. 22. Mai 1896) aus Bad
Tölz wegen beleidigender Äußerungen über
die Regierung.

($& 2 HG)

Verfahren eingestellt

5. Spt. 1941 - 7. Mai 1942
(1 Js So 696/41)

Ermittlungsverfahren gegen den Metzger
Friedrich GRICHTMAIER (geb. 17. Aug. 1911)
aus Freising wegen ablehnender ÄAußerungen
über den Krieg.

(HG)

Verfahren eingestellt

19. Jun. 1941 - 22. Apr. 1942
(1 Js So 698/41)

Ermittlungsverfahren gegen den Kraftfahrer
Franz EDER (geb. 7. Aug. 1908) aus Berch-
tesgaden, weil er den Krieg einen "Krampf"
genannt haben soll, der nicht notwendig
ewesen wäre.
HG)

Verfalr ei eingestellt

31. Aug. 1941 - 14. Feb. 1942
(1 Js So 699/41)

Ermittlungen gegen den Straßenbahnfahrer
Anton SCHNABEL (geb. 21. Nov. 1901) aus
München, SPD-Mitglied, weil er einen SA-
Mann beleidigt haben soll.

Verfahren eingestellt (Beschluß nicht im A

Schnabel war vom 27. Apr. 1933 bis 1. Mai
1333 in Schutzhaft.
