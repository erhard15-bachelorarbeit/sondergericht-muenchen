(3530)

(3531)

(3532)

(3533)

(3534)

4679

4680

4681

4682

4683

Ermittlungsverfahren gegen die Privatiere

Maria ZACHER (geb. 13. Apr. 1863) aus
München, weil sie sich beleidigend über
führende Persönlichkeiten des Dritten
Reiches geäußert hat.

($$ 1,2 HG; 88 51,134b StGB)

Verfahren eingestellt

9. Feb. 1939 - 3. Mai 1939
(1 Js So 298/39)

Ermittlungsverfahren gegen den Dentisten
Johann MERKL (geb. 9. Okt. 1879) und seine
Frau Josephine (geb. 11. Mai 1889) aus
München, weil sie sich unberechtigt als
alte Parteimitglieder bezeichnet und
außerdem Hitler und Mussolini "Schwerver-
brecher" genannt haben sollen.

($ 2 HG)

Verfahren eingestellt

7. Feb. 1939 - 21. Jun. 1939
(1 Js So 304/39)

Ermittlungsverfahren gegen den städt. Ar-
beiter Simon NUDLBICHLER (geb. 24. Okt.
1897) aus Egling (Lkr. Landsberg a. L.),
weil er das Dritte Reich einen "Saustall"
und Hitler einen "Kerl" genannt haben
soll.

(HG)

Verfahren eingestellt

7. Feb. 1939 - 25. Mrz. 1939
(1 Js So 305/39)

Ermittlungsverfahren gegen den Hilfsarbeiter
Martin SCHORER (geb. 21. Okt. 1877) aus
Unterigling (Lkr. Landsberg a.L.), weil

er sich beleidigend über die HJ und den
BDM geäußert haben soll.

($ 2 HG)

Verfahren eingestellt

13. Feb. 1939 - 7. Jun. 1939
(1 Js So 306/39)

Ermittlungsverfahren gegen den UntermeiS-
ter Emil HAUG (geb. 8. Apr. 1900) aus
Lindau, weil er sich abfällig über Hit-ı
ler geäußert und ihn der Kriegstreiberel
bezichtigt haben soll.

(HG)

Verfahren eingestellt

8. Feb. 1939 - 30. Jun. 1939
(1 Js So 310/39)
