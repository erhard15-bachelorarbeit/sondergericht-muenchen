(6076)

(6077)

(6078)

(6080)

10524

10525

10526

10527

10528

(1 KMs So 204/41)

Prozeß gegen die Dienstmagd Lina MICHLBAUER
(geb. 5. Nov. 1913) aus Steindorf (Lkr.
Griesbach) wegen verbotenen Umgangs mit
einem französischen Kriegsgefangenen.

Urteil: 1 Jahr 3 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 4 Wehrkraft VO)

2. Okt. 1941 - 5. Spt. 1942
(1 KMs So 205/41)

Prozeß gegen den Mühlenrichter Johann
WIESLIVLOR(geb. 10. Jun. 1873), BVP-Mit-
glied, wegen abwertender Außerungen über
die"Hitlerbande".

Urteil: 6 Monate Gefängnis
($ 134b StGB)

18. Jan. 1941 - 27. Okt. 1942
(1 KMs So 206/41)

Prozeß gegen den Invalidenrentner Max WIMMER
(geb. 21. Jul. 1885) aus Obernzell (Lkr.
Wegscheid), die Hauptlehrersehefrau Mathilde
KRIEGL (geb. 4. Okt. 1896) aus Landshut und
die Spenglermeistersehefrau Kreszenz HAMMEL
(geb. 5. Jul. 1900) aus Obernzell wegen der
Äußerung in Obernzell, "daß Reichsmarschall
Göring in Linz in Schutzhaft sei".

Urteil: Wimmer 4 Monate Gefängnis; Kriegl
1 Monat Gefängnis oder 150.-RM
Geldstrafe; Hammel Freispruch

($ 1 HG)

4. Spt. 1941 - 5. Mrz. 1942
(1 kMs So 207/41)

Prozeß gegen die Dienstmagd Lina EYRISCH
(geb. 18. Jan. 1920) aus Thalmässing (Lkr.
Eichstätt) wegen verbotenen Umgangs mit
einem französischen Kriegsgefangenen.

Urteil: 1 Jahr 3 Monate Zuchthaus, 2 Jahre
Ehrverlust
(3 4 Wehrkraft VO)

28. Aug. 1941 - 18. Aug. 1943
(1 KMs So 208/41)

Prozeß gegen die Dienstmagd Berta STEMPFL
(geb. 25. Mrz. 1900) aus Weingold (Lkr.
Pfarrkirchen) wegen verbotenen Umgangs mit
einem französischen Kriegsgefangenen.
