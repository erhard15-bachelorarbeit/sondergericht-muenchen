(879)

(880)

(881)

(882)

(883)

8016

8017

8018

8019

8020

(geb. 13. Okt. 1909) aus München, NSDAP-
Mitglied, wegen Betrugs-

Verfahren eingestellt

6. Mai 1935 - 23. Nov. 1936
(S Pr 150/35)

Prozeß gegen den Elektrotechniker Peter
FELS (geb. 21. Jan. 1913), ohne festen
Wohnsitz, SA-Mitglied, wegen Betrugs und
Diebstahls.

Urteil: 2 Jahre 6 Monate Gefängnis

7. Jun. 1935 - 24. Dez. 1937
(S Pr 151/35)

Prozeß gegen den Musiker Wolfgang EDER
(geb. 16. Mrz. 1899) aus Zwiesel (Lkr.
Regen) wegen kritischer Äußerungen über di
nat. soz. Regierung.

Urteil: 59 Monate Gefängnis

18. Mrz. 1935 - 19. Mai 1938
(BB Br 152/35)

Prozeß gegen den Versicherungsvertreter
Michael MAIER (geb. 16. Apr. 1908) aus
Eichenau (Lkr. Fürstenfeldbruck) wegen
kritischer politischer ÄAußerungen.

Urteil: 60 RM Geldstrafe

16. Jul. 1935 - 12. Okt. 1935
(S Pr 153/35)

Prozeß gegen den Schlosser Cölestin NEU-
BAUER (geb. 13. Jun. 1900) aus München
wegen kritischer Äußerungen gegenMinister
Wagner.

Urteil: 60 RM Geldstrafe

23. Mai 1935 - 26. Okt. 1935
(S Pr 154/35)

Prozeß gegen die Bürogehilfin Wally
SCHINDLER (geb. 24. Feb. 1889) aus München
wegen einer kritischen Äußerung über das
Winterhilfswerk.

Urteil: 2 Monate Gefängnis oder 120 RM
Geldstrafe

3. Mai 1935 - 17. Mai 1939
(S Pr 155/35)
