(358)

(359)

(360)

(361)

(362)

(363)

7705

7706

7707

7708

7709

7710

Prozeß gegen den Kaufmann Emil KALTENBACH
(geb. 10. Jun. 1881) aus München wegen kri-
tischer Äußerungen über das NS-Regime.

Urteil: 6 Monate Gefängnis

22. Aug. 1933 - 25. Okt. 1934
(S Pr 383/34)

Prozeß gegen den Korbmacher und Hausierer
Karl BÜCHERL (geb. 5. Spt. 1895) aus Ach-
dorf (Lkr. Landshut) wegen abwertender
Äußerungen über lat.Sozz..

Urteil: Freispruch

23. Dez. 1933 - 8. Mrz. 1934
(S Pr 39/34)

Prozeß gegen den kfm. Angestellten Anton
KÜHBANDNER (geb. 20. Jul. 1906) aus München
wegen kritischer Äußerungen in pseudonymen
Briefen über Nat.Sozz..

Urteil: 3 Monate Haft
Nach 2 Monaten in Schutzhaft.

30. Spt. 1933 - 19. Jun. 1934
(S Pr 40/34) |

Prozeß gegen die Ehefrau eines Berufsober-
feuerwehrmannes Josefine WEINKAUF (geb. 5.
Mrz. 1908) aus München, SPD-Anhängerin,
wegen kritischer Äußerungen über das NS-
Regime.

Urteil: 200 RM Geldstrafe oder 40 Tage
Gefängnis

16. Aug. 1933 — 24. Aug. 1934

(S Pr 231/34)

Prozeß gegen den Glasermeister Anton FORSERE
(geb. 21. Nov. 1879) aus Großenhag (Lkr.
Pfaffenhofen) wegen kritischer Äußerungen
über Hitler.

Urteil: 4 Monate Gefängnis
30. Nov. 1933 - 27. Jun. 1934
(S Pr 42/34)

Prozeß gegen den Schäfflermeister Bartholo-
mäus ALBRECHT (geb. 12. Jul. 1879) aus Mün-
chen wegen kritischer Äußerungen gegen den

Urteil: 5 Monate Gefängnis

6. Nov. 1933 - 24. Aug. 1934
(S Pr 43/34)
