(1580)

(1581)

(1582)

(1583)

(1584)

(1585)

8546

8547

8548

8549

8550

8551
+8574
8576

Prozeß gegen den Erbhofbauern Josef HUNDS-
EDER (geb. 21. Jan. 1896) aus Inchenhofen
(Lkr. Aichach) wegen kritischer politischer
Äußerungen.

Urteil: 6 Wochen Gefängnis oder 150 RM
Geldstrafe

20. Okt. 1936 - 9. Apr. 1937
(16 KMs So 40/37)

Prozeß gegen die Putzerin Kreszenz HANNES
(geb. 27. Nov. 1897) aus München wegen
Betätigung als Ernste Bibelforscherin.

Urteil: 7 Monate Gefängnis

14. Jan. 1937 - 18. Spt. 1937
(16 KMs So 41/37)

Prozeß gegen den Schlosser Adolf WIENINGER
(geb. 28. Nov. 1878) aus Grafmühle (Lkr.
Passau) wegen kritischer politischer
Äußerungen.

Urteil: 4 Monate Gefängnis

17. Spt. 1936 - 22. Apr. 1937
(16 KMs So 42/37)

Prozeß gegen den Schreiner Johann BUCH-
FELLNER (geb. 12. Dez. 1890) und den
Schreiner Johann OBER (geb. 29. Aug. 1885),
beide KPD-Mitglieder aus Burghausen (Lkr.
Altötting), wegen abwertender Äußerungen
über Innenminister Wagner und andere Nat.
Sozz..

Urteil: Freispruch

25. Aug. 1936 - 13. Apr. 1937
(16 KMs So 43/37)

Prozeß gegen den Schlosser Johann WINTER
(geb, 14. Apr. 1899) aus Landshut, KPD-
Mitglied, wegen kritischer Äußerungen
über das Regime.

Urteil: 10 Monate Gefängnis

11. Spt. 1936 - 30. Jul. 1937
(16 KMs So 44/37)

Prozeß gegen verschiedene Gruppen der Zeugen
Jehovas:

Gruppe Justizangestellter Karl Johannes
ZIMMERMANN (geb. 30. Mai 1898) aus München;

Urteil: 2 Monate bis 1 Jahr 9 Monate
Gefängnis
