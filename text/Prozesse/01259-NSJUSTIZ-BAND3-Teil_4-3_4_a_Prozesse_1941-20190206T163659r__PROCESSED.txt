(5945)

(5946)

(5947)

(5948)

(5949)

(5950)

10

10394

10395

10396

10397

10398

Prozeß gegen den Dentisten Josef MUTZ (geb.
16. Okt. 1893) aus München wegen politi-
scher Außerungen.

Urteil: 4 Monate Gefängnis
$ 2 HG)

11. Nov. 1940 - 26. Mrz. 1942
(1 KMs So 69/41)

Prozeß gegen den kath. Pfarrer Johann
GNOGLER (geb. 3. Mrz. 1888) aus Hebrams-
dorf (Lkr. Rottenburg), BVP-Mitglied, we-
gen einer Äußerung über die Kriegslage.

Urteil: 3 Monate Gefängnis
$ 2 HG)

23. Spt. 1940 - 22. Aug. 1944
(1 kMs So 70/41)

Prozeß gegen den Vertreter Georg KRU-
SEITZ(geb. 21. Mrz. 1892) aus München we-
gen Äußerungen in Dillingen über die wirt-
schaftliche und politische Lage.

Urteil: 1 Jahr 5 Monate Gefängnis
& 2 HG)

4. Dez. 1940 - 23. Apr. 1942
(1 KMs So 71/41)

Prozeß gegen den Landwirt Josef KLEMMER
(geb. 19. Mrz. 1887) aus Ustersbach (Lkr.
Augsburg) wegen Äußerungen über die Re-
gierung.

Urteil: 1 Jahr Gefängnis
($ 2 HG)

11. Nov. 1940 - 27. Feb. 1942
(1 kMs So 72/41)

Prozeß gegen den Versicherungsvertreter
Gregor RAMPP (geb. 12. Mrz. 1881) aus
Augsburg wegen einer Äußerung über die
NSDAP.

Urteil: 1 Jahr Gefängnis
($ 2 HG)

17. Jan. 1941 - 30. Jan. 1942
(1 KMs So 73/41)

Prozeß gegen den Landwirt und Holzhauer
Johann WESTERMAIR (geb. 27. Dez. 1882)
aus Eurasburg (Lkr. Friedberg) wegen
einer Außerung über Hitler.

Urteil: 5 Monate Gefängnis
