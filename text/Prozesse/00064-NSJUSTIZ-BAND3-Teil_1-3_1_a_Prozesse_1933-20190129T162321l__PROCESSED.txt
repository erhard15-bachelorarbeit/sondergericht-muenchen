(194)

(195)

(196)

(197)

(198)

7547

7548

7549

7550

7550

Prozeß gegen den Rechtsanwalt Emil HIRSCH
(geb. 2. Dez. 1869) aus München wegen der
Bemerkung, nicht einmal Hitler könne die
Wahrheit sagen. H. war Mitglied des Stahlhel

Urteil: Freispruch

26. Jul. 1933 - 4. Dez. 1933
(S Pr 243/33)

Prozeß gegen den Pelzzurichter Richard
PFALZGRAF (geb. 24. Spt. 1914) aus München
wegen Weiterverkaufs einer kommunistischen
"Neuen Zeitung". P. ist Mitglied kommuni-
stischer Organisationen gewesen.

Urteil: 3 Monate Gefängnis

Der Prozeß kam durch eine Angabe des Schutz-
häftlings Johann KNOLL (geb. 17. Jan. 1901)
in Gang.

2. Aug. 1933 - 21. Mrz. 1934
(S Pr 244/33)

Prozeß gegen den Zimmermann Matthias MAYER
(geb. 21. Spt. 1871) aus Grün wald bei
München wegen der Behauptung, die Nat.Sozz.
hätten den Reichstag angezundet.

Urteil: Freispruch

13. Jun. 1933 - 30. Jun. 1934
(S Pr 245/33 und 248/33)

Prozeß gegen den Hilfsarbeiter Bruno
WESTERMEIER (geb. 15. Jun. 1900) aus
München, die Hilfsarbeitersehefrau

Ottilie GRIMM (geb. 10. Jul. 1891) und
die Hausangestellte Ottilie Karolina
WENSAUER (geb. 14. Nov. 1914), beide aus
München, wegen Verbreitung kommunistischer
Flugschriften.

Urteil: Westermeier 3 Monate Gefängnis,
Grimm und Wensauer Freispruch

(S pr8167337 —- 10. Feb. 1934

Prozeß gegen die Maschinistensehefrau Maria
KOBLER (geb. 25. Feb. 1901) aus München we-
gen Verbreitung kommunistischer Druckschrif-
ten. Der Ehemann Josef KOBLER (geb. 4. Nov.
1895) ist zwecks Strafverbüßung in Nürnberg.
Aus dem Mitgliederverzeichnis der "Roten
Hilfe", gefunden bei dem Landessekretär HOLYX
geht hervor, daß sie Mitglied von "Roter
Hilfe" und KPD ist.

Urteil: 1 Jahr Gefängnis

27. Spt. 1933 - 2. Mai 1941
(S Pr 247/33)
