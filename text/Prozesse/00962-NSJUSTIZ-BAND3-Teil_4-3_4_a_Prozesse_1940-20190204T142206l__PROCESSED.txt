(4551)

(4552)

(4553)

(4554)

(4555)

9549

9550

9552

9553

(83 20,271,529,742,40443 StGB; 8 2 VVO)

12. Dez. 1939 - 23. Okt. 1943
(1 KLs So 102/40)

Prozeß gegen den Schlosser Erich MAYER
(geb. 17. Nov. 1907) aus München, wegen
Diebstahls.

Urteils: 2 Jahre Zuchthaus, Aberkennung
der burgerlichen Ehrenrechte
auf 3 Jahre
(88 242,244 StGB; 8 2 VVO)

15. Apr. 1940 - 22. Aug. 1944
(1 KLs So 103/40)

Prozeß gegen den Eisenbahnarbeiter Jo-
hann DAX (geb. 7. Apr. 1894) aus Walles-
hausen (Lkr. Landsberg), wegen versuchter
Notzucht.

Urteils 1 Jahr 6 Monate Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
rechte auf 3 Jahre
($ 176 StGB; 8 4 VVO)

28. Dez. 1939 - 23. Apr. 1942
(1 KLs So 104/40)

Prozeß gegen den Bauschlosser Hermann
RUSING (geb. 8. Mai 1919) wegen Betrugs.

Urteil: 3 Jahre 6 Monate Zuchthaus, Ab-
erkennung der burgerlichen Enren-
rechte auf 2 Jahre
( 6 Ordensgesetz; $8$ 20,43,263

StGB; 3 4 VVO)

22. Feb. 1940 - 16. Feb. 1944
(1 KLs So 105/40)

Prozeß gegen den Ausgeher Vinzenz ECKL
(geb. 17. Mai 1879) aus Munchen, wegen
Unterschlagung.

Urteil: 4 Jahre 6 Monate Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
rechte auf 3 Jahre
(88 246,266 StGB; $8 4 VVO)

53. Feb. 1940 - 24. Okt. 1944
(1 KLs So 106/40)

Prozeß gegen den Hausmeister Ferdinand
KAISER (geb. 5. Jan. 1909) aus Munchen,
wegen Unterschlagung.

Urteil: 3 Janre 7 Monate Zuchthaus,
Aberkennung der bürgerlichen
Bhrenrochte auf 3 Jahre
