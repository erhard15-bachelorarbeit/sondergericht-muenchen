(233)

(234)

(235)

(236)

(237)

(238)

7585

7586

7587

7588

7589

7590

Prozeß gegen den Hilfsarbeiter Karl 5SCHILLI1
GER (geb. 6. Aug. 1870) aus München, KPD-Mit
glied, wegen Verbreitung kommunistischer
Schriften.

Urteil: 8 Monate Gefängnis

4. Okt. 1933 - 13. Okt. 1934
(S Pr 285/33)

Prozeß gegen den Versicherungsbeamten Karl
RÖSENER (geb. 7. Okt. 1900) aus München,
weil er sich über seine schlechte Behandlung
im KZ Dachau beklagte.

Urteil: 1 Monat Gefängnis, umgewandelt
in 200 RM Geldstrafe

8. Spt. 1933 - 28. Aug. 1934
(S Pr 287/33)

Prozeß gegen den Schneider Joseph HUBER
(geb. 1. Mai 1914) aus München wegen Be-
trugs.

Urteil: 6 Monate Gefängnis

1. Spt. 1933 - 18. Jul. 1934
(S Pr 288/33)

Prozeß gegen die Kontoristin Hildegard
LÖW (geb. 24. Dez. 1913) aus München
wegen abfälliger Bemerkungen über Nat.Sozz.

Urteil: 1 Monat Gefängnis, umgewandelt in
150 RM Geldstrafe

11. Nov. 1933 - 2. Okt. 1934

Prozeß gegen den Händler Otto KERLING (geb.

13. Jan. 1880) aus München wegen abwerten-

der Äußerungen über die Regierung.

Urteil: 6 Wochen Haft

16. Okt. 1933 - 15. Mai 1934

(S Pr 290/33)

Prozeß gegen den Hilfsarbeiter August NEFF
(geb. 18.Mai 1909), KPD-Mitglied, aus
München, wegen Verteilung kommunistischer
Druckschriften.

Urteil: 6 Monate Gefängnis

4. Nov. 1933 - 28. Mai 1934
(S Pr 291/33)
