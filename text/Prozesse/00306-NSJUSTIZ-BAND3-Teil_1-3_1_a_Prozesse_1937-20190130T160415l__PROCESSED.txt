(1450)

(1451)

(1452)

(1453)

9145

9146

9147

9148

Prozeß gegen die Maurersehefrau Katharina

KERNBICHLER (geb. 24. Jun. 1911) aus Karls«.
(Lkr. Berchtesgaden) wegen der Behauptung,

Hıtler sei so wie Rohm veranlagt.

Urteil: 5 Monate Gefängnis

24. Jun. 1937 - 5. Mai 1938
(1 KMs So 63/37)

Prozeß gegen den Vorarbeiter Ludwig KRING
(geb. 20. Dez. 1884) aus München wegen Be-
tätigung als Ernster Bibelforscher.

Urteil: 5 Monate Gefängnis

27. Jul. 1937 - 6. Mai 1938
(1 KMs So 64/37)

Prozeß gegen den Hilfsarbeiter Otto KAUF-
MANN (geb. 13. Apr. 1890) aus München we-
gen kritischer Bemerkungen uber die Anlage
eines Truppenübungsplatzes und die Juden-
politik.

Urteil: 9 Monate Gefängnis

20. Mrz. 1937 - 5. Apr. 1941
(1 KMs So 65/37)

Prozeß gegen den Warenverteiler Michael LEX
(geb. 3. Feb. 1886) und 6 weitere Personen;
die Zuschneiderswitwe Rosa GUNTHER (geb.
22. Feb. 1886), die Köchin Elisabeth WINKLER
(geb. 18. Mrz. 1883), den Bieraufkäufer Fran
Xaver MEYER (geb. 15. Mai 1878), dessen Ehe-
frau Anna MEYER (geb. 23. Okt. 1889), deren
Schwester, die Spenglersehefrau Emma NEIBER
(geb. 20. Nov. 1892), die Schlossersehefrau
Maria LODER (geb. 20. Mrz. 156899), alle aus
München, wegen Betätigung als Ernste Buiıbel-
forscher.

Urteil: Lex 1 Jahr Gefangnis; Günther
6 Monate Gefängnis; Winkler 2 Mo-
nate Gefängnis; Franz Meyer 5 Mo-
nate Gefängnis; Anna Meyer 3 Mo-
nate Gefängnis; Neiber 2 Monate
Gefängnis; Loder 6 Wochen Gefängnis
Anlagen: 1 Flugzettel "Offener Brief";
7 Flugzettel "Resolution"

13. Apr. 1937 - 25. Apr. 1941
(1 KMs So 66/37)
