(5519)

(5520)

(5521)

(5522)

9972

9972

9974

9975

(1 KLs So 13/41)

Prozeß gegen den Hilfsarbeiter Max WAGNER
(geb. 19. Jun. 1898) aus Haslachhof (Lkr.
Passau) wegen Untreue und Unterschlagung.

Urteil: 2 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf

3 Jahre
(88 246,266 StGB; $8 4 VVO)

13. Okt. 1940 - 28. Mai 1943
(1 KLs So 14/41)

Prozeß gegen den Steinhauergehilfen Fried-
rich SCHÄTZL (geb. 1. Jan. 1921), den
Steinhauer Walderich ECKERL (geb. 8. Okt.
1913), dessen Ehefrau Walburga ECKERL
(geb. 30. Spt. 1908), den Landwirt Stefan
HOFMANN (geb. 18. Dez. 1900), die landw.
Taglöhnerin Therese SCHÄTZL (geb. 11. Spt.
1912), alle aus Perling (Lkr. Passau),
wegen Abhörens ausländischer Rundfunksender.

Urteil: Friedrich Schätzl 1 Jahr 2 Monate
Zuchthaus; Walderich Eckerl 2
Jahre Zuchthaus; Hofmann 1 Jahr
2 Monate Zuchthaus; Therese Schätzl
und Walburga Eckerl Freispruch
($ 1 Rdf.VO)

17. Jul. 1940 - 9. Feb. 1943
(1 KLs So 15/41)

Prozeß gegen die Kontoristin Luise Hermine
HOFMANN (geb. 12. Nov. 1912) aus München
wegen Betrugs und Abtreibung.

Urteil: 2 Jahre Zuchthaus, 100.-RM Geld-
strafe oder 10 Tage Zuchthaus,
Aberkennung der bürgerlichen Eh-

renrechte auf 3 Jahre
(8 3 HG; 88 144,145 Österr. StGB)

27. Okt. 1939 - 28. Jan. 1943
(1 KMs So 16/41)

Prozeß gegen die Begmannsehefrau Maria
WAGNER (geb. 17. Spt. 1903) aus Peiting
(Lkr. Schongau) wegen verbotenen Umgangs
mit einem französischen Kriegsgefangenen.

Urteil: 1 Jahr 2 Monate Zuchthaus, Aber-
kennung der bürgerlichen Ehren-
rechte auf 2 Jahre
(3 4 Wehrkraft VO)

30. Jan. 1941 - 6. Apr. 1942
(1 KLs So 17/41)
