50)

(551)

(552)

(553)

(554)

(555)

8947 |

8948

8949

8950

8951

8952

Prozeß gegen den Hilfsarbeiter Josef OBER-
STADLER (geb. 15. Okt. 1879) aus München
wegen Beschimpfung Hitlers und des Dritten
Reiches.

Urteil: 50 RM Geldstrafe oder 10 Tage Haft

22. Spt. 1934 - 22. Feb. 1935
(S Pr 278/34)

Prozeß gegen den Monteur Johann HOLDERFRIED
(geb. 3. Mai 1898) aus München wegen der
wiederholten Behauptung, Baldur v. Schirach
habe sich wegen Geldangelegenheiten und
weil er sich an Mädchen vergangen habe, er-
schossen. -

Urteil: 60 RM Geldstrafe oder 15 Tage Haft

17. Spt. 1934 - 23. Apr. 1935
(S Pr 279/34)

Prozeß gegen den Steinhauer Anton NEUMÜLLER
(geb. 14. Mai 1903) aus München wegen ab-
wertender Äußerungen in Mitterham (er.

Bad Aibling) über die Regierung. N. war
NSDAP-Mitglied.

Urteil: Freispruch - (

30. Aug. 1934 - 9. Nov. 1934
(S Pr 280/34) -

Prozeß gegen den Wagner Walter SCHUBERT
(geb. 12. Nov. 1914) wegen kritischer
Äußerungen in Bodenmais (Lkr. Regen) über
einzelne Regierungsmitglieder.

Urteil: 4 Monate Gefängnis

17. Spt. 1934 - 26. Feb. 19353
(S Pr 281/34)

Prozeß gegen den Kesselschmied Sebastian
SCHINDLER (geb. 18. Jan. 1903) aus München,
vom 16. Jul. 1933 bis 14. Feb. 1934 im KZ
Dachau, wegen Verbreitung von Greuelnach-
richten über das KZ Dachau.

Urteil: Freispruch

30. Jun. 1934 - 7. Nov. 1934
(S Pr 282/34)

Prozeß gegen den kfm. Angestellten Max
DANNER (geb. 11. Apr. 1899) aus München
wegen kritischer Äußerungen über den Röhm-
Putsch.

Urteil: Freispruch

16. Aug. 1934 - 7. Nov. 1934
(S Pr 283/34)
