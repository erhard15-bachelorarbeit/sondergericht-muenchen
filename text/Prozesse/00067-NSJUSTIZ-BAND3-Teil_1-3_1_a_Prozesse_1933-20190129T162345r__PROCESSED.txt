(209)

(210)

(211)

(212)

(213)

(214)

7563

7564

7565

7566

7567

7568

Prozeß gegen den Hilfsarbeiter Josef SCHUPP
geb. 1. Mai 1905), den Weber Johann STICKLER
geb. 10. Dez. 1890), den Schlosser Anton

RAST (geb. 28. Nov. 1892), alle aus Augsburg,

und den Maurer Friedrich SEEHÜTER (geb. 1.

Feb. 1909) aus Ingolstadt wegen Verbreitung

kommunistischer Druckschriften, Rast auch

wegen Nichtablieferung eines Gewehres.

Urteil: Schupp Freispruch, Stickler und
Rast je 5 Monate Gefängnis, See-
hüter 2 Monate Gefängnis

53. Okt. 1933 - 2. Jul. 1934
(S Pr 262/33)

Prozeß gegen den Hilfsarbeiter Engelbert
REITER (geb. 5. Feb. 1898) aus München we-
gen Beleidigung Hitlers.

Urteil: Freispruch

14. Jul. 1933 - 27. Dez. 1933
(S Pr 263/33)

Prozeß gegen die Rentnerin Maria THANN-
HAUSER (geb. 21. Mrz. 1933) aus München
wegen Erzählung des Gerüchts von einem
Sektgelage im Braunen Haus.

Urteil: 3 Monate Gefängnis

24. Jun. 1933 - 24. Mai 1934
(S Pr 264/33)

Prozeß gegen den Landwirt Xaver HIRSCH-
VOGEL (geb. 12. Mrz. 1898) aus Buchloe
(Lkr. Kaufbeuren) wegen abwertender
Äußerungen über die Regierung.

Urteil: Freispruch

19. Okt. 1933 - 23. Feb. 1934
(S Pr 265/33)

Prozeß gegen den Bäcker und Taglöhner
Johann NEUSWIRTH (geb. 23. Dez. 1894)
aus Böhming (Lkr. Eichstätt) wegen kri-
tischer Äußerungen über die Regierung.

Urteil: Freispruch

8. Aug. 1933 - 27. Dez. 1933
(S Pr 266/33)

Prozeß gegen die Auflegerin Therese BÜCHERL
(geb. 31. Dez. 1892) und die Losverkäuferin
Paula Alma GRIMM (geb. 11. Aug. 1896), bei-
