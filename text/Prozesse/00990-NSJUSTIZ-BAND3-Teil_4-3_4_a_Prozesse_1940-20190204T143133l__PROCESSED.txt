(4670)

(4671)

(4672)

9667

9668

9669

(4 KLs So 48/40)

Prozeß gegen den Hifsarbeiter Franz
Xaver STRASSBURGER (geb. 24. Nov. 1912)
wegen Diebstahls ın Kempten.

Urteil: 4 Jahre L Mcerate Zuchthaus, kAb-
erkennung aer turgerlichen Eh-
renrecnte auf 3 Jahre
($8 2,242 ,243 StGB)

21. Aug. 1940 wegen Widerstands erschoss:

14. Mrz. 1940 - LA. Spt. 1940
(5 KLs So 44/40)

Prozeß gegen den Kaifmann Bernhard MÜHL-
HEIMS (geb. 23. Apr. 1390) aus München
wegen der Behauptung, er sei Offizier,
ohne es zu sein.

Urteil: 1 Jahr Gefangnis, ab 5 Monate
3 Wochen Schutz- und Untersu-
chungshaft
(3 6 Ordenogesetz)

9. Feb. 1940 - 26. Jan. 1942
(5 KLs So 45/40)

Prozeß gegen daen Kraftwagenführer Josef
HALBRITTER (geb. 5. Spt. 1909), fruher
SA-Mitglied, den Vorarbeiter Franz HIERL
(geb. 28. Mrz. 1905), den Beifahrer Jo-
sef MAYER (geb. 3. Jan. 19975), den Vor-
arbeiter Albert SENS (geb. 6. Jun. 1904),
alle aus Ingolstadt, wegen Diebstahls
und Unterschlagung.

Urteil: Halbritter 4 Jahre Zuchthaus,
Aberkennung der bürgerlichen
Ehrenrechte auf 3 Jahrej5Hierl
3 Jahre Zuchthaus, Aberkennung
der burgerlichen Ehrenrechte
auf 3 Jahre; Mayer 2 Jahre 6
Monate Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte
auf 2 Jahre; Sens 1 Jahre 5 Mo-
nate Zuchthaus, Aberkennung der
bürgerlichen Enrenrechte auf 2
Jahre
($$ 242,246,259 StGB; $ 4 VVO)

Prozeß gegen den Bauern und Gastwirt
Karl MAYER (geb. 4. Jun. 1909), SA- und
NSDAP-Mitglied, aus Eichstätt wegen
Sachhehlerei ın gleicher Sache.

Urteil: 300.-RM Geldstrafe oder 2 Mona-
te Gefängnis
