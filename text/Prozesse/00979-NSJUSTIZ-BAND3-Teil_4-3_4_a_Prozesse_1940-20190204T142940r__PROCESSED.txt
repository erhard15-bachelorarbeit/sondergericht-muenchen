(4624)

(4625)

(4626)

9622

9623

9624

(2 KLs So 11/40)

Vollstreckungs- und Gnadenakten aus
dem Prozeß gegen den Metzgermeister
August STEFFEL (geb. 25. Mrz. 1883)
aus Altötting wegen Schwarzschlachtung.

Urteil: 1 Jahr 2 Monate Gefängnis, 300.
RM oder 6 Tage Gefängnis und
6000.-RM Ersatzstrafe oder 120
Tage Gefängnis
($ 1 KWVO)

14. Jan. 1941 - 21. Jan. 1943
(2 KLs So 12/40)

Prozeß gegen den Gärtnereipächter Al-
bert BÖCK (geb. 18. Aug. 1904) aus Mün-
chen, wegen Schwarzschlachtung von Fer-
keln.

Urteil: 300.-RM Geldstrafe oder 30 Tage
Gefängnis
($$ 12 WarenverkehrVO)

30. Mrz. 1940 - 1. Jul.1942
(2 KLs So 13/40)

Prozeß gegen die Gastwirtschaftsbe-
sitzerin Käthe OBERPRILLER (geb. 1.
Jun. 1903), die Gastwirtin Katharina
HOCHHÄUSLER (geb. 22. Dez. 1882), den
Metzger Franz HOCHHÄUSLER (geb. 24.
Aug. 1912), den landw. Arbeiter Simon
GEIDL (geb. 25. Mrz. 1920), alle aus
München, wegen Schwarzschlachtung.

Urteils Oberpriller 2 Jahre Gefängnis,
700.-RM Geldstrafe oder 14
Tage Gefängnis, 1400.-RM Wert-
ersatz oder 28 Tage Gefängnis;
Katharina Hochhäusler 10 Monate
Gefängnis, 800.-RM Geldstrafe
oder 16 Tage Gefängnis; Franz
Hochhäusler 2 Jahre Gefängnis,
800.-RM Geldstrafe oder 16 Tage
Gefängnis, 2500.-RM Wertersatz
oder 50 Tage Gefängnis; Geidl
3 Monate Gefängnis, 100.-RM
Geldstrafe oder 2 Tage Gefäng-
nis
($ 1 KWVO; $$ 396,398 RAO;

$& 1-5 Schlachtsteuergesetz;
$ 49 StGB)

12. Aug. 1940 - 19. Apr. 1945
(2 KLs So 15/40)
