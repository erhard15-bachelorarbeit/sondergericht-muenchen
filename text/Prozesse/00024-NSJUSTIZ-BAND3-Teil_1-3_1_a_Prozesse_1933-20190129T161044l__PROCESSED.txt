NER (geb. 4. Jun. 1904), den Hilfsarteit
Georg LAMPRECHT (geb. 19. Aug. 1906), al
aus Burghausen (Lkr. Altötting), wegen A
singens der "Internationale" u.a., während
die SA die Hakenkreuzfahne am Rathaus hißte
und das Deutschlandlied sang, Zum Teil Vor-
an aus Untersuchungshaft oder Schutz-
aft.

r
e

&
l
Di
mn

Urteils: Danner Freispruch, Haxpointner 8
Monate Gefängnis, Breu Gesaststra-
fe 1 Jahr und 4 Monate (bereits zu
1 Jahr verurteilt), Neudecker 6 Mo-
nate Gefängnis, Steinl 3 Monate Ge-
fängnis, Mayr Gesamtstrafe 1 Jahr
und 2 Wochen (bereits zu 1 Jahr ver
urteilt), alle übrigen zu 2 Monaten
Gefängnis verurteilt.

10. Mrz. 1933 - 14. Aug. 1934
(S Pr 4/33)

Prozeß gegen den Bauhilfsarbeiter Josef
NEUDECKER (geb. 25. Feb. 1897) aus Hasel-
furth (Lkr. Landshut) wegen abwertender
Äußerungen über SA und SS und Reichskanz-
ler Hitler. Vorführung aus Untersuchungs-
haft.

Urteil: 7 Monate Gefängnis

26. Mrz. 1933 - 12. Nov. 1933
(S Pr 5/33)

Prozeß gegen den Taglöhner Josef SCHRÖCK
geb. 21. Nov. 1911) aus Höllmannsried
Lkr. Regen) wegen Bettels.

Urteil: 1 Woche Haft, durch die Unter-
suchungshaft verbüßt

1. Apr. 1933 - 25. Apr. 1933
(S Pr 6/33)

Prozeß gegen den Mechaniker Julius EISEN-
BRAND (geb. 21. Nov. 1879), ohne festen
Wohnsitz, wegen der Behauptung in Beilngries
der Reichstagsbrand in Berlin sei von der
NSDAP gelegt worden und pol. Gefangene wür-
den mißhandelt.

Urteil: 1 Jahr Gefängnis

3. Apr. 1933 - 10. Mai 1933
(S Pr 7/33)
