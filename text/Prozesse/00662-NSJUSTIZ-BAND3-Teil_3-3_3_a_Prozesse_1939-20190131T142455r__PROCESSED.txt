(3143)

(3144)

(3145)

(3146)

(3147)

(3148)

9172

9173

9174

9175

9176

Prozeß gegen den Maurer Johann BREITENLAD-
NER (geb. 23. Jun. 1916), den Gärtner Josef
STIERSTORFER (a0. 3. Dez. 13973 den Sattler
Alfred TÜRCK (geb. 4. Aug. 1914), alle aus
München wegen Diebstahls.

Urteil: Breitenladner 1 Jahr Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
rechte auf 3 Jahre; Stiersdorfer
4 Monate Gefängnis; Türck Freispruch
($ 3 HG; 88 44, 49, 242 StGB)

24. Jan. 1939 - 20. Apr. 1940
(1 KLs So 20/39)

Prozeß gegen den Hilfsarbeiter Franz NEUNER
(geb. 22. Feb. 1917) aus Mittenwald (Lkr.
Garmisch-Partenkirchen) wegen Diebstahls.

Urteil: 8 Monate Gefängnis
(8 3 HG; $$ 242, 243 StGB)

17. Mrz. 1939 - 2. Okt. 1939
(1 KLs So 21/39)

siehe Inv. Nr. 3155 (StAnw. 9183)
(1 KLs So 22/39)

Prozeß gegen den Kaufmann Stephan HARTMANN
(geb. 1. Spt. 1904) wegen Unterschlagung in
Augsburg.

Urteil: 5 Monate Gefängnis
$ 5 HG; Ordensgesetz; 8 246 StGB)

H. wurde bereits am 25. Feb. 1938 vom
Schoffengericht Koblenz wegen unberechtig-
ten Tragens von Abzeichen zu 2 Monaten
Gefängnis verurteilt.

19. Okt. 1938 - 29. Jan. 1940
(1 KLs So 23/39)

Prozeß gegen den kfm. Angestellten Wenzel
ANTOSCH (geb. 29. Apr. 1915) aus Landshut,
früher SA-Mitglied, wegen Betrugs.

Urteil: 2 Jahre 8 Monate Gefängnis
$ 3 HG; 38 264, 268, 270 StGB)

20. Jan. 1939 - 6. Nov. 1941
(1 KLs So 24/39)

Prozeß gegen den Halbjuden und Buchhalter
Erich REIF (geb. 16. Okt. 1913) aus Mün-
chen, NSDAP-Mitglied, wegen Unterschla-

gung.
Urteil: 2 Jahre Zuchthaus, Aberkennung der

bürgerlichen Ehrenrechte auf 3 Jahre
(88 3, 6 HG; 88 246, 266 StGB)

9. Feb. 1939 - 29. Nov. 1940
(1 KLs So 25/39) cn
