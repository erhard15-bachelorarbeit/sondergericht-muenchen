(5889)

(5890)

(5891)

(5892)

(5893)

10336

10337

10338

10339

10340

Prozeß gegen den Versicherungsinspektor
Georg SCHREYEGG (geo. 10. Apr. 1889) aus
München, früher SPD-Mitglied, wegen Auße-
rungen uber Hitier.

Urteil: 2 Jahre 4 Monate Gefängnis
($ 2 HG)

6. Okt. 1940 - 19. Feb. 1943

(1 KMs So 11/41)

Prozeß gegen den Sattler Heinrich KLOUCECK
(geb. 18. Jul. 1904) aus Inzell (Lkr.
Traunstein) wegen kritischer Außerungen
über das Regime und den Krieg.

Urteil: 6 Monate Gefängnis
$ 2 HG; 3 134a StGB)

26. Jul. 1940 - 8. Mrz. 1944
(1 KMs So 12/41)

Prozeß gegen die Hausgehilfin Franziska
INNINGER (eb. 25. Jun. 1900) aus Berch-
tesgaden wegen Äußerungen uber Hitler, den
Krieg und die Regierung.
Urteil: 1 Jahr 6 Monate Gefängnis

$ 1 HG)

I. wurde bereits einschlägig verwarnt; ein
einschlägiges Verfahren gegen sie wurde
wegen Amnestie eingestelit.

4. Spt. 1940 - 7. Apr. 1942
(1 KMs So 13/41)

Prozeß gegen die Maklersehefrau Maria KLEIN-
BÖHL (geb. 15. Apr. 1885) aus Moosen (Lkr.
Traunstein) wegen Abhörens ausländischer
Rundfunksender und politischer Außerungen.

Urteil: 1 Jahr 6 Monate Zuchthaus
8 2 HG; 83$ 1,2 Rädf.VO)

8. Mai 1940 - 24. Feb. 1942
(1 KMs So 14/41)

Prozeß gegen den Glasmacher Anton KÖPPL
(geb. 29. Aug. 1901) aus Regenhütte (Lkr.
Regen) wegen Äußerungen über Hitler.

Urteil: 8 Monate Gefangnis
($ 2 HG)

26. Mai 1940 - 28. Feb. 1942
(1 KMs So 15/41)
