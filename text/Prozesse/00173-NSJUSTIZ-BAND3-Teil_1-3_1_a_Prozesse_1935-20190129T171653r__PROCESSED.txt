7906
(769) 7907
F 7908
(771) 7909
(772) 7910

(773) 7911

-

4.

1

Prozeß gegen den Schäffler Otto VORHOLZER
(geb. 17. Mai 1913) aus Landshut wegen
Musizierens auf der Straße und Tragens
eines dem Parteiabzeichen der NSDAP sehr
ähnlichen Zeichens.

Urteil: 6 Monate Gefängnis

23. Jan. 1935 - 7. Jul. 1938
(S Pr 31/35)

Prozeß gegen den Spengler Georg DENGL
(geb. 13. Dez. 1881) aus München wegen
der Behauptung, er sei im KZ Dachau
gewesen.

Urteil: 1 Monat Gefängnis

16. Jan. 1935 - 13. Apr. 1935
(S Pr 32/35)

Prozeß gegen den Melker Felix MORETTL
(geb. 12. Mrz. 1912) aus Gut Voglsang
(Lkr. Miesbach), Sozialdemokrat, italie-
nischer Staatsbürger, wegen abträglicher
Äußerungen über Hitler.

Urteil: 5 Monate Gefängnis

4. Feb. 1935 - 9. Jul. 1935
(S Pr 33/35)

Prozeß gegen den Schreibwarenhändler
Michael MITTERMEIER (geb. 6. Jun. 1886)
aus Neubiberg (Lkr. München) wegen kri-
tischer Äußerungen über die politische
Lage.

Urteil: 4 Monate Gefängnis

26. Nov. 1934 - 9. Okt. 1935
(S Pr 34/35)

Prozeß gegen die Hausierhändlerin Anna
HUMMEL (geb. 19. Okt. 1892) aus Traun-
stein wegen der Behauptung, Stoffe würden
bald sehr knapp werden.

Urteil: 1 Monat Gefängnis
23. Nov. 1934 - 26. Nov. 1935
(S Pr 35/353)

Prozeß gegen den Landfahrer Ernst
SCHIESSEL (geb. 15. Feb. 1903) aus

- Ichenhausen (Lkr. Günzburg) wegen

der Behauptung, Hitler habe kürzlich
aus München fliehen müssen, sonst
wäre er erschossen worden.
