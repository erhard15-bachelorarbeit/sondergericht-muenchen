(3361)

(3362)

(3363)

(3364)

(3365)

9416

9417

9418

9419

9420

Prozeß gegen den Bauarbeiter Luitpold
FUCHSENTHALER (geb. 4. Fab. 1911) aus Mün-
chen wegen einer abwertenden Äußerung über
die SA-Uniform.

Urteil: 5 Monate Gefängnis
$ 2 HG)

F. befand sich vom 16. Jul. 1936 bis 5. Aug
1936 im KZ.

25. Aug. 1939 - 13. Feb. 1940
(1 KMs So 188/39)

Prozeß gegen den Brauer Eduard JÄGER (geb.
15. Jun. 1876) aus Pfarrkirchen wegen Be-
trugs beim Kauf von Pferden für die Reichs-
wehr.

Urteil: 1 Jahr 9 Monate Zuchthaus und
4000,- RM Geldstrafe oder 40 Tage
Zuchthaus
($ 263 StGB)

13. Spt. 1939 - 17. Feb. 1945
(1 KMs So 189/39)

Prozeß gegen den Hilfsarbeiter Johann MARX
(geb. 19. Jun. 1885) aus Burgberg (Lkr.
Sonthofen), früher KPD-Mitglied und Ange-
höriger der "roten Armee" in München,
wegen der Äußerung: "Ich möchte in Sont-
hofen sein und eine Bombe in das Auto des
Führers werfen."

Urteil: 1 Jahr 2 Monate Gefängnis
($ 2 HG)

14. Apr. 1939 - 11. Spt. 1940
(1 KMs So 190/39)

Prozeß gegen den kath. Geistlichen Erich
SELZLE (geb. 21. Mrz. 1906) aus München
wegen politischer Außerungen und Erzählens
von politischen Witzen in Immenstadt (Lkr.
Sonthofen).

Urteil: 1 Jahr Gefängnis
($ 2 HG)

16. Dez. 1937 - 22. Jun. 1944
(1 KMs So 191/39)

Prozeß gegen den Hilfsarbeiter Karl BITZER
(geb. 22. Nov. 1906) aus Kempten, früher
KPD-Mitglied, wegen politischer Äußerungen
in Pfronten (Lkr. Füssen).

Urteil: 9 Monate Gefängnis
$ 1 HG)

22. Aug. 1939 - 11. Jun. 1940
(1 KMs So 192/39)
