(977)

(978)

(979)

(980)

(981)

8114

8115

8116

8117

8118

Prozeß gegen den Erbhofbauern Älbert SCHWE/
(geb. 16. Nov. 1881) aus Rieden (Lkr. Weilhe
wegen der Äußerung, bevor er dem Winterhilfs.
werk etwas gebe, gebe er dem Pfarrer.

Urteil: Freispruch

15. Nov. 1935 - 17. Feb. 1936
(S Pr 257/35)

Prozeß gegen den Malergehilfen Heinrich RIEG®|
(geb. 11. Spt. 1913) aus München wegen der
Äußerung in Kaufbeuren, Gelder des Winterthıli
werkes würden für militärische Bauten verwen
det werden.

Urteil: 4 Monate Gefängnis

1. Spt. 1935 - 14. Mrz. 1936
(S Pr 258/35)

Prozeß gegen den kath. Kooperator Max KRONER
(geb. 12. Spt. 1902) aus Tann (Lkr. Pfarr-

kirchen) wegen kritischer politischer ÄAuße-
rungen in Predigten.

Urteil: Freispruch

2. Aug. 1935 - 3. Mrz. 1936
(S Pr 259/35)

Prozeß gegen den Gastwirt und BErbhofbauern
Johann GROTTER (geb. 12. Jul. 1895) aus
Berletzhausen (Lkr. Eichstätt) wegen kriti-
scher politischer Außerungen.

Urteil: Freispruch

12. Nov. 1935 - 25. Mrz. 1936
(S Pr 260/35)

Prozeß gegen den Gastwirt und Bauern Max
BERTL (geb. 22. Jan. 1876) aus Kirchberg
(Lkr. Schongau) wegen der Behauptung,

Hitler habe Geld ins Ausland verschoben.

Urteil: 4 Monate Gefängnis

30. Nov. 1935 - 19. Mai 1938
(S Pr 261/35)
