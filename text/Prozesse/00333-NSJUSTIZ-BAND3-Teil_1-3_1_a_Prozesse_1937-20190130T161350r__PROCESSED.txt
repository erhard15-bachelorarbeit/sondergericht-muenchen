Gruppe Hilfsarbeiter Albert FISCHL (geb.
27. Mrz. 1897) aus Miesbach;

Urteil: 4 Monate bis 9 Monate Gefängnis,
z.T. Schutzhaft

Gruppe Hutgeschäftsinhaber Josef WAGNER
(geb. 29. Dez. 1897) aus Rosenheim;

Urteil: bis zu einem Monat Gefängnis und
Verfahrenseinstellung

Gruppe Braumeister Josef GLASSENHART (geb.
2. Mrz. 1887) aus Ichenhausen (Lkr. Günz-
burg)

Urteil: 5 bis 8 Monate Gefängnis

Gruppe Hilfsarbeiter Josef FLUNK (geb.
8. Mai 1894) aus Freilassing (Lkr. Laufen);

Urteil: 5 Monate 2 Wochen bis 6 Monate
Gefängnis

Gruppe Bauersehefrau Katharina STUMPFEGGER
(geb. 14. Apr. 1892) aus Hofham (Lkr. Lau-
fen);

Urteil: 6 Wochen bis 8 Monate Gefängnis
und Freispruch

Gruppe Josef SIEBER (geb. 7. Apr. 1878),
Sozialrentner aus Freilassing;

Urteil: 7 Monate Gefängnis

Gruppe Maurer Josef SCHAUMAIER (geb. 18.
Mrz. 1896) aus Traunstein;

Urteils: 2 bis 8 Monate Gefängnis

Der Akt enthält die Namen der Gruppenmit-
glieder.

Anlagen: 2 "Offener Brief"; 2 "Resolution"
2 Broschüren "Entscheidung"; 1
Schreibmaschinenkopie "Alltäg-
liches aus Deutschland"; 1 "Die
Internationale Ernste Bibelfor-
schervereinigung (IBV), deren
Grundsätze und Organisation".

2. Mai 1933 - 12. Mai 1941
(16 KMs So 45-47, 70-71/37)
