(5681)

(5682)

(5683)

(5684)

(5685)

10132

10133

10134

10135

10136

Prozeß gegen den Bücherrevisor Reinhard
KUHLMEY (geb. 20. Jan. 1907) aus München,
DNVP-, dann NSDAP-Mitglied, wegen Untreue
und Unterschlagung.

Urteil: 6 Jahre 6 Monate Zuchthaus, Geld-
strafen, Aberkennung der bürgerlichen
BEhrenrechte auf 3 Jahre, 3 Jahre
Berufsverbot (unter Einbeziehung
eines früheren Urteils vom 7. Okt.
1940)

(38 246,263,266,267,268 StGB;
$8 4 VVO)

19. Jul. 1940 - 1. Spt. 1944
(3 KLs So 27/41)

Prozeß gegen den Hauswart Karl KNIES (geb.
25. Mai 1898) aus Memmingen wegen eines
Fahrraddiebstahls.

Urteil: 9 Monate Gefängnis
($ 242 StGB)

25. Jan. 1941 - 30. Jul. 1943
(3 KLs So 28/41)

Prozeß gegen den Maurer Ludwig KUMM (geb.
15. Feb. 1912) aus Ruchis (Lkr. Sonthofen)
wegen Diebstahis.

Urteil: 1 Jahr Zuchthaus
(38 242,244 StGB)

6. Feb. 1941 - 1. Dez. 1942
(3 KLs So 29/41)

Prozeß gegen die Ofensetzersehefrau Agnes
KULGART (geb. 30. Aug. 1910) aus Osterath
(Lkr. Krefeld) und Anna STIEBER (geb. 27.
Dez. 1898) aus Augsburg wegen Diebstahls
in Augsburg.

Urteil: Kulgart 10 Monate Gefängnis; Stie-
ber Freispruch
(88 242,243,299 StGB)

31. Okt. 1940 - 8. Mrz. 1945
(3 kLs So 33/41)

Prozeß gegen den Hilfsarbeiter Franz LANG
(geb. 6. Apr. 1901) aus München wegen
Diebstahls.

Urteil: 5 Jahre Zuchthaus, 500.-RM Geld-
strafe oder 5 Tage Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
rechte auf 10 Jahre, Sicherungs-

verwahrung
(88 20a 47? DL DL6 259 7267 268 StGHB:;:
