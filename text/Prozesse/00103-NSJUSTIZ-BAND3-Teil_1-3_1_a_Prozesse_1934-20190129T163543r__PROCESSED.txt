(394)

(395)

(396)

(397)

(398)

77h2

7743

7744

7745

7746

7747

Prozeß gegen den Maurergehilfen Josef ENDHA
(geb. 27. Aug. 1904) aus München wegen der
Äußerung, Regierungsmitglieder könnten sich
bereichern wollen.

Urteil: 4 Monate Gefängnis

12. Dez. 1933 - 6. Spt. 1934
(S Pr 76/34)

Prozeß gegen den Schreinergehilfen Josef
KISLER (geb. 3. Spt. 1914) aus München
wegen der Behauptung, die Nat.Sozz. hätten
den Reichstag angezündet.

Urteil: 5 Monate und 10 Tage Gefängnis

2. Dez. 1933 - 25. Aug. 1934
(S Pr 77/34)

Prozeß gegen den Fahrradhändler Alois
FALTINGER (geb. 26. Aug. 1912) aus Passau,
österreichischer Staatsbürger, wegen ab-
wertender Äußerungen über Hitler.

Urteil: 5 Monate Gefängnis

30. Jan. 1934 - 25. Aug. 1934
(S Pr 78/34)

Prozeß gegen den Schlosser Georg STOCKINGER
(geb. 22. Mrz. 1909) aus Schalterbach (Lkr.
Deggendorf) wegen Diebstahls und Betrugs.

Urteil: 9 Monate Gefängnis

30. Jan. 1934 - 14. Nov. 1934
(S Pr 79/34)

Prozeß gegen die Kaufmannsfrau Maria
STEINBEISSER (geb. 5. Jan. 1888) aus Mün-
chen wegen abwertender Äußerungen übe

SA und NSDAP. ;

Urteil: Freispruch

12. Jan. 1934 - 28. Mrz. 1934
(S Pr 80/34)

Prozeß gegen den Kaufmann Peter KUTZ (geb.
1. Mrz. 1909) aus München, NSDAP-Mitglied,
wegen der Behauptung, Hitler und Röhm
seien "schwul".

Urteil: Freispruch

17. Jan. 1934 - 16. Apr. 1934
(S Pr 81/34)
