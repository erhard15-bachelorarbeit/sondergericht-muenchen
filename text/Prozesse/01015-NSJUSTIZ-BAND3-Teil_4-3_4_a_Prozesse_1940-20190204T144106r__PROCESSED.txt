(4793)

(4794)

(4795)

(4796)

(4797)

9790

9791

9792

9793

9794

Prozeß gegen den Bauern Äaver DULLINGER
(geb. 5. Aug. 1897) aus Aigen (Lkr.
Griesbach), NSDAP-Mitglied, weil er der
Musterungskommission die Krankheit
eines gemusterten Pferdes verschwiegen
hatte.

Urteil: 4 Monate Gefängnis, 300.-RM
Geldstrafe oder 30 Tage Gefäng-
nis

($ 263 StGB)

14. Spt. 1939 - 1. Jun. 1943
(1 KMs So 104/40)

Prozeß gegen die Kraftdroschkenführers-
ehefrau Franziska WITTMANN (geb. 25.
Spt. 1906) aus München wegen politi-
scher Bemerkungen.

Urteil: 1 Jahr Gefängnis
$ 1 HG)

10. Jan. 1940 - 26. Apr. 1945
(1 KMs So 105/40)

Verfahren gegen den Maschinentechniker
Kurt ZIMMERMANN (geb. 9. Mai 1894) aus
München wegen Außerungen über führende
Nat.Sozz. in Budapest.

Verfahren eingestellt
($ 2 HG; $3 4 StGB; $3 258,261 ungarisches
StGB)

12. Aug. 1939 - 10. Spt. 1940
(1 KMs So 106/40)

Prozeß gegen den kfm. Angestellten Ernst
MEYER (geb. 2. Apr. 1911) aus Löcknitz
(Lkr. Ludwigslust), Halbjude, wegen
Äußerungen über den NS.

Urteil: 6 Monate Gefängnis
(9 2 HG)

19. Nov. 1939 - 23. Aug. 1944
(1 KMs So 107/40)

Prozeß gegen den Metzger Matthias HEIT-
MEYER (geb. 6. Jan. 1875) aus München
wegen politischer Außerungen.

Urteil: 6 Monate Gefängnis
$ 2 HG; $ 51 StGB)

11. Dez. 1939 - 6. Feb. 1941
(1 KMs So 108/40)
