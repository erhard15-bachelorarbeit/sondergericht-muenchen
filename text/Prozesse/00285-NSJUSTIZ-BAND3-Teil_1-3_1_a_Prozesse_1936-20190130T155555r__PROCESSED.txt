(1358)

(1359)

(1360)

(1361)

(1362)

9052

9053

9054

9055

9056

Urteil: 10 Monate Gefängnis

7. Nov. 1935 - 5. Aug. 1936
(AK 125/36)

Prozeß gegen den Konditor Karl SPÄTH
(geb. 13. Noy. 1914) aus München wegen
abwertender Äußerungen in Törwang (Lkr.
Rosenheim) über Hitler.

Urteil: Freispruch

7. Okt. 1935 - 13. Okt. 1936
(AK 130/36)

Prozeß gegen den Fotografen Georg FUCHS
(geb. 1. Jul. 1876) aus München wegen
kritischer Äußerungen in Irschenhausen
(Lkr. Wolfratshausen) über die Zeitungs-
berichtserstattung von Devisenprozessen.

Urteil: 4 Monate Gefängnis

26. Jul. 1935 - 19. Mai 1938
(AK 131/36)

Prozeß gegen den Lackierermeister Ludwig
KOLLINGER (geb. 14. Jun. 1893) aus München,
NSDAP-Mitglied, wegen der Behauptung, er
werde Hitler erschießen, wenn er dazu die
Mittel bekomme.

Urteil: 1 Jahr 6 Monate Gefängnis

K. wurde nach Strafverbüßung der Gestapo
überstellt.

27. Jul. 1935 - 9. Jun. 1937
(AK 138/36)

Prozeß gegen den Arbeiter Wolfgang Max
Johannes SONNENSCHMIDT (geb. 13. Okt.
1912) aus München wegen abwertender
Äußerungen in Siegsdorf (Lkr. Traunstein)
über das Regime. S. war bereits vom 23.
Okt. bis 12. Nov. 1934 im KZ Dachau.

Urteil: 10 Monate Gefängnis

11. Okt. 1935 - 28. Spt. 1936
(AK 141/36)

Prozeß gegen den Hilfsarbeiter Johann
SCHERZL (geb. 8. Jun. 1908) aus Oberst-
dorf (Lkr. Sonthofen), NSDAP- und SA-
Mitglied, wegen einer abwertenden Außerung
über die Regierung.

Urteil: 4 Monate Gefängnis

16.Spt. 1935 - 20. Mai 1938
(AK 142/36)
