(5924)

(5925)

(5926)

(5927)

(5928)

(5929)

10373

10374

10375

10376

10377

Prozeß gegen den Bahnarbeiter Josef MAYER
(geb. 4. Jan. 1913) aus München wegen
falscher Berichte in Böhl (Lkr. Speyer)
über den Zweck von Truppentransporten nach
Italien.

Urteil: 4 Monate Gefängnis
8 1 HG)

10. Jan. 1941 - 25. Aug. 1942
(1 KMs So 46/41)

Prozeß gegen Wilhelmine RECHER aus Bad
Aibling.

(nur 1 Brief)

s. auch Staatsanw. 9955 (Nr. 4958).

($ 4 Wehrkraft VO)
(1 KMs So 47-48/41)

Prozeß gegen die Bedienung Helene MATT
(geb. 19. Mrz. 1910) aus Lindenberg (Lkr.
Lindau) wegen der Behauptung, Feldmarschall
Milch sei hingerichtet worden.

Urteil: 6 Wochen Gefängnis
8 1 HG)

27. Nov. 1940 - 14. Jul. 1941
(1 KMs So 49/41)

Prozeß gegen den Gastwirt Peter RIEGER
(geb. 4. Jan. 1891) aus Gaymoos (Lkr.
Mühldorf) wegen Äußerungen über den Krieg.

Urteil: 1 Jahr 3 Monate Gefängnis
($ 2 HG)

24. Dez. 1940 - 16. Mrz. 1943
(1 KMs So 50/41)

Prozeß gegen den kath. Pfarrer Josef LECH-
NER (geb. 24. Mrz. 1879) aus Söllhuben
(Lkr. Rosenheim) wegen politischer Äuße-
rungen.

Urteil: 6 Monate Gefängnis
& 2 HG)

11. Dez. 1940 - 4. Jul. 1944
(1 KMs So 51/41)

Prozeß gegen den Käsermeister Ludwig HECK
geb. 16. Nov. 1897) aus Frickenhausen
Lkr. Memmingen) wegen politischer Äuße-

rungen.

Urteil: 3 Jahre 6 Monate Gefängnis
(S 2 HG. 8 4 Wehrkraft VO)
