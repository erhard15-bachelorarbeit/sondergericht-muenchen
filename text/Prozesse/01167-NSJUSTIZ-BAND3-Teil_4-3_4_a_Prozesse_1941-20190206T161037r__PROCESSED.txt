(5532)

(5533)

(5534)

(5535)

9985

9986

9987

9988

Prozeß gegen den Hilfsarbeiter Anton SCHÄRFF
(geb. 25. Mai 1922) aus Augsburg wegen
eines Handtaschendiebstahls während der
Verdunkelung.

Urteil: 10 Jahre Zuchthaus, ÄAberkennung
der bürgerlichen Ehrenrechte auf
10 Jahre
($ 242 StGB; $ 2 WVVO)

19. Apr. 1941 - 10. Nov. 1944
(1 KLs So 27/41)

Prozeß gegen den Büroarbeiter Alfred SEITZ
(geb. 13. Spt. 1893) aus Rosenheim, früher
SA-Mitglied, wegen Betrugs und unbefugten
Uniformtragens.

Urteil: 5 Jahre Zuchthaus, 600.-RM Geld-
strafe oder 60 Tage Zuchthaus,
Aberkennung der bürgerlichen FEh-
renrechte auf 5 Jahre, Sicherungs-
verwahrung
($ 6 Ordensgesetz; 3$8 20a,132,264,

267 268 StGB)

1. Jun, 1943 in das KZ Mauthausen ver-
bracht, Akten am 8. Jun. 1943 weggelegt.

25. Feb. 1941 - 8. Jun. 1943
(1 KLs So 28/41)

Prozeß gegen den Hilfskassenarzt Theodor
WEISS (geb. 17. Aug. 1913) aus Langerrin-
gen (Lkr. Schwabmünchen) wegen Abtreibung,
fahrlässiger Tötung und Totschlag.

Urteil: 15 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
10 Jahre
(88 212,218,222 StGB)

23. Feb. 1941 - 12. Apr. 1945
(1 KLs So 29/41)

Prozeß gegen den Bohrer Alfred BONAITA (geb.
6. Nov. 1915), italienischer Staatsbürger
den Fräser Ernst DEIL (geb. 24. Dez. 1921),
den Fliesenleger Erich HUBER (geb. 8. Jun.
1923), den Fräser Alfons SCHMIDL (geb.

28. Aug. 1921), den Dreher Josef RAUMER
(geb. 25. Apr.1923), alle aus Augsburg,
wegen groben Unfugs und Körperverletzung.

Urteil: Bonaita 2 Jahre 6 Monate Gefängnis;
Deil 4 Monate Gefängnis; Schmidl
6 Monate Gefängnis; Huber 8 Wochen
Gefängnis; Rauner 10 Wochen Gefäng-
nis mit Probezeit bis 1. Jun. 1945
($$ 1,3,9 Jad; 88 223 ,223a ,360 StGB)
