(5754)

(5755)

(5756)

(5757)

(5758)

10205

10206

10207

19208

1909209

(4 KLs So L4/41)

Prozeß gegen den polnischen Hilfsarbeiter
Jan BAYNO (geb. 8. Dez. 1917) aus München
wegen versuchter Nowtzucht.

Urteil: 3 Jahre Zuchthaus, 3 Jahre Ehr-
verlust
(83 43,177 SB)

4. Jun. 7941 - 14. A 1944

(4 KLs So 46/41)

Prozeß gegen den Lusverkaufer Mathias
BAUER (geb. 3. Jan. 1900) aus Füssen we-
gen Betrugs.

Urteil: 3 Jahre Zuchthaus, 300.-RM Geld-
strafe oder 6 Tace Zuchthaus, 3
Jabr. Enrverlust
(88 253,264 ,2E6 StGB; 8 4 VVO)

31. Mai 1941 - 9. Jun. 1944

(4 KLs So 48/41)

Prozeß gegen den Handelsangestellten Josef
FASCHING (geb. 16. Aug 1902), den Kellner
Hermann HIRSCHLER (eb. 9. Apr. 1920),
Halbjude, und der Ctto HERRLEIN (geb.

10. Jan. 1895). alle aus Munchen, wegen
Manteldiekstahls bzw. ifilehlerei.

Urteils Fasching mel Tcdesstrafe, 10 Jahre
Zuchthais, Ehrverlust auf Lebens-
zeit; Hırschler 2 Jahre Gefängnis;
Herrlein Freispruch
(88 20a,242,244,259, bes, 264 StGB;

8 4 vo)

12. Feb. 1941 - 4. Okt. 1943
(4 KLS So Ä9/41)

Prozeß gegen den polnischen Zivilarbeiter
Kasmier BILSKI (geb. 21. Feb. 1921) aus
Dingolfing wegen Disbstahls und Betrugs-

Urteil: 3 Jahre 3 Monate Zuchthaus, 3 Jahre
BEhrverlusrt
(88 242,263 StGB; 8 2 VVO)

28. Mai 1941 - 29. Jan 1945
(4 KLs So 50/41)

Prozeß gegen die polnische Landarbeiterin
Maryanna GIERA (geb. 9. Jul. 1923) aus
Ingolstadt wegen falscher Anschuldigung.

Urteils 3 Jahre Gefangnis
(383 1,3,9 JG; $ 164 StGB)

21. Feb. 1941 L 8. Mrz. 1944
(4 KLls So 51/41)
