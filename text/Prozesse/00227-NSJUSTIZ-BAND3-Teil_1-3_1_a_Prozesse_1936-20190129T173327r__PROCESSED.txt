(1048)

(1049)

(1050)

(1051)

(1052)

8049

8186

8187

8128

8189

Prozeß gegen den Verwaltungssekretär Franz
Paul ALBRECHT (geb. 27. Feb. 1888) aus Mün-
chen wegen beleidigender Außerungen über
Hitler und Göring.

Urteil: Freispruch

31. Mai 1935 - 28. Feb. 1937
(16 KMs So 31/36)

Prozeß gegen den Kaffeehausbesitzer Paul
LEUBNER (geb. 10. Apr. 1893) aus Bayrisch-
zell (Lkr. Miesbach) wegen der Behauptung,
Dr. Ley habe einen jüdischen Urgroßvater.

Urteil: 3 Monate Gefängnis

12. Feb. 1936 - 31. Mai 1938
(16 KMs So 32/3060)

Prozeß gegen den Rentner Peter FRIEDRICH
(geb. 9. Apr. 1866), den Oberpostschaffner
August KOOB (geb. 23. Feb. 1889), dessen
Ehefrau Therese KOOB (geb. 7. Jun. 1890),
die Schneiderin Hildegard KOOB (geb. 28.
Nov. 1914), die Schneiderin Barbara
STÜTZINGER (geb. 19. Nov. 1905), die
Schäfflersfrau Maria HARGASSER (geb. 6.
Mrz. 1898), die Invalidensfrau Babette
BACKMUND (geb. 19. Spt. 1869), die Metzgers-
frau Anna GNAN (geb. 14. Mai 1884), alle
aus München, wegen der Behauptung, in
einem weiblichen Arbeitsdienstlager sei
der größte Teil der Insassen durch Bur-
schen aus einem benachbarten Arbeitsdienst-
lager geschwängert worden.

Urteil: Freispruch

11. Spru. 1935 - 28. Mai 1936
(16 Klas So 33/36)

Prozeß gegen den Hilfsarbeiter Alfons
HOFMANN (geb. 9. Feb. 1908) aus Augsburg
wegen Verbreitung von Greuelnachrichten
über das KZ Dachau.

Urteil: 5 Monate Gefängnis

28. Jan. 1936 - 19. Mai 1938
(16 KMs So 34/36)

Prozeß gegen den Landwirt Josef GEIGER
(geb. 26. Feb. 1891) aus Giggenhausen

Lkr. Freising) wegen abwertender Äußerun-
gen über die HJ und die NSDAP.

Urteil: 4 Monate Gefängnis

29. Dez. 1935 - 31. Mai 1938,
(16 KMs So 35/36)
