(5632)

(5633)

10084

19085

(2 KLs So 67/41)

Prozeß gegen den Viehhändler Ludwig PRELLER
(geb. 10. Aug. 1902) aus Haidenkofen (Lkr.
Landau), Pg., den Bauern und Fleisch-
beschauer Ludwig KULZER (geb. 21. Jul.
1893) aus Haidenkofen, den Bauern Rupert
PETZENHAUSER (geb. 1. Spt. 1915) aus Lich-
ting (Lkr. Straubing) wegen Schwarzschlach-
tens und Beihilfe dazu.

Urteil: Preller 2 Jahre Gefängnis, 25950.-
RM + 3700.-RM Geldstrafen oder
10 + 37 Tage Gefängnis; Kulzer
1 Jahr Gefängnis, 50.-RM + 2000.-RM
Geldstrafen oder 2 + 20 Tage Ge-
fängnis; Petzenhauser 300.-RM +
20.-RM + 50.-RM Geldstrafen oder
30 + 1 + 1 Tag Gefängnis
($ 1 KWVO; 8 396 RAO; 0 1-5

Schlachtsteuergesetz; 88 348
359 StGB; $ 1 Verbr.R.Str.VO)

13. Mai 1941 - 18. Mrz. 1945
(2 KLs So 68/41)

Prozeß gegen den Metzgermeister Ludwig
MESSERKLINGER (geb. 19. Jun. 1897), den
Metzgermeister Sebastian SEIDENSCHWARZ
(geb. 9. Feb. 1905), dessen Ehefrau Maria
SEIDENSCHWARZ (geb. 23. Okt. 1903), den
Fleischbeschauer Walter KAUFMANN (geb.
28. Nov. 1895), alle Pg., den Metzger
Johann FEUCHT (geb. 21. Jul. 1888), alle
aus Vilshofen, den Landwirt Ludwig HÖLZL
(geb. 26. Jun. 1878), aus Zeitlarn (Lkr.
Vilshofen), die Metzgergehilfenehefrau
Agnes SCHIESSL (geb. 5. Mai 1893), den
Angestellten Johann GERHARDINGER (geb.
24. Jun. 1886), letztere beide aus VMils-
hofen, die Fabrikarbeitersehefrau Rosa
BIRKENEDER (geb. 27. Jun. 1905), den Land-
wirt Georg FUCHSGRUBER (geb. 23. Mrz.
1886) , letztere beide aus Hundsöd (Gde.
Alkofen, Lkr. Vilshofen) und den Land-
wirt Josef ZIERER (geb. 13. Mrz. 1871) aus
Vilshofen, wegen Schwarzschlachtens.

Urteil: Messerklinger 1 Jahr 3 Monate
Gefängnis, 300 + 3200.-RM Geld-
strafen oder 12 + 32 Tage Gefängnis;
Sebastian Seidenschwarz 1 Jahr
8 Monate Gefängnis, 300 + 3200.-RM
Geldstrafen oder 12 + 32 Tage Ge-
fängnis; Maria Seidenschwarz 1 Jahr
Gefängnis, 300 + 3200.-RM Geldstrafen
oder 12 + 32 Tage Gefängnis;
Kaufmann unter Einrechnung von 9
Monaten Gefängnis aus einem Urteil
des Sch-G-Passau vom 26. Mai 1941
