(444)

(445)

(446)

(447)

(448)

(449)

8803

8804

8805

880/

8808

8809

Prozeß gegen Johann ZÖRGIEBEL (geb. 311. Jul.
1861) aus München wegen der Behauptung, Hit.
ler habe im Braunen Haus ein Sektgelage vor-
gefunden.

Urteil: 150 RM Geldstrafe oder 30 Tage Ge-
fängnis

5. Feb. 1934 - 20. Spt. 1934

(S Pr 130/34)

Prozeß gegen die Schriftstellersfrau Johanna
UTSCH (geb. 1. Mrz. 1905) aus München wegen
beleidigender Äußerungen über die Minister

Wagner und Schemm.

Urteil: Freispruch

22. Feb. 1934 - 9. Jun. 1934
(S Pr 131/34)

Prozeß gegen den Bahnarbeiter Johann KLINGL
(geb. 14. Jun. 1897) aus Neumarkt (Lkr. Mb]
dorf) wegen der Behauptung, der Reichstags-
brand-Prozeß sei eine "gekaufte" Sache.

Urteil: 6 Monate Gefängnis

5. Apr. 1934 - 25. Aug. 1934
(S Pr 132/34)

Prozeß gegen den Kaufmann Anton SPAETH (geb.
6. Jan. 1877) aus München wegen Devisenver-
gehens. 7

Urteil: 1 Jahr Gefängnis

16. Nov. 1933 - 2. Nov. 1934
(S Pr 134/34)

Prozeß gegen den Stationskommandanten a.D.
Josef BIEDERMANN (geb. 24. Aug. 1873) aus
Irfersdorf (Lkr. Eichstätt) wegen abwerten-
der Äußerungen über die SA.

Urteil: Freispruch

19. Mrz. 1934 - 19. Jun. 1934
(S Pr 135/34)

Prozeß gegen den Buchdrucker Andreas LEHNER
(geb. 2. Apr. 1905) aus Haag (Lkr. Wasser-
burg) wegen kritischer Äußerungen über den

Urteil: Freispruch

27. Dez. 1933 - 7. Jun. 1934
(S Pr 136/34)
