(331)

(332)

(33)

(334)

(335)

7674

7675

7676

7677

7678

Urteil: Freispruch

30. Okt. 1933 - 14. Feb. 1934
(S Pr 6/34)

Prozeß gegen den Kaufmann Karl GÖTHERT (geb.
6. Nov. 1904) aus München wegen der Behaup-
tung, im KZ Dachau würden Kommunisten miß-

handelt.

Urteil: 2 Monate Gefängnis

11. Aug. 1933 - 24. Aug. 1934
(S Pr 7/34)

Prozeß gegen den Kleinspediteur Josef GRUBER
(geb. 9. Dez. 1901), Mitglied des Deutsch-
Nationalen Handelsgehilfenverband, aus Mün-
chen, wegen der Behauptung, einige Pgg. hät-
ten nach Hitlers Abfahrt auf Parteikosten
weitergezecht.

Urteil: 300 RM Strafe oder 30 Tage Gefängnis

5. Aug. 1933 - 3. Aug. 1934
(S Pr 8/34)

Prozeß gegen den Landwirt Markus KLAIBER (geb.
30. Apr. 1885), NSDAP-Mitglied, und den Bauern
Georg ZECH (geb. 4. Mai 1877), Mitglied des
Bauernbundes, beide aus Immelstetten (Lkr.
Mindelheim) wegen kritischer Bemerkungen über
das Winterhilfswerk.

Urteil: Klaiber 100 RM Strafe oder 1 Monat
Gefängnis; Zech 200 RM Strafe oder
1 Monat Gefängnis

22. Nov. 1933 - 16. Apr. 1934
(S Pr 9/34)

Prozeß gegen den Automechaniker Josef LEITZ
(geb. 17. Mai 1915) aus München wegen kri-
tischer Äußerungen gegen Hitler.

Urteil: 2 Monate Gefängnis

13. Nov. 1933 - 23. Aug. 1934
(S Pr 10/34)

Prozeß gegen die Dienstmagd Katharina
RUHLAND (geb. 13. Spt. 1913) aus Mering

(Lkr. Friedberg) wegen der Behauptung,

die Nat. Sozz. hätten den Reichstag angezündet.

Urteil: 4 Monate Gefängnis

29. Nov. 1933 - 15. Mai 1934
(S Pr 11/34
