(261)

(262)

(263)

(264)

(265)

7613

7614

7615

7616

7617

sching (Lkr. Starnberg) wegen der Behauptung,
Deutschland werde von 3 Idioten regiert.

Urteil: Freispruch

23. Okt. 1933 - 30. Jan. 1934
(S Pr 314/33)

Prozeß gegen den Reisenden Josef MERK (geb.
4. Feb. 1893) aus Ottobeuren (Lkr. Memmingen)
wegen kritischer Äußerungen über die NSDAP.

Urteil: Freispruch

16. Okt. 1933 - 5. Feb. 1934
(S Pr 315/33)

Prozeß gegen den Dekorateur Wilhelm BACH
(geb. 25. Mai 1896), den Kaminkehrer Seba-
stian TASCHNER (geb. 1. Apr. 1901) beide
aus München, wegen kritischer Äußerungen
zur Politik.

Urteil: Bach 3 Monate Gefängnis und 6 Wo-
chen Haft, Taschner 5 Monate Ge-
fängnis

26. Okt. 1933 - 30. Okt. 1934
(S Pr 316/33)

Prozeß gegen den Schachtmeister Michael
MÜHLBAUER (geb. 23. Nov. 1877) aus Mühl-
dorf wegen der Behauptung, die Nat.Sozz.
hätten den Reichstag ahgezündet.

Urteil: 6 Monate Gefängnis, ab 6 Wochen
Untersuchungshaft

31. Okt. 1933 - 9. Mai 1934
(S Pr 317/33)

Prozeß gegen den Säger Otto FENT (geb. 25.
Jan. 1898) aus Feldkirchen bei München
wegen beleidigender Äußerungen über die SA.

Urteil: 4 Wochen Haft

7. Spt. 1933 - 8. Spt. 1934
(S Pr 318/33)

Prozeß gegen den Schneider Lorenz SCHOBER
(geb. 12. Mrz. 1904) aus Augsburg wegen
der Behauptung, die Nat.Sozz. hätten den
Reichstag angezündet.

Urteil: 7 Manate Gefängnis

6. Nov. 1933 - 31. Aug. 1934
(S Pr 319/33)
