(1454) 9149

(1455) 9150

(1456) 9151

(1457) 8423

Prozeß gegen den Schlosser Josef HÖSS
(geb. 24. Nov. 1898) wegen Verbreitung
Von Greuelgeschichten über das KZ
Dachau in München.

Urteil: 4 Monate Gefängnis

21. Mai 1937 - 19. Okt. 1937
(1 kMs So 68/37)

Prozeß gegen die Friseursehefrau Rosa
ASCHENBRENNER (geb. 27. Apr. 1885) aus
Untermenzing (München), USPD-Mitglied,
dann KPD-Mitglied, dann SPD-Mitglied,
wegen der Außerung, deutsche Flieger
hätten die spanische Stadt Guernica
beschossen.

Urteil: 6 Wochen Gefängnis

3. Jun. 1937 - 23. Feb. 1938
(1 KMs So 69/37)

Prozeß gegen die Schlossersehefrau Maria
GRADL (geb. 25. Okt. 1886), die Kutschers-
ehefrau Anna SCHMIDMAIER (geb. 31. Jul.
1897), die holländische Staatsangehörige
Anna Therese FOL (geb. 20. Jan. 1877),
alle aus München, wegen Betätigung für die
"Reformbewegung der siebenten Tags-
Adventisten".

Urteil: je 1 Monat Gefängnis

Das Verfahren in der säben Sache gegen
die Schreinerswitwe Auguste ECKER (geb.
15. Aug. 1875) aus München wurde einge-
stellt.

Im Akt Vernehmungsprotokoll des SoG-
Frankfurt a.M. des Schneiders Jacob
HARTMANN (geb. 14. Okt. 1892) aus Eichen
(Lkr. Hanau) über die "Siebenten Tags-
Adventisten" vom 25. Jan. 1937 und
hektographierte Abschrift der Vernehmungs-
niederschrift des Führers der "Siebenten
Tags-Adventisten (Reformbewegung)" Otto
LUFT (geb. 27. Okt. 1897) aus Hannover
in Hannover vom 10. Nov. 1936.

18. Nov. 1936 - 5. Dez. 1938
(; kMs So 70/37)

Prozeß gegen den Fabrikarbeiter Alois
KOLLES (geb. 30, Mrz. 1911) aus Bernau
(Lkr. Rosenheim) wegen der Äußerung,

auch führende Nat.Sozz. seien homosexuell.

Urteil: 7 Monate Gefängnis

7. Jul. 1937 - 29. Mrz. 1941
(1 KMs So 71/37)
