(4639)

(4640)

(4641)

(4642)

9637

9638

9639

9640

Prozeß gegen den Fabrikdirektor Walter
KNAPPICH (geb. 27. Jun. 1899), früher
NSDAP-Mitglied, und dessen Ehefrau Sofie
KNAPPICH (geb. 26. Mai 1898), beide

aus Augsburg, wegen Sachhehlerei.

Urteil: Freispruch
$& 1 KWVO)

6. Aug. 1940 - 6. Jan. 1943
(2 KLs So 27/40)

Vollstreckungsheft aus dem Prozeß gegen
den Hilfsarbeiter Josef WAIDMANN (geb.
15. Mrz. 1904) aus München, wegen Dieb-
stahls.

Urteils 8 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
5 Jahre, Sicherungsverwahrung
(88 20a,42,242 StGB)

Waidmann starb am 8. Aug. 1943 im KZ
Mauthausen.

(3 KLs So 7/40)

Prozeß gegen den Mechaniker Fritz LEUEN-
BERGER (geb. 19. Jun. 1910) aus München
wegen Diebstahls.

Urteil: 7 Jahre Zuchthaus, 24mal 50.-RM
Geldstrafe oder 24mal 5 Tage
Zuchthaus, Aberkennung der bür-
gerlichen Ehrenrechte auf 5
Jahre, Sicherungsverwahrung

11. Jan. 1940 - 6. Jan. 1943
(3 KLs So 25/40)

Prozeß gegen den polnischen Landarbeiter
Franz KRAWOCZYK (geb. 3. Mrz. 1915) aus
Roth (Lkr. Neu-Ulm), wegen versuchten
Totschlags in Beuren (Lkr. Neu-Ulm).

Urteil: 2 Jahre 6 Monate Zuchthaus
(88 211,212 StGB)

nach Beendigung des Strafvollzugs weiter
in Staatspolizeihaft.

21. Jun. 1940 - 18. Jan. 1943
(3 KLs So 28/40)

Prozeß gegen den Mechaniker Johann WEISS
(geb. 9. Jun. 1905) aus Kempten, wegen
Diebstahis.

Urteil: 5 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
5 Jahre, Sicherungsverwahrung
($$ 20a,242-245 StGB)
