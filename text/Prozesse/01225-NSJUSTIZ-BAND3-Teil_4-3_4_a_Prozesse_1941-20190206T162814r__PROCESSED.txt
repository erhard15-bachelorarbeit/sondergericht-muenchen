(5779)

(5780)

(5781)

(5782)

(5783)

10230

10131

10232

10233

10234

Prozeß gegen die polnische Hilfsarbeiterin
Josefa JANIK (geb. 12. Dez. 1920) aus
München wegen Diebstahls.

Urteil: 1 Jahr Zuchthaus, 2 Jahre Ehrver-
lust
(3 242 StGB; 8 4 VVO)

4. Okt. 1941 - 24. Feb. 1942
(4 KLs So 76/41)

Prozeß gegen den Bergmann Karl EICHNER
(geb. 28. Dez. 1906) aus Penzberg (Lkr.
Weilheim) wegen Diebstahls.

Urteil: 5 Jahre Zuchthaus, 5 Jahre Ehr-

verlust
(38 20a,242,243 StGB; 3 2 VVO)

28. Apr. 1941 - 20. Aug. 1944
(4 KLs So 77/41)

Prozeß gegen den Reichsbahnleitungsbauar-
beiter Zdenek GRUNCL (geb. 30. Dez. 1918)
aus München wegen Volltrunkenheit.

Urteil: 2 Jahre Gefängnis
($ 330a StGB)

14. Spt. 1941 - 1. Aug. 1943
(4 KLs So 78/41)

Prozeß gegen die Hilfsarbeiterin Kunigunde
TRAUTNER (geb. 19. Jul. 1903) aus München
wegen Diebstahls und Betrugs.

Urteil: 4 Jahre Zuchthaus, mal 100.-RM
Geldstrafen oder 60 Tage Zuchthaus,
4 Jahre Ehrverlust, Sicherungs-
verwahrung

(88 20a,242,244,263,264 StGB;

8 4 VVO)

Gest. 14. Apr. 1943 in polizeilicher
Vorbeugungshaft im KZ Ausschwitz.

12. Jul. 1941 - 21. Apr. 1943
(4 KLs So 80/41)

Prozeß gegen den polnischen Landarbeiter
Joseph BOLEK (geb. 15. Mrz, 1904) aus
Petersbuch (Lkr. Hilpoltstein) wegen
Diebstahls.

Urteil: Freispruch
($ 242 StGB; 383 4 VVO)

8. Jul. 1941 - 17. Feb. 1943
(4 KLls So 81/41)
