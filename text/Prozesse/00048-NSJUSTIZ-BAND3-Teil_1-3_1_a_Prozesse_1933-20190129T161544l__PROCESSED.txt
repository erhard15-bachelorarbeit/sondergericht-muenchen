4)

(115)

(116)

(117)

(118)

7467

7468

7469a

7470

7471

Prozeß gegen den Schneider Anton GLEIXNER
(geb. 30. Mrz. 1910) aus München wegen be-
leidigender Außerungen über Göring u.a..

Urteil: 5 Monate Gefängnis

28. Jul. 1933 - 1. Feb. 1934
(S Pr 160/33)

Prozeß gegen den Gärtner und SS-Mann Niko-
laus SCHLEMMER (geb. 1. Jan. 1910) aus Mün-
chen wegen der Behauptung, er habe am Brau-
nen Haus als Posten gestanden, als Hitler
dort Parteigenossen bei einem Schlemmerge-
lage angetroffen habe.

Urteil: 7 Monate Gefängnis

28. Jun. 1933 - 30. Mrz. 1934
(S Pr 161/33)

Prozeß gegen den Hilfsarbeiter Andreas
SCHILLING (geb. 14. Mai 1915), den Hilfsar-
beiter Josef KLFEINGÜTL (geb. 3. Mai 1913),
den Metallschleifer Engelbert SCHERÜBL (geb.
15. Dez. 1912); alle aus München, wegen Ver-
breitung kommunistischer Flugblätter.

Urteil: 10 Monate Gefängnis für Schilling,
je 6 Monate Gefängnis für Klein-
gütl und Scherübl.

Das Verfahren wurde mangels Beweise
vor der Anklageerhebung eingestellt
egen den Schuhmacher Anton FORSTER
(geb. 9. Apr. 1905) und den Drechs-
ler Anton SCHUDER (geb. 12. Jun. 19
beide aus München.

15. Jul. 1933 -- 8. Aug. 1934
(S Pr 162/33)

Prozeß gegen den Kraftwagenführer Adalbert
REISER (geb. 1. Feb. 1892) aus Kempten we-
gen Äußerungen über mögliche Besetzung
Österreichs durch SS und SA und über Mini-
ster Wagner. R. war bis Feb. 1932 SPD-Mit-
glied.

Urteil: 4 Monate Gefängnis
23. Jun. 1933 - 28. Feb. 1934
(S Pr 163/33)

Prozeß gegen den Hilfsarbeiter Wilhelm
SCHORER (geb. 25. Nov. 1914) aus München
wegen Verteilens kommunistischer Schriften.

Urteil: 3 Monate Gefängnis, ab 3 Wochen
Untersuchungshaft
