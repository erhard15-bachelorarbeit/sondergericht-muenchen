(1842)

(1843)

(1844)

(1845)

(1846)

8626

8627

8628

8629

8630

Am 2. Mrz. erhielt D. Strafunterbrechung
und wurde in das "Arbeits- und Erziehungs-
lager Mauthausen verlegt". Am 26. Mrz. 194
wurde die Weglegung seiner Akten Verfügt.

28. Jan. 1938 - 26. Mrz, 1943
(1 KLs So 21/38)

Prozeß gegen den Lagerarbeiter Clement
BAUMANN (geb. 14. Okt. 1898) aus Augs-
burg, früher NSDAP-Mitglied, wegen Ur-
kundenfälschung.

Urteil: 10 Monate Gefängnis

29. Jul. 1938 - 29. Mai 1939
(1 KL s So 22/38)

Prozeß gegen den Arbeiter Karl Josef
ZELLER (geb. 18. Aug. 1920) wegen
Diebstahls und Urkundenfälschung in
Isen (Lkr. Wasserburg), Salzburg
(Österreich) und Simbach (Lkr, Pfarr-
kirchen).

Urteil: 11 Monate Gefängnis

19. Spt. 1938 - 23. Aug. 1939
(1 KLs So 23/38)

Prozeß gegen den Hilfsarbeiter Paul
EISENMANN (geb. 5. Aug. 1912) aus
München wegen Verfälschung eines
Eisenbahnfahrscheines.

Urteil: 2 Monate Gefängnis
5. Spt. 1938 - 10. Jan. 1939

Prozeß gegen den Lastkraftwagenführer
Rudolf Max DIETRICH (geb. 22. Jun.

1912) aus Miesbach wegen Betrugs und
Diebstahls in Böbing (Lkr. Schongau).

Urteil: 2 Jahre Gefängnis

21. Nov. 1938 - 3. Mrz. 1939
(1 KLs So 25/38)

Prozeß gegen den Landwirtssohn Michael
WÖRLE (geb. 6. Jan. 1923), dessen Bruder
Georg WÖRLE (geb. 28. Apr. 1920), beide
aus Eschenberg (Lkr. Füssen) und d.nm
Dienstknecht Josef Karl LEUPRECHT (geb.
7. Mai 1923) aus Ehenbichl (Tirol) wegen
Sprengstoffvergehens.

Urteil: Georg Wörle 4 Monate 14 Tage
Gefängnis; Michael Wörle und
Karl Leuprecht je 3 Monate
Gefängnis e

1. Spt. 1938 - 13. Apr. 1942
(1 KLs So 26/38) p 365
