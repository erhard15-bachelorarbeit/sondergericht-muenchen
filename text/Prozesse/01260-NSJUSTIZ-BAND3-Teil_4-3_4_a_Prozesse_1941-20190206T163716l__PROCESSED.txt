(5951)

(5952)

(5953)

(5954)

(5955)

10399

10400

10401

10402

10403

Prozeß gegen die Verkäuferin Klara GRUBER
(geb. 15. Jul. 1921) und die Küchengehlfin
Maria MESSNER (geb. 23. Mai 1920), beide
aus München, wegen verbotenen Umgangs mit
den französischen Kriegsgefangenen Albert
DUBART und August RODA.

Urteil: je 3 Monate Gefaängnis
(8 4 Wehrkraft VO

12. Apr. 1941 - 26. Apr. 1943
(1 KMs So 75/41)

Prozeß gegen den Glaser Richard THOMA (geb
19. Feb. 1886) aus Blaichach (Lkr. Sont-
hofen) wegen politischer Äußerungen.

Urteil: 6 Monate Gefängnis
$ 2 HG; $ 134b StGB)

14. Okt. 1940 - 21. Aug. 1944
(1 KMs So 77/41)

Prozeß gegen die Landwirtstochter Maria
HÖHENLEITNER (geb. 28. Jul. 1917) aus
Etzenhausen (Lkr. Dachau) wegen verbotenen
Umgangs mit dem französischen Kriegsgefan-
genen Victor de CLEROQUE.

Urteil: 1 Jahr 8 Monate Zuchthaus
8 4 Wehrkraft VO)

8. Mai 1941 - 26. Mrz. 1945
(1 KMs So 78/41)

Prozeß gegen die Hilfsarbeiterin Viktoria
BÖSWIRTH (geb. 30. Aug. 1913) aus Dachau
wegen verbotenen Umgangs mit dem franzö-
sischen Kriegsgefangenen Gaston DEJOIFE.

Urteil: 1 Jahr 8 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 4 Wehrkraft VO)

8. Mai 1941 - 23. Feb. 1944
(1 KMs So 79/41)

Prozeß gegen den Bauern Albert RUCHTI (geb.
28. Jul. 1880) aus Biberschwang (Lkr. Kemp-
ten) wegen Äußerungen über Hitler.

Urteil: 1 Jahr 8 Monate Gefängnis
($ 2 Ha)
R. war Bauernbund-Mitglied.

27. Dez. 1940 - 1. Jun. 1943
(1 KMs So 80/41)
