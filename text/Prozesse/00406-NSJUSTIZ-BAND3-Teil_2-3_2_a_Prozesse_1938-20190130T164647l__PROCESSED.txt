(1882)

(1883)

(1884)

(1885)

(1886)

(1887)

8666

8667

8668

8669

8670

8671

Prozeß FB den Kaufmann Alfred KUGLAND
(geb. 14. Mrz. 1907) aus München wegen
politischer Außerungen.

Urteil: 3 Monate Gefängnis

14. Spt. 1937 - 5. Mai 1938
(1 kMs So 37/38)

Prozeß gegen den Kaufmann Karl KIRSCHNER
(geb. 10. Dez. 1884) wegen der Behauptung
in Farn ach (Lkr. Rosenheim), Göring habe
eine Halbjüdin geheiratet.

Urteil: 6 Monate Gefängnis

8. Dez. 1937 - 17. Mai 1938
(1 KMs So 38/38)

Prozeß gegen den jüdischen Kaufmann
Max SCHWARZ (geb. 22. Aug. 1898) aus
München wegen Äußerungen über das
Regime.

Urteil: 7 Monate Gefängnis

20. Aug. 1937 - 4. Apr. 1941
(1 KMs So 39/38)

Prozeß gegen die Arbeiterin Sofie
NEUMAIER (geb. 11. Dez. 1906) aus
Freising wegen der Behauptung, Alt-
PG zu sein und wegen Äußerungen über
das Regime.

Urteil: 8 Monate Gefängnis

17. Aug. 1937 - 24. Apr. 1941
(1 KMs So 40/38)

Prozeß gegen den Hilfsarbeiter Johann
POPP (geb. 13. Nov. 1886) aus München
wegen Erzählens von Greuelnachrichten
über das KZ Dachau.

Urteil: 6 Monate Gefängnis

2. Spt. 1937 - 3. Mai 1938
(1 KMs So 41/38)

Prozeß gegen den Landwirt und Badbesitzer
Johann HIESSERER (geb. 27. Dez. 1893) aus
Pilzweg (Lkr. Passau) wegen abwertender
Bemerkungen über führende Nat.Sozz..

Urteil: 4 Monate Gefängnis

253. Jan. 1938 - 14. Mai 1938
(1 KMs So 42/38)
