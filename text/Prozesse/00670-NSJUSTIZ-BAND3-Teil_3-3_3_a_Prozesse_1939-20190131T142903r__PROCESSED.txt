(3180)

(3181)

(3182)

(3183)

(3184)

9234

9235

9236

9237

9238

Prozeß gegen die Hausgehilfin Therese
KUGLER (geb. 2. Mai 1905) aus Bad Reichen-
hall wegen Diebstahls.

Urteil: 6 Jahre Zuchthaus, 50,- und 50,- RM
Geldstrafe oder 10 und 1090 Tage
Zuchthaus, Aberkennung der bürger-
lichen Ehrenrechte auf 5 Jahre
(88 20, 42, 242, 2144, 246, 263,
26h StB; $ 4 WO)

26. Mrz. 1943 der Polizei übergeben,
27. Aug. 1943 Weglegung der Akten.

25. Spt. 1939 - 27. Aug. 1943
(1 KLs So 59/39)

Prozeß gegen den kfm. Angestellten Stefan
EUCKER (geb. 27. Aug. 1896) aus München
wegen Abhörens ausländischer Rundfunksen-
der.

Urteil: Freispruch
($ 1 Rdf VO)

8. Spt. 1939 - 24. Jan. 1940
(1 kLs So 60/39)

Prozeß gegen den Glasschleifer Franz LIND-
NER (geb. 5. Okt. 1910, gest. 25. Jan. 1941)
und den Glasgraveur Max KILLINGER (geb.

8. Okt. 1908), beide aus Rabenstein (Lkr.
Regen), wegen Abhörens ausländischer Rund-
funksender.

Urteil: Lindner 7 Monate Gefängnis; Killin-
er Freispruch
(5 1 Rdf VO)

11. Spt. 1939 - 7. Mai 1941
(1 KLs So 61/39)

Prozeß gegen den Bäcker Anton LAYER (geb.
5. Jun. 1881) aus München wegen der Äußerung
daß Göring's Frau nicht arisch sei.

Urteil: 5 Monate Gefängnis
$ 1 HG)

8. Nov. 1938 - 14. Okt. 1939
(1 KMs So 1/39)

Prozeß gegen den Hilfsarbeiter Otto MAYER
(geb. 17. Nov. 1890) aus Ebenhausen (Lkr-
Ingolstadt) wegen kritischer Außerungen über
führende Nat. Sozz.

Urteil: 8 Monate Gefängnis

$ 1 HG; 8 134 StGR)
:". Lfd ich vor dem Urteil 4 Monate in
Polizes*- und Schutzhaft.

17. Aug. 1938 - 5. Dez.1
(1 KMs So 2/39) 739 6
