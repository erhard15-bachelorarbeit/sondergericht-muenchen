(4594)

(4595)

(4596)

(4597)

9592

9593

9594

9595

Urteil: Todesstrafe, 15 Jahre Zuchthaus,
Aberkennung der burgerlichen
Ehrenrechte auf Lebenszeit, Siche-
rungsverwahrung
(88 20a 306,308 StGB; $ 3 WVVO)

18. Okt. 1938 - 14. Dez. 1940
(1 KLs So 145/40)

Prozeß gegen den polnischen Landarbeiter
Stanislaus JASKOLSKI (geb. 2. Mai 1918)
aus Zusmarshausen (Lkr. Augsburg), wegen
Vergiftung eines Pferdes.

Urteil: Freispruch
9 Tierschutzgesetz; $8 2 Wehr-
kraft VO)

27. Apr. 1940 - 14. Jan. 1941
(1 KLs So 146/40)

Prozeß BEN den Schmied Alfred CBERG
(geb. 26. Mai 1920) wegen eines Raub-
überfalls in München.

Urteil: Todesstrafe, Aberkennung der
bürgerlichen Ehrenrechte auf Le-
benszeit
(8 249 StGB; 3 2 VVO)

29. Spt. 1940 - 17. Jan. 1941
(1 KLs So 147/40)

Prozeß gegen den Korbflechter Johann
BREY (geb. 1. Apr. 1915) aus Neustadt
a.d.Donau (Lkr. Kelheim), wegen Dieb-
stahls und Betrugs wahrend der Ver-
dunkelung.

Urteil: 5 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte
auf 4 Jahre
($ 3 HG; $8$ 242,243,263 StGB;

$ 2 vvo)

26. Jun. 1940 - 4. Nov. 1944
(1 KLs So 148/40)

Prozeß gegen den tschechischen Schmied
Alois KAKAU (geb. 23. Mrz. 1911) und
den tschechischen Hilfsarbeiter Wenzel
HOLENDA (geb. 22. Aug. 1918), beide aus
München, wegen gefährlicher Korperver-
letzung.

Urteil: Kaka 5 Jahre Zuchthaus, Aber-
kennung der bürgerlichen Ehren-
rechte auf 5 Jahre; Holenda 3
Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
3 Jahre
(8 223 StGB: 8 2 VVO)
