(1033)

(1034)

(1035)

(1036)

(1037)

(1038)

8171

8172

8172

8174

8176

Prozeß gegen den Kapuzinerpater Franz
JUNGBAUER (geb. 11. Jan. 1901) aus Mün-
chen wegen &bvertender Äußerungen über
führende Nat.Sozz. in einer Predigt.

Urteil: Freispruch

24. Okt. 1935 - 7. Mai 1936
(16 KMs So 15/36)

Prozeß gegen den Maschinisten Georg RAHM
(geb. 1. Aug. 1902) aus München, KPD-An-
hänger, wegen Aufbewahrung kommunistischer
Schriften.

Urteil: 8 Monate Gefängnis

13. Jan. 1936 - 14. Spt. 1936
(16 KMs So 16/36)

Prozeß gegen den Käser Johann ROTTMAIR
geb. 25. Dez. 1889) aus Zusmarshausen
Lkr. Augsburg) wegen kritischer ÄAußerunge

über die Stimmung der Bevölkerung in

Börlas (Lkr. Sonthofen).

Urteil: 6 Monate Gefängnis

7. Jan. 1936 - 19. Mai 1938
(16 KMs So 17/36)

Prozeß gegen den Wagner Franz Xaver
HURZLMEIER (geb. 20. Feb. 1904) aus
Wahlsdorf (Lkr. Rottenburg) wegen poli-
tischer Schimpfereien.

Urteil: 7 Monate Gefängnis

9. Jan. 1936 - 4. Jul. 1936
(T6 KMs So 18/36)

Prozeß gegen den Fabrikarbeiter und
Friseur Alois UNNÜTZER (geb. 7. Spt. 1889)
aus Tacherting (Lkr. Traunstein) wegen ab-
wertender Außerungen über das Regime.

Urteil: 6 Monate Gefängnis

30. Nov. 1935 - 20. Mai 1938
(16 KMs So 19/36)

Prozeß gegen den Bauernsohn Michael
FREIDLING (geb. 23. Okt. 1902) aus
Westerried (Lkr. Markt Oberdorf) wegen
abwertender Äußerungen über das Regime.

Urteil: 3 Monate Gefängnis

10. Feb. 1936 - 19. Mai 1938
(16 KMs So 21/36) 3
