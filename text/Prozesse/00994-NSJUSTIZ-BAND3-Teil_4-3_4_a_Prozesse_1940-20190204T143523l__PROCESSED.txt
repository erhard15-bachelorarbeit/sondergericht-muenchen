(4688)

(4689)

(4690)

(4691)

(4692)

9685

9686

9787

9688

9689

Prozeß gegen den Reichsbahnarbeiter
Josef SCHREIER (geb. 24. Jan. 1912)
aus Aichach wegen Diebstahls.

Urteil: 4 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte
auf 4 Jahre.

(88 242,243 StGB; $ 2 VVO)

20. Spt. 1940 - 25. Jun. 1943
(5 KLs So 87/40)

Prozeß gegen die Hausangestellte Therese
REITER (geb. 28. Dez. 1920) wegen Dieb-
stahls in Ingolstadt.

Urteil: 2 Jahre Zuchthaus
(88 242-245 StGB)

14. Mai 1940 - 24. Apr. 1941
(5 KLs So 88/40)

Prozeß gegen den Taglöhner Alois REICH-
HART (geb. 29. Spt. 1897) aus Radsperre
(Lkr. Kempten) wegen Diebstais.

Urteil: 1 Jahr 6 Monate Zuchthaus
(88 51,242,244,245 StGB; $ 2 VVC)

20. Spt. 1940 - 6. Mai 1942
(5 KLs So 90/40)

Prozeß gegen den Lokomotivführer Erhard
MEISTER (geb. 14. Spt. 1912) aus München,
wegen Diebstahls.

Urteil: 8 Jahre Zuchthaus, Aberkennung
der bürgerlichen BEhrenrechte
auf 10 Jahre
(88 242,243 StGB; $ 2 VVO)

8. Jul. 1940 - 16. Jul. 1942
(5 KLs So 91/40)

Prozeß gegen den Hilfsarbeiter Franz
RUDOLF (geb. 8. Apr. 1875) aus Kempten
wegen Diebstahls wahrend der Verdunke-
lung.

Urteil: 4 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
3 Jahre
(88 242-245 StGB; $ 2 VVO)

19. Okt. 1940 - 5. Feb. 1944
(5 KLs So 92/40)
