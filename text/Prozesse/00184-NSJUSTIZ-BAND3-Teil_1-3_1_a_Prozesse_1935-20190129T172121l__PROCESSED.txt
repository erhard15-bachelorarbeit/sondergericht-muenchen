(825)

(826)

(827)

(828)

(829)

7963

7964

7965

7966

7967

Urteil: 0 Monate Gefängnis

26. Mrz. 1935 - 2. Dez. 1935
(S Pr 89/35)

Prozeß gegen den Dienstknecht Matthias
EGGER (geb. 16. Dez. 1905) aus Kulbing
(Lkr. Laufen), KPD-Anhänger, wegen ab-
wertender Äußerungen über das Regime.

Urteil: 6 Monate Gefängnis

253. Mrz. 1933 - 11. Dez. 1935
(S Pr 90/35)

Prozeß gegen den Dienstknecht Josef BAUER
(geb. 4. Jul. 1913) aus Bergen (Lkr.
Freising) wegen der Behauptung in Inz-
kofen (Lkr. Freising), Hitler sei ein
"Spinaterer".

Urteil: 55 Monate Gefängnis

11. Apr. 1935 - 17. Mai 1938
(S Pr 91/35)

Prozeß gegen den Dienstknecht Johann
HAFENEDER (geb. 1. Mai 1906) aus Dombach
(Lkr. Vilsbiburg) wegen Betrugs.

Urteil: 11 Monate Gefängnis

18. Mrz. 1935 - 27. Feb. 1936
(S Pr 92/35)

Prozeß gegen den Friseurgehilfen Josef
KNOBEL (geb. 15. Dez. 1901) aus Kempten,
Mitglied im kath. Gesellenverein, wegen
kritischer Äuerungen über politische Ding

Urteil: 3 Monate Gefängnis

10. Apr. 1935 - 14. Nov. 1935
(S Pr 73/35)

Prozeß gegen den Maschinenschlosser
Fridolin FOCHREINER (geb. 13. Spt. 1893)
aus München, KPD-Mitglied, wegen der Er-
zählung, 10 SS-Leute seien im KZ Dachau
wegen Lebensmittelschmuggel erschossen
worden.

Urteil: 5 Monate Gefängnis

H. war von Ende Juni 1933 - 18. Mrz. 1934
als Schutzhä ftling im KZ Dachau.

6. Feb. 1935 - 22. Mai 1939
(S Pr 94/35)
