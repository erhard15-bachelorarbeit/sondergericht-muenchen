(183)

(184)

(185)

(186)

(187)

(188)

7537

7538

7539

7540

7469b

7541

Frozeß gegen die Hilfsarbeiter Georg (get.
14. Aug. 1911) und Josef (geb. 24. Dez.

1912) DIRSCHERL, beide aus München, wegen
feindlicher Äußerungen gegen die Regierur;.

Urteil: Georg D. 10 Monate Gefängnis,
Josef D. 8 Monate Gefängnis

Josef D. wird nach der Entlassung aus
Strafhaft ins KZ Dachau überstellt.

17. Aug. 1933 - 20. Okt. 1934
(S Pr 232/33)

Prozeß gegen den Maschinenschlosser Johann
METZELER (geb. 13. Feb. 1882) aus Memminger
wegen Beleidigung der Regierung.

Urteil: 8 Monate Gefängnis

15. Spt. 1933 - 5. Jul. 1934
(S Pr 233/33)

Prozeß gegen den Hilfsarbeiter Joseph
LUDWIG (geb. 10. Aug. 1908) aus München
wegen der Behauptung, in München habe es
50 - 60 Tote und etwa 300 Schwerverletzte
gegeben.

Urteil: 5 Monate Gefängnis

22. Mai 1933 - 22. Feb. 1934
(S Pr 234/33)

Prozeß gegen den Hilfsarbeiter Ludwig
MÄNNER (geb. 25. Aug. 1888) aus Landshut
wegen Unfug und Betrug.

Urteil: 1 Jahr Gefängnis

9. Aug. 1933 - 10. Okt. 1934
(S Pr 235/33)

Prozeß gegen den Obsthausierer Huro
HAUPPENBERGER (geb. 30. Mrz. 1898 aus
München wegen kritischer Außerungen
über das KZ Dachau.

Urteil: Freispruch mangels Beweisen

21. Aug. 1933 - 6. Dez. 1933
(S Pr 236/33)

Prozeß gegen den Kunstmaler Paul Friedrich
Wilhelm KÄUSCHKE (geb. 25. Spt. 1882) aus
München wegen kritischer Äußerungen über
den NS. K. war KP-Mitglied gewesen.

Urteil: 6 Wochen Gefängnis

9. Jul. 1933 - 3. Apr. 1934
(S Pr 237/33)
