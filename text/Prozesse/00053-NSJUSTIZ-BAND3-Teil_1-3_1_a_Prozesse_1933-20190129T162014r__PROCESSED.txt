(136) 7489 Prozeß gegen den Kürschner Albert LÖRCHER
(geb. 12. Jun. 1913) aus München wegen Wei-
tergabe einer kommunistischen Flugschrift
zur Verteilung.

Urteil: 11 Monate Gefängnis

In der gleichen Sache ist Andreas SCHILLING
angeklagt, der als Zeuge dient.

10. Aug. 1933 - 29. Jul. 1934
(S Pr 182/33)

(137) 7490 Prozeß gegen den Hilfsarbeiter Jakob SCHMID
(geb. 14. Okt. 1878) aus München, SPD-Mit-
. glied, wegen der Behauptung "die Hitler"
s hätten aus dem Gewerkschaftshaus 3 Millionen
Mark gestohlen.

Urteil: 4 Monate Gefängnis, ab 6 Wochen
Untersuchungshaft

15. Jul. 1933 - 11. Dez. 1933
(S Pr 183/33)

(138) 7491 Prozeß gegen den Hilfsarbeiter Matthias
SPANN (geb. 21. Jan. 1878) aus München
wegen beleidigender Äußerungen über
Hitler.

Urteil: 6 Monate Gefängnis, ab 6 Wochen
Untersuchungshaft

27. Jul. 1933 - 12. Feb. 1934
(S Pr 184/33)

(139) 8153 Prozeß gegen den Schlosser Josef SEEBERGER
(geb. 15. Spt. 1899) wegen Diebstahl in
Grafing (Lkr. Ebersberg) und Ebersberg.

Urteil: 3 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
5 Jahre

Vor der Haftentlassung Prozeß mit nach-
träglicher Anordnung der Sicherungsver-
wahrung, 1943 an die Polizei zur Ein-
lieferung in das KZ Mauthausen übergeben.

27. Jun. 1933 - 13. Apr. 1943
(S Pr 185/33)

(140) 7492 Prozeß gegen den Hilfsarbeiter Ludwig ZIERL
(geb. 12. Spt. 1897) aus München wegen un-
begründeter Behauptungen über das KZ Dachau.

Urteil: 10 Monate Gefängnis

9. Jun. 1933 - 27. Aug. 1934
(S Pr 186/33) S
