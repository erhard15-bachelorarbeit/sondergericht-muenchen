189)

(190)

(191)

(192)

(193)

7542

7543

7544

7545

7546

Prozeß gegen den Landwirt Johann SCHLACHT-
BAUFR (geb. 15. Jun. 1895) aus Rohrbach
(Lkr. Pfaffenhofen) wegen der Behauptung,
Hitler habe mit Begleitung einmal in einem
Hotel in Augsburg eine Zeche von 10 000 RM
in 5 - 8 Tagen gemacht.

Urteil: 200 RM Geldstrafe, ersatzweise
20 Tage Haft

19. Aug. 1933 - 22. Jun. 1934
(S Pr 238/33)

Prozeß gegen den Kraftwagenführer Georg
KELLNER (geb. 9. Jun. 1909) aus Passau
wegen Einberufung und Leitung einer kommu-
nistischen Demonstration in Tittling (Lkr.
Passau). K. wechselte dann öfters die Gren-
ze nach Böhmen und benützte dort den Namen
"Hans STEFFEN", wurde aber schließlich
abgeschoben, weil er keine Papiere auf
diesenNamen hatte.

Urteil: 3 Jahre Zuchthaus

13. Okt. 1933 - 20. Okt. 1936
(S Pr 239/33)

Prozeß gegen den Hilfsarbeiter Johann
GOLLWITZER (geb. 18. Feb. 1901) aus Mün-
chen wegen Verbreitung kommunistischer
Flugblätter.

Urteil: 4 Monate Gefängnis, ab 2 Monate
Untersuchungshaft

21. Aug. 1933 - 12. Feb. 1934
(S Pr 240/33)

Prozeß gegen den Sattler Anton PFEFFER
(geb. 29. Dez. 1888) aus München wegen
beleidigender Äußerungen über die Re-
gierung.

Urteil‘ 6 Monate Gefängnis

5. Spt. 1933 - 13. Jun. 1934
(S Pr 241/33)

Prozeß gegen den Schneider Walter SCHMELZER
(geb. 22. Mrz. 1909) aus München wegen Ver-
breitung kommunistischer Flugblätter. Sch.
ist Mitglied der "Roten Hilfe" gewesen.

Urteil: 2 Monate Gefängnis

Als Verteiler der kommunistischen "Neuen
Zeitung wird Sebastian SCHEUGENPFLUG ge-
nannt.

13. Jul. 1933 - 16. Mrz. 1934
(S Pr 242/33)
