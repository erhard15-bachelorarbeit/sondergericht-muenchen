(839)

(840)

(841)

(842)

(843)

(844)

7976

7977

7978

7979

7980

7981

Prozeß gegen den Maurer Alfons EGLSEER
(geb. 6. Apr. 1904) aus Mietenkam (Lkr.
Traunstein) wegen beleidigender Äußerun-
gegen gegen die SA.

Urteil: 4 Monate Gefängnis

19. Apr. 1935 - 28. Okt. 1935
(S Pr 107/35)

Prozeß gegen den Versicherungsdirektor
Ulrich SCHARFFE (geb. 9. Apr. 1894) aus
München wegen abwertender Äußerungen
über die Partei. Sch.war Mitglied des
Gaustabes des BNSDJ, Gau München.

Urteil: 4 Monate Gefängnis

5. Apr. 1935 - 13. Mrz. 1936
(S Pr 108/35)

Prozeß gegen den Gärtner Konrad SCHUHMANN
(geb. 4. Jan. 1906), ohne festen Wohnsitz,
wegen Betrugs, Bettels, Diebstahls.

Urteil: 2 Jahre 3 Monate Zuchthaus,
Aberkennung der bürgerlichen
Ehrenrechte auf 95 Jahre

10. Jan. 1935 - 10. Jul. 1937
(S Pr 109/35)

Prozeß gegen den Immobilien-Geschäftsin-
haber Josef NEUMEIER (geb. 27. Jan. 1888)
aus München wegen kritischer Äußerungen
über führende Nat.Sozz..

Urteil: Freispruch

5. Jan. 1935 - 12. Okt. 1935
(S Pr 110/35)

Prozeß gegen den kath. Pfarrer Thomas
ANGERMEIER (geb. 20. Dez. 1871)aus Groß-
sinzemoos (Lkr. Dachau) wegen einer kri-
tischen Äußerung gegen die Sammlung zur
Mütterhilfe.

Urteil: 2 Monate Gefängnis oder 400 RM
Geldstrafe

12. Mai 1935 - 153. Nov. 1935
(S Pr 111/35)

Prozeß gegen den Gipser Bernhard KRAFT
(geb. 23. Okt. 1876) aus Lindau wegen
abwertender Äußerungen über das Winter-
hilfswerk.

Urteil: Freispruch

1 Mai 1935 - 9. Aug. 1935
(S Pr 112/35)
