(4764)

(4765)

(4766)

(4767)

(4768)

(4769)

9761

9762

9763

9764

9765

9766

Prozeß gegen die Kernmacherin Maria
RAMMINGER (geb. 22. Nov. 1911) aus
München wegen politischer Außerungen.

Urteil: 6 Monate Gefängnis, ab 2 Mo-
nate Polizeihaft
($8 1,2 HG)

26. Nov. 1939 - 8. Aug. 1940
(1 KMs So 74/40)

Prozeß gegen den Elektriker Johann
PISCHL (geb. 27. Feb. 1896) aus München,
KPD-Mitglied, wegen der Behauptung,

die Wehrmacht habe im Polenfeldzug Gift
verwendet.

Urteil: Freispruch
$ 1 Ha)

9. Jan. 1940 - 6. Mai 1940
(1 KMs So 75/40)

Prozeß gegen den Elektromonteur Maxi-
milian RAUNECKER (geb. 19. Jan. 1883)
aus München wegen politischer Außerungen.

Urteil: 1 Jahr Gefängnis
2 HG)

30. Aug. 1939 - 12. Apr. 1941
(1 KMs So 76/40)

Prozeß gegen den Spengler Lorenz KREITH-
MEIER (geb. 7. Aug. 1912) aus München,
früher SA- und NSDAP-Mitglied, wegen
Äußerungen zum Kriegsbeginn.

Urteil: 9 Monate Gefängnis
$ 2 HG)

30. Okt. 1939 - 17. Apr. 1941
(1 KMs So 77/40)

Prozeß gegen den Bauern Karl AUERN-
HAMMER (geb. 18. Mrz. 1881) aus Diet-
furt (Lkr. Weißenburg) wegen Äußerungen
über den NS.

Urteil: Verfahren wegen Amnestie ein-
estellt
(S 2 HC)

20. Okt. 1939 - 8. Mai 1940
(1 KMs So 79/40)

Prozeß gegen den Schmiedmeister Fried-
rich OBERNÖDER (geb. 18. Jan. 1910)
aus Geislohe (Lkr. Weißenburg) wegen
politischer Außerungen.
