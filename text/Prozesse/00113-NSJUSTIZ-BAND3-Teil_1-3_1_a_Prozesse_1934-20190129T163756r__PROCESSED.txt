(450)

(451)

(452)

(453)

(454)

(455)

8810

8811

8812

8813

8814

8815

Prozeß gegen den Glasmacher Johann Nepomuk
WALLNER (geb. 10. Apr. 1901) und den Glas-
macher Albert ROBL (geb. 18. Mrz. 1903), bei-
de aus Riedlhütte (Lkr. Grafenau) wegen ab-
wertender Außerungen über die örtliche SA,

Urteil: je 6 Wochen Haft

24. Apr. 1934 - 8. Jun. 1934
(S Pr 137/34)

Prozeß gegen den Hilfsarbeiter Josef LEITER-
MANN (geb. 21. Mai 1903) aus Schützling (Lkr.
Altötting) wegen der Behauptung, Hitler sei
homosexuell veranlagt. L. war.früher KPD-
Mitglied.

Urteil: 6 Monate Gefängnis

31. Jan. 1934 - 24. Aug. 1934
(S Pr 138/34)

Prozeß gegen den Kaufmann Oskar MAUSSNER
(geb. 3. Spt. 1897) aus Treuchtlingen (Lkr.
Weißenburg) wegen der Äußerung, Reichsmini-
ster Seldte habe sich "kaufen" lassen.

Urteil: Freispruch

15. Apr. 1934 - 15. Jun. 1934
(S Pr 139/34)

Prozeß gegen den Pressephotographen Wilhelm
LIPINSKI (geb. 17. Aug. 1911) wegen Dieb-
stahls und Betrugs in Tegernsee (Lkr. Mies-
bach).

Urteil: 2 Jahre Gefängnis

8. Feb. 1934 - 23. Feb. 1936
(S Pr 140/34)

Prozeß gegen die Haushälterin Ursula HOF-

STALLER (geb. 1. Apr. 1901) aus Heimstetten
(Lkr. München) wegen der Behauptung, Gelder
des Winterhilfswerks würden zweckentfremdet.

Urteil: Freispruch
11. Mrz. 1934 - 11. Jun. 1934
(S Pr 141/34) .

Prozeß gegen den Spengler Korbinian HUBER
(geb. 17. Mrz 1896) aus München wegen
abwertender Äußerungen über die NSDAP.

Urteil: 5 Monate Gefängnis

18. Mrz. 1934 - 24. Aug. 1934
(S Pr 142/34)
