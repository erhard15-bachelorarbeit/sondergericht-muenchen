(6041)

(6042)

(6043)

(6044)

10489

10490

10491

10492

Ein Verfahren gegen das Fflichtjahrmädchen
Elisabeth GINDHART (geb. 24. Apr. 1926)
aus Peissenberg in derselben Sache wurde
abgetrennt.

22. Aug. 1941 - 8. O0««t. 1943
(1 KMs So 167/41)

Prozeß gegen die Hilfsarbeitersehefrau
Karolim FROMM (geb. 30. Dez. 1910) aus
Weilh.im wegen verbotenen Umgangs mit dem
französischen Kriegsgesfar.genen Antonio
DUARTE.

Urteil: 1 Jahr 8 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 4 Wehrkraft VO)

15. Aug. 1941 - 26. Jan. 1943
(1 KMs So 168/41)

Prozeß gegen die Schlossersehefrau Maria
LINDERMÜLLER (geb. 20. Nov. 1899) aus 01l-
ching (Lkr. Fürstenfeldbruck) wegen der
Behauptung: "Wagner und Weber sind auch
davon. Den Weber hapven sie in Lindau er-
wischt, als er in die Schweiz wollte."

Urteil: 6 Monate Gefängnis
($ 1 Ha)

28. Mai 1941 - 4. Mrz. 1942
(1 KMs So 169/41)

Prozeß gegen die Dienstmagd Arna HEGER
(geb. 6. Jun. 1921) aus Waälpersdorf (Lkr.
Rattenburg) wegen verbotenen Verkehrs mit
dem französischen Kriegsgefangenen Marius
BORDE.

Urteil: 1i Jahr Zuchthaus, 2 Jahre Ehrver-
Lust
($ 4 Wehrkraft VO)

28. Aug. 1941 - 28, Aug. 1942
(1 KMs So 170/41)

Prozeß gegen die Modistin Anna MOSER (geb.
22. Apr. 1901) aus Taufkirchen (Lkr. Erding)
wegen verbotenen Umgangs mit französischen
Kriegsgefangenen.

Urteil: 6 Monate Gefängnis
($ 4 Wehrkraft VO)

Das Verfahren gegen die Schülerin Elisabeth
SEISENBERGER (ge. 15. Nov. 1923) wegen
derselben Sache wird an die Jugendstraf-
kammer des LG-M-I abgegeben.

4. Jul. 1941 - 30. Mrz. 1942
(1 KMs So 171/41)
