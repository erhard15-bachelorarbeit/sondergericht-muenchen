Gruppe Hilfsarbeiter Alfred Hermann ALERKECHT
(geb. 22. Feb. 1899) aus München;

Urteil: 6 Wochen bis 8 Monate Gefängnis

Gruppe Georg LINDNER (geb. 2. Mai 1888)
aus München;

Urteil: 2 bis 6 Monate Gefängnis

Gruppe Anna LETTENTHALER (geb. 8. Okt.
1886) aus München;

Urteil: 3 bis 6 Monate Gefängnis und
Freispruch

Gruppe Reisender Josef RAUWOLF (geb. 5.
Jul. 1875) aus München;

Urteil: 2 bis 3 Monate Gefängnis
und Freispruch

Gruppe Marie FRIEDL (geb. 13. Okt. 1897)
aus München;

Urteil: 2 bis 95 Monate Gefängnis

Gruppe Werkvorsteher a.D. Andreas GASSEN-
HUBER (geb. 1. Apr. 1873) aus München;

Urteil: 2 bis 3 Monate Gefängnis

Gruppe Postschaffner Johann KÖLBL (geb.
20. Dez. 1897) aus München;

Urteil: 3 Monate bis 2 Jahre Gefängnis

Gruppe Therese KAINZ (geb. 16. Jul. 1889)
aus München;

Urteil: 6 Wochen bis 4 Monate Gefängnis
und Freispruch

Gruppe Oberingenieur Rudolf THOMAE (geb. 2.
Aug. 1887) aus München;

Urteil: 3 Monate bis 1 Jahr 4 Monate
Gefängnis und Verfahrenseinstellung

Gruppe Müller Ludwig WIMMER (geb. 11. Jul.
1899) aus Landshut;

Urteil: 3 Monate bis 1 Jahr 2 Monate
Gefängnis
