(5879)

(5880)

(5881)

(5882)

(5883)

(5884)

10326

10327

10328

10329

10330

10331

Prozeß geg
MEIER (geb
wegen abwe

en den Baupolier Ulrich LÄMMER-
. 27. Jun. 1891), aus Augsburg,
rtender Außerungen über das Regime

Urteils 7 Monate Gefängnis
$ 2 Ha)
L. war bereits 3mal in Schutzhaft.
8. Jul. 1940 - 4. Apr. 192
(1 KMs So 1/1)

Prozeß gegen den Hilfsarbeiter Franz MAGG
(geb. 6. Feb. 1872) aus München wegen
Äußerungen über Hitler und den Krieg.

Urteil: 1 Jahr Gefängnis

(8 2 HG)
26. Spr. 1940 - 14. Nov. 1941
(1 KM So 2/41)

Prozeß gegen den Limonadenfabrikanten Josef
BRASSLER (eb. 24. Feb. 1900) aus Burgheim
(Lkr. Neuburg) wegen Äußerungen in Obern-
dorf (Lkr. Donauwörth) über Italien, die
Wehrmacht und Hitler.

Urteil: 3 Monate Gefängnis
(8 2 HG)

3. Mrz. 1940 - 12. Jul. 1941
(1 KMs So 3/41)

Prozeß gegen den Werkzeugmacher Josef BAU-
SEWRIN (geb. 30. Mrz. 1909) aus München
wegen Äußerungen uber Hitlers Kriegspolitik.

Urteil: 8 Monate Gefängnis
$& 2 HG)

3. Spt. 1949 - 31. Aug. 1944
(1 KMs So 4/41)

Prozeß gegen den Justizangestellten Ernst
FISCHER (geb. 17. Jan. 1902) aus Augsburg
wegen Äußerungen über das Regime.

Urteil: 1 Jahr 3 Monate Gefängnis
($ 2 HG)

F. kam nach Strafverbüßung in Polizeihaft

4. Okt. 1940 - 3. Jan. 1942
(1 KMs So 5/41)

Prozeß gegen den Mechaniker Wilhelm LIE-
BERWIRTH (geb.19. Spt. 1866) aus Auerbach
(Lkr. Chemnitz) wegen einer Äußerung in
Jesendorf (Lkr. Vılsbiturg) über das Mut-
terkreuz.
