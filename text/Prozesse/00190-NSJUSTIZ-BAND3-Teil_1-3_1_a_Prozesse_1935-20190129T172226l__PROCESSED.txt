(856)

(857)

(858)

(859)

(860)

(861)

7993

7994

7995

7996

7997

7998

Prozeß gegen den Schlosser Hans CECCONI
(geb. 14. Jul. 1915) aus Reichersbeuern
Lkr. Bad Tölz) wegen Betrugs.

Urteil: 1 Jahr Gefängnis

22. Jan. 1935 - 14. Apr. 1936
(S Pr 124/35)

Prozeß gegen den kfm. Vertreter Artur
SIEBERT (geb. 25. Spt. 1896) aus München
wegen positiver Äußerungen über Sally
EPPSTEIN.

Urteil: 4 Monate Gefängnis

25. Mai 1935 - 19. Mai 1938
(S Pr 125/35)

Prozeß gegen den Diplomlandwirt Wilhelm
STEGMANN (geb. 13. Jun. 1899) und den Bau-
unternehmer Josef FASSER (geb. 22. Feb.
1899), beide aus Seeshaupt (Lkr. Starhberg)
wegen unvorsichtiger politischer Außerungen.

Urteil: Stegmann Freispruch; Fasser 6 Mo-
nate Gefängnis

28. Dez. 1934 - 17. Mai 1938
(S Pr 126/35)

Prozeß gegen den Schneider Franz Xaver
KOLMER ( eb. 26. Aug. 1900) aus Otters-
kirchen (Lkr. Vilshofen) und den Schacht-
meister Philipp RIPPERGER (geb. 2. Aug.
1901) aus Passau wegen Sprengstoffvergehens

Urteil: für beide Freispruch

9. Dez. 1934 - 12. Nov. 1935
(S Pr 127/35)

Prozeß gegen den Kupferschmied Benedikt
FEDERL (geb. 14. Feb. 1896) aus Freising
wegen abwertender Äußerungen über führende
nat.soz. Politiker.

Urteil: 6 Monate Gefängnis,
24. Mai 1935 - 4. Dez. 1935
(S Pr 128/35)

Prozeß gegen den Gastwirt Ludwig LAPPER
(geb. 28. Jul. 1881) aus Rosenheim wegen
politischer Äußerungen gegen die Regierung.

Urteil: 4 Monate Gefängnis

21. Mai 1935 - 10. Jul. 1937
(S Pr 129/35)
