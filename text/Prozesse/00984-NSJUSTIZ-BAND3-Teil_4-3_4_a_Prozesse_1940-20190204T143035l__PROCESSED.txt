(4643)

(4644)

(4645)

(4646)

9641

9642

10132

96453

(3 KLs So 32/40)

Prozeß gegen den Metzger Theodor WAX
(geb. 16. Spt. 1906) aus München wegen
Diebstahls und Betrugs.

Urteil: 8 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte
auf 8 Jahre, SicherungsVver-
wahrung
(88 20a,242,263,264,267,268 St63ö3)

8. Mrz. 1940 - 6. Jan. 1943
(3 KLs So 34/40)

Prozeß gegen den Ausgeher Eberhard CBST
(geb. 28. Spt. 1901) aus Munchen wegen
Diebstahls.

Urteils 10 Jahre Zuchthaus, Aberkennung
der burgerlichen Ehrenrechte
auf 10 Jahre, Sicherungsverwah-
rung
(88 20a,242-244 StGB; $ 2 VVO)

6. Jan. 1943 in das KZ Neuengamme Vver-
bracht, gest. 10. Mai 1943.

Der Kraftfahrer Ludwig GLOSSNER (geb. 14.
Dez. 1890) aus Munchen wurde in dieser
Sache freigesprochen

11. Dez. 1939 - 6. Jan. 1943
(3 KLs So 45/40)

Prozeß gegen den Bucherrevisor Reinhard
KUHLMEY (geb. 20. Jan. 1907) aus München,
erst DNVP-, dann NSDAP-Mitglied, wegen
Bekrugs und Untreue.

Urteil: 5 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte
auf 3 Jahre, 3 Jahre Berufs-
verbot
(38 43,263,266 StGB; 8 4 VVO)

19. Jul. 1940 - 1. Spt. 1944
(3 KLs So 47/40)

Prozeß gegen den Hilfsarbeiter Leo
LECHNER (geb. 8. Mai 1897) aus München
und die Haushälterin Maria BLÖSSL (geb.
27. Apr. 1912) aus Bad Tolz wegen Dieb-
stahls und Hehlerei.

Urteil: Lechner 8 Jahre Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
rechte auf 5 Jahre; Blössl 1
Jahr Gefangnıs
($$ 49,50,242-244,259 StGB; WVVO)

25. Mrz. 1938 - 16. Jun. 1944
(3 KLs So 49/40)
