5847)

(5848)

(5849)

10296

10297

10298

10299

10300

Prozeß gegen den Metzgergehilfen Edwin
Ludwig ROSENHAMMER (geb. 1. Okt. 1922)
aus München wegen Wurstdiebstahls.

Urteil: 1 Jahr Gefängnis
($ 242 StGB)

s.a. St.Anw. 10297

13. Spt. 1941 - 17. Jul. 1942
(5 KLs So 83/41)

Prozeß gegen den Metzgergehilfen Edmund
GOLINSKI (geb. 23. Jun. 1913) aus München
wegen Wurstdiebstahls.

Urteil: 1 Jahr Gefängnis
($ 242 StGB)

s.a. St.Anw. 10296

13. Spt. 1941 - 7. Okt. 1943
(5 KLs So 84/41)

Prozeß gegen den Bäckergehilfen Anton HALL-
HUBER (geb. 24. Apr. 1922), den Bäcker-
gehilfen Josef REIHMMEIER (geb. 12. Feb.
1923), den Kanonier Franz SCHMALHOFER (geb.
8. Spt. 1921), den Bäckerlehrling Alfred
FORSTER (geb. 17. Jul. 1923), alle aus
München, wegen Fahrraddiebstahls.

Urteil: Hallhuber 4 Jahre Zuchthaus, 4
Jahre Ehrverlust; Reithmeier 1 Jahr
3 Monate Gefängnis; bSchmalhofer
2 Jahre Zuchthaus, 2 Jahre Ehr-
verlust; Forster 10 Monate Gefäng-

nis
(38 159,242,263 StGB; $8 2,4 VVO)
Hallhuber starb am 2. Okt. 1944 an Tbo.

11. Jul. 1941 - 10. Okt. 1944
(5 KLs So 87/41)

Prozeß gegen den Telegraphenleitungsauf-
seher Johann MARKAU (geb. 21. Jun. 1896)
aus München, NSDAP-Mitglied, wegen Dieb-
stahls.

GC

Urteil: 10 Jahre Zuchthaus, 10 Jahre Ehr-

verlust
($ 243 StGB; 88 2,4 VVO)

21. Nov. 1941 - 24. Aug. 1944
(5 KLs So 89/41)

Prozeß gegen den Hilfsarbeiter Anton SCHLÖGEL

(geb. 6. Okt. 1888) aus Sonthofen wegen
Diebstahls.

Urteil: 2 Jahre 6 Monate Zuchthaus, 2 Jahre
Ehrverlust
(8 242 StGB; $8 4 VVO)
