(5861)

(5862)

(5863)

(5864)

10310

10311

10312

10313

Prozeß gegen den staatenlosen Landarbeiter
aus Polen Alexander MATWIRYEW (geb. 18.
Feb. 1920) aus Messmering ( Lkr. Mühldorf)
wegen einss 5S5ittlichkeitsvergehens.

Urteil: 3 Jahre Zuchthaus
(8 185 StGB; 8 4 VVO)

Strafvollstreckung am 4. Jan. 1943 unter-
brochen, Akten weggelegt am 8. Jun. 1943.

9. Spt. 1941 - 8. Jun. 1943
(5 KLs So 104/41)

Prozeß gegen den polnischen Landarbeiter
Johann SAR (geb. 23. Okt. 1917) aus Gei-
senried (Lkr. Marktoberdorf) wegen Dieb-
stahls.

Urteil: 5 Jahre verschärftes Straflager
und Todesstrafe

($ 1 Gew.Verbr.VO; Polen-Juden-

Straf VO; $ 242 StGB; 8 4 WVO)

26. Okt. 1941 - 6. Mrz. 1942
(5 KLs So 105/41)

Prozeß gegen den Schmiedhelfer Jakob MAYER
(geb. 9. Mai 1922), den Schlosser Johann
LINK (geb. 7. Nov. 1922), den Lastkraft-
wagenbeifahrer Albert GSCHÖSSER (geb. 11.
Jun. 1923), den Feilenhauerlehrling Otto
RAGER (geb. 3. Feb. 1923), den Sattler-
lehrling Karl KOFLER (geb. 25. Spt. 1923),
den Braüerlehrlıing Jakob FORSTER (geb. 6.
Feb. 1925), alle aus Augsburg, wegen Not-
zucht.

Urteil: Mayer, Link, Gschösser je 4 Jahre
Gefängnis; Rager, Kofler, Forster
je 2 Jahre Gefängnis

JGG; 3 177 StGB

18. Okt. 1940 - 28. Mrz. 1944
(7 KLs So 14,17/41)

Prozaß gegen den Hilfsschlosser Karl Heinz
HEPNER (geb. 31. Jan. 1924) aus Lindau
wegen Diebstahls und versuchten Totschlags

Urteil: 10 Jahre Gefängnis
$ 1 Gew.Verbr.VO; 88 3,9,13 JGG;
88 43,214,242243 StB; $ 1 WO)

2. Feb. 1941 - 11. Jul. 1941
(7 KLs So 26/41)
