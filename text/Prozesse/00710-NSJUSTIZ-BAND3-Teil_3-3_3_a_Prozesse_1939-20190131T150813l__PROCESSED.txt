(3383)

(3384)

(3385)

(3386)

(3387)

9338

9439

9442

9443

Qt44

Prozeß gegen die Geschäftsführersehefrau
Elisabeth ADLER (geb. 16. Aug. 1905) aus
München wegen Verstoßes gegen die Lebens-
mittelbewirtschaftung.

Urteil: 4 Monate Gefängnis
$ 1, 9 vo ü.ldw.Erz.)

27. Spt. 1939 - 26. Mrz. 1943
(2 KMs So 8/39)

Prozeß gegen den Weber Karl MENGELE (geb.
16. Mrz. 1909), KPD-Mitglied, 1933 einige
Monate in Schutzhaft, dann entlassen, von
1. Dez. 1933 bis 22. Mrz. 1935 im KZ Dachau,
und den Tiefbauarbeiter Eduard BRAUN (geb.
16. Spt. 1900), beide aus Augsburg, wegen
Erzählens von Greuelnachrichten über das
KZ Dachau und Erzählens politischer Witze.

Urteil: Verfahren eingestellt
(HG)

Braun kam 1937 in Schutzhaft

3. Feb. 1938 - 18. Jul. 1938
(AK 2/39)

Prozeß gegen die Blumenbinderin Paula SEIFERT
(geb. 18. Aug. 1905) aus München wegen der
Behauptung, Hitler, Goebbels, Lutze und
Himmler hätten sich bei der Eröffnung des
Gärtnerplatz-Theaters in München stark be-
trunken.

Urteil: Verfahren wegen Amnestie eingestellt
(88 1, 2 HG)

27. Feb. 1939 - 5. Jul. 1939
(AK 202/39)

Prozeß gegen den Kaufmann Richard BAUR (geb.
18. Spt. 1900) aus München, förderndes SS-
Mitglied, wegen unbedachter politischer
Äußerungen in Berlin.

Urteil: Verfahren wegen Amnestie einge-
stellt
(HG)

25. Mai 1938 - 5. Aug. 1939
(AK 230/39)

Prozeß gegen den Metzgermeister Hans SCHMID
(geb. 27. Okt. 1885) aus Haar (Lkr. München)
wegen politischer Äußerungen.

Urteil: Verfahren eingestellt
(HG)

10. Jun. 1939 - 16. Okt. 1939
(AK 330/39)
