(1413)

(1414)

(1415)

(1416)

(1417)

9105

9106

9107

9108

9109

Urteil: Freispruch

14. Apr. 1937 - 16. Spt. 1937
(1 KMs So 20/37)

Prozeß gegen den Amtswart Josef RESCH
(geb. 10. Apr. 1896) aus München wegen
einer Äußerung über eine angebliche
Homosexualität Hitlers.

Urteil: 6 Monate Gefängnis

NS
(1 KMs So 21/37)

Prozeß gegen den Eisendreher Johann REIM
(geb. 15. Jun. 1903) aus München, SPD-
Mitglied, wegen kritischer Außerungen
über Hitler und das KZ Dachau.

Urteil: 5 Monate Gefängnis

12. Jan. 1937 - 13. Mai 1938
(1 KMs So 22/37)

Prozeß gegen den Hilfsarbeiter Adam
BÜHLER (geb. 22. Nov. 1892) aus Ingol-
stadt wegen abwertender Äußerungen
über führende Nat.Sozz..

Verfahren eingestelit

25. Feb. 1937 - 21. Mai 1938
(1 KMs So 23/37)

Prozeß gegen den Hilfsarbeiter Ludwig
STAUDINGER (geb. 2. Jun. 1887) aus
Garmisch-Partenkirchen wegen einer ab-
wertenden Äußerung über das Regime.

Urteil: 2 Monate Gefängnis

14. Jan. 1937 - 26. Jul. 1937
(1 KMs So 24/37)

Prozeß gegen die Schneiderin Anna KREUZER
(geb. 8. Aug. 1898), die Packerin Amalie
WAGNER (geb. 1. Okt. 1901), die Schreiner-
meistersehefrau Walburga STEGMANN (geb.
27. Dez. 1872), die Registratorsehefrau
Elisabeth WÖRL (geb. 8. Okt. 1896), den
Vertreter Karl Ludwig WOLFRUN (geb. 530.
Jun. 1915), alle aus München, wegen Be-
tätigung als Ernste Bibelforscher.

Urteil: Kreuzer 5 Monate Gefängnis;
Wagner 4 Monate Gefängnis;
Stegmann 3 Monate Gefängnis;
Wörl 6 Wochen Gefängnis;
Wolfrun 5 Wochen Gefängnis
