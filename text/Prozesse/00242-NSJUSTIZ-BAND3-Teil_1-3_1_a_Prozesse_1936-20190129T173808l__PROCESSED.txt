(1128)

(1129)

(1130)

(1131)

(1132)

(1133)

8265

8266

8267

8268

8269

8270

Prozeß gosen den Schuhmacher Georg BRODWOLF
(geb. 26. Jan. 1896) wegen der Behauptung
in Grafenau, er sei im KZ Dachau gewesen
und dort geschlagen worden.

Urteil: 8 Monate Gefängnis

25. Mrz. 1936 - 22. Dez. 1936
(16 KMs So 114/36)

Prozeß gegen den Monteur Richard MOTZ (geb,
24. Dez. 1901) wegen Betrugsversuchs in
Ringelwies (Lkr. Deggendorf).

Urteil: 6 Monate Gefängnis
12. Jun. 1936 - 18. Dez. 1936
(16 KMs So 115/36)

Prozeß gegen den Landwirt Josef SPRENZINGER
(geb. 12. Nov. 1872) aus Landau a.d. Isar
wegen einer kritischen Äußerung über die HJ.

Urteil: Freispruch

25. Mrz. 1936 - 17. Aug. 1936
(16 KMs So 116/36)

Prozeß gegen den Taglöhner Josef DIETZWEG

(geb. 28. Feb. 1897) aus Viechtach wegen
abwertender Äußerungen wegen einer Radio-
übertragung von politischen Reden.

Urteil: 7 Monate Gefängnis

16. Mrz. 1936 - 29. Okt. 1936
(16 KMs So 117/36)

Prozeß gegen den Schreiner August BAIER
(geb. 9. Aug. 1895) aus Dachau, früher
KPD-Mitglied und SA-Mitglied, wegen kri-
tischer politischer Außerungen.

Urteil: 3 Monate Gefängnis

10. Mrz. 1936 - 20. Mai 1938
(16 KMs So 118/36)

Prozeß gegen den Gürtler Paul GÄRTNER (geb.
4. Jun. 1909) aus München, tschechischer
Staatsbürger, NSDAP-Mitglied, wegen der Be-
hauptung, er gehöre einer Feme-Organisation
an.

Urteil: 2 Monate Gefängnis

13. Mrz. 1936 - 19. Spt. 1936
(16 KMs So 119/36)
