(4627)

(4628)

(4629)

9629

9626

9627

Prozeß gegen den Vertreter mlaslus
FACKLER (geb. 3. Nov. 1890) aus Leiters-
hofen (Lkr. Augsburg), NSDAP-Mitglied,
wegen Verwendung von Bezugsscheinen,
die für andere Zwecke ausgefüllt waren,
zu Fleischeinkauf.

Urteil: 5 Jahre Zuchthaus, Aperkennung
der bürgerlicher. Ehrenrechte
auf 5 Jahre
( $ 1 KWVO )

29. Feb. 1940 - 16. Okt. 1944
(2 KLs So 16/40)

Prozeß gegen den Metzger Ferdinand
ENTFELLNER (geb. 11. Mai 1:95) aus
Altenmarkt (Lkr. Traunstein), wegen
Schwarzschlachtunzg.

Urteil: 10 Monate Gefangnis, 190.-RM
Geldstrafe oder 19 Tage Gefäng-
nıs, 1372,80.-RM Wertersatz-
strafe oaer 27 Tape SGefaengnis
($ 1 KWVO; $ 3. RAO)

11. Mrz. 1940 - 12. Jan. 1943

(2 KLs So 17/42)

Prozeß gegen den Metzpfernmeister Josef
WIRTH (geb. 11. Jun. 13197), den Vieh-
transporteur Emil FAUL (geb. 197. Jun.
1906), beide aus Miürchen, ien Gastwirt
und Viehhandler Franz WAID (n- 16.
Nov. 1902) aus Feld«irobren (Lkr. München
den Hausmeister Karl HliLID (gb. 21. Nov.
1876) aus München, den Landwirt Fritz
BARTL (geb. 6. Okt. 1914) aus lieufin-
sing (Lkr. Erding), den Metzger Alois
LEIMGARTNER (geb. 26. Mai 1911), den
Bäckergehilfen Josef MURSCH (geb. 2.
Aug. 1904), beige aus Mr.chen, wegen
Schwarzschlachturg.

Urteil: Wirth 2 Jahre 10 Monate Gefaäng-
nıs und Geld- coder Gefangnis-
strafe; Bartl 1 Janr 2 Monate
Gefangnis und Geld- oier Gefansg-
nisstrafe; Faul 1 Jahre 6 Mo-
nate Gefangnis und Geld- oder
Gefängnisstrafe; Heidg 6 Monate
Gefängnis und Geld- oder Gefäng-
nisstrafe; Leimpartner und Maursc
Je 2 Monate Gefengnis und Geld-
oder Gefangnisstrafe
(8 1 KWVO; 8 3936 RAO; 6 1-5

Schlachtsteuergesetz; $5$ L9
50 5GB; $ 1 Verbr.Ristr.VÖ)

25. Jun. 1940 - 30. Jan. 19345
(2 KLs So 18/40)
