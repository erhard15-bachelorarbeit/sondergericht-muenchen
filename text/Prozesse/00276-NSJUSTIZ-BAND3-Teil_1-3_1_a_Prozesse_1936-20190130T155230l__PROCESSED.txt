(1312)

(1313)

(1314)

(1315)

(1316)

9006

9007

9008

9009

9010

Prozeß gegen den Taglöhner Max KIRCH-
LECHNER (geb. 23. Spt. 1896) aus Dacha,
Mitglied der Roten Hilfe, wegen abwerten-
der Äußerungen über die SA.

Urteil: 10 Monate Gefängnis

21. Okt. 1935 - 7. Spt. 1936
(AK 42/36)

Prozeß gegen den Gewerbeoberlehrer Karl
HÖLLERER (geb. 17. Mai 1895) aus München
wegen kritischer politischer Äußerungen
in Bihlerdorf (Lkr. Sonthofen).

Urteil: Freispruch

Ein Verfahren in der selben Sache gegen
den Laboratoriums-Anssistenten Rudolf
LIEGEL (geb. 20. Apr. 1895) fand wegen
des Todes des Angeklagten nicht statt,
egen den Gastwirtssohn Peter WALDOHR
(geb. 30. Jul. 1900) wurde es eingestellt.

5. Aug. 1935 - 4. Jun. 1936
(AK 43/36)

Prozeß gegen den Holzhauer Jakob HASEL-
BERGER (geb. 19. Dez. 1898) aus Vorder-
berg (Lkr. Sonthofen) wegen Erzählens
eines politischen Witzes und abwertender
politischer Äußerungen in Peiting (Lkr.
Schongau).

Urteil: 4 Monate 15 Tage Gefängnis

3. pt. 1935 - 14. Mai 1936

(AK 51/36)

Prozeß gegen den Landwirt Ludwig KÖLLN-
BERGER (geb. 25. Feb. 1912) aus Breiten-
bach (Lkr. Pfarrkirchen), NSDAP-Mitglied,

wegen kritischer Bemerkungen über das
Regime.

Urteil: 6 Wochen Gefängnis

13 Spt. 1935 - 19. Mai 1938
(AK 54/36)

Prozeß den Hilfsarbeiter Franz MAYER
(geb. 24. Mrz. 1872), den Hilfsarbeiter
Albert STANDL (geb. 30. Mrz. 1878), den
Fuhrknecht Josef HÖFLMAIER (geb. 26. Feb.
1877), österreichischer Staatsangehöriger,

den Schwerkriegsbeschädigten Franz STAMPFL (geb;
1. Okt. 1893), österreichischer Staatsangehöri-
ger, alle aus Laufen, wegen Verbreitung der Be-
hauptung, führende Nat.Sozz. aus Laufen
