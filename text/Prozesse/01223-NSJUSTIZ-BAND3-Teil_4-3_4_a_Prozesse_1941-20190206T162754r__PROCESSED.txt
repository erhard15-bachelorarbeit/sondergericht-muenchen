(5769)

(5770)

(5771)

(5772)

(5773)

(5774)

10220

10221

10222

10223

10224

10225

Prozeß gegen den Fernfahrer Josef Oskar
FÖRSTL (geb. 11. Feb. 1910) aus München
Wegen Betrugs.

Urteil: 4 Jahre Zuchthaus, 400.-RM + 600.-
RM Geldstrafen oder 8 + 12 Tage
Zuchthaus, 4 Jahe Ehrverlust, Siche-

rungsverwahrung
(388 20a,263,264 StGB; 3 4 VVO)

26. Jul. 1941 - 15. Mrz. 1943
(4 KLs So 64/41)

Prozeß gegen die polnische Landarbeiterin
Josefa DZIEWIECKA (geb. 24. Mrz. 1922)

aus Oberfahlheim (Lkr. Neu-Ulm) wegen Dieb-
stahls.

Urteil: 1 Jahr Zuchthaus
($ 242 StGB; 3 4 VVO)

13. Spt. 1941 - 16. Spt. 1942
(4 KLs So 67/41)

Prozeß gegen den Bergjungmann Heinrich
BRINKMANN (geb. 29. Apr. 1923) aus Bochum
wegen Diebstahls.

Urteil: 3 Jahre Zuchthaus, 3 Jahre Ehr-

verlust
(3 242 StGB; 83 4 WVVO)

11. Spt. 1941 - 27. Feb. 1943
(4 KLs So 68/41)

Prozeß gegen den Verkäufer Gerhard BARTSCH
(geb. 14. Mai 1923) aus Berlin wegen Dieb-
stahls.

Urteil: 4 Jahre Zuchthaus, 2 Jahre Ehr-

verlust
(88 43, 242,246,263 StGB; 8 2 VVO)

27. Spt. 1941 - 11. Spt. 1944
(4 KLs So 69/41)

Prozeß gegen den Landwirt Karl AUERNHAMMER
(geb. 18. Mrz. 1881) aus Dietfurt (Lkr.
Weißenburg) wegen Diebstahls.

Urteil: Freispruch
(88 242,243 StGB; $3 4 VVO)

13. Jul. 1941 - 9. Jan. 1942
(4 KLs So 70/41)

Prozeß gegen den Geflügelhändler Rudolf
FALGENHAUER (geb. 28. Feb. 1900) aus Zart-
lesdorf (bei Wien) wegen Betrugs.
