(5930)

(5931)

(5932)

(5934)

10378

10379

10380

10381

10382

(1 KMs So 52/41)

Prozeß gegen die Büglerin Hildegard TRÖFMER
(geb. 23. Nov. 1914) aus Berlin wegen Auße-
rungen in Bad Tölz über die Luftangriffe
auf Berlin und über die SS.

Urteil: Gesamtstrafe 1 Jahr 4 Monate Ge-
fängnis unter Einrechnung von 10
und 3 Monaten Gefängnis aus Ur-
teilen des AG-Berlin
($ 2 HG)

3. Okt. 1940 - 25. Okt. 1942
(1 KMs So 53/41)

Prozeß gegen den Schafhalter Josef FEHL-
BERGER (geb. 3. Jan. 1899) wegen einer
bedauernden Äußerung in Hammernsdorf (Lkr.
Erding) über das Mißlingen des Attentats
im Bürgerbräukeller.

Urteil: Freispruch
$ 2 HG)

16. Feb. 1940 - 5. Aug. 1941
(1 KMs So 55/41)

Prozeß gegen den Vorarbeiter Josef GROSS
(geb. 26. Jul. 1904) aus Mühldorf, früher
Reichsbanner-, dann KPD- und dann SA-Mit-
gliea, wegen einer Äußerung in Töging (Lkr.
Altötting) über die Lebensmittelversorgung.

Urteil: 3 Monate Gefängnis
& 1 HG)

24. Jan. 1941 - 23. Aug. 1944
(1 KMs So 56/41)

Prozeß gegen den kath. Kommoranten und
Kaplan a.D. Johann HANN (geb. 15. Jul.
1901) aus Neuburg a.d. Donau wegen Auße-
rungen über den Krieg und Hitler.

Urteil: 2 Jahre Gefängnis
2 HG)

21. Okt. 1940 - 13. Apr. 1942
(1 KMs So 57/41)

Prozeß gegen den kath. Pfarrer Johann Bap-
tist ZEUSS (geb. 19. Jun. 1879) aus Berg
(Lkr. Griesbach), BVP-Mitglied, wegen der
Äußerung, Chamberlain sei ein Ehrenmann
gewesen.

Urteil: 4 Monate Gefängnis
8 2 Ha)

20. Nov. 1940 - 8. Mai 1941
(1 KMs So 58/41)
