(3201)

(3202)

(3207)

(3204)

(3205)

9255

9256

9257

9258

9259

Prozeß gegen den Hilfsarbeiter Johann
SCHIESSL (geb. 23. Jun. 1881) aus Weißen-
burg wegen Schimpfens über die Regierung.

Urteil: 10 Monate GefängLis
$& 2 HG)

13. Nov. 1938 - 29. Okt. 1939
(1 KMs So 21/39)

Prozeß gegen den Schneider Willibald STEI-
NER (geb. 25. Feb. 1907) aus Berlin wegen
Äußerungen in Walting (Lkr. Weißenburg)
über die Lebensmittelversorgung in Berlin
und der Behauptung, wenn man Bilddokumente
davon zeige, komme man in das KZ.

Urteii:4 Monate 2 Wochen Gefängnis
$ 2 Ha)

6. Nov. 1938 - 30. Dez. 1939
(1 KMs So 22/39)

Prozeß gegen die Privatlehrerin Laura HÄFNER
(geb. 16. Mai 1885) aus Inning (Lkr. Starn-

berg), erst USPD-, dann SPD-Mitglied, wegen

politischer Außerungen.

Urteil: 5 Monate Gefängnis
$& 1 HG)

27. Spt. 1938 - 25.Jun. 1942
(1 KMs So 23/39)

Prozeß gegen die Frau des Staatsministers
a.D. Esser, Therese ESSER (geb. 19. Feb.
1902) aus München und die Oberingenieursfrau
Anna AMON (geb. 8. Apr. 1892) aus München
wegen Beleidigung des Reichsschatzmeisters
der NSDAP SCHWARZ.

Urteil: Esser 3 Monate Gefängnis
Amon Freispruch
($ 1 HG; 8 186 StGB)

13. Mai 1937 - 28. Spt. 1939
(1 KMs So 24/39)

Prozeß gegen die Hausgehilfin Else COHN
(geb. 29. Okt. 1915) aus München, Halbjüdin,
wegen abwertender Äußerungen über Hitler.

Urteil: 4 Monate Gefängnis
$ 2 HG)

12. Nov. 1938 - 1. Okt. 1939
(1 KMs So 25/39)
