(4703)

(4704)

(4705)

(4706)

(4707)

9700

9701

9702

9703

9704

Prozeß gegen den Maurergesellen Josef
PFLEGHART (geb. 7. Dez. 1911) aus Stein
(Lkr. Sonthofen) wegen Äußerungen über
Hitler.

Urteil: 10 Monate Gefängnis, ab 6 Monate
Schutzhaft
($ 2 HG) .

31. Mrz. 1939 - 26. Dez. 1940
(1 KMs So 9/40)

Prozeß gegen den Maurer Theodor MÜLLER
(geb. 17. Feb. 1905) aus Kempten wegen
Außerungen über Hitler.

Urteil: 4 Monate Gefängnis, verbüßt
durch Polizei- und Schutzhaft
($ 2 HG)

8. Spt. 1939 - 28. Feb. 1940
(1 KMs So 10/40)

Prozeß gegen den Käser Ludwig GEISEN-
BERGER (geb. 8. Apr. 1908) aus Bennin-
gen (Lkr. Memmingen), wegen Äußerungen
über das Regime.

Urtel: 4 Monate Gefängnis, ab 15 Wochen
Polizei- und Schutzhaft
($ 2 HG)

10. Okt. 1939 - 25. Apr. 1941
(1 KMs So 11/40)

Prozeß gegen den Hilfsarbeiter Franz
SWOBODA (geb. 1. Jun. 1910) aus Augs-
burg, Tscheche, wegen der Behauptung,
die Deutschen hätten bei der Besetzung
der Tschechoslowakei "wüst gehaust".

Urteil: 1 Jahr Gefängnis, ab 4 Monate
Schutzhaft
($ 1 HG)

nach Strafverbüßung in das KZ Sachsen-
hausen verbracht.

18. Okt. 1939 - 11. Nov. 1940
(1 KMs So 12/40)

Prozeß gegen den Hilfsarbeiter Georg
BRAUN (geb. 11. Jan. 1877) aus Bäumen-
heim (Lkr. Donauwörth), wegen Äußerungen
über die Politik Hitlers gegenüber dem
Sudetenland.

Urteil: 7 Monate Gefängnis
$& 2 HG)

14. Jan. 1939 - 22. Aug. 1941
(1 KMs So 13/40)
