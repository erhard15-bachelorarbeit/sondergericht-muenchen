Prozeß gegen den Bahngehilfen Johann SCHNEI-
DER (geb. 14. Apr. 1902) aus München wegen
Vertriebs kommunistischer Flugschriften.
Vorführung aus der Untersuchungshaft.

Urteil: 4 Monate und 1 Woche Gefängnis
Anlage: 1 "Sturmfahne Organ der Werk-
tätigen Südbayerns" (8 Bll.)

6. Apr. 1933 - 30. Aug. 1933
(S Pr 50/33)

Prozeß gegen die dänische Gebrauchsgraphikerin
Grete DICH-SCHNEEKLOTH (geb. 24. Mrz. 1903)
aus München wegen der Beteiligung an der Her-
stellung kommunistischer Druckschriften. Vor-
führung aus Untersuchungshaft.

Urteil: 4 Monate Gefängnis

Anlagen: 1 Brief an das dänische Konsulat,
1 Quittungsblock des Antifaschisti-
schen Kampfblocks Südbayern München
mit Entwürfen, 2 dänische kommuni-
stische Liederblätter, 1 Zeitungs-
kopfentwurf der "Neuen Zeitung", 1
Zeitungsüberschrift (Schriftprobe),
1 Mitgliedsbuch der "Roten Hilfe
Deutschlands"

10. Apr. 1933 - 18. Mai 1933
(S Pr 58/33)

Prozeß gegen den Schreiner Michael FISCH-
BACHER (3 24. Spt. 1904) aus Aich (Lkr.
Rosenheim) wegen Sprengstoffvergehens.

Urteil: 3 Monate Gefängnis

2. Jan. 1933 - 4. Jan. 1934
(S Pr 59/33)

Prozeß gegen den Sattler Johann METZLER
(geb. 13. Jan. 1901) auf Wanderschaft we-
gen beleidigender Äußerungen gegen Hitler
u.a. in Durach (Lkr. Kempten). Vorführung
aus Untersuchungshaft.

Urteil: 5 Monate Gefängnis

21. Apr. 1933 - 23. Okt. 1933
(S Pr 60/33)

Prozeß gegen den Arbeiter Franz SCHLIERS-
MAIER (geb. 31. Mai 1886) aus Bösenreutin
(Lkr. Lindau) wegen der Beh auptung, der
kommunistische Brandstifter des Reichstags-
gebäudes
