Prozeß gegen den Hafner Karl RIEDER (geb.
25. Apr. 1910), den Bergmann Anton MWINTE:!
(geb. 26. Aug. 1895) und die Invaliden-
rentnersfrau Margarethe STELZL (geb. 21.
Feb. 1900), alle aus München, wegen Ver-
gehens gegen das Sprengstoffgesetz. Vor-
führung von Rieder und Winter aus Unter-
suchungshaft.

Urteil: Rieder 5 Monate, Winter 4 Monate
und Stelzl 3 Monate Gefängnis,
Stelzl später begnadigt

25. Apr. 1933 - 4. Dez. 1933
(S Pr 118/33)

Prozeß gegen den Schlosser und Heizungs-
monteur Josef KOBLER (geb. 4. Nov. 1895)
aus München wegen Verbreitung kommunisti-
scher Flugschriften. Vorführung aus Unter-
suchungshaft.

Urteil: 1 Jahr Gefängnis
Anlage: 1 "Sturmfahne - Organ der Werk-
tätigen Südbayerns"

11. Apr. 1933 - 6. Jul. 1934
(S Pr 119/33)

Prozeß gegen den Kaufmann Hans MALETZKE
(geb. 2. Mrz. 1902), den Student Franz
KINSKOFER (geb. 18. Feb. 1902), den Iso-
lierer Georg GIGLBERGER (geb. 26. Okt. 1903),
den Bauhilfsarbeiter Josef‘GERBL (geb. 11.
Apr. 1907) und Josef VOGL (geb. 17. Mai
1891), alle aus München, wegen Verbreitung
kommunistischer Schriften. Vorführung aus
Untersuchungshaft.

Urteil: Maletzke 10 Monate und Kinskofer
7 Monate Gefängnis, Giglberger,
Vogl und Gerbl Freispruch

Anlagen: 9 "Das Tribunal - Zentralorgan der
roten Hilfe Deutschlands", April
1933

Der kommunistische Schlosser Josef KOBLER
hatte sich bei Vogl verborgen, s. Nr. 777.

29. Mai 1933 - 4. Mai 1934
(S Pr 120/33)

Prozeß gegen den Zitherlehrer Adolf DENTL
(geb. 24. Apr. 1896) aus München wegen
Weitergabe eines kommunistischen Flugblattes.

Urteil: 150 RM, ersatzweise 15 Tage Haft

30. Mai 1933 - 6. Apr. 1934
(S Pr 121/33)
