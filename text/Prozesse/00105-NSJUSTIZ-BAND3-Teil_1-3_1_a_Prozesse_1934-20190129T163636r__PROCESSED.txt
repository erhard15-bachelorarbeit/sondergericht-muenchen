(405)

(406)

(407)

(408)

(409)

(410)

7754

7755

7756

7757

7759

7760

(S Pr 87/34)

Prozeß gegen den Holzhändler Adalbert
LINSMAYER (geb. 23. Apr. 1887) aus München
wegen kritischer Äußerungen über Nat.Sozz..

Urteil: 3 Monate Gefängnis

29. Dez. 1933 - 10. Okt. 1934
(S Pr 88/34)

Prozeß gegen den Instrumentenmacher Franz
Xaver DIETZ (geb. 28. Feb. 1907) aus Mün-
chen wegen abwertender Äußerungen über die
NSDAP.

Urteil: 6 Monate Gefängnis

2. Nov. 1933 - 31. Aug. 1934
(S Pr 89/34)

Prozeß gegen den Weber Karl RENNER (geb.
12. Feb. 1913) aus Augsburg wegen Betrugs
und Urkundenfälschung.

Urteil: 11 Monate Gefängnis

16. Feb. 1934 - 28. Mai 1935
(S Pr 90/34)

Prozeß gegen den Landwirt Xaver STEIB (geb.
10. Nov. 1899) aus Buchdorf (Lkr. Donau-
wörth) und den Gastwirt Telesphorus HAUN-
STETTER (geb. 25. Mrz. 1892) aus Schwenningen
(Lkr. Dillingen) wegen Sprengens eines
Baumes im Jahr 1921.

Urteil: Freispruch

14. Jan. 1934 - 26. Jun. 1934
(S Pr 91/34)

Prozeß gegen den kfm. Angestellten Heinz
Karl Hans KLÜSS (geb. 21. Aug. 1913),
verhaftet in Rosenheim, wegen Amtsanmaßung,
Freiheitsberaubung, Betrug, Urkundenfäl-
schung u.a..

Urteil: 1 Jahr 6 Monate Gefängnis
8. Jan. 1934 - 1. Apr. 1934
(S Pr 93/34)

Prozeß gegen den Hausierer Johann PELZER
(geb. 20. Apr. 1893) aus Mühldorf wegen
abwertender Äußerungen über Hitler.

Urteil: 3 Monate Gefängnis

6. Dez. 1933 - 22. Jun. 1934
(S Pr 94/34)
