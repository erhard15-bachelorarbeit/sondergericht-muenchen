(1072)

(1073)

(1074)

(1075)

8209

8210

8211

8212

Kaufmann Wilhelm JUNG (geb. 9. Feb. 1893),
alle aus München, wegen Verteilung von
Schriften und Propaganda für die Ernsten
Bibelforscher.

Urteil: Mutschler 1 Jahr 9 Monate Ge-
fängnis; Kammerer 1 Jahr 2 Monate
Gefängnis; Jung 10 Monate Gefäng-
nis

Anlagen: 1 Gesetz über die religiöse Kinder-
erziehung (Abschrift); 1 "Seine
Organisation" (hektographiert,

24 sS.); 1 "Simson" (gedruckt, un-
paginiert, 16 S.); 1 "Simson"
(hektographiert, Kommentar zum
vorigen, 14 5S.); 1 "Frage über
Militärdienst" (Durchschlag)

9. Jan. 1936 - 9. Nov. 1937
(16 KMs So 55/36)

Prozeß gegen den Maschinisten Josef
GRÖBMILLER (geb. 26. Mai 1892) aus Göggin-
gen (Lkr. Augsburg) wegen Erzählens von
politischen Witzen.

Urteil: Verfahren eingestellt

8. Dez. 1935 - 23. Jun. 1936
(16 KMs So 56/36)

Prozeß gegen den Arbeiter und Schneider

Bernhard FPRÄNDL (geb. 18. Feb. 1882) aus
Tutzing (Lkr. Starnberg), KPD-Mitglied,

wegen einer kritischen Äußerung über die
Regierung.

Urteil: 6 Monate Gefängnis

B. war bereits vom 11. Mrz. 1933 bis
13 Apr. 1933 im KZ Dachau.

6. Apr. 1936 - 19. Mai 1938
(16 KMs So 57/36)

Prozeß gegen den Austragsbauern Michael
HURLER (geb. 5. Mrz. 1875) aus Fronhofen
(Lkr. Höchstädt) wegen abwertender Äußerun-
gen über die Regierung.

Urteil: 4 Monate Gefängnis

8. Mai 1935 - 19. Mai 1938
(16 KMs So 58/36)

Prozeß gegen den Graphologen Hans MÜLLER
(geb. 4. Aug. 1889) aus München wegen der
Behauptung, Hitler sei homosexuell veran-
lagt.

Urteil: 1 Jahr 6 Monate Gefängnis

26. Feb. 1936 - 9. Dez. 1937
(16 KMs So 59/36) 209
