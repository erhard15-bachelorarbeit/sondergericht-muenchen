(5642)

(5643)

(5644)

(5645)

10094

10095

10096

10097

(Lkr. Weissenburg) wegen zu niedriger
Gewichtsangaben bei Schlachtvieh.

Urteil: Steib, Haller, Berner je 2 Monate
Gefängnis; Schneider 1 Monat
Gefängnis
(3 1 Verbr.R.Str.VO)

2. Aug. 1941 - 24. Feb. 1944
(2 KLs So 77/41)

Prozeß gegen die Metzgermeistersehefrau
Maria HOPFNER (geb. 9. Feb. 1915) aus
Pappenheim (Lkr. Weissenburg) wegen zu
niedriger Gewichtsangaben bei Schlachtvieh.

Urteil: 10 Monate Gefängnis, 50.-RM + 250.-
RM Geldstrafen oder je 1 Tag Ge-
fängnis für 50.-RM
($ 1 KWVO; $3 396,418 RAO; 8$ 1-5

Schlachtsteuergesetz)

4. Aug. 1941 - 8. Mrz. 1945
(2 KLS So 78-/41)

Prozeß gegen den Metzgermeister Johann
Karl HÜBNER (geb. 2. Mai 1911) aus Pappen-
heim (Lkr. Weissenburg) wegen zu niedriger
Gewichtsangeben bei Schlachtvieh.

Verfahren wegen Abwesenheit des Angeklagten

eingestellt

($ 1 KWVO; 88 396,401,418 RAO; 88$ 1-5
Schlachtsteuergesetz)

5. Mrz. 1941 - 22. Jul. 1943
(2 KLs So 79/41)

Prozeß gegen den Kraftfahrer Rupprecht
SIPPEL (geb. 15. Aug. 1901) wegen Dieb-
stahls, Betrugs und Arbeitsvertragsbruchs.

Urteil: 1 Jahr 6 Monate Zuchthaus, Aber-
kennung der bürgerlichen Ehren-
rechte auf 2 Jahre .
($ 1 KWVO; 388 242,263 StGB; 3 2
Lohn VO)

S5. war vom 15. Jan. 1936 bis 15. Mai 1937
im KZ Dachau.

12. Feb. 1941 - 1. Mai 1943
(2 KLs So 80/41)

Prozeß gegen den Käsereipächter Emil WECHSEL
(geb. 15. Aug. 1893) aus Seibothen (Lkr.
Kempten) wegen Verkaufs von Käse ohne
Bezugsscheine dafür zu erhalten.
