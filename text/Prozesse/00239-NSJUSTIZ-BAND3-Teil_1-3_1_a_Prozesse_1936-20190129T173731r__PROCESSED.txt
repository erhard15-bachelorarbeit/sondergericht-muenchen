(1115) 8252

(1116) 8253

(- 30. Nov. 1907), beide aus Peißenberg
Lkr. Weilheim), den Wollwarengeschäftsin-
haber Klement DREXL (geb. 30. Nov. 1880)

aus Hechendorf (Lkr. Weilheim), den Maurer
Lorenz SCHINDLER (geb. 1. Jul. 1871), den
Händler Xaver SCHWALLER (geb. 19. Apr. 1902),
beide aus Murnau (Lkr. Weilheim), den Ober-
postschaffner August STETTINGER (geb. 14.
Jun. 1889) aus Peißenberg, wegen Fortführung
der Organisation der Ernsten Bibelforscher.

Urteil: Engel und Lehmann je 1 Jahr Gefängnis;
Bals und Schuster 7 Monate Gefängnis;
Drexl 3 Monate Gefängnis; Schindler
und Schwaller je 2 Monate Gefängnis

Anlage: "Der Wachtturm", 15. Apr. 1934

30. Mrz. 1936 - 7. Dez. 1937
((16 KMs So 99/36)
Prozeß gegen den Lokomotivführer a.D. Franz
WALCH (geb. 19. Mai 1878) aus Oberwöhr (Lkr.
Rosenheim), bereits wegen der Verbreitung
von Schriften der Ernsten Bibelforscher mit
1 Monat Gefängnis bestraft (amnestiert), den
Schuhmacher Valentin STRASSHOFER (geb. 3.
Feb. 1883), österreichischer Staatsbürger
aus Rosenheim, den Landwirt Anton WAGELE
(geb. 2. Feb. 1877) und seine Ehefrau Anna
WÄGELE (geb. 16. Mai 1865), beide aus Au
(Gemeinde Hochstätt, Lkr. Rosenheim), den
Uhrmacher Edmund PRANDSTÄTTER (geb. 9. Mai
1885), wegen Fortführung der Vereinigung
der Ernsten Bibelforscher.
Urteil: Walch 6 Monate Gefängnis;
Strasshofer 4 Monate Gefängnis;
Anton Wägele 3 Monate Gefängnis;
Anna Wägele 2 Monate Gefängnis;
Prandstätter Freispruch

1. Jun. 1936 - 20. Mai 1938
(16 KMs So 100/36)

Prozeß gegen den Zimmermann Josef CHRIST
(geb. 17. Mai 1904) aus Hammel (Lkr. Augs-
burg) wegen der Behauptung, Hitler werde
von einem Juden beraten.

Urteil: 3 Monate Gefängnis

12. Mai 1936 - 31. Mai 1938
(16 KMs So 101/36)
