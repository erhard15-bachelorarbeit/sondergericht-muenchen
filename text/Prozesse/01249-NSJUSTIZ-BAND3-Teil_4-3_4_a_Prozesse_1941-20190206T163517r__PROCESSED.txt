(5894)

(5895)

(5896)

(5897)

(5898)

10341

10342

10343

10344

10345

Prozeß gegen Margarethe ENGELHARDT (geb.
22. Jun. 1911) aus Weißenburg wegen der
Behauptung, ihr Ehemann, der Steinbruch-
arbeiter Karl ENGELHARDT, der seit Spt.
1939 wegen Ehestreitigkeiten im KZ Ora-
nienburg war, sei im Mrz. 1940 keines na-
türlichen Todes gestorben, sondern schlecht
behandelt worden.

Urteil: 1 Monat Gefängnis
$ 1 HG)

2. Apr. 1940 - 26. Mrz. 1943
(1 KMs So 16/41)

Prozeß gegen die Rentnersehefrau Maria
REINHARDT (geb. 9. Dez. 1891) aus München
wegen Äußerungen über die Regierung und

über die Behandlung der jüdischen Familie
Schild aus München während der Reichskristall-
nacht.

Urteil: Freispruch
($ 2 HG)

11. Spt. 1940 - 13. Mrz. 1941
(1 KMs So 17/41)

Prozeß gegen die Köchin Maria SUMMERER
(geb. 22. Nov. 1897) aus Prien (Lkr. Ro-
senheim) wegen Äußerungen über den Krieg
und die offizielle Propaganda.

Urteil: 1 Jahr Gefängnis
$ 2 HG)

2. Spt. 1940 - 11. Nov. 1942
(1 kKMs So 18/41)

Prozeß gegen den Schlosser Hans RUMMEL
(geb. 24. Mai 1896) aus Illertissen wegen
einer Außerung über Hitler.

Urteil: 8 Monate Gefängnis
($ 2 Ha)

30. Jun. 1940 - 26. Mrz. 1943
(1 KMs So 19/41)

Prozeß gegen den Regierungsamtmann Alfred
BISCHOFF (geb. 13. Feb. 1878) und dessen
Ehefrau Erna BISCHOFF (geb. 11. Feb. 18853),
beide aus Gräfelfing (Lkr. München) wegen
politischer Außerungen.

Urteil: Alfred Bischoff 1 Jahr Gefängnis;
Erna Bischoff 10 Monate Gefängnis
($ 2 HG)

Alfred Bischoff wurde nach Straferlaß zur
Gestapo verbracht.
