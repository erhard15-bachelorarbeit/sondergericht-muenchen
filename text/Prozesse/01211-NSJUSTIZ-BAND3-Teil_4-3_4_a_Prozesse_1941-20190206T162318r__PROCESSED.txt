(5709)

(5710)

(5711)

(5712)

(5713)

10160

10161

10162

10163

10164

5. Mrz. 1942 an die Gestapo ausgeliefert.

25. Apr. 1941 - 5. Mrz. 1942
(3 KLs So 66/41)

Prozeß gegen den Metzger Johann WIRTH (geb.
23. Dez. 1908) aus München wegen Diebstahls

und Betrugs.

Urteil: 5 Jahre Zuchthaus, 5 Jahre Ehr-
verlust
(88 242,263 StGB; 3 4 WVVO)

10. Jun. 1941 - 24. Apr. 1945

(3 KLs So 69/41)

Prozeß gegen den Hilfsarbeiter Miloslaw
CARDA (geb. 8. Okt. 1920), Tscheche, und
den Metallpolierer Johann TOLACZI-KOLESAR
(geb. 23. Jul. 1919), Tscheche, beide aus
Munchen, wegen Diebstahls.

Urteil: Carda 2 1/2 Jahre Zuchthaus, 2 Jahre
Ehrverlust; Tolaczi 1 1/2 Jahre
Zuchthaus, 2 Jahre Ehrverlust
(88 242,243 StGB; 838 2,4 VVO)

11. Spt. 1941 - 24. Mrz. 1943
(3 KLs So 71/41)

Prozeß gegen den Maschinenführer Johann

Baptist WEBER (geb. 5. Apr. 1900) aus
Ismaning (Lkr. München) wegen Diebstahls.

Urteil: 1 Jahr Gefängnis

(38 242,243 StGB)
30. Aug. 1941 - 24. Jul. 1942
(3 KLs So 74/41)

Prozeß gegen den polnischen Hilfsarbeiter
Anton POST (geb. 9. Jul. 1918) aus München
wegen Diebstahls.

Urteil: 1 Jahr Zuchthaus, Aberkennung der
bürgerlichen Ehrenrechte auf 2

Jahre

($ 242 StGB; 83 4 VVO)
9. Spt. 1941 - 27. Nov. 1942
(3 KLs So 75/41)

Prozeß gegen den polnischen Hilfsarbeiter
Josef PRZYBYLOWICZ (geb. 25. Jan. 1914)
aus München wegen Diebstahis.

Urteil: 2 Jahre Zuchthaus, Aberkennung der
bürgerlichen Ehrenrechte auf 2 Jahre.
($ 243 StGB; 3 4 VVO)

1. Spt. 1941 - 24. Feb. 1944
(? KLs So 76/41)
