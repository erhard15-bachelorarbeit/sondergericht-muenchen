(1934) 8719 Prozeß gegen den Forstarbeiter Heinrich
BEER (geb. 21. Okt. 1883) aus Ramsau
(Lkr. Berchtesgaden) wegen abwertender
Äußerungen über das Winterhilfswerk und
über Göring.

Urteil: Verfahren eingestellt

22. Mrz. 1938 - 10. Aug. 1938
(1 KMs So 92/38)

(1935) 8720 Prozeß gegen den Hilfsarbeiter Ludwig
PLENTINGER (geb. 2. Feb. 1900) wegen
Äußerungen in Priel (1F-. Pfaffenhofen)
io ie Kirchenpolitik.

Urteil: 7 Monate Gefängnis

12. Okt. 1937 - 27. Jul. 1941
(1 KMs So 93/38)

(1936) 8721 Prozeß gegen die Zimmerpoliersehefrau
Luise PUTZ (geb. 27. Mai 1890) aus Hals
(Lkr. Passau) wegen politischer Äuße-
rungen.

Urteil: Freispruch

Das Verfahren in der selben Sache gegen
den Ehemann Georg PUTZ (geb. 21. Mai
1886) aus Hals wurde wegen Amnestie
eingestellt.

6. Aug. 1937 - 8. Dez. 1938
(1 KMS So 94/38)

(1937) 8722 Prozeß gegen den Gärtner Matthias
HEEL (geb. 11. Mrz. 1870) aus Bad
Wörishofen (Lkr. Mindelheim) wegen
der Außerung, er habe gehört, Hitler
sei homosexuell veranlagt.

Urteil: Verfahren eingestellt

11. Jul. 1938 - 24. Okt. 1938
(1 KMs So 95/38)

(1938) 8723 Prozeß gegen den Pfannenflicker Franz
Josef ZETTL (geb. 11. Spt. 1896) und
den Arbeiter Ludwig ZEITITL (geb. 24.
Okt. 1909), beide aus Möhren (Lkr.
Donauwörth) wegen abwertender Äußerungen
über Hitler.

Urteil: Franz Josef Z. 10 Monate
Gefängnis; Ludwig Z. Verfahren
eingestellt wegen Amnestie

Franz Josef Z. war bereits in Polizeihaft.

27. Jun. 1938 - 9. Mai 1939
(1 KMs So 96/38)
