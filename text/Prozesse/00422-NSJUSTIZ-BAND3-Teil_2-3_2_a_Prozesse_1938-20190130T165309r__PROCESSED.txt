(1964)

(1965)

(1966)

(1967)

(1968)

(1969)

8749

8750

8751

8752

8753

8754

Prozeß gegen den Likörfabrikanten Fritz
SCHLECHT (geb. 22. Jan. 1896) aus Mün-
chen wegen einer Äußerung über Goebbels.

Urteil: 4 Monate Gefängnis

29. Spt. 1938 - 10. Aug. 1942
(1 KMs So 121/38)

Prozeß gegen den Eisenhändler Johann
GEYER (geb. 5. Apr. 1901) aus Eisen-
felden (Lkr. Altötting), früher KPD-
Mitglied, wegen Außerungen in Kurz-

eichet (Lkr. Passau) über einen mög-
lichen Krieg.

Urteil: 5 Monate Gefängnis

14. Jun. 1938 - 22. Mrz. 1939
(1 KMs So 122/38)

Prozeß gegen den Tierheilkundigen
August ALBRECHT (geb. 20. Spt. 1873)
aus Kempten wegen Äußerungen über
die Regierung und das Takenkreuz.

Urteil: 5 Wochen Gefängnis
A. war bereits 3 Wochen in Polizeihaft.

7. Aug. 1938 - 25. Mrz. 1939
(1 KMs So 123/38)

Prozeß gegen den Hausierer STETTNER
(geb. 12. Mai 1892) aus München wegen
Äußerungen über die Wohlfahrtspflege.

Urteil: 4 Monate Gefängnis

18. Jun. 1938 - 27. Aug. 1939
(1 KMs So 124/38)

Prozeß gegen den Ingenieur Wilhelm
AUFER (geb. 16. Aug. 1898) aus Augsburg
wegen kritischer Äußerungen über
Hitlers Politik.

Urteil: 4 Monate Gefängnis

A. war bereits 10 Wochen in Schutzhaft.
19. Okt. 1938 - 23. Feb. 1939

(1 kKMs So 125/38)

Prozeß gegen den Schlosser Josef
KOPP (geb. 4. Nov. 1903) aus Voglöd
(Lkr. Passau) wegen abwertender ÄAußerun«

über Hitler.
Urteil: 6 Monate Gefängnis

17. Okt. 1938 - 6. Mai 1939
(1 KMs So 126/38)
