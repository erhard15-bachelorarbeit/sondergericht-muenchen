(1053)

(1054)

(1055)

(1056)

(1057)

(1058)

8190

8191

8192

8193

8194

8195

Prozeß gegen den Dienstknecht Josef BAUR
(geb. 4. Jun. 1897) wegen der Behauptung,
es werde bald ein Krieg mit den Franzosen
kommen,in Habertsweiler (Lkr. Schwabmünchen).

Urteils 7 Wochen Gefängnis

11. Mrz. 1936 - 6. Jul. 1936
(16 KMs So 36/36)

Prozeß gegen den Landwirt Alfons HAÄAUGENEDER
(geb. 4. Apr. 1899) aus Nürnberg (Lkr. Pfarr.
kirchen) wegen beleidigender Äußerungen über
Hitler.

Urteils 3 Monate Gefängnis

12. Jan. 1936 - 20. Mai 1938
(16 KMs So 37/36)

Prozeß gegen den Gipser Georg BURKART (geb.
3. Mai 1894) aus Bühl (Lkr. Günzburg) wegen
Erzählens eines politischen Witzes.

Urteil: Freispruch

12. Dez. 1935 - 27. Mai 1936
(16 KMs So 38/36)

Prozeß gegen den Erbhofbauern Johann SCHIEGL
(geb. 8. Jul. 1905) aus Meilenhofen (Lkr.
Eichstätt) wegen kritischer Äußerungen über
das Erbhofgesetz und Hitler.

Urteil: 6 Monate Gefängnis

12. Jan. 1936 - 19. Mai 1938
(16 KMs So 39/36)

Prozeß gegen den Möbeltransportarbeiter
August GREITHER (geb. 26. Spt. 1906) aus
München wegen Erzahlens eines politischen
Witzes.

Urteils 3 Monate Gefängnis

18. Nov. 1935 - 31. Mai 1938
(16 KMs So 40/36)

Prozeß gegen den Brauereiarbeiter Alfons
FISCHER (geb. 15. Apr. 1907) aus München
wegen einer herabsetzenden Äußerung über
den Blutorden,

Urteil: 6 Monate Gefängnis

6. Dez. 1935 - 20. Mai 1938
(16 KMs So 41/36)
