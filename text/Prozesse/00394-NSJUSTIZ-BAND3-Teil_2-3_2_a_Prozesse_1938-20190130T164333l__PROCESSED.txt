(1825)

(1826)

(1827)

(1828)

(1829)

(1830)

8609

8610

8611

8612

8613

8614

Prozeß gegen den Zimmerer Josef HÖRNER
(geb. 27. Apr. 1898) aus Munchen wegen
Widerstands gegen die Staatsgewalt und
Totschlagversuchs.

Urteil: Unterbringung in einer Heil-
und Pflegeanstalt

9. Apr. 1937 - 15. Mrz. 1941
(1 KLs So 5/38)

Prozeß gegen den Hilfsarbeiter Josef
WEISHÄUPL (geb. 23. Jan. 1918) aus
Habischried (Lkr. Regen) wegen Diebstahls

Urteil: 10 Monate Gefängnis

21. Okt. 1937 - 25. Aug. 1933
(1 KLs So 6/38)

Prozeß gegen den Hilfsarbeiter Bernhard
HACKER (geb. 21. Jun. 1916) aus Heiners-
reuth (Lkr. Bayreuth) wegen Diebstahls
und Betrug.

Urteil: 10 Monate Gefängnis

22. Nov. 1937 - 30. Okt. 1939
(1 KLs So 7/38)

Prozeß gegen den Invaliden Leonhard
KLEIN (geb. 7. Okt. 1880) aus Munchen
wegen Betrugs.

Urteil: 1 Jahr 2 Monate Zuchthaus;
Aberkennung der bürgerlichen
Ehrenrechte auf 3 Jahre

14. Feb. 1938 - 28. Apr. 1939
(1 KLs So 8/38)

Prozeß gegen den Gütlerssohn Rudolf

MAIER (geb. 4. Maı 1908) aus Schwar-
zenberg (Lkr. Rohrbach, Österreich)

wegen versuchten Totschlags an zwei

Zollbeamten im Dienst.

Urteil: 8 Jahre Zuchthaus; Aberkennung
der bürgerlichen Ehrenrechte
auf 5 Jahre

19. Spt. 1935 - 13. Mai 1944
(1 KLs So 9/38)

Prozeß gegen den Schreiner Josef STRASSER
(geb. 10. Spt. 1918) aus Neuodotting (Lkr.
Altötting) wegen Diebstahls.

Urteil: 1 Jahr 6 Monate Gefängnis

19. Feb. 1938 - 23. Aug. 1939
(1 KLs So 10/38)
