(784)

(785)

(786)

an:

7922

7923

7924

SPD-Mitglied, den Brauereiarbeiter Josef
FORSTER (geb. 9. Jan. 1898), Zugführer bei
der "Eisernen Front", den Hilfsarbeiter
Josef VIERLBÖCK (geb. 27. Feb. 1897), SFD-
und "Eiserne Front"-Anghöriger, den Gast-
wirt Josef BERGHAMMER (geb. 26. Okt. 18889),
Mitglied von SPD, Reichsbanner, Eiserne
Front, alle aus München, wegen Verbreitung
kommunistischer Druckschriften und eines
katholischen Wahlflugblattes (Text im Akt).

Urteil: Einstellung des Verfahrens, nach-
dem das OLG seine Zuständigkeit
vegeint hat

Anlagen: 1 Kopie "Sozialistische Aktion",
4. Feb. 1934; je 1 "Inprekorr"
Nrr. 13 - 15, April 1934; je 1
"Die Rote Fahne" Ende April,
Anfang Mai, Anfang Juni 1934;
"Deutsche Zeitung Bohemia", 15.
Juli 1934, S. 4 - 10 (verboten);
1 Flugblatt "Unsere Parole zum 12.
November"; 1 Tarnprospekt "Was
jeder Eisenbahner braucht, kauft
er bei Tietz, deutsches Geschäft".

Brunner wurde bei seiner Entlassung aus
der Untersuchungshaft der BPP überstellt.

3. Dez. 1934 - 20. Jun. 1935
(S Pr 46/35)

Prozeß gegen den jüdischen Kaufmann Max
SUFRIN (geb. 20. Jun. 1896) und den Händ-
ler Georg KULZER (geb. 28. Apr. 1894),
beide aus München, wegen einer Erzählung
von politischen Witzen.

Urteil: Verfahren eingestellt

20. Dez. 1934 - 15. Mai 1935
(S Pr 47/35)

Prozeß gegen die Bardame Wally KNOSPE
(geb. 31. Mai 1906) aus München wegen
Weitergabe des Buches "Die Memoiren des
Stabschefs Röhm".

Urteil: 3 Monate Gefängnis
27. Dez. 1934 - 16. Dez. 1935
(S Pr 48/35)

Prozeß gegen den Kaufmann Ottc HUBER
(geb. 7. Dez. 1900) aus Weissenburg
wegen Erzählung von Greueln aus der
Röhm-Affaire und dem KZ Dachau.

Urteil: 50 RM Geldstrafe oder 10 Tage Haf

25. Jan. 1935 - 4. Mai 1935
(S Pr 49/35)
