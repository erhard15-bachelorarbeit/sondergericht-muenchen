(3274)

(3275)

(3276)

(3277)

(3278)

9328

9329

9330

9331

9332

Prozeß gegen den Automechaniker Josef
LENTNER (geb. 15. Apr. 1899) aus München,
Angehöriger des Reichsbanners, wegen po-
litischer Äußerungen. A
Urteil: 6 Monate Gefängnis”

(&$ 2 HG)

6. Okt. 1938 - 25. Apr. 1940
(1 KMs So 96/39) .

Prozeß gegen den Hilfsarbeiter Hermann B6OSI
(geb. 19. Apr. 1889) aus München, 1935 für
3 Monate 13 Tage im KZ, wegen der Äußerung,
im Ausland sei das Leben besser und der
Behauptung, er habe gesehen, wie im KZ ein
Mann erschossen worden sei.

Urteil: 7 Monate Gefängnis

$ 1 HG)
25. Mrz. 1939 - 8. Nov. 1939
(1 KMs So 97/39) .

Prozeß gegen den kath. Pfarrer Josef SIGL
(geb. 9. Spt. 1907) aus Unterwindach (Lkr.

. Landsberg) wegen der Äußerung in Hirsch-

brunn (Lkr. Nördlingen), "Es ist nicht
wahr, daß wir Priester mit Bibelsprüchen
auf den Lippen faulenzend durch's Land
ziehen. Diese Horde übelster Schmähredner
will damit nur das Priestertum schlecht
machen". |

Urteil: 6 Monate Gefängnis
$ 2 HG)

11. Spt. 1938 - 27. Apr. 1943
(1 KMs So 98/39)

Prozeß gegen den Hilfsarbeiter Philipp
Albert VOLK (geb. 14. Jul. 1905) aus Augs-
burg wegen Verbreitung von Greuelnachrich-
ten in Darmstadt über das KZ Dachau.

Urteil: 4 Monat& Gefängnis

$ 1 Ha) |
11. Nov. 1938 - 18. Mrz. 1939
(1 KMs So 99/39):

Prozeß gegen den Molkereipächter Johann
SCHMID (gb. 4. Aug. 1898) aus Unterfinning
(Lkr. Landsberg) wegen beleidigenden Äußeru
gen über das Dritte Reich.

Urteil: 3 Monate Gefängnis
($ 134 a StGB)

22. Feb. 1939 - 8. Aug. 1939
(1 KMs So 100/39)
