(5602)

(5603)

(5604)

(5605)

10054

19055

100536

10057

Sicherheits- und Hilfsdienstes (S.H.D.).

Urteil: Gefängnisstrafen zwischen 2 Jahren
unl 4 Monaten und Geläöstrafen zwi-
schen 2000.-RM und 180.-RM oder 1
Tag Gefängnis für je 50.-RM Strafe.
($ 1 kKkWwWVO; 8 396 RAO; 8$ 1-5
Schlachtsteuergesetz)

13. Mrz. 1942 - 1. Nov. 1944
(2 KLs So 35/L1342/42)

Prozeß gegen den Bauern Josef WÖRL (geb.
5. Spt. 1882) aus Hettenhausen (Lkr. Paffen-
hofen) wegen Schwarzschlachtens.

Urteil: 1 Jahr 2 Monate Zuchthaus, 150.-
RM + 500.-RM Geldstrafe oder 6 +
10 Tage Gefängnis, Aberkennung der
burgerlıichen Ehrenrechte auf 2
Jahre
($ 1 KWVO; $8 396 RAO; 38 1-5
Schlachtsteuergesetz

24. Jan. 1941 - 23. Mrz. 1944
(2 KLs So 36/41)

Prozeß gegen den Metzgermeister Georg KOR-
BEL (geb. 8. Jun. 18738) aus Freilassing
(Lkr. Traunstein), NSDAP-Mitglied, wegen
Schwarzschlachtens.

Urteil: 1 Jahr 10 Monate Gefängnis
($ 1 kwVo)

24. Mrz. 1941 - 23. Feb. 1945
(2 KLs So 37/41)

Prozeß gegen den Direktor Dr. Karl Wilhelm
ZENTZ (geb. 27. Mrz. 1907) aus München
wegen Bezugs einer überhöhten Benzinmenge.

Urteil: Freispruch
($ 1 KWVO)

3. Aug. 1940 - 18. Dez. 1941
(2 KLs So 38/41)

Prozeß gegen den polnischen Landarbeiter
Wladyslaw KUBACKI (geb. 20. Jun. 1896)
aus Schellenbergerhof (Lkr. Donauwörth)
wegen Diebstahls von Eiern und Hennen.

rteil: 4 Jahm Zuchthaus, Aberkennung der
bürgerlichen Ehrenrechte auf 4
Jahre
($ 1 kWVO; 88 32-34,242 StGB)
21. Apr. 1943 an dies Polizei übergeben,
20. Aug. 1943 Akten weggelegt.

16. Mai 1941 - 20. Aug. 1943
(2 KLs So 39/41)
