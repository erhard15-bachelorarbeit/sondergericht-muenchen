(4923)

(4924)

(4925)

(4926)

(4927)

9920

9921

9922

9923

9924

Prozeß gegen den Lagerarbeiter Karl SCHMID
(geb. 11. Jul. 1896) aus Augsburg wegen
Äußerungen über den Krieg und der falschen
Behauptung, er sei Pg.

Urteil: 5 Monate Gefängnis, verbüßt durch
Polizei- und Schutzhaft
($8 2,4 HG)

25. Nov. 1940 ın das KZ Dachau rücküber-
stellt.

15. Mai 1940 - 25. Nov. 1940
(1 KMs So 239/40)

Prozeß gegen Anastasia DODELL (geb. 17. Dez.
1891) aus Oderding (Lkr. Weilheim), früher
Mitglied der NS-Frauenschaft, wegen einer
Äußerung über den Krieg und die Rundfunk-
nachrichten.

Urteil: 3 Monate Gefängnis
(8 2 HG)

19. Mai 1940 - 20. Apr. 1943
(1 KMs So 240/40)

Prozeß gegen die Sackflickerin Josefine
KEIS (geb. 16. Mai 1908) aus München wegen
Äußerungen über den Reichstagsbrand, die
Gestapo, die Judenpolitik und den Krieg.

Urteil: 6 Monate Gefängnis
3 1,2 HG)

19. Jul. 1940 - 27. Spt. 1941
(1 KMs So 241/40)

Prozeß gegen den Schreinerwerkmeister Ludwig
OBERMAIER (geb. 28. Jul. 1884) aus München
wegen politischer Außerungen.

Urteil: 1 Jahr Gefängnis

(3 2 Ha)
14. Mai 1940 - 18. Apr. 1944
(1 KMs So 242/40)

Prozeß gegen den Packer Stefan IGERL (geb.
25. Dez. 1893) aus München wegen einer
Bemerkung uber die Regierung.

&8 2 HG

20. Jul. 1940 - 4. Feb. 1941
(1 KMs So 243/40)

Urteil: 6 Mans Gefängnis
