Prozeß gegen den Sattler Leonhard RUF (geb.
17. Mai 1905) aus Amberg (Lkr. Mindelheim)
wegen Amtsanmaßung und Bedrohung.

Urteil: 3 Monate Gefängnis

25. Mrz. 1933 - 17. Apr. 1934
(S Pr 21/33)

Prozeß gegen den Kaufmann und Seilermeister
Georg SCHÖBERL (geb. 10. Apr. 1872) aus Er-
ding, wegen Nichtablieferung von Waffen.

Urteil: Freispruch
12. Apr. 1933 - 9. Mai 1933

Prozeß gegen den Kaufmann Otto RENNER (geb.
20. Mai 1914) aus München wegen unbefugten
Tragens des NSDAP-Parteiabzeichens.

Urteil: 50 RM Geldstrafe oder 1 Monat Ge-
fängnis

1. Apr. 1933 - 14. Jul. 1933
(S Pr 23/33)

Prozeß gegen den Kraftwagenführer Lorenz
SAGERER (geb. 11. Apr. 1899) aus München,
früher NSDAP- und SA-Mitglied, wegen unbe-
fugten Tragens des NSDAP-Parteiabzeichens.

Urteil: 1 Monat Gefängnis

1. Apr. 1933 - 20. Jul. 1933
(S Pr 24/33)

Prozeß gegen den Melker Peter WEBER (geb.
29. Mai 1907) aus Immenstadt (Lkr. Sont-
hofen) wegen unerlaubten Waffenbesitzes.

a

Urteil: 4 Monate Gefängnis

9. Apr. 1933 - 31. Aug. 1933
(S Pr 25/33)

Prozeß gegen den Gütler und Holzarbeiter
Johann HASLINGER (geb. 8. Feb. 1895) aus
Steinbachl (Lkr. Laufen) wegen Nichtab-

lieferung von Waffen.

Urteil: 150.-- RM Geldstrafe oder 10 Tage
Gefängnis

8. Apr. 1933 - 10. Mai 1933
(S Pr 26/33)
