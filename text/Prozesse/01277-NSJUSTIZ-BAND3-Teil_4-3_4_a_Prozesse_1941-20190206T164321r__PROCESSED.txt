Urteil: 1 Jahr 3 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 4 Wehrkraft VO)

11. Spt. 1941 - 27. Jul. 1944
(1 KMs So 162/41)

(6036a) 10834 Prozeß gegen den Kaufmann Adolf PAUL aus
Kaufbeuren.

Urteil: 1 Jahr 8 Monate Gefängnis

18. Mai 1944
(1 KMs So 163/41)

(6037) 10485 Prozeß gegen den Schlosser Wilhelm KUBICA
(geb. 1. Mai 1900) aus Augsburg wegen
Äußerungen über den Krieg und führende
Nat.S5ozz. und wegen eines Bekenntnisses
zum Kommunismus.

Urteil: 1 Jahr 3 Monate Gefängnis
8 2 HG)

5. Jul. 1941 - 13. Mrz. 1944
(1 KMs So 164/41)

(6038) 10486 Prozeß gegen den Rangieraufseher Karl FREY
(geb. 13. Jun. 1898) aus München wegen
einer Äußerung über die Zukunft des NS.

Urteil: 5 Monate Gefängnis
$& 2 HG)

10. Apr. 1941 - 20. Jun. 1942
(1 KMs So 165/41)

(6039) 10487 Prozeß gegen den städt. Verwaltungsinspek-
tor Ludwig MUNDBROT (geb. 26. Apr. 1886)
aus München, früher BVP-Mitglied, wegen
politischer Bemerkungen auf einem Kalen-
der des "Völkischen Beobachters".

Urteil: 4 Monate Gefängnis
& 2 HG)

Anlage: der beschriebene Kalender.

16. Apr. 1941 - 3. Spt. 1943
(1 KMs So 166/41)

(6040) 10488 Prozeß gegen die Dienstmagd Maria AICHIN-
GER (geb. 27. Apr. 1907) aus Peissenberg
(Lkr. Weilheim) wegen verbotenen Umgangs
mit den französischen Kriegsgefangenen
Josepf BONNET und NOEL.

Urteil: 2 Jahre Zuchthaus, 2 Jahre Ehr-
verlust
(S$ 4 Wehrkraft VO)
