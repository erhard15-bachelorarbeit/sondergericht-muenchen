(5650)

(5651)

10101

10102

Verkäufer Heinrich HAIN (geb. 3. Jun.
1902), den Buchhalter Rudolf KLEIN (geb.
1. Mrz. 1905), den Großschlächter Josef
PRILLER (geb. 6. Jan. 1899) ,(gest. 18.
Dez, 1943), den Metzgermeister Anton
WINKLMANN (geb. 13. Mai 1905), alle aus
München wegen Schwarzschlachtens und Bei-
hilfe dazu.

Urteil: Engelhart und Distler Freispruch;
Hain 2 Jahre 6 Monate Gefängnis;
Oberbeil 3 Jahre 6 Monate Gefäng-
nis;*10000.-RM Geldstrafe oder.
100 Tage Gefängnis; Klein 2 Jahre
6 Monate Gefängnis; Priller 6 Mo-
nate Gefängnis, 1000.-RM Geld-
strafe oder 20 Tage Gefängnis;
Winklmann 3 Monate Gefängnis
($ 1 KWVo; 3 1 Preis-Straf VO;

$ 1 Verbr.R.Str.VO)

(Nur Straf- und Gnadenhefte)

18. Apr. 1942 - 23. Feb. 1945
(2 KLs So 85/41)

Prozeß gegen den polnischen Metzger und
landw. Arbeiter Josef MROZINSKI (geb.

15. Feb. 1909) aus Hohenwart (Lkr. Kauf-
beuren) wegen Beihilfe zu Schwarzschlach-

tungen.

Urteil: 6 Monate Straflager
($ 1 KWVO; Polen-Juden-Straf VO;

$ 49 StGB)

22. Nov. 1941 - 21. Spt. 1942
(2 KLs So 86/41)

Prozeß gegen die Gast- und Landwirtschafts-
besitzerinnen Rosa (geb. 31. Aug. 1895),
Maria (geb. 14. Nov. 1883) und Kreszenz
WELZ (geb. 9. Dez. 1890) und den landw.
Arbeiter Johann HERZ (geb. 22. Mrz. 1914),
alle aus Hohenwart (Lkr. Kaufbeuren) we-
gen Schwarzschlachtens und Beihilfe dazu.

Urteil: Rosa und Maria Welz je 1 Jahr 2
Monate Zuchthaus, 200.-RM + 2100.-
RM Geldstrafen oder 1 Tag Gefäng-
nis für je 50.-RM; Kreszenz Welz
8 Monate Gefängnis, 200.-RM -+
2100.-RM Geldstrafe oder 1 Tag Ge-
fängnis für je 50.-RM; Herz 4 Mo-
nate Gefängnis, 50.-RM + 500.-RM
Geldstrafe oder 1 Tag Gefängnis für
je 50.-RM
($ 1 KWVO; 88 301,396,401 RAO;

88 1-4 Schlachtsteuergesetz)

1. Feb. 1941 - 18. Jun. 1944
(2 KLs So 87/41)

 

* 5E

.&

te „ van 6 Monate Gefäng- 1145
