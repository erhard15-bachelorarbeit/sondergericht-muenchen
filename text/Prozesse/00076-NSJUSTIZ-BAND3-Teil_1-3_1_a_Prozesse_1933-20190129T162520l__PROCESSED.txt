(255)

(256)

(257)

(258)

(259)

(260)

7607

7608

7609

7610

7611

7612

Prozeß gegen den Eisendreher Karl FROSCHER-
MAIER (geb. 7. Jun. 1901) aus Planegg (Lkr.
München) wegen der Behauptung, V.d.Lubbe sei
Nazi u.ä.. F. war Anhänger der KPD, dann An-
wärter für den Werksturm der SA.

Urteil: 6 Monate Gefängnis

F. gibt an, er sei früher mit dem im KZ
Dachau befindlichen Ortsgruppenleiter der
KPD LEITNER öfters verkehrt.

6. Okt. 1933 - 24. Aug. 1934
(S Pr 309/33)

Prozeß gegen den Schneider Johann STEGMANN
(geb. 12. Apr. 1912) aus Waalhaupten (Lkr.
Kaufbeuren), Kommunist, wegen beleidigender
Äußerungen gegen das Regime.

Urteil: 4 Monate Gefängnis

12. Nov. 1933 - 13. Apr. 1934
(S Pr 310/33)

Prozeß gegen den Hilfsarbeiter Robert
BLETTINGER (geb. 19. Apr. 1907) aus Dachau,
kommunistisch gesinnt, wegen der Behauptung,
Hitler habe v.d. Lubbe eine Million Reichs-
mark gegeben u.ä..

Urteil: 8 Monate Gefängnis

18. Spt. 1933 - 13. Spt. 1934
(S Pr 311/33)

Prozeß gegen Brigitte MAIER, ohne Beruf,
(geb. 5. Mai 1904) und die Putzerin Katha-
rina MAIER (geb. 30. Dez. 1907), beide

aus München, wegen beleidigender Äußerungen
über Hitler.

Urteil: Brigitte Maier freigesprochen
Katharina Maier 5 Monate Gefängnis

18. Aug. 1933 - 12. Spt. 1934
(S Pr 312/33)

Prozeß gegen Sebastian GLÜCK (geb. 19. Jan.
1915), ohne Beruf, aus Augsburg, wegen
Betrugs.

Urteil: 1 Jahr Gefängnis
ab 2 Monate Untersuchungshaft

7. Nov. 1933 - 13. Dez. 1934
(S Pr 313/33)

Prozeß gegen die Kunstmalerswitwe Amalia
VOLKHAUSEN (geb. 12. Aug. 1879) aus Herr-
