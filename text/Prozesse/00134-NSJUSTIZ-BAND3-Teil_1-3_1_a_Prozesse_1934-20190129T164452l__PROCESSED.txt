(569)

(570)

(571)

(572)

(573)

8967

8968

8969

8970

8971

Urteil: 5950 RM Geldstrafe oder 10 Tage
Gefängnis

15. Okt. 1934 - 9. Jul. 1935
(S Pr 297/34)

Prozeß gegen den Heizer Georg HARTMANN (geb,
9. Dez. 1872) aus München, SPD-Anhänger, we-
gen abwertender Außerungen über das Regime.

Urteil: 3 Monate Gefängnis

13. Okt. 1934 - 30. Apr. 1935
(S Pr 298/34)

Prozeß gegen den Koch Christian HORNUNG (get
25. Apr. 1909) aus München wegen Verbreitur;
eines sozialistischen Flugblattes.

Urteil: 3 Monate Gefängnis

21. Aug. 1934 - 3. Jan. 1935
(S Pr 299/34)

Prozeß gegen den Schneider Johannes PELZ
(geb. 2. Spt. 1906), früher Mitglied der
NSDAP, wegen Zechbetrugs in Günzburg.

Urteil: 7 Monate Gefängnis

25. Okt. 1934 - 6. Mrz. 1935
(S Pr 300/34)

Prozeß gegen den Devisenbuchprüfer Karl
SCHÄTZ (geb. 26. Dez. 1900) aus München,
SA-Mitglied, wegen kritischer Außerungen
über die wirtschaftliche Lage.

Urteil: 200 RM Geldstrafe oder 2 Monate
Gefängnis

Die zuständige Beamtenfachschaft erwirkte
nach dem Urteil die Kündigung.

Verfahren gegen die Kontoristin Margarete
SCHÄTZ (geb. 27. Apr. 1906) in der selben
Sache eingestellt.

15. Okt. 1934 - 29. Apr. 1936
(S Pr 301/34)

Prozeß gegen den Techniker Johann HIRSCH-
MANN (geb. 9. Apr. 1900) aus München und
den Hilfsarbeiter Josef KERSCHER (geb. 18.
Jan. 1913) aus München wegen Verteilens
illegaler Flugblätter.
