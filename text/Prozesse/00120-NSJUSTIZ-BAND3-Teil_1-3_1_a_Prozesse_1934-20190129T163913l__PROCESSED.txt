(488)

(489)

(490)

(491)

(492)

(493)

8849

8850

8851

8853

8854

8855

Urteil: 100 RM Geldstrafe oder CU lage
Gefängnis

6. Apr. 1934 - 23. Aug. 1934
(S Pr 176/34)

Prozeß gegen den Arbeiter Andreas HEIGL (g
26. Mrz. 1896) aus Ingolstadt wegen einer
abwertenden Äußerung über die Regierung.

Urteil: 4 Wochen Gefängnis

17. Mai 1934 - 2. Aug. 1934
(S Pr 177/34)

Prozeß gegen den Gastwirt Alois STEIRKHOFZ
(geb. 2. Jan. 1873) und den Maurer Franz
HILTL (geb. 15. Mai 1873), beide aus Munck
wegen Devisenvergehens.

Urteil: Freispruch

25. Mai 1934 - 18. Spt. 1934
(S Pr 178/34)

Prozeß gegen den Landhelfer August MAERKEL
(geb. 21. Okt. 1911) aus Fuchswinkel (Lkr.
Freising) wegen der Behauptung, im KZ Dach
würden Schützhäftlinge geschlagen.

Urteil: 2 Monate Gefängnis

2. Apr. 1934 - 24. Aug. 1934
(S Pr 179/34)

Prozeß gegen den Kupferschmied Johann HUB:
(geb. 20. Aug. 1689) aus Redenfelden (Lkr.
Rosenheim) wegen der Behauptung, Hitler ar
beitete auf einen Krieg hin. H. war frühen
KPD-Mitglied.

Urteil: 3 Monate Gefängnis

31. Jan. 1934 - 24. Aug. 1934
(S Pr 181/34)

Prozeß gegen die Schreinersfrau Josefa
PRÖBSTLE (geb. 12. Jun. 1873) aus Hinter-
rottach (Lkr. Kempten) wegen Beschimpfung
Hitlers.

Urteil: Verfahren eingestellt

4. Mai 1934 - 11. Aug. 1934

(S Pr 182/34)

Prozeß gegen die Verkäuferin Christine Mar
FRÜHWALD (geb. 24. Jan. 1915) aus München
gen kritischer Bemerkungen in einem Brief
nach England.
