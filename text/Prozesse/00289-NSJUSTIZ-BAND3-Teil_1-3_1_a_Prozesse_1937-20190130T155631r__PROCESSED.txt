(1372)

(1373)

(1374)

(1375)

(1376)

9064

9065

9066

9067

9068

Prozeß gegen den Pressevertreter Wilhelm
WEIGLE (geb. 8. Dez. 1890) aus München,
früher HJ-Mitglied, wegen Betrugs.

Urteil: 1 Jahr 8 Monate Zuchthaus und
200 RM Geldstrafe oder 20 Tage
Zuchthaus

21. Okt. 1936 - 30. Jul. 1938
(1 KLs So 1/37)

Prozeß gegen die Hausangestellte Wilhelmine
WICHMANN (geb. 27. Jan. 1912) aus Berlin
wegen Diebstahls in München.

Urteil: Unter Einbeziehung einer weiteren
Strafe von 6 Monate (AG Hagen vom
3. Jun. 1937) 10 Monate Gefängnis
Gesamtstrafe

26. Apr. 1937 - 1. Nov. 1938
(1 KLs So 2/37)

Prozeß gegen den Förster Anton GERTIL (geb.
(23. Okt. 1905), österreichischer Staats-
bürger, wegen Betrugs.

Urteil: 2 Jahre 6 Monate Zuchthaus, Ab-
erkennung der bürgerlichen Ehren-
-h--

19. Nov. 1935 - 29. Spt. 1939
(1 KLs So 3/37)

Prozeß gegen den Kontoristen Johann Baptist
BERNAUER (geb. 10. Nov. 1892) wegen Dieb-
stahls.

Urteil: 10 Jahre Zuchthaus, Aberkennung
der bürgerlichen Ehrenrechte auf
10 Jahre, Sicherungsverwahrung

31. Mrz. 1937 - 3. Dez. 1937
(1 KLs So 6/37)

Prozeß gegen den Metzgerlehrling Franz
HUBER (geb. 28. Jul. 1921), den Metzger-
meister Georg SELBERDINGER (geb. 6. Mrz.
1911), dessen Bruder, den Metzgermeister
Peter SELBERDINGER (geb. 23. Nov. 1902),
den Metzgergehilfen Thaddäus HELMINGER

geb. 5. Spt. 1911), alle aus Nußdorf

Lkr. Traunstein), wegen eines Sprengstoff-
anschlages auf das Auto eines Hauptlehrers
und Kreisamtsleiters für Kommunalpolitik.
