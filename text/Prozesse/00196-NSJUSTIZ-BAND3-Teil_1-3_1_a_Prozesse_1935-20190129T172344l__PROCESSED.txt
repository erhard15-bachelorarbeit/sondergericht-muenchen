(890)

(891)

(892)

(893)

8027

8028

8029

5030

Urteil: Friedrich 2 Monate Gefängnis oder
300 RM Geldstrafe; Randlinger 20
Tage Gefängnis oder 60 RM Geld-
strafe

9. Jan. 1935 - 24. Dez. 1935
(S Pr 161/35)

Prozeß gegen den Mesner und Gütler Johann
STUFFER (geb. 7. Okt. 1872) aus Nußdorf

(Lkr. Rosenheim), BVP-Anhänger, wegen kri-
tischer Äußerungen über politische Dinge.

Urteil: 2 Monate Gefängnis

24. Mrz. 1935 - 24. Apr. 1936
(S Pr 162/35)

Prozeß gegen den Sennereiaufseher Benedikt
HEILIGENSETZER (geb. 31. Okt. 1885) aus
Mauerstetten (Lkr. Kaufbeuren), den Sägen-
feiler Peter FINK (geb. 28. Mai 1878) aus
Irsengrund (Lkr. Lindau), den Landwirt
Engelbert OHMAYER (geb. 25. Nov. 1863)
aus Langenried (Lkr. Lindau), alle Anhänger
der Winterpartei, wegen kritischer Außerun-
gen über den Reichstagsbrand und die Röhm-
Affaire.

Urteil: Heiligensetzer 4 Monate Gefängnis;
Fink 7 Monate Gefängnis; Ohmayer
8 Monate Gefängnis, vor Antritt
der Strafe verstorben

23. spt. 1935 - 17. Mai 1938
(S Pr163/35)

Prozeß gegen den Vertreter Josef KBEES (geb.
3. Mai 1898) wegen kritischer Äußerungen
gegen die Verurteilung von Nonnen wegen
Devisenschieberei im Lkr. Marktoberdorf.

Urteil: 4 Monate Gefängnis

4. Jun. 1935 - 22. Mai 1939
(S Pr 164/35)

Prozeß gegen den Invaliden Anton BIHLER,
(geb. 28. Mrz. 1888) aus Memmingen, KPD-
Mitglied, wegen abwertender Außerungen
üher den ReichsyagsLrwraud.

Urteil: 8 Monate Gefängnis

15. Jun. 1935 - 29. Mrz. 1937
(S Pr 165/35)
