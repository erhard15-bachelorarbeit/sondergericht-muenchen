(1377)

(1378)

(1379)

(1380)

(1381)

9069

90970

9071

9072

9073

Urteil: Helminger 1 Jahr Gefängnis;
Peter S. 1 Jahr 3 Monate Zuchthaus;
Georg S. 2 Jahre Zuchthaus;
Huber 10 Monate Gefängnis

28. Mai 1937 - 27. Jun. 1944
(1 KLs So 7/37)

Prozeß gegen den Hilfsarbeiter Franz MONZ
(geb. 2. Feb. 1918) aus Riedhof (Lkr.
Marktoberdorf), österreichischer Staats-
bürger, wegen eines Sprengstoffanschlages
auf den Lehrer in Stetten (Lkr. Mindelheir)

Urteil: 1 Jahr 3 Monate Zuchthaus

M. wurde am 28. Mai 1940 wieder verhaftet
und bis zur Wiedererlangung der Wehrwürdig-
keit am 27. Aug. 1943 im KZ Ravensbrück
verwahrt.

3. Apr. 1937 - 26. Okt. 1938
(1 KLs So 8/37)

Prozeß gegen den Bauhilfsarbeiter Alois
SPIELEDER (geb. 23. Jan. 1903) aus München,
NSDAP- und SA-Mitglied, wegen Betrugs.

Urteil: 1 Jahr 4 Monate Gefängnis

24. Mai 1937 - 24. Okt. 1938
(1 KLs So 9/37)

Prozeß gen den Kaufmann Walter MEYER
(geb. 16. Feb. 1912) aus Augsburg, pol.
Leiter der NSDAP, wegen Betrugs.

Urteil: 7 Monate Gefängnis

14. Aug. 1937 - 31. Mai 1938
(; KLs So 10/37)

Prozeß gegen den Kaufmann Ferdinand
EISENMENGER (geb. 29. Feb. 1888) aus
Augsburg wegen Wuchers, Betrugs und
kritischer politischer Außerungen.

Urteil: 2 Jahre 2 Monate Gefängnis und
2000 RM Geldstrafe

8. Feb. 1937 - 1. Jan. 1939
(1 KLs So 11/37)

Prozeß gegen den Kaufmann Josef SPANN
(geb. 7. Dez. 1876) und den Kaufmann

Josef SPANN (geb. 3. Nov. 1903), beide
aus Mindelheim wegen Devisenvergehens.
