(3832)

(3833)

(58353)

(5836)

N
0
_

10

10282

10283

10284

102853

Prozeß gegen die Hilfsarbeiterin Charlotte
STRICK (geb. 20. Jun. 1920) aus München
wegen Diebstahls und anderem.

Urteil: 3 Jahre Zuchthaus, 3 Jahre Ehr-
verlust
(88 242,243,246,327 StGB; $ 4 WVO)

8. Apr. 1941 - 9. Jul. 1944
(5 KLs So 62/41)

Prozeß gegen den Schreibgehilfen Ottmar
MAURUS (geb. 28. Apr. 1906) aus Augsburg
wegen eines Fahrraddiebstahls.

Urteil: 1 Jahr 6 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 242 StGB; $3 2 VVO)

25. Mai 1941 - 7. Jun. 1944
(5 KLs So 63/41)

Prozeß gegen den Arbeiter Friedrich HAGN
(geb. 12. Nov. 1908) und den Mikhgeschäfts-
inhaber Gotthard BRUNNER (geb. 28. Jul.
1879), beide aus München, wegen Diebstahls.

Urteil: Hagn unter Einrechnung von 2 Jahren
6 Monaten Zuchthaus aus dem Urteil
des SoG-M vom 25. Okt. 1940 3 Jahre
Zuchthaus, 2 Jahre Ehrverlust;
Brunner Freispruch
(88 242,243 StB; $ 2 VVO)

28. Okt. 1940 - 5. Okt. 1943
(5 KLs So 65/41)

Prozeß gegen die Postfacharbeiterin Katha-
rina HORNSTEINER (geb. 5. Okt. 1883) aus
Mittenwald (Lkr. Garmisch-Partenkirchen)
wegen Diebstahls von Feldpostpäckchen.

Urteil: 1 Jahr 10 Monate Zuchthaus, 100.-RM
Geldstrafe oder 10 Tage Zuchthaus,
2 Jahre Ehrverlust
(88 246,348-350,354 StGB; $ 4 WVVO)

3. Jun. 1941 - 1. Mai 1943
(5 KLs So 69/41)

Prozeß gegen die Mechanikersehefrau Maria
SEITZ (geb. 22. Mai 1912) aus Kempten wegen
Diebstahls.

Urteil: 2 Jahre 6 Monate Zuchthaus, 2 Jahre
Ehrverlust
($ 242 StGB; 8 4 WVO)

25. Jul. 1941 - 9. Feb. 1944
(5 KLs So 70/41)
