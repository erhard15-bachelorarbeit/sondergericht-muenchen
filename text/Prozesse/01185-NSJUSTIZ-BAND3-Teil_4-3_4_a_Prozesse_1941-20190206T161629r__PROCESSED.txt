(5606)

(5607)

(5608)

(5609)

10058

10059

10060

10061

Przeß gegen den Hotelbesitzer Eugen WEISS
(geb.10. Jun. 1903), dessen Ehefrau Kres-
zenz WEISS (geb. 25. Jun. 1906) und dessen
Bruder Anton WEISS (geb. 16. Mrz. 1902),
alle aus Augsburg, wegen Kaufs von schwarz-
geschlachtetem Fleisch.

Urteil:Kreszenz Weiß 8 Monate Gefängnis;
Eugen Weiß 6 Monate Gefängnis;
Anton Weiß 2 Monate Gefängnis;
dazu jeweils Geldstrafen
(& 1 KWVOo; 88 396,403 RAO)

24. Dez. 1940 - 16. Apr. 1945
(2 KLs So 40/41)

Prozeß gegen den Metzgermeister Anton
ICHTL (geb. 28. Feb. 1901), NSDAP-Mit-
glied, und den Fleischbeschauer Johann
MÜLLER (geb. 6. Okt. 1876), beide aus
Gessertshausen (Lkr. Augsburg), wegen
Schwarzschlachtens und Beihilfe dazu.

Urteil: Dichtl 1 Jahr 2 Monate Gefängnis,
400.-RM Geldstrafe; Müller 8 Mo-
nate Gefängnis, 100.-RM Geldstra-
fe; beide gesamtverbindlich 1400.-
RM Ersatzgeldstrafe; für je 50.-
RM der Geldstrafe bei UneMbring-
lichkeit 1 Tag 6Gefängnis
($ 1 KWVO; $8 396,418 RAO; 8$

1-5 Schlachtsteuergesetz)

10. Jul. 1941 - 28. Apr. 1945
(2 KLs So 41,47/41)

Prozeß gegen den Metzger Xaver SCHMAUS
(geb. 23. Okt. 1910) aus Pöttmes (Lkr.
Aichach) wegen Schwarzschlachtens.

Urteil: 7 Monate Gefängnis, 200.-RM +
950.-RM Geldstrafen oder 4 + 19 Tage
Gefängnis
(8 1 KWVO; $ 396 RAO; $$ 1-5

Schlachtsteuergesetz

17. Jan. 1941 - 26. Jul. 1944
(2 KLs So 42/41)

Prozeß gegen den Gastwirt Johann HACKL

(geb. 3. Mai 1895), NSDAP-Mitglied, den
Metzger Hans BRUGGER (geb. 26. Jun. 1903),
den Metzgermeister Josef KLAR (geb. 23.

Mrz. 1882), alle aus Pöttmes (Lkr. Aichach),
und den Landwirt Jakob SCHMAUS (geb. A4.

Apr. 1890) aus Walda (Lkr. Neuburg), wegen
Schwarzschlachtens.
