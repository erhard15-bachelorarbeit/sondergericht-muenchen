(585) 7773

(586) 7774

(557) 7775

(588) 7776

(589) 7777

Urteil: 3 Monate Gefängnis

24. Okt. 1934 - 2. Apr. 1935
(S Pr 313/34)

Frozeß gegen den Schuhmacher Michael BERG-
MANN (geb. 23. Dez. 1910) aus München wegen
Amtsanmaßung, Betrugs, Diebstahls, Urkunden-
fälschung.

Urteil: 2 Jahre Gefängnis

19. Jun. 1934 - 12. Aug. 1936
(S Pr 314/34)

Prozeß gegen die Schreinermeistersehefrau
Maria LEIDESCHER (geb. 8. Dez. 1885) aus
Westernach (Lkr. Mindelheim) und den Händ-
ler Xaver BADER (geb. 14. Jun. 1884) aus
Mindelheim, SA-Mitglied, wegen der Behaup-
tung, Hitler habe mit einer Jüdin Kinder,
die er versorge.

Urteil: 150 RM Geldstrafe oder 1 Monat
Gefängnis
Bader 4 Monate Gefängnis

28. Spt. 1934 - 17. Jan. 1936
(S Pr 315/34)

Prozeß gegen den Schmied Stefan WALTER (geb.
15. Feb. 1903) aus Großselham (Lkr. Miesbach)
wegen der Behauptung, Hitler sei schwul.

Urteil: 4 Monate Gefängnis

12. Nov. 1934 - 12. Jul. 1935
(S Pr 316/34)

Prozeß gegen den Reisenden Johann KOHLER
(geb. 25. Dez. 1861) aus München wegen
kritischer Äußerungen wegen der religiösen
Einstellung Hitlers und des N5S.

Urteil: Freispruch

13. Nov. 1934 - 16. Jan. 1935
(S Pr 317/34)

Prozeß gegen den Hilfsarbeiter Anton WIESEND
(geb. 28. Nov. 1906) aus München wegen der
Behauptung, der Stadtrat Josef HIRSCH (geb.
8. Dez. 1899) sei im KZ Dachau erschossen
worden u.a..

Urteil: 6 Monate Gefängnis

6. Okt. 1934 - 18. Jun. 1935
(S Pr 318/34)
