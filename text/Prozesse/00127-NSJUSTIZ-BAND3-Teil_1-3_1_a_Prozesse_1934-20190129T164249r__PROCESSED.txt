(528)

(529)

(530)

(531)

(532)

(533)

8925

8926

8927

8928

8929

8930

Prozeß gegen den Gelegenheitsarbeiter Florian
KRAMHÖLLER (geb. 1. Jun. 1871) aus Deggendorf
und die Zimmermannsehefrau Sofie SCHNEIDER
(geb. 11. Nov. 1900) aus Freundorf (Lkr. Deg-
gendorf) wegen Sprengstoffdelikts.

Urteil: Kramhöller 1 Jahr 1 Monat Gefängnis
Schneider Freispruch

16. Feb. 1934 - 26. Nov. 1935
(S Pr 256/34)

Prozeß gegen den Dienstknecht Johann HIRSCH-
MANN (geb. 8. Apr. 1905) wegen Erzählens von
Greuelnachrichten über das KZ Dachau in Saul-
grub (Lkr. Garmisch-Partenkirchen).

Urteil: Verfahren wegen Amnestie eingestellt

2. Jul. 1934 - 20. Spt. 1934
(S Pr 257/34)

Prozeß gegen den Hilfsarbeiter Wilhelm ECKMEIER
(geb. 25. Aug. 1914) aus Höchfelden (Lkr. Alt-
ng wegen Betrugs in Neuötting (Lkr. Alt-
ötting).

Urteil: Verfahren wegen Amnestie eingestellt

18. Jun. 1934 - 12. Okt. 1934
(S Pr 258/34)

Prozeß gegen den städt. Kanzleisekretär Fried-
rich MAGERL (geb. 20. Spt. 1887) aus München,
Stahlhelm-Mitglied, wegen der Behauptung, nach
dem Röhm-Putsch würden Stürme döes Stahlhelms
bei der nächsten Abstimmung mit "Nein" stimmen.

Urteil: 40 RM Geldstrafe oder 8 Tage Gefängnis

19. Aug. 1934 - 1. Feb. 1935 ,
(S Pr 259/34)

Prozeß gegen den Hilfsarbeiter Ludwig SCHNEID
(geb. 1. Jul. 1899) und den Schreiner Robert
SCHERF (geb. 4. Jan. 1901), beide aus München,

wegen Weitergabe der KPD-Flugschrift "Imprekorr

vom 28. Apr. 1934.

+

Urteil: Schneid 1 Jahr 6 Monate Gefängnis
Scherf 1 Jahr Gefängnis

16. Apr. 1934 - 12. Mrz. 1936
(S Pr 260/34)

Prozeß gegen den Schriftleiter Matthias WÜRZER
(geb. 20. Jan. 1885) aus München wegen der Be-
hauptung im Anschluß an die Röhm-Revolte, Hit-
Jar sei ein Mörder.
