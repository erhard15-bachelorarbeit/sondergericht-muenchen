(830)

(831)

(832)

(833)

(834)

(835)

9722

7968

7969

7970

7971

7972

Prozeß gegen den kath. Geistlichen Julius
BRAUN (geb. 30. Aug. 1872) aus Fulda wegen
Schreibens eines Artikels "Die Wahrheit
über die katholische Kirche in Deutsch-
land" für die Baseler Nachrichten (Original
im Akt, nicht veröffentlicht).

Urteil: 4 Monate Gefängnis

11. Mai 1935 - 23. Dez. 1935
(S Pr 95/35)

Prozeß gegen den Landwirt Josef WEIGL
(BE 14. Jan. 1882) aus Stellenmoos
Lkr. Kempten) wegen kritischer Äuße-
rungen über die Partei.

Urteil: Verfahren eingestellt

11. Nov. 1934 - 8. Jul. 1935
(S Pr 96/35)

Prozeß gegen den Schmied Georg HILNGRAINER
(geb. 9. Okt. 1906) aus Jarzt (Lkr. Frei-
sing) wegen Betrugs in München.

Urteil: 9 Monate Gefängnis

22. Feb. 1935 - 26. Feb. 1936
(S Pr 97/35)

Prozeß gegen den Maschinenschlosser
Ferdinand Robert HAUER (geb. 30. Mai
1901), österreichischer Staatsbürger,
wegen Betrugs in München.

Urteil: 1 Jahr 3 Monate Zuchthaus

12. Jan. 1935 - 19. Jun. 1936
(S Pr 98/35)

Prozeß gegen den Abteilungsleiter Ludwig
MÜNICH (geb. 26. Jul. 1880) wegen kriti-
scher Äußerungen über das Regime.

Urteil: Verfahren eingestelit

19. Feb. 1935 - 8. Jul. 1935
(S Pr 99/35)

Prozeß gegen den Bürodiener Hans BAUER
(geb. 15. Mai 1908), den Metzger Franz
Xäver BRAUN (geb. 17. Feb. 1908), den
Posthelfer Georg HANEDER (geb. 6. Feb.
1908), die Oberpostschaffnerswitwe Fran-
ziska HANEDER (geb. 28. Feb. 1885),

alle aus München, wegen Besitz und
Verbreitung kommunistischer Druckschriften.
