(523)

(524)

(525)

(526)

(527)

8909

8911

8914

8916

8924

Urteil: 1 Monat Gefängnis

14. Apr. 1934 - 31. Aug. 1934
(S Pr 221/34)

Prozeß gegen den Landwirt Otmar LINDNER (geb.
17. Aug. 1879) aus Lindenberg (Lkr. Lindau)
wegen kritischer Äußerungen über das Regime.

Urteil: Freispruch

3. Jul. 1934 - 26. Spt. 1934
(S Pr 240/34)

Prozeß gegen den Arbeiter Jakob MAIER (geb.
24. Jul. 1877) aus Erding-Klettham wegen
der Äußerung, der Korbmacher Ludwig NEUMEI>R,
der im Herbst 1933 wegen der Ausierung,die
Nat.Sozz. hätten den Reichstag angezündet,
in Haft war, könne recht gehabt haben.

Urteil: BEinstellung des Verfahrens wegen
Amnestie

18. Jul. 1934 - 25. Spt. 1934
(S Pr 242/34)

Prozeß gegen den Hilfsarbeiter Anton FROM
(geb. 17. Jul. 1903) aus Greding (Lkr. Hil-
poltstein) wegen der Behauptung im Anschluß
an eine Unterhaltung über Röhm : "Wir haben
noch mehr solche Lumpen, der oberste der Re-
gierung ist auch ein Lump, ein Spinater,
sonst hätte er schon längst geheiratet".

Urteil: 8 Monate Gefängnis

25. Jul. 1934 - 15. Jun. 1935
(S Pr 245/34)

Prozeß gegen den Bauern Josef MAIER (geb.
10. Jan. 1903) aus Treidelheim (Lkr. Neu-
burg), früher NSDAP-Mitglied, wegen Be-
drohung.

Urteil: Wegen Amnestie Verfahren eingestellt

29. Jun. 1934 - 17. Okt. 1934
(S Pr 247/34)

Prozeß gegen den Müllerssohn Anton KRON-
SCHNABEL aus 4eiliermühle (Lkr. Regen), SA-
Mitglied, wegen Sprengens des Maibaumes der
Landwirtssöhne Josef SCHILLER (27 Jahre alt)
und Josef SCHILLER (19 Jahre alt) aus Unter-
mitterdorf (Lkr. Regen).

Verfahren wegen Amnestie eingestellt.

7. Mai 1934 - 26. Spt. 1934
(S5 Pr 255/34)
