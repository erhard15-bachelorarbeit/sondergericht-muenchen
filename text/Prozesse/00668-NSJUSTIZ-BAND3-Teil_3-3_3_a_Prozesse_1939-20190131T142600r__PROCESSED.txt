(3171)

(3172)

(3173)

(3174)

(3175)

9199
- 9225

9226

9227

9228

9229

Prozeß gegen den Schlosser Johann EICHHORN
(geb. 8. Okt. 1906) aus München wegen Notzucht
und Mordes.

Urteil: Todesstrafe, Aberkennung der bür-
gerlichen Ehrenrechte auf Lebens-
zeit
(88 32, 211 StGB)

Im Akt Anklage gegen den Hilfsarbeiter Jo-
hann WAGNER (geb. 30. Mrz. 1906) aus Gern-
linden (Lkr. Fürstenfeldbruck) wegen ver-
suchter Notzucht.

Verfahren eingestellt

13. Aug. 1928 - 11. Nov. 1943
(1 KLs So 49/39)

Prozeß gegen die Landwirtsehefrau Maria
KREUZPOINTNER (geb. 13. Spt. 1903) aus
Stetten (Lkr. Mühldorf) wegen versuchter
Brandstiftung.

Urteil: 4 Jahre Zuchthaus, Aberkennung der
bürgerlichen Ehrenrechte auf 3 Jah-

re
(88 43, 306, 308; $ 3 VVO)

9. Nov. 1939 - 1. Jul. 1944
(1 KLs So 50/39)

Prozeß gegen den Geschäftsführer Franz PÖGL
(geb. 19. Jul. 1897) aus München, früher
NSDAP-Mitglied, wegen Betrugs.

Urteil: 2 Jahre 3 Monate Gefängnis
($ HG; 88 263, 264, 267, 268 StGB)

30. Dez. 1938 - 15. Dez. 1943
(1 KLs So 51/39)

Prozeß gegen den Uhrmacherlehrling Johann
WEISS (geb. 7. Okt. 1920) und den Mechani-
ker Josef NEUMAIER (geb. 25. Nov. 1919),

beide aus München, wegen Bandendiebstahls.

Urteil: je 4 Jahre Zuchthaus
(65 242, 243 StGB)

17. Aug. 1939 - 24. Jul. 1943
(1 KLs So 52/39)

Prozeß gegen den landw. Dienstknecht Albert
STANZL (geb. 9. Aug. 1920) aus München we-
gen Brandstiftung in Obermalching (Lkr. Fir-
stenfeldbruck).

Urteil: Todesstrafe, Aberkennung der bür-
gerlichen Ehrenrechte auf Lebens-
zeit, begnadigt zu 10 Jahren Zucht-
haus
(38 306, 308 StGB; 8 3 VVO) ko
