(1196)

(1197)

(1198)

(1199)

(1200)

(1201)

8333

8334

8335

8336

58337

8338

Prozeß gegen den Immobilienmakler Wilhelm
REISS (geb. 1. Okt. 1891) aus Lochham (Lkr.
München) wegen kritischer ÄAußerungen über
das Regime.

Urteil: 8 Monate Gefängnis

1. Feb. 1936 - 4. Dez. 1936
(16 KkMs So 183/36)

Prozeß gegen den Arzt Dr. Alois SCHEDEL
(geb. 28. Mrz. 1891) aus Mittenwald (Lkr.
Garmisch-Partenkirchen) wegen kritischer
politischer Außerungen.

Urteil: 6 Monate Gefängnis

8. Mrz. 1936 - 20. Mai 1938
(16 KMs So 184/36)

Prozeß gegen den Kaufmann Walter IMELMANN
(geb. 21. Mai 1906) wegen politischer
Schimpfereien in Langerringen (Lkr. Schwab-
münchen).

Urteil: Verfahren eingestellt

16. Apr. 1936 - 12. Nov. 1936
(16 KMs So 185/36)

Prozeß gegen den Dienstknecht Max CERWENY
(geb. 3. Mai 1909) wegen kritischer Äuße-
rungen über Göring und Erzählens von Greu-
elgeschichten über das KZ Dachau in Au
(Lkr. Bad Aibling).

Urteil: 6 Monate Gefängnis

9. Jun. 197° - 18, Jun. 1938
(15 KMs So 186/35)

Frmittlungsverfahren gegen den kfm. Ange-
stellten Karl REINCKE (geb. 16. Jan. 1906)
aus München wegen der Behauptung, Hitler
stamme von einem Juden ab.

Urteil: Verfahren eingestellt
5. Jul. 1936 - 7. Apr. 1937
(16 kMs So 187/36)

Prozeß gegen den Geschäftsstellenleiter
Franz BREM (geb. 31. Dez. 1891) aus München
wezen kritischer politischer ÄAußerungen.

Urteil: 5 Monate Gefängnis

2. Jun. 1936 - 6. Mai 1938
(16 KMs So 188/36)
