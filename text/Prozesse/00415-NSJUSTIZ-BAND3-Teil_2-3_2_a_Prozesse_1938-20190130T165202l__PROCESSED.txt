(1932)

(1933)

za)

8717

8718

Anlagen: 1 "Exorzismus gegen den

Satan und die aufrührerischen
Engel"; 7 "Offener Brief an

den Herrn Reichsminister für
Volksaufklärung und Propaganda,
Dr. Josef Goebbels"; 1 Auszug
aus "Offener Brief an...
Goebbels mit Abschrift von

"Der Kulturkampf in Deutsch-
land" aus "Freiburg.Nachr."

Nr. 119 (1937); 1 "Flammen-
zeichen rauchen. Predigt
Kardinal Faulhabers ... am

4. Jul. 1937", hektographiert;

1 Predigt des Bischofs von
Eichstätt Dr. Michael Rackl

in Ingolstadt am 4. Jul. 197,
hektographiert; 1 "Predigt

des H.H. Dompfarrers Johannes
Kraus im Hohen Dom zu Eichstätt
am 31. Januar 1937"; 1 "Offener
Brief an Herrn Reichsminister
Kerrl, Berlin, Ende Februar 1937"
von D. Otto Dibelius, hektogra-
phiert; 1 Führerbrief des Gau-
grafen von "Neudeutschland Schwa-
bengau" vom 22. Feb. 1935, hekto-
graphiert; 1 "Oberhirtliches MVort
des Bischofs v. Eichstätt zu den
Sittlichkeitsprozessen gegen kath
Geistliche"; 1 "Predigt des H.H.
Pater Rupert Mayer S.J. im Hohen
Dom zu Eichstätt am 28. Feb. 193
über Konfessionalismus; 1 "offene
Brief an ... Goebbels"

24. Aug. 1937 1 Jul. 1943

(1 Klils So 89/38

Prozeß gegen den Kaufmann Nikolaus OBER-
MAYER (geb. 7. Dez. 1893) aus München
wegen einer abwertenden Außerung über
Hitler.

Urteil: 7 Monate Gefängnis

0. war bereits 5 Monate 3 Wochen in
Schutzhaft.

2. Jan. 1938 - 5 Apr. 1941

(1 KMs So 90/38

Prozeß gegen den Großhändler Hans Hermann
GARMS (geb. 18. Mrz. 1906) aus München
wegen politischer Außerungen.

Urteil: 8 Monate Gefängnis

G. befindet sich seit Frühjahr 1939 im
KZ Mauthausen.

26. Feb. 1938 - 29. Mrz. 1941
(1 KMs So 91/38)
