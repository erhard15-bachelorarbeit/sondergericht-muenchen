import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
from utils import traverse, show_image
import matplotlib.pyplot as plt
import logging
from tqdm import tqdm
import time

class PreprocessImages():
    def __init__(self):
        self.in_folder = ".\processing\in"
        self.out_folder = ".\processing\out"
        self.out_folder_p = self.out_folder + "\Prozesse\\"
        self.out_folder_e = self.out_folder + "\Eingestellte_Verfahren\\"

        for i in [self.in_folder, self.out_folder_p, self.out_folder_e]:
            os.makedirs(i, exist_ok=True)

        in_dir = os.listdir(self.in_folder)
        if len(in_dir) == 0:
            print("The input folder is empty – nothing to pre-process")
            exit()

    def traverse(self, dir):
        """
        Takes a directory as an argument and outputs a dictionary
        in the following format: { relative_path_to_file: filename}
        """
        queue = {}
        for root, dirs, files in os.walk(dir):
            for name in files:
                queue.update( {os.path.join(root, name): name} )
        return queue

    def preprocess(self, debug=False, skip=False):
        self.queue = self.traverse(self.in_folder)
        count = 0
        for path, file in tqdm(self.queue.items()):
            if "Prozess" in file:
                out_filename = self.out_folder_p + file.split(".png")[0] + "__PROCESSED.png"
            elif "Eingestellte" in file:
                out_filename = self.out_folder_e + file.split(".png")[0] + "__PROCESSED.png"
            else:
                print("Ignoring file " + str(file))
                continue
        
            if skip:
                print("Skipping preprocessing since `skip' was specified")
                os.system('copy ' + path + " " + out_filename)
                continue
            # Open the image and convert it to grayscale
            image = cv2.imread(path, 0)
            logging.info('Opening image ' + path)
            logging.info('Converting to grayscale...')
            _, blackAndWhite = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)


            if debug:
                logging.info('Showing the image (press "q" to continue)')
                label = "STAGE ONE: GREYSCALE"
                show_image(label, image)
            # Find and exclude small elements
            logging.info('Removing small dotted regions (dust, etc.)...')
            nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(blackAndWhite, None, None, None, 8, cv2.CV_32S)
            sizes = stats[1:, -1] #get CC_STAT_AREA component
            img2 = np.zeros((labels.shape), np.uint8)

            for i in range(0, nlabels - 1):
                if sizes[i] >= 40:   #filter small dotted regions
                    img2[labels == i + 1] = 255

            image = cv2.bitwise_not(img2)

            if debug:
                logging.info('Showing the image (press "q" to continue)')
                label = "STAGE TWO: REMOVE DUST"
                show_image(label, image)

            cv2.imwrite(out_filename, image)
            logging.info('Writing the modified image...')
            # ------ START CROPPING ----- #
            image = cv2.imread(out_filename)
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # Load image, grayscale, Gaussian blur, Otsu's threshold
            blur = cv2.GaussianBlur(gray, (5,5), 0)
            thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
            logging.info('Applying Otsu\'s Threshold')


            horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (25,4))
            vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,32))
            detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
            detected_vlines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)

            for l in [detected_lines, detected_vlines]:
                cnts = cv2.findContours(l, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = cnts[0] if len(cnts) == 2 else cnts[1]
                for c in cnts:
                    cv2.drawContours(thresh, [c], -1, (0,0,0), 50)
                    cv2.drawContours(image, [c], -1, (255,255,255), 50)

            # Create rectangular structuring element and dilate
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18,18))
            dilate = cv2.dilate(thresh, kernel, iterations=4)
            logging.info('Dilating text regions')

            try:
                # Find contours and draw rectangle
                cnts, hierarchy = cv2.findContours(dilate, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                logging.info('Extracting contours')
                # Search for contours and append their coordinates into an array
                arr = []
                for i,c in enumerate(cnts):
                    # Exclude small elements
                    x,y,w,h = cv2.boundingRect(c)
                    # Exclude oddly shaped elements
                    if w/h > 8 or h/w > 1.6:
                        continue
                    arr.append((x,y))
                    arr.append((x+w,y+h))
                # Calculate the coordinates and crop the image
                logging.info('Cropping the image')
                x,y,w,h = cv2.boundingRect(np.asarray(arr))
                image = image[y:y+h,x:x+w]
                if debug:
                    logging.info('Showing the image (press "q" to continue)')
                    label = "STAGE FOUR: CROPPED IMAGE"
                logging.info('Writing to ' + out_filename)
            except cv2.error:
                pass
            cv2.imwrite(out_filename, image)
            count += 1
        print("Pre-processing complete!")
        print("Processed " + str(count) + " images")