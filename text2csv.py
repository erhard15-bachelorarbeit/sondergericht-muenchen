import fuzzywuzzy
import pandas as pd
import re
from fuzzywuzzy import fuzz, process
from utils import traverse, fuzzy_replace
from itertools import chain
from datetime import datetime
import locale
from tqdm import tqdm
from shutil import rmtree
from os import makedirs
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

class Text2Pandas:
    def __init__(self):
        self.queue = traverse("text")

    def combine(self, ptype):
        lines = []
        for path, file in self.queue.items():
            with open(path, "r", encoding="utf8") as f:
                if ptype in file:
                    for line in f.readlines():
                        lines.append(line)
        return lines

    def clean(self, lines):
        text = ""
        for line in lines:
            if any([
                line == "\n",
                line == "",
                line == "\x0c",
                re.match(r"\(?[0-9]{1,5}\)?\n", line),
                line is None
            ]):
                continue
            line = line.replace("ı", "i") 
            line = line.replace("-—", "-") 
            line = fuzzy_replace("Ermittlungsverfahren", line)
            line = fuzzy_replace("Prozeß", line)

            # Otherwise the script treats n.89 as two cases
            line = re.sub(r"(?<!Das )Verfahren gegen d\w{2}", "Prozeß gegen die", line)

            # Sometimes they also write 'Anklage'?
            line = re.sub(r"(?<!Die )Anklage gegen d\w{2}", "Prozeß gegen die", line)

            line = fuzzy_replace("Urteil", line)
            line = fuzzy_replace("Anlage", line)

            # If line ends with - or )
            if re.match(r".*\-$", line):
                # Remove hyphens
                line = re.sub(r"\-$", "", line)

                text += line 
            # Otherwise add a space to the end of the line
            else:
                text += line + " "
        return text

    def clean_date(self, date):
        # Delete all junk, only leave alphanumeric symbols
        date = date.replace("C", "0")
        date = re.sub(r'^[^\d]*?(?=\d)', '', date)
        date = re.sub(r"[^a-zA-Z\d\s:]", " ", date)
        date = re.sub(r"\s+", " ", date)
        # Try to auto-correct the month name, remove extraspaces
        date = date.strip().split(" ")
        if len(date) != 3:
            return ""
        list_of_months = ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
        correct_month = process.extractOne(date[1], list_of_months)[0]
        date[1] = correct_month

        # Discard the last number if year length is more than 4
        if len(date[2]) > 4:
            date[2] = date[2][:-1]
        # Add a 1 if the date is too short
        elif len(date[2]) < 4 and date[0] != "1":
            date[2] = "1" + date[2]
        try:
            date2 = int(date[2])
        except ValueError:
            return ""
        # Unless NSDAP was prosecuting toddlers or vampires, those dates are probably garbage
        if int(date[2]) > 1940 or int(date[2]) < 1810:
            return ""
        date = " ".join(date)
        # Set locale to German (for short month names like Aug, Sep, etc.)
        locale.setlocale(locale.LC_TIME, "de_DE") 

        # Try with the '19 Aug 1900' format first, then try 19 08 1900 (rare)
        try:
            dtobj = datetime.strptime(date, '%d %b %Y')
        except ValueError:
            try:
                dtobj = datetime.strptime(date, '%d %m %Y')
            except:
                return ""

        # ISO 8601 format
        resdate = datetime.strftime(dtobj, "%Y-%m-%d")
        return resdate


    def convert(self, ev=False):
        if ev:
            df_columns = {"Verfahrensnummer": '',
                        "Name": '',
                        "Beruf": '',
                        "Geburtsdatum": ''}
            splitregex = re.compile(r"(Prozess|Ermittlungsverfahren) g.{3}n (?=d\w{2}|verschiedene)")
            ptype = "Eingestellte_Verfahren"
        else:
            df_columns = {"Prozessnummer": '',
                        "Name": '',
                        "Beruf": '',
                        "Geburtsdatum": '',
                        "Urteil": '',
                        "Anlagen": ''}
            splitregex = re.compile(r"Prozeß g.{3}n (?=(d\w{2})?|verschiedene)")
            ptype = "Prozesse"

        df = pd.DataFrame(columns=df_columns.keys())
        text = self.clean(self.combine(ptype))
        processes = re.split(splitregex, text)
        previous_number = ""
        count = 0
        for process in tqdm(processes):
        #for process in processes[230:240]:
            try:
                if len(process) < 5:
                    continue
            except:
                continue
            # Remove extra spaces
            process = process.strip()
            # Get the case number
            # Opening parenthesis
            # 1-3 numbers or letters (S, AK, 16)
            # Optionally, 2-3 letters and a space (KLs, Pr)
            # Optionally, 2 letters (So)
            # 1-3 numbers, backslash, two last digits of a year
            number = re.search(r"\(+[\d|\w]{1,3} \w{0,3} *(\w{2} )*\d{1,3}.[3|4]\d\)", process)
            # Try finding the number according to the regex, save it to 'previous_number' outside of the loop
            try:
                number = number.group(0)
                previous_number = number
            # If the number isn't found, reconstruct it according to the previous number
            # Split along the whitespace, get the last object (3/39), split along the backslash, get the first object (3)
            # Convert to int, add +1, replace the previous number with the "current" one
            except AttributeError:
                try:
                    previous_number_int = int(previous_number.split(" ")[-1].split("/")[0])
                    number = previous_number.replace(str(previous_number_int), str(previous_number_int + 1)) + "?"
                    previous_number = number
                except:
                    continue
            # Remove the surrounding parentheses
            number = number.replace("(", "").replace(")", "")

            # Extract attachments (Anlagen)
            try:
                attachment = process.split("Anlage")[1]
                begin = [":", "n:"]
                if attachment.startswith(tuple(begin)):
                    attachment = process.split(":")[1]
                attachment = attachment.strip()
            except IndexError:
                attachment = ""

            # Extract verdict (Urteil)
            try:
                verdict = re.findall(r"(?<=Urteil).*?(?=(?:\d{1,}(?:\.|,)|Anlage))", process)
                verdict = verdict[0].strip()
            except IndexError:
                verdict = ""
            # Try to find out the double name
            # name = process.split("Prozeß gegen")[0]
            # name = re.split("\(?geb", name)[0]

            # Get the occupation, name, surname and birthdate
            # - One capital letter + up to 30 lowercase letters [occupation]
            # - One capital letter + up to 14 lowercase letters [first name]
            # - An optional "von"
            # - Followed by an uppercase last name
            regex = re.findall(r"(?:[A-ZÖÄÜ\-]{1,5}[a-z\-öäü]{1,30} )?(?:[A-ZÖÄÜ]{1}[a-zö\-äü]{2,14} ){1}(?:von)?.{,2}[A-ZÖ\-ÄÜ]{3,25}(?: \(?.*?[\)}])?", process)

            # - Not "KPD" or "NSDAP"
            #regex = [ i for i in regex if "NSDAP" not in i and "KPD" not in i ]
            
            # Slice and dice
            for i in regex:
                i_orig = i
                # Split the birth date starting with "(" and the rest of the string
                i = i.split("(")
                if len(i) < 2:
                    i = re.split(" \we.{1,3}(?=\d{1,2}\.)", i[0])

                # That's our job + name + surname
                firstpart = i[0].strip()

                # Leave only letters
                firstpart = re.sub(r"[^a-zöüäA-ZÖÄÜ\s\-]", "", firstpart)
                firstpart = firstpart.split(" ")
                # Remove empty items
                firstpart = [i for i in firstpart if i]

                # If we have 3 words, the string includes a job
                if len(firstpart) == 3 or (len(firstpart) > 3 and re.match(r"[A-ZÖÄÜ]", firstpart[2])):
                    job = firstpart[0]
                    name = firstpart[1]
                    surname = firstpart[2]
                # Otherwise only the name + surname
                else:
                    try:
                        job = ""
                        name = firstpart[0]
                        surname = firstpart[1]
                    except:
                        continue
                name = name + " " + surname

                # LET'S CHECK FOR DOUBLE AND TRIPLE FIRST NAMES IN THE MOST CURSED WAY POSSIBLE
                # Load a list of German names from https://www.usna.edu/Users/cs/roche/courses/s15si335/proj1/files.php%3Ff=names.txt&downloadcode=yes
                with open("names.txt", "r") as names:
                    # lowercase all names, remove the newline symbol
                    names = [ i.lower().replace("\n", "") for i in names.readlines() ]
                # If the job is in the list of the name – it's probably a name
                if job.lower() in names:
                    name = job + " " + name
                    # Let's search for the original job + name string in the file and capture the previous word
                    job = process.split(i_orig)[0].strip().split(" ")[-1]
                    # If we're dealing with a triple name, do it again
                    if job.lower() in names:
                        name = job + " " + name
                        job = process.split(i_orig)[0].strip().split(" ")[-2]
                

                # Convert job to nominative, remove Genitiv and n-Deklination (Bankbeamten, KPD-Mitgliedes, usw.)
                if job.endswith("es") or job.endswith("sten"):
                    job = job.strip()[:-2]
                elif job.endswith("en") or job.endswith("s"):
                    job = job.strip()[:-1]
                else:
                    pass
                

                # Let's check the surname just to be sure
                surname = name.split(" ")[-1]
                # Is it an all-caps word? If not, discard the name item
                if re.match(r"([^A-ZÖÄÜ\-]|NSDAP|KPD)", surname) or len(surname) < 4:
                    continue



                # Going back to the full string – if found something beyond the date, it's probably garbage
                if len(i) > 2:
                    i = [i[0]]


                # If our string split in two, we have a date
                if len(i) > 1:
                    # Remove extra spaces
                    # Break it down into words
                    birthdate = i[1].strip().split(" ")
                    # If we have less than 3 words, no date was detected
                    if len(birthdate) < 3:
                        birthdate = ""
                    else:
                        # Remove trash in the beginning
                        birthdate = " ".join(birthdate)
                        # Correct errors in the date, convert it to a modern format
                        birthdate = self.clean_date(birthdate)
                # Otherwise, we don't
                else:
                    try:
                        after_name = process.split(name)[1][0:40]
                        birthdate = re.findall(r"\(?g?eb.{1,2}\d{1,2}.{1,2}\w{2,3}.{1,2}\d{4}", after_name)
                        birthdate = self.clean_date(birthdate[0])
                    except IndexError:
                        birthdate = ""
                
                # We need different column depending on the type of the document (Prozeß or E.V.)
                if ev:
                    df_columns = {"Verfahrensnummer": number,
                                "Name": name,
                                "Beruf": job,
                                "Geburtsdatum": birthdate}
                else:
                    df_columns = {"Prozessnummer": number,
                                "Name": name,
                                "Beruf": job,
                                "Geburtsdatum": birthdate,
                                "Urteil": verdict,
                                "Anlagen": attachment}

                df = df.append(df_columns, ignore_index=True)

            count += 1
        try:
            print("Total cases: {}".format(len(df["Prozessnummer"].unique())))
        except KeyError:
            print("Total cases: {}".format(len(df["Verfahrensnummer"].unique())))
        print("Total rows: {}".format(len(df)))
        print("Non-empty dates: {}".format(len(df[df['Geburtsdatum'] != ""])))
        print("Non-empty jobs: {}".format(len(df[df['Beruf'] != ""])))
        try:
            print("Non-empty verdicts: {}".format(len(df[df['Urteil'] != ""])))
        except KeyError:
            pass
        #rmtree("xls", ignore_errors=True)
        #makedirs("xls", exist_ok=False)
        df.to_excel("xls/" + ptype + ".xlsx")