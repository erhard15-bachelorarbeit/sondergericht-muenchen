import os
import time
from fuzzywuzzy import fuzz
import cv2
from shutil import rmtree


def fuzzy_replace(str_a, str_b, orig_str):
    l = len(str_a.split()) # Length to read orig_str chunk by chunk
    splitted = orig_str.split()
    for i in range(len(splitted)-l+1):
        test = " ".join(splitted[i:i+l])
        if fuzz.ratio(str_a, test) > 75: #Using fuzzwuzzy library to test ratio
            before = " ".join(splitted[:i])
            after = " ".join(splitted[i+1:])
            return before+" "+str_b+" "+after #Output will be sandwich of these three strings


def traverse(dir):
    """
    Takes a directory as an argument and outputs a dictionary
    in the following format: { relative_path_to_file: filename}
    """
    queue = {}
    for root, dirs, files in os.walk(dir):
        for name in sorted(files):
            queue.update( {os.path.join(root, name): name} )
    return queue


def fuzzy_replace(replace, orig_str):
    try:
        words = orig_str.split()
    except AttributeError:
        return orig_str
    new_words = []
    for word in words:
        if fuzz.ratio(word, replace) > 75:
            new_words.append(replace)
        else:
            new_words.append(word)
    new_str = " ".join(new_words)
    return new_str


def show_image(label, image_object):
    """
    Shows the CV2 on the screen and waits for 4 seconds (or for user to press letter "q")
    """
    resize_factor = 3
    cv2.namedWindow(label, cv2.WINDOW_NORMAL)  #  2. use 'normal' flag
    h,w = image_object.shape[:2]  #  suits for image containing any amount of channels
    h = int(h / resize_factor)  #  one must compute beforehand
    w = int(w / resize_factor)  #  and convert to INT
    cv2.resizeWindow(label, w, h)  #  use variables defined/computed BEFOREHAND
    cv2.imshow(label, image_object)
    cv2.waitKey()
    cv2.destroyAllWindows()
    return

def cleanup():
    """
    Removes all working directories except for 'in'
    """
    print("Deleting the working directories...")
    rmtree('processing/out', ignore_errors=True)
    rmtree('text', ignore_errors=True)
    print("Done!")